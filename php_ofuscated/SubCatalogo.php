<?php
include_once('Catalogos.php');
include_once('Core.php');

/**
Clase SubCatalogo, contiene los métodos necesarios para el manejo del módulo empresas

@author Fernando Carreon
@version 1.0
**/
class SubCatalogo extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase SubCatalogo
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function SubCatalogo($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una sucursal a través de un id.
    
    @bitacora Acceso a la información de la sucursal
    @param id de la sucursal a obtener el resultado
    @return arreglo con los datos de la sucursal
    **/
    public function getCatalogoById($id){
        $this->c->q("SELECT * FROM subcatalogos WHERE sub_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Subcatalogos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(2, 3), true);
        $emp = new Catalogos();
        $data[1] = $emp->getUnidadById($data[1]);
        return $data;
    }
    /**
    Método principal de la clase Sucursales
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllSubcatalogos':
                return $this->getAllSubcatalogos();
            break;
            case 'saveSubcatalogo':
                return (!isset($_SESSION['edit-Subcatalogos'])) ? $this->saveSubcatalogo() : $this->updateSubCatalogos();
            break;
            case 'getCatalogoById':
                return $this->getCatalogoById($_POST['param']);
            break;
            case 'delSucursal':
                return $this->deleteSucursal($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las sucursales
    
    @bitacora Acceso a la información básica de todas las sucursales
    @param void
    @return arreglo de sucursales
    **/
    public function getAllSubcatalogos(){
        $v = array();
        unset($_SESSION['edit-Subcatalogos']);
        $this->c->q("SELECT sub_id, sub_cat_id, cat_nombre, sub_nombre FROM subcatalogos LEFT JOIN catalogos ON sub_cat_id = cat_id WHERE sub_deleted = '0' AND cat_deleted = '0' ORDER BY sub_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(2, 3), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea una sucursal nueva. Tiene relación con la clase Dirección
    
    @bitacora Guardado de una nueva sucursal
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveSubcatalogo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createSubcatalogos(array($p[0], $p[1], $p[2]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la sucursal en la base de datos
    
    @bitacora Creación de una nueva sucursal
    @param arreglo con los datos de la sucursal
    @return identificador de la sucursal agregada en la base de datos
    
    **/
    public function createSubcatalogos($v){
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $v = $this->u8($v, array(1, 2, 3), false);
        $this->c->q("INSERT INTO subcatalogos VALUES(NULL, '".$v[2]."', '".$v[0]."', '".$v[1]."', '".$this->d."', '0')");
        return $this->c->last('subcatalogos');        
    }
    /**
    Método que actualiza la información de la sucursal
    
    @bitacora Actualización de la sucursal
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateSubCatalogos(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Subcatalogos']))
            return $arr;
        $sucursal = $this->getCatalogoById($_SESSION['edit-Subcatalogos']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$sucursal[2].' por '.$p[0]);
        $this->c->q("UPDATE subcatalogos SET sub_nombre = '".$p[0]."', sub_descr = '".$p[1]."', sub_cat_id = '".$p[2]."', sub_fecha = '".$this->d."' WHERE sub_id = '".$sucursal[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Subcatalogos']);
        return $arr;
    }
    /**
    Método que marca como eliminada una sucursal
    
    @bitacora Eliminación de la sucursal
    @param identificador de la sucursal en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteSucursal($id){
        $this->hasAccess(get_class($this));
        $sucursal = $this->getCatalogoById($id);
        $this->log($this, __FUNCTION__, 'bitacora',$sucursal[2]);
        unset($_SESSION['edit-Subcatalogos']);
        $this->c->q("UPDATE subcatalogos SET sub_deleted = '1' WHERE sub_id = '".$sucursal[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>