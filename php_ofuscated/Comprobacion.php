<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Proveedores.php');
require_once('Core.php');

/**
Clase DÉBIL Comprobación, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Comprobacion extends Core{
    public $c;
    public $d;
    public $tipo;
    /**
    Constructor de la clase Comprobacion
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Comprobacion($c, $tipo){
        $this->c = $c;
        $this->d = date('YmdHis');
        $this->tipo = $tipo;
    }
    /**
    Método principal de la clase comprobación
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'setIdComprobacion':
                return $this->setIdComprobacion($_POST['param']);
            break;
            case 'comprobarGasto':
                return $this->comprobarGasto($_POST['param']);
            break;
            case 'comprobarPrestamo':
                return $this->comprobarPrestamo($_POST['param']);
            break;
            case 'comprobarOrdenCompra':
                return $this->comprobarOrdenCompra($_POST['param']);
            break;
            case 'comprobarOrdenVenta':
                return $this->comprobarOrdenVenta($_POST['param']);
            break;
            case 'getComprobacionesById':
                return $this->getComprobacionesById();
            break;
            case 'getComprobanteById':
                return $this->getComprobanteById($_POST['param']);
            break;
            case 'delComprobante':
                return $this->delComprobante($_POST['param']);
            break;
            case 'comprobarVenta':
                return $this->comprobarVenta($_POST['param']);
            break;
                
        }
    }
    /**
    Método que marca como eliminado un comprobante
    
    @bitacora Eliminación de un comprobante
    @param identificador de un comprobante en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delComprobante($id){
        $comprobante = $this->getComprobanteById($id);
        switch($this->tipo){
            case 1:
                $this->hasAccess('Gastos');
                $this->c->q("SELECT gas_estado FROM gastos WHERE gas_ref IN (SELECT com_ref FROM comprobaciones WHERE com_id = '".$id."') LIMIT 1;");
                if ($this->c->r(0) == '1')
                    return array('comprobado');
                $this->log($this, 'delComprobante', 'bitacora', 'id: '.$comprobante[0].'. Referencia: '.$comprobante[2]);
            break;
            case 2:
                $this->hasAccess('Prestamos');
                $this->c->q("SELECT pre_estado FROM prestamos WHERE pre_ref IN (SELECT com_ref FROM comprobaciones WHERE com_id = '".$id."') LIMIT 1;");
                if ($this->c->r(0) == '1')
                    return array('comprobado');
                $this->log($this, 'delComprobante', 'bitacora', 'id: '.$comprobante[0].'. Referencia: '.$comprobante[2]);
            break;
            case 3: //Comprobante de orden de compra
                
            break;
            case 5:
                $this->hasAccess('CuentasPorCobrar');
                $this->c->q("SELECT ven_cubierto, ven_monto FROM ventas WHERE ven_id = '".$comprobante[6]."' LIMIT 1;");
                $cubierto = $this->c->fr();
                $cubierto[0] -= $comprobante[0];
                $cubierto[1] = ", ven_estado = '0'";
                $this->c->q("UPDATE ventas SET ven_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE ven_id = '".$comprobante[6]."' LIMIT 1;");
                $this->log($this, 'delComprobante', 'bitacora', 'id: '.$comprobante[0].'. Referencia: '.$comprobante[2]);
            break;
        }
        $this->c->q("DELETE FROM comprobaciones WHERE com_id = '".$id."' LIMIT 1;");
        return array('true');
    }
    public function delComprobanteByRef($id){
        switch($this->tipo){
            case 1:
                $this->hasAccess('Gastos');
            break;
            case 2:
                $this->hasAccess('Prestamos');
            break;
            case 3:
                $this->hasAccess('OrdenDeCompra');
            break;
        }
        $this->c->q("DELETE FROM comprobaciones WHERE com_tipo = '".$this->tipo."' AND com_ref = '".$id."';");
        return array('true');
    }
    /**
    Obtiene los datos de un comprobante a través de un id.
    
    @bitacora Acceso a la información de un comprobante
    @param id del comprobante a obtener el resultado
    @return arreglo con los datos del comprobante
    **/
    public function getComprobanteById($id){
        $this->c->q("SELECT com_monto, com_formaPago, com_cuenta, com_us_comprobador, com_observaciones, '', com_ref FROM comprobaciones WHERE com_id = '".$id."' LIMIT 1;");
        $data = $this->c->fr();
        $data = $this->u8($data, array(2, 3, 4), true);
        return $data;
    }
    /**
    Método que obtiene toda la información básica de los gastos
    
    @bitacora Acceso a la información básica de todos los gastos
    @param void
    @return arreglo de los gastos
    **/
    public function getComprobacionesById(){
        switch($this->tipo){
            case 1:
                $var = $_SESSION['edit-Gastos'];
            break;
            case 2:
                $var = $_SESSION['edit-Prestamos'];
            break;
            case 3:
                $var = $_SESSION['edit-OrdenCompra'];
            break;
            case 5:
                $var = $_SESSION['edit-OrdenVenta'];
            break;
        }
        $this->c->q("SELECT com_id, com_ref, com_fecha, com_monto, com_formaPago, com_us_comprobador, com_observaciones, com_cuenta FROM comprobaciones WHERE com_ref = '".$var."' AND com_tipo = '".$this->tipo."' ORDER BY com_id;");
        $arr = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(4, 5, 6, 7), true);
            array_push($arr, $row);
        }
        return $arr;
    }
    /**
    Método que comprueba un gasto
    
    @bitacora Comprobación de un gasto
    @param void
    @return arreglo del estado de la comprobación del gasto
    **/
    public function comprobarGasto($p){
        $p = $this->u8($p, array(2, 3, 4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['edit-Referencia'].' - '.$p[0]);
        $this->c->q("INSERT INTO comprobaciones VALUES(NULL, '".$this->tipo."', '".$_SESSION['edit-Referencia']."', '".$_SESSION['us_id']."', '".$p[0]."', '".$p[1]."', '".$p[2]."', '".$p[3]."', '".$p[4]."', '".$this->d."')");
        $this->c->q("SELECT gas_cubierto, gas_monto FROM gastos WHERE gas_ref = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        $cubierto = $this->c->fr();
        $cubierto[0] += $p[0];
        $cubierto[1] = ($cubierto[0] == $cubierto[1]) ? ", gas_estado = '1'" : "";
        $this->c->q("UPDATE gastos SET gas_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE gas_ref = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        return array('true', $this->c->last('comprobaciones'));
    }
    /**
    Método que comprueba un gasto
    
    @bitacora Comprobación de un préstamo
    @param void
    @return arreglo del estado de la comprobación del gasto
    **/
    public function comprobarPrestamo($p){
        $p = $this->u8($p, array(2, 3, 4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['edit-Referencia'].' - '.$p[0]);
        $this->c->q("INSERT INTO comprobaciones VALUES(NULL, '".$this->tipo."', '".$_SESSION['edit-Referencia']."', '".$_SESSION['us_id']."', '".$p[0]."', '".$p[1]."', '".$p[2]."', '".$p[3]."', '".$p[4]."', '".$this->d."')");
        $this->c->q("SELECT pre_cubierto, pre_monto FROM prestamos WHERE pre_ref = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        $cubierto = $this->c->fr();
        $cubierto[0] += $p[0];
        $cubierto[1] = ($cubierto[0] == $cubierto[1]) ? ", pre_estado = '1'" : "";
        $this->c->q("UPDATE prestamos SET pre_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE pre_ref = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        return array('true', $this->c->last('comprobaciones'));
    } 
    /**
    Método que comprueba una cuenta por pagar
    
    @bitacora Comprobación de una cuenta por pagar
    @param void
    @return arreglo del estado de la comprobación del gasto
    **/
    public function comprobarOrdenCompra($p){
        $p = $this->u8($p, array(2, 3, 4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['edit-Referencia'].' - '.$p[0]);
        $this->c->q("INSERT INTO comprobaciones VALUES(NULL, '".$this->tipo."', '".$_SESSION['edit-Referencia']."', '".$_SESSION['us_id']."', '".$p[0]."', '".$p[1]."', '".$p[2]."', '".$p[3]."', '".$p[4]."', '".$this->d."')");
        $this->c->q("SELECT orc_cubierto, orc_monto FROM ordendecompra WHERE orc_id = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        $cubierto = $this->c->fr();
        $cubierto[0] += $p[0];
        $cubierto[1] = ($cubierto[0] == $cubierto[1]) ? ", orc_estado = '1'" : "";
        $this->c->q("UPDATE ordendecompra SET orc_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE orc_id = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        return array('true', $this->c->last('comprobaciones'));
    }
    /**
    Método que comprueba una venta
    
    @bitacora Comprobación de una venta con id
    @param void
    @return arreglo del estado de la comprobación de la venta
    **/
    public function comprobarVenta($p){
        $p = $this->u8($p, array(2, 3, 4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['edit-Referencia'].' - '.$p[0]);
        $this->c->q("INSERT INTO comprobaciones VALUES(NULL, '".$this->tipo."', '".$_SESSION['edit-Referencia']."', '".$_SESSION['us_id']."', '".$p[0]."', '".$p[1]."', '".$p[2]."', '".$p[3]."', '".$p[4]."', '".$this->d."')");
        $this->c->q("SELECT ven_cubierto, ven_monto FROM ventas WHERE ven_id = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        $cubierto = $this->c->fr();
        $cubierto[0] += $p[0];
        $cubierto[1] = ($cubierto[0] >= $cubierto[1]) ? ", ven_estado = '1'" : "";
        $this->c->q("UPDATE ventas SET ven_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE ven_id = '".$_SESSION['edit-Referencia']."' LIMIT 1;");
        
        return array('true', $this->c->last('comprobaciones'));
    }
    /**
    Método que comprueba una orden de compra
    
    @bitacora Comprobación de una orden de compra
    @param void
    @return arreglo del estado de la comprobación dela orden de compra
    **/
    public function comprobarOrdenDeCompra($ref, $monto, $formaPago, $cuenta, $comprobador, $observaciones){
        $observaciones = WebService::$g->utf8(false, $observaciones);
        $cuenta = WebService::$g->utf8(false, $cuenta);
        $this->log($this, __FUNCTION__, 'bitacora', $ref.' - '.$ref);
        $this->c->q("INSERT INTO comprobaciones VALUES(NULL, '".$this->tipo."', '".$ref."', '".$_SESSION['us_id']."', '".$monto."', '".$formaPago."', '".$cuenta."', '".$comprobador."', '".$observaciones."', '".$this->d."')");
        $this->c->q("SELECT orc_cubierto, orc_monto FROM ordendecompra WHERE orc_id = '".$ref."' LIMIT 1;");
        $cubierto = $this->c->fr();
        $cubierto[0] += $monto;
        $cubierto[1] = ($cubierto[0] == $cubierto[1]) ? ", orc_estado = '1'" : "";
        $this->c->q("UPDATE ordendecompra SET orc_cubierto = '".$cubierto[0]."'".$cubierto[1]." WHERE orc_id = '".$ref."' LIMIT 1;");
        return array('true', $this->c->last('comprobaciones'));
    }
    /**
    Método que establece una referencia en memoria
    
    @bitacora Generación de una referencia en memoria
    @param void
    @return arreglo del estado de la referencia en memoria
    **/
    public function setIdComprobacion($id){
        $_SESSION['edit-Referencia'] = $id;
        switch($this->tipo){
            case 1:
                $vx = array('gas','gastos', 'ref');
            break;
            case 2:
                $vx = array('pre','prestamos', 'ref');
            break;
            case 3:
                $vx = array('orc','ordendecompra', 'id');
            break;
            case 5:
                $vx = array('ven','ventas', 'id');
            break;
            case 4:
                return array(true);
        }
        $this->c->q("SELECT ".$vx[0]."_monto, ".$vx[0]."_cubierto FROM ".$vx[1]." WHERE ".$vx[0]."_".$vx[2]." = '".$id."' LIMIT 1;");
        return array('true', $this->c->fr());
    }
}
?>