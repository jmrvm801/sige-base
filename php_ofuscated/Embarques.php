<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Inventarios.php');
require_once('Proveedores.php');
require_once('Core.php');
/**
Clase Embarques, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Embarques extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Precios
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Embarques(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un precio a través de un id.
    
    @bitacora Acceso a la información de un precio
    @param id del precio a obtener el resultado
    @return arreglo con los datos del precio
    **/
    public function getEmbarqueById($id){
        $this->c->q("SELECT * FROM embarques WHERE emb_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Embarque'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(2, 3, 4, 5, 6), true);
		$data[10] = array();
		$this->c->q("SELECT * FROM notas WHERE not_emb_id = '".$id."' ORDER BY not_fecha DESC;");
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(4), true);
			array_push($data[10], $row);
		}
        return $data;
    }
    /**
    Método principal de la clase Precios
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'saveEstado':
                return $this->saveEstado($_POST['param']);
            break;
            case 'getAllEmbarques':
                return $this->getAllEmbarques();
            break;
            case 'saveEmbarque':
                return (!isset($_SESSION['edit-Embarque'])) ? $this->saveEmbarque() : $this->updateEmbarque();
            break;
            case 'getEmbarqueById':
                return $this->getEmbarqueById($_POST['param']);
            break;
            case 'finalizarEmbarque':
                return $this->finalizarEmbarque($_POST['param']);
            break;
			case 'eliminarEmbarque':
				return $this->eliminarEmbarque($_POST['param']);
			break;
        }
    }
	/**
    Método que crea un estado asociado a un embarque
    
    @bitacora Creación de un estado para el embarque No.: 
    @param void
    @return arreglo de los precios
    **/
	public function saveEstado($v){
		$this->hasAccess(get_class($this));
		$v = $this->u8($v, array(2), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
		$this->c->q("INSERT INTO notas VALUES(NULL,'".$_SESSION['us_id']."', '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$this->d."')");
		return array('true');
	}
    /**
    Método que obtiene toda la información básica de los embarques
    
    @bitacora Acceso a la información básica de todos los embarques
    @param void
    @return arreglo de los embarques
    **/
    public function getAllEmbarques(){
        $v = array();
        unset($_SESSION['edit-Embarque']);
		//$where = ($_SESSION['accesoTotal'] == '1') ? "" : " AND pre_su_id = '".$_SESSION['sucursal']."'";
		$this->c->q("SELECT emb_id, emb_embarque, emb_chofer, emb_origen, emb_destino, emb_fechaCreado FROM embarques WHERE emb_estado = '0' ORDER BY emb_fechaCreado DESC;");
		$d = new db();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4), true);
			$d->q("SELECT not_estado, not_fecha FROM notas WHERE not_emb_id = '".$row[0]."' ORDER BY not_fecha DESC LIMIT 1;");
			$row[6] = ($d->nr() == 0) ? array('-1','') : $d->fr();
            array_push($v, $row);
        }
		$d->cl();
        return $v;
    }
    /**
    Método que crea un embarque.
    
    @bitacora Guardado de un nuevo precio
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveEmbarque(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createEmbarque($p);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un precio en la base de datos
    
    @bitacora Creación de un nuevo precio
    @param arreglo con los datos del precio
    @return identificador del precio agregado en la base de datos
    
    **/
    public function createEmbarque($v){
        $v = $this->u8($v, array(0, 1, 2, 3, 4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO embarques VALUES(NULL, '".$_SESSION['us_id']."', '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '0', '".$this->d."', '')");
        $embarques = $this->c->last('embarques');
        return $embarques;        
    }
	/**
    Método que inserta los datos de un precio en la base de datos
    
    @bitacora Actualización de un embarque con No.: 
    @param arreglo con los datos del precio
    @return identificador del precio agregado en la base de datos
    
    **/
    public function updateEmbarque(){
		$v = $_POST['param'];
        $v = $this->u8($v, array(0, 1, 2, 3, 4), false);
		$this->c->q("UPDATE embarques SET emb_embarque = '".$v[0]."', emb_chofer = '".$v[1]."', emb_origen = '".$v[2]."', emb_destino = '".$v[3]."', emb_observ = '".$v[4]."' WHERE emb_id = '".$_SESSION['edit-Embarque']."' LIMIT 1;");
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        return array('true');
    }

    /**
    Método que marca como eliminado un precio
    
    @bitacora Finalización del embarque No.:
    @param identificador de un precio en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function finalizarEmbarque($id){
        $this->hasAccess(get_class($this));
        $embarque = $this->getEmbarqueById($id);
        unset($_SESSION['edit-Embarque']);
        $this->log($this, __FUNCTION__, 'bitacora', $embarque[0]);
        $this->c->q("UPDATE embarques SET emb_estado = '1', emb_fechaFinalizado = '".$this->d."' WHERE emb_id = '".$embarque[0]."' LIMIT 1;");
        return array('true');
    }
	/**
    Método que marca como eliminado un precio
    
    @bitacora Eliminación del embarque No.:
    @param identificador de un precio en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function eliminarEmbarque($id){
        $this->hasAccess(get_class($this));
        $embarque = $this->getEmbarqueById($id);
        unset($_SESSION['edit-Embarque']);
        $this->log($this, __FUNCTION__, 'bitacora', $embarque[0]);
        $this->c->q("DELETE FROM embarques WHERE emb_id = '".$embarque[0]."' LIMIT 1;");
        $this->c->q("DELETE FROM notas WHERE not_emb_id = '".$embarque[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>