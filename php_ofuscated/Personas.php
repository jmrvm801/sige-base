<?php
require_once('Core.php');
class Personas extends Core{
    public $c;
    public function Personas($c){
        $this->c = $c;
    }
    public function createPersona($v){
        $v = $this->u8($v, array(0, 5, 7, 8), false);
        $this->c->q("INSERT INTO personas VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."', '".$v[7]."', '".$v[8]."', '".$v[9]."', '".date('YmdHis')."')");
        return $this->c->last('personas');
    }
    public function getPersonaById($id){
        $this->c->q("SELECT * FROM personas WHERE pe_id = '".$id."' LIMIT 1;");
        $persona = $this->c->fr();
        $persona[8] = $this->numberDateToString($persona[8]);
        $persona = $this->u8($persona, array(1, 6, 8, 9), true);
        return $persona;
    }
    public function updatePersonaById($id, $v){
        $v = $this->u8($v, array(0, 5, 7, 8), false);
        $v[7] = $this->stringDateToNumber($v[7]);
        $this->c->q("UPDATE personas SET pe_razon = '".$v[0]."', pe_genero = '".$v[1]."', pe_curp = '".$v[2]."', pe_rfc = '".$v[3]."', pe_correo = '".$v[5]."', pe_civil = '".$v[6]."', pe_fechaNac = '".$v[7]."', pe_imss = '".$v[8]."', pe_licencia = '".$v[9]."', pe_fecha = '".date('YmdHis')."' WHERE pe_id = '".$id."' LIMIT 1;");
        return true;
    }
    public function deletePersonaById($id){
        $this->c->q("DELETE FROM personas WHERE pe_id = '".$id."' LIMIT 1;");
        return true;
    }
}
?>