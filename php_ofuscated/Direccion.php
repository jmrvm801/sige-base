<?php
require_once('Core.php');
class Direccion extends Core{
    public $c;
    public function Direccion($c){
        $this->c = $c;
    }
    public function createDir($v){
        $v = $this->u8($v, array(0, 1, 2, 3, 4, 5, 6), false);
        $this->c->q("INSERT INTO direcciones VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."')");
        return $this->c->last('direcciones');
    }
    public function getDirById($id){
        $this->c->q("SELECT * FROM direcciones WHERE di_id = '".$id."' LIMIT 1;");
        $dir = $this->c->fr();
        $dir = @$this->u8($dir, array(1, 2, 3, 5), true);
        return $dir;
    }
    public function updateDirById($id, $v){
        $v = $this->u8($v, array(0, 1, 2, 4), false);
        $this->c->q("UPDATE direcciones SET di_direccion = '".$v[0]."', di_ciudad = '".$v[1]."', di_estado = '".$v[2]."', di_cp = '".$v[3]."', di_pais = '".$v[4]."', di_telefono = '".$v[5]."', di_movil = '".$v[6]."' WHERE di_id = '".$id."' LIMIT 1;");
        return true;
    }
    public function deleteDirById($id){
        $this->c->q("DELETE FROM direcciones WHERE di_id = '".$id."' LIMIT 1;");
        return true;
    }
}
?>