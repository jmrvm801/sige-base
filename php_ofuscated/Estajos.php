<?php
require_once('Core.php');
/**
Clase Estajos, contiene los métodos necesarios para el manejo del módulo catalogos

@author Fernando Carreon
@version 1.0
**/
class Estajos extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Estajos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Estajos($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una unidad a través de un id.
    
    @bitacora Acceso a la información de la unidad
    @param id de la unidad a obtener el resultado
    @return arreglo con los datos de la unidad
    **/
    public function getEstajoById($id){
        $this->c->q("SELECT * FROM estajos WHERE est_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Estajos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
    /**
    Método principal de la clase Estajos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllEstajos':
                return $this->getAllEstajos();
            break;
            case 'saveEstajo':
                return (!isset($_SESSION['edit-Estajos'])) ? $this->saveEstajo() : $this->updateEstajos();
            break;
            case 'getEstajoById':
                return $this->getEstajoById($_POST['param']);
            break;
            case 'delEstajos':
                return $this->deleteEstajos($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las estajos
    
    @bitacora Acceso a la información básica de todos las estajos
    @param void
    @return arreglo de las estajos
    **/
    public function getAllEstajos(){
        $v = array();
        unset($_SESSION['edit-Estajos']);
        $this->c->q("SELECT est_id, est_fecha, pe_razon, est_unidades FROM estajos LEFT JOIN usuarios ON us_id = est_empleado LEFT JOIN personas ON pe_id = us_pe_id LEFT JOIN departamentos ON us_dep_id = dep_id WHERE dep_su_id = '".$_SESSION['sucursal']."' AND est_eliminado = '0' ORDER BY est_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(2), true);
			$row[1] = $this->numberDateToString($row[1]);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea una unidad nueva.
    
    @bitacora Guardado de una nueva unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveEstajo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
		$p[0] = $this->stringDateToNumber($p[0]);
		if ($p[2] == 'TODOS_ESTAJO'){
			$d = new db();
			$d->q("SELECT us_id, pe_razon FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id WHERE us_jornada = '0' AND us_dep_id IN (SELECT dep_id FROM departamentos WHERE dep_su_id = '".$_SESSION['sucursal']."') ORDER BY pe_razon;");
			while($usid = $d->fr()){
				$p[2] = $usid[0];
				$this->createEstajos($p);
			}
			$d->cl();
		} else
        	$this->createEstajos($p);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la unidad en la base de datos
    
    @bitacora Creación de una nueva unidad
    @param arreglo con los datos de la unidad
    @return identificador de la unidad agregada en la base de datos
    
    **/
    public function createEstajos($v){
        $v = $this->u8($v, array(0, 1, 3), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO estajos VALUES(NULL, '".$_SESSION['us_id']."', '".$v[2]."', '".$v[0]."', '".$v[1]."', '".$v[3]."', '".$this->d."', '0')");
        return $this->c->last('estajos');        
    }
    /**
    Método que actualiza la información de una unidad
    
    @bitacora Actualización de una unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateEstajos(){
		return array('error');
		/*
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Estajos']))
            return $arr;
        $unidad = $this->getEstajoById($_SESSION['edit-Estajos']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$unidad[1].' por '.$p[0]);
        $this->c->q("UPDATE estajos SET ded_nombre = '".$p[0]."', ded_monto = '".$p[1]."', ded_fecha = '".$this->d."' WHERE ded_id = '".$unidad[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Estajos']);
        return $arr;
		*/
    }
    /**
    Método que marca como eliminada una unidad
    
    @bitacora Eliminación de una unidad
    @param identificador de una unidad en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteEstajos($id){
        $this->hasAccess(get_class($this));
        $unidad = $this->getEstajoById($id);
        unset($_SESSION['edit-Estajos']);
        $this->log($this, __FUNCTION__, 'bitacora', $unidad[1]);
        $this->c->q("UPDATE estajos SET est_eliminado = '1' WHERE est_id = '".$unidad[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>