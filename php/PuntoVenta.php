<?php
require_once('Inventarios.php');
include_once('Comprobacion.php');
require_once('Core.php');
/**
Clase PuntoVenta, contiene los métodos necesarios para el manejo del módulo punto de ventas

@author Fernando Carreon
@version 1.0
**/
class PuntoVenta extends Core{
    public $c;
    public $d;
    public $data;
    public function __construct(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Método que obtiene la información básica del punto de venta
    
    @bitacora obtención de los datos básicos del punto de venta
    @param void
    @return arreglo con la información de los puntos de venta
    **/
    public function getAllPuntoVenta(){
        $v = array();
		$dia = WebService::$g->srfecha('-180','Ymd');
		//echo "SELECT ven_id, p1.pe_razon, ven_monto, ven_fecha, p2.pe_razon, ven_credito FROM ventas LEFT JOIN usuarios ON ven_us_id = us_id LEFT JOIN personas AS p1 ON p1.pe_id = us_pe_id LEFT JOIN clientes ON cli_id = ven_cli_id LEFT JOIN personas AS p2 ON p2.pe_id = cli_pe_id WHERE ven_tipo = '1' AND ven_su_id = '".$_SESSION['sucursal']."' AND ven_fechaE >= '".$dia."' ORDER BY ven_id DESC;";
		
		
        $this->c->q("SELECT ven_id, p1.pe_razon, ven_monto, ven_fecha, p2.pe_razon, ven_credito FROM ventas LEFT JOIN usuarios ON ven_us_id = us_id LEFT JOIN personas AS p1 ON p1.pe_id = us_pe_id LEFT JOIN clientes ON cli_id = ven_cli_id LEFT JOIN personas AS p2 ON p2.pe_id = cli_pe_id WHERE ven_tipo = '1' AND ven_su_id = '".$_SESSION['sucursal']."' AND ven_fecha >= '".$dia."' ORDER BY ven_fecha DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 4), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene la información básica del punto de venta
    
    @bitacora obtención de los datos básicos del punto de venta
    @param void
    @return arreglo con la información de los puntos de venta
    **/
    public function getAllCotizaciones(){
        $v = array();
        $this->c->q("SELECT ven_id, pe_razon, ven_monto, ven_fecha FROM ventas LEFT JOIN usuarios ON ven_us_id = us_id LEFT JOIN personas ON pe_id = us_pe_id WHERE ven_tipo = '2' AND ven_su_id = '".$_SESSION['sucursal']."' ORDER BY ven_id DESC LIMIT 50;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene la información básica de las ordenes de venta
    
    @bitacora obtención de los datos básicos de las ordenes de venta
    @param void
    @return arreglo con la información de los puntos de venta
    **/
    public function getAllOrdenesVenta(){
        $v = array();
        $this->c->q("SELECT ven_id, pe_razon, ven_monto, ven_fecha FROM ventas LEFT JOIN usuarios ON ven_us_id = us_id LEFT JOIN personas ON pe_id = us_pe_id WHERE ven_tipo = '3' AND ven_su_id = '".$_SESSION['sucursal']."' ORDER BY ven_id DESC LIMIT 50;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene la información básica de las ordenes de venta
    
    @bitacora obtención de los datos básicos de las ordenes de venta
    @param void
    @return arreglo con la información de los puntos de venta
    **/
    public function getAllVentasPagar(){
        $v = array();
		$this->c->q("SELECT ven_id, ven_fecha, ven_monto, ven_estado, ven_monto, ven_observaciones, pe_razon FROM ventas LEFT JOIN sucursales ON su_id = ven_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN clientes ON cli_id = ven_cli_id LEFT JOIN personas ON pe_id = cli_pe_id WHERE ven_tipo = '1' AND ven_estado = '0' AND ven_credito = '1' AND ven_su_id = '".$_SESSION['sucursal']."' ORDER BY ven_id DESC;");
		$d = new db();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(5, 6), true);
			$montito = 0;
			$d->q("SELECT com_formaPago, com_cuenta, com_monto FROM comprobaciones WHERE com_tipo = '5' AND com_ref = '".$row[0]."';");
			if ($d->nr() > 1){
				$row[3] = 'Ver pagos para detalles';
				$row[4] = 'Ver pagos para detalles';
				while($monto = $d->fr()){
					$montito += $monto[2];
				}
			} else {
				if ($d->nr() == 1){
					$tr = $d->fr();
					$montito += $tr[2];
					$tr = $this->u8($tr, array(1), true);
					$row[3] = $this->getFormaPago($tr[0]);
					$row[4] = $tr[1];
				} else{
					$row[3] = 'Sin datos';
					$row[4] = 'Sin datos';
				}
			}
			$row[7] = $row[2] - $montito;
			if ($row[7] > 0)
				array_push($v, $row);
        }
		$d->cl();
        return $v;
    }
    public function run($method){
        switch($method){
			case 'detalleDeudaLista':
			case 'detalleDcubrLista':
				return $this->detalleDeudaLista($_POST['param']);
			break;
			case 'detalleAbonoLista':
				return $this->detalleAbonoLista($_POST['param']);
			break;
			case 'getCPCresumido':
				return $this->getCPCresumido();
			break;
			case 'getCPCliente':
				return $this->getCPCliente($_POST['param']);
			break;
			case 'CPCpagos':
				return $this->CPCpagos($_POST['param']);
			break;
			case 'getPVventas':
				return $this->getPVventas($_POST['param']);
			break;
            case 'getAllVentasPagar':
                return $this->getAllVentasPagar();
            case 'getAllPuntoVenta':
                return $this->getAllPuntoVenta();
            case 'getAllCotizaciones':
                return $this->getAllCotizaciones();
            case 'getAllOrdenesVenta':
                return $this->getAllOrdenesVenta();
            case 'getBasic':
                return $this->getBasic(((isset($_POST['param'])) ? $_POST['param'] : ''));
            case 'setCliente':
                return $this->setCliente($_POST['param']);
            case 'getProductsByParam':
                return $this->getProductsByParam($_POST['param']);
            case 'savePos':
                return $this->savePos($_POST['param']);
            case 'saveCotizacion':
                return $this->saveCotizacion($_POST['param']);
            case 'updateCotizacion':
                return $this->updateCotizacion($_POST['param']);
            case 'updateOrdenVenta':
                return $this->updateOrdenVenta($_POST['param']);
            case 'saveOrdenVenta':
                return $this->saveOrdenVenta($_POST['param']);
            case 'formalizeVenta':
                return $this->formalizeVenta($_POST['param']);
            case 'getSimpleTicket':
                return $this->getSimpleTicket($_POST['param']);
            case 'getComplexTicket':
                return $this->getComplexTicket($_POST['param']);
            case 'delCotizacion':
                return $this->delCotizacion($_POST['param']);
            case 'delOrdenVenta':
                return $this->delOrdenVenta($_POST['param']);
            case 'convertIntoOrdenVenta':
                return $this->convertIntoOrdenVenta($_POST['param']);
            case 'validateOrdenVenta':
                return $this->validateOrdenVenta($_POST['param']);
			case 'cancelTicketVenta':
				return $this->cancelTicketVenta($_POST['param']);
            case 'setIdComprobacion':
            case 'comprobarVenta':
            case 'getComprobacionesById':
            case 'delComprobante':
                $_SESSION['edit-OrdenVenta'] = $_POST['param'];
                $comp = new Comprobacion($this->c, 5);
                return $comp->run($method);
            default:
                return array('null');
        }
    }
	public function detalleDeudaLista($p){
		$this->c->q("SELECT ven_id, ven_fecha, ven_monto, ven_cubierto, ven_observaciones FROM ventas WHERE ven_su_id = '".$_SESSION['sucursal']."' AND ven_cli_id IN(SELECT ven_cli_id FROM ventas WHERE ven_id = '".$p."') AND ven_estado = '0' AND ven_credito = '1' ORDER BY ven_fecha DESC;");
		$arr = array();
		while($row = $this->c->fr()){
			$row[5] = $row[4];
			$row = $this->u8($row, array(4), true);
			$row[4] = $row[2] - $row[3];
			if ($row[4] > 0)
				array_push($arr, $row);
		}
		return $arr;
	}
	public function detalleAbonoLista($p){
		$this->c->q("SELECT ven_id, ven_monto, ven_cubierto FROM ventas WHERE ven_su_id = '".$_SESSION['sucursal']."' AND ven_cli_id IN(SELECT ven_cli_id FROM ventas WHERE ven_id = '".$p."') AND ven_estado = '0' AND ven_credito = '1';");
		$arr = array();
		while($row = $this->c->fr()){
			$row[2] = $row[1] - $row[2];
			if ($row[2] > 0)
				array_push($arr, $row[0]);
		}
		$arr = implode("', '", $arr);
		$arrb = array();
		$this->c->q("SELECT com_id, com_ref, com_fecha, com_monto, com_formapago FROM comprobaciones WHERE com_tipo = '5' AND com_ref IN('".$arr."') ORDER BY com_fecha DESC");
		while($row = $this->c->fr()){
			$row[4] = $this->getFormaPago($row[4]);
			array_push($arrb, $row);
		}
		return $arrb;
	}
	public function cancelTicketVenta($p){
		$this->c->q("SELECT ven_id, ven_products, ven_monto FROM ventas WHERE ven_su_id = '".$_SESSION['sucursal']."' AND ven_id = '".$p."' LIMIT 1;");
		$id = $this->c->fr();
		$id[1] = json_decode($id[1]);
		if ($id[2] != '0'){
			$inv = new Inventarios();
			$mov = new Movimientos($this->c);
			for($i = 0; $i < count($id[1]); $i++){
				$value = $inv->existInventarioSell($_SESSION['sucursal'], $id[1][$i][0]);
				$mov->createMovimiento($value, 0, $id[1][$i][1], WebService::$g->utf8(false, "entrada por cancelacion de ticket de punto de venta con folio: ".$p));
			}
			$this->c->q("UPDATE ventas SET ven_monto = '0' WHERE ven_id = '".$p."' LIMIT 1;");
			return array('true');
		} else
			return array('false');
	}
	public function getPVventas($p){
		$v = array();
		$p[0] = $this->stringDateToNumber($p[0]);
		$p[1] = $this->stringDateToNumber($p[1]);
		if ($p[5] == '1')
			$p[5] = " AND ven_credito = '1' ";
		else if ($p[5] == '0')
			$p[5] = " AND ven_credito = '0' ";
		else
			$p[5] = "";
		$p[2] = ($p[2] != '') ? " AND ven_cli_id = '".$p[2]."' " : "";
		$p[3] = ($p[3] != '') ? " AND ven_us_id = '".$p[3]."' " : "";
		$p[4] = ($p[4] != '') ? " AND ven_id = '".$p[4]."' " : "";
		$this->c->q("SELECT ven_id, p1.pe_razon, p2.pe_razon, ven_monto, ven_fecha, ven_credito FROM ventas LEFT JOIN usuarios ON ven_us_id = us_id LEFT JOIN personas AS p1 ON p1.pe_id = us_pe_id LEFT JOIN clientes ON cli_id = ven_cli_id LEFT JOIN personas AS p2 ON p2.pe_id = cli_pe_id WHERE ven_tipo = '1'".$p[5]." ".$p[2]." ".$p[3]."".$p[4]." AND (ven_fecha >= '".$p[0]."' AND ven_fecha <= '".$p[1]."') AND ven_su_id = '".$_SESSION['sucursal']."' ORDER BY ven_id DESC;");
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(1, 2), true);
			array_push($v, $row);
		}
		return $v;
	}
	public function CPCpagos($p){
		$v = array();
		$p[2] = ($p[2] != '') ? " AND ven_cli_id = '".$p[2]."' " : "";
		$p[0] = $this->stringDateToNumber($p[0]);
		$p[1] = $this->stringDateToNumber($p[1]);
		$this->c->q("SELECT com_ref, com_fecha, pe_razon, com_monto, com_formaPago FROM comprobaciones LEFT JOIN ventas ON ven_id = com_ref LEFT JOIN clientes ON ven_cli_id = cli_id LEFT JOIN personas ON pe_id = cli_pe_id WHERE ven_su_id = '".$_SESSION['sucursal']."' AND com_tipo = '5' AND (com_fecha >= '".$p[0]."000000' AND com_fecha <= '".$p[1]."235959') ".$p[2]." ORDER BY com_id DESC;");
		$d = new db();
        while($row = $this->c->fr()){
			$row = $this->u8($row, array(2), true);
			$row[4] = $this->getFormaPago($row[4]);
			array_push($v, $row);
        }
		$d->cl();
        return $v;
	}
	public function getCPCliente($p){
		$v = array();
		$p[2] = ($p[2] != '') ? " AND ven_cli_id = '".$p[2]."' " : "";
		$p[3] = ($p[3] == '1') ? " AND ven_estado = '0' " : "";
		$p[0] = $this->stringDateToNumber($p[0]);
		$p[1] = $this->stringDateToNumber($p[1]);
		$this->c->q("SELECT ven_id, ven_fecha, ven_monto, ven_estado, pe_razon, pe_razon FROM ventas LEFT JOIN sucursales ON su_id = ven_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN clientes ON cli_id = ven_cli_id LEFT JOIN personas ON pe_id = cli_pe_id WHERE ven_tipo = '1' ".$p[3]." AND ven_credito = '1' AND ven_su_id = '".$_SESSION['sucursal']."' ".$p[2]." AND (ven_fecha >= '".$p[0]."' AND ven_fecha <= '".$p[1]."') ORDER BY ven_id DESC;");
		$d = new db();
        while($row = $this->c->fr()){
			$pago = 0;
            $row = $this->u8($row, array(5), true);
			$d->q("SELECT com_formaPago, com_monto FROM comprobaciones WHERE com_tipo = '5' AND com_ref = '".$row[0]."';");
			if ($d->nr() > 1){
				$row[3] = 'Ver pagos para detalles';
				while($tr = $d->fr())
					$pago += $tr[1];
			} else {
				if ($d->nr() == 1){
					$tr = $d->fr();
					$row[3] = $this->getFormaPago($tr[0]);
					$pago += $tr[1];
				} else{
					$row[3] = 'Sin datos';
				}
			}
			$row[4] = $row[2] - $pago;
			array_push($v, $row);
        }
		$d->cl();
        return $v;
	}
	public function getCPCresumido(){
		$this->c->q("SELECT pe_id, pe_razon, SUM(ven_monto), SUM(ven_cubierto), ven_id FROM ventas LEFT JOIN clientes ON ven_cli_id = cli_id LEFT JOIN personas ON pe_id = cli_pe_id WHERE ven_su_id = '".$_SESSION['sucursal']."' AND ven_estado = '0' AND ven_credito = '1' GROUP BY ven_cli_id");
		$arr = array();
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(1), true);
			$row[5] = $row[4];
			$row[4] = $row[2] - $row[3];
			if ($row[4] > 0)
				array_push($arr, $row);
		}
		return $arr;
	}
    /**
     * Metódo que cambia una cotización en una orden de venta
     *
     * @bitacora Se ha convertido una cotización en orden de venta con id
     * @param $p number de la cotización
     * @return array con el estado del cambio
     */
    public function convertIntoOrdenVenta($p){
        $this->c->q("UPDATE ventas SET ven_tipo = '3' WHERE ven_tipo = '2' AND ven_id = '".$p."' LIMIT 1;");
        return array('true', $p);
    }
    /**
     * Método que obtiene la información de la venta para formato completo.
     *
     * @bitacora validar información de la orden de venta
     * @param number de la venta
     * @return array|mixed|null con la información de la venta
     */
    public function validateOrdenVenta($p){
        $_SESSION['venta-id-finish'] = $p;
        $this->c->q("SELECT ven_products, ven_su_id FROM ventas WHERE ven_id = '".$p."' AND ven_tipo = '3' LIMIT 1;");
        if ($this->c->nr() == 0)
            return array('false');
        $venta = $this->c->fr();
        $venta[0] = WebService::$g->json(false, $venta[0]);
        $inv = new Inventarios();
        $mov = new Movimientos($this->c);
        
        for($i = 0; $i < count($venta[0]); $i++){
            $value = $inv->existInventarioSell($venta[1], $venta[0][$i][0]);
            $pz = $mov->checkForUnitInWarehouse($value, $venta[0][$i][1]);
            if (!$pz){
                $this->c->q("SELECT pre_nombre, art_nombre, uni_nombre, in_actual FROM inventarios LEFT JOIN precios ON pre_id = in_pre_id LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE in_id = '".$value."' LIMIT 1;");
                $cm = $this->c->fr();
                $cm = $this->u8($cm, array(0, 1, 2), true);
                $cm[4] = $venta[0][$i][1];
                return array('insufficient', $cm);
            }
        }
        return array('true');
    }
    /**
     * Método que obtiene la información de la venta para formato completo.
     *
     * @bitacora obtención de datos de la venta
     * @param number de la venta
     * @return array|mixed|null con la información de la venta
     */
    public function getComplexTicket($p){
        $_SESSION['venta-id'] = $p;
        $this->c->q("SELECT * FROM ventas WHERE ven_id = '".$p."' LIMIT 1;");
        $venta = $this->c->fr();
        $venta = $this->u8($venta, array(13, 14, 15, 16, 17), true);
        $this->c->q("SELECT em_nombre, su_nombre, di_direccion, di_ciudad, di_estado, di_cp, di_pais, di_telefono, di_movil FROM sucursales LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN direcciones ON su_di_id = di_id WHERE su_id = '".$venta[1]."' LIMIT 1;");
        $venta[1] = $this->c->fr();
        $venta[1] = $this->u8($venta[1], array(0, 1, 2, 3, 4, 5, 6, 7, 8), true);
        if ($venta[2] != '0'){
            $this->c->q("SELECT pe_razon, di_direccion, di_ciudad, di_estado, di_cp, di_pais, di_telefono, di_movil FROM clientes LEFT JOIN personas ON pe_id = cli_pe_id LEFT JOIN direcciones ON pe_di_id = di_id WHERE cli_id = '".$venta[2]."' LIMIT 1;");
            $cm = $this->c->fr();
            $cm = $this->u8($cm, array(0, 1, 2, 3, 4, 5, 6, 7), true);
            $cm[8] = $venta[2];
            $venta[2] = $cm;
        } else
            $venta[2] = array('Venta general','','','','','','','','');
        $this->c->q("SELECT pe_razon FROM usuarios LEFT JOIN personas ON pe_id = us_pe_id WHERE us_id = '".$venta[3]."' LIMIT 1;");
        $venta[3] = WebService::$g->utf8(true, $this->c->r(0));
        $venta[4] = WebService::$g->json(false, $venta[4]);
        for($i = 0; $i < count($venta[4]); $i++){
            $this->c->q("SELECT art_nombre, uni_nombre, fa_nombre, art_code, pre_nombre, art_codigoSat, art_unidadSat, pre_id, pre_mayoreo, pre_menudeo, in_actual FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN unidades ON uni_id = art_uni_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN inventarios ON pre_id = in_pre_id WHERE pre_id = '".$venta[4][$i][0]."'");
            $venta[4][$i][5] = $this->c->fr();
            $venta[4][$i][5] = $this->u8($venta[4][$i][5], array(0, 1, 2, 3, 4, 5, 6), true);
        }
        return $venta;
    }

    /**
     * Método que obtiene la información de la venta para formato en ticket de venta.
     *
     * @bitacora obtención de datos de la venta
     * @param $p number de la venta
     * @return array|mixed|null con la información de la venta
     */
    public function getSimpleTicket($p){
        $this->c->q("SELECT * FROM ventas WHERE ven_id = '".$p."' LIMIT 1;");
        $venta = $this->c->fr();
        $venta = $this->u8($venta, array(13, 14, 15, 16, 17), true);
        $this->c->q("SELECT em_nombre, su_nombre, di_direccion, di_ciudad, di_estado, di_cp, di_pais, di_telefono, di_movil FROM sucursales LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN direcciones ON su_di_id = di_id WHERE su_id = '".$venta[1]."' LIMIT 1;");
        $venta[1] = $this->c->fr();
        $venta[1] = $this->u8($venta[1], array(0, 1, 2, 3, 4, 5, 6, 7, 8), true);
        if ($venta[2] != '0'){
            $this->c->q("SELECT pe_razon FROM clientes LEFT JOIN personas ON pe_id = cli_pe_id WHERE cli_id = '".$venta[2]."' LIMIT 1;");
            $venta[2] = WebService::$g->utf8(true, $this->c->r(0));
        } else
            $venta[2] = 'venta general';
        $this->c->q("SELECT pe_razon FROM usuarios LEFT JOIN personas ON pe_id = us_pe_id WHERE us_id = '".$venta[3]."' LIMIT 1;");
        $venta[3] = WebService::$g->utf8(true, $this->c->r(0));
        $venta[4] = WebService::$g->json(false, $venta[4]);
        for($i = 0; $i < count($venta[4]); $i++){
            $this->c->q("SELECT art_nombre, uni_nombre FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE pre_id = '".$venta[4][$i][0]."'");
            $venta[4][$i][5] = $this->c->fr();
            $venta[4][$i][5] = $this->u8($venta[4][$i][5], array(0, 1), true);
        }
        return $venta;
    }

    /**
     * Método que guarda los datos del punto de venta
     *
     * @bitacora Creación de una nueva venta con id
     * @param $p array con la información de la venta realizada
     * @return array con el estado del guardado y último identificador
     */
    public function savePos($p){
        $this->hasAccess('PuntoVenta');
        //Revisar si hay inventario para estos productos
        $inv = new Inventarios();
        $mov = new Movimientos($this->c);
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            if ($value == -1)
                return array('noexist', $p[0][$i][0]);
            $value = $mov->checkForUnitInWarehouse($value, $p[0][$i][1]);
            if (!$value)
                return array('insufficient', $p[0][$i][0]);
        }
        $json = WebService::$g->json(true, $p[0]);
		$p[1][9] = $this->stringDateToNumber($p[1][9]);
        if (isset($_SESSION['venta-id-finish'])){
            $id = $this->updatePos(3, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7]));
            $p[1] = $this->u8($p[1], array(3, 4, 5, 6, 7), false);
            $this->c->q("UPDATE ventas SET ven_camion = '".$p[1][3]."', ven_marca = '".$p[1][4]."', ven_placas = '".$p[1][5]."', ven_operador = '".$p[1][6]."', ven_observaciones = '".$p[1][7]."', ven_fecha = '".$this->d."', ven_tipo = '1' WHERE ven_id = '".$_SESSION['venta-id-finish']."' LIMIT 1;");
        } else {
            $id = $this->createPos(1, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7], $p[1][8], $p[1][9]));
        }
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        $comp = new Comprobacion($this->c, 4);
        $comp->setIdComprobacion($id);
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            $mov->createMovimiento($value, 1, $p[0][$i][1], WebService::$g->utf8(false, "Salida por punto de venta con folio: ".$id));
        }
        if (isset($p[2]))
            for($i = 0; $i < count($p[2]); $i++)
                $comp->comprobarVenta(array($p[2][$i][0], $p[2][$i][1], $p[2][$i][2], $_SESSION['us_id'], $p[2][$i][3]));
        //Crear punto de venta
		unset($_SESSION['venta-id-finish']);
        return array('true', $id);
    }

    /**
     * Método que guarda los datos de una cotización
     *
     * @bitacora Creación de una nueva cotización con id
     * @param $p array con la información de la cotización realizada
     * @return array con el estado del guardado y último identificador
     */
    public function saveCotizacion($p){
        $this->hasAccess('Cotizaciones');
        //Revisar si hay inventario para estos productos
        $inv = new Inventarios();
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            if ($value == -1)
                return array('noexist', $p[0][$i][0]);
        }
        $json = WebService::$g->json(true, $p[0]);
		$p[1][9] = $this->stringDateToNumber($p[1][9]);
        $id = $this->createPos(2, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7], 0, $p[1][9]));
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        return array('true', $id);
    }
    /**
     * Método que guarda los datos de una cotización
     *
     * @bitacora Creación de una nueva cotización con id
     * @param $p array con la información de la cotización realizada
     * @return array con el estado del guardado y último identificador
     */
    public function saveOrdenVenta($p){
        $this->hasAccess('Cotizaciones');
        //Revisar si hay inventario para estos productos
        $inv = new Inventarios();
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            if ($value == -1)
                return array('noexist', $p[0][$i][0]);
        }
        $json = WebService::$g->json(true, $p[0]);
        $id = $this->createPos(3, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7], 0, 0));
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        return array('true', $id);
    }
    /**
     * Método que actualiza una cotización
     *
     * @bitacora Actualización de una cotización con id
     * @param $p array con la información de la cotización modificada
     * @return array con el estado del guardado y su último identificador.
     */
    public function updateOrdenVenta($p){
        $this->hasAccess('OrdenDeVenta');
        //Revisar si hay inventario para estos productos
        $inv = new Inventarios();
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            if ($value == -1)
                return array('noexist', $p[0][$i][0]);
        }
        
        $json = WebService::$g->json(true, $p[0]);
        $id = $this->updatePos(3, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7]));
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        return array('true', $id);
    }
    /**
     * Método que actualiza una cotización
     *
     * @bitacora Actualización de una cotización con id
     * @param $p array con la información de la cotización modificada
     * @return array con el estado del guardado y su último identificador.
     */
    public function updateCotizacion($p){
        $this->hasAccess('Cotizaciones');
        //Revisar si hay inventario para estos productos
        $inv = new Inventarios();
        for($i = 0; $i < count($p[0]); $i++){
            $value = $inv->existInventarioSell($_SESSION['su_id'], $p[0][$i][0]);
            if ($value == -1)
                return array('noexist', $p[0][$i][0]);
        }
        
        $json = WebService::$g->json(true, $p[0]);
        $id = $this->updatePos(2, array($json, $p[1][0], 0, $p[1][0], $p[1][1], $p[1][2], $p[1][3], $p[1][4], $p[1][5], $p[1][6], $p[1][7]));
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        return array('true', $id);
    }

    /**
     * Método que actualiza los datos de una venta en la base de datos
     *
     * @bitacora actualización de una venta con id
     * @param $p array con los datos de la venta
     * @return number id de la venta modificada
     * @param $tipo number cotización u orden de venta
     * @return bool
     */
    public function updatePos($tipo, $p) { //Solo se permiten tipo 2 y 3
        if ($tipo == 1)
            return -1;
        $id = (isset($_SESSION['venta-id-finish'])) ? $_SESSION['venta-id-finish'] : $_SESSION['venta-id'];
        $p = $this->u8($p, array(6, 7, 8, 9, 10), false);
        $this->c->q("UPDATE ventas SET ven_su_id = '".$_SESSION['su_id']."', ven_cli_id = '".$_SESSION['cli_id_select']."', ven_products = '".$p[0]."', ven_bruto = '".$p[1]."', ven_dscto = '0', ven_sbtotal = '".$p[3]."', ven_iva = '".$p[4]."', ven_monto = '".$p[5]."', ven_fecha = '".$this->d."' WHERE ven_id = '".$id."' AND ven_tipo = '".$tipo."' LIMIT 1;");
        return $id;
    }

    /**
     * Método que guarda los datos de la venta en la base de datos
     *
     * @bitacora Creación de una nueva venta con id
     * @param $p array con los datos de la venta
     * @return number id de la venta creada
     * @param $tipo number cotización u orden de venta
     */
    public function createPos($tipo, $p){
        $p = $this->u8($p, array(6, 7, 8, 9, 10), false);
        $this->c->q("INSERT INTO ventas VALUES(NULL, '".$_SESSION['su_id']."', '".$_SESSION['cli_id_select']."', '".$_SESSION['us_id']."', '".$p[0]."', '".$p[1]."', '".$p[2]."', '".$p[3]."', '".$p[4]."', '".$p[5]."', '".$tipo."', '0', '0', '".$p[6]."', '".$p[7]."', '".$p[8]."', '".$p[9]."', '".$p[10]."', '".$p[12]."', '".$p[11]."', '".$this->d."')");
        $id = $this->c->last('ventas');
        return $id;
    }

    /**
     * Método que devuelve los artículos con base en algún nombre.
     *
     * @bitacora acceso al menú
     * @param $v string nombre del tipo de venta a realizar
     * @return array
     */
    public function getProductsByParam($v){
        $v[1] = ($v[1] == 'cotizacion' || $v[1] == 'ordendeventa') ? "" : " AND in_actual > 0";
		$this->c->q("SELECT DISTINCT pre_id, pre_nombre, art_nombre, uni_nombre, fa_nombre, pre_mayoreo, pre_menudeo, in_actual FROM precios LEFT JOIN inventarios ON in_pre_id = pre_id LEFT JOIN articulos ON pre_art_id = art_id LEFT JOIN unidades ON uni_id = art_uni_id LEFT JOIN familias ON fa_id = art_fa_id WHERE pre_su_id = '".$_SESSION['sucursal']."' ".$v[1]." AND (fa_nombre LIKE '%".$v[0]."%' OR pre_nombre LIKE '%".$v[0]."%' OR art_nombre LIKE '%".$v[0]."%') AND pre_deleted = '0' ORDER BY art_nombre LIMIT 3;"); 
        $arr = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4), true);
            array_push($arr, $row);
        }
        return $arr;
    }

    /**
     * Método que establece la información del cliente para el punto de venta
     *
     * @bitacora establecimiento del cliente para el punto de venta
     * @param $p number id del cliente
     * @return array
     */
    public function setCliente($p){
        $_SESSION['cli_id_select'] = $p;
        $this->c->q("SELECT pe_razon FROM clientes LEFT JOIN personas ON pe_id = cli_pe_id WHERE cli_id = '".$p."' LIMIT 1;");
        $nombre = $this->c->r(0);
        $nombre = WebService::$g->utf8(false, $nombre);
        return array($nombre);
    }

    /**
     * Método que obtiene la información básica del punto de venta
     *
     * @bitacora Obtención de los datos básicos para el punto de venta
     * @param $id number|string id de la sucursal en caso de existir
     * @return array|mixed|null con la información básica
     */
    public function getBasic($id){
		$this->c->q("SELECT em_id, em_nombre, su_id, su_nombre FROM sucursales LEFT JOIN empresas ON su_em_id = em_id WHERE su_id = '".$_SESSION['sucursal']."' LIMIT 1;");
        $row = $this->c->fr();
        $this->c->q("SELECT pe_razon FROM usuarios LEFT JOIN personas ON pe_id = us_pe_id WHERE us_id = '".$_SESSION['us_id']."' LIMIT 1;");
        $row[4] = $this->c->r(0);
        $row = $this->u8($row, array(1, 3, 4), true);
        $_SESSION['su_id'] = $_SESSION['sucursal'];
        return $row;
    }

    /**
     * Método que elimina una cotización
     *
     * @bitacora Eliminación de una cotización con id
     * @param $p number id de la cotización a eliminar
     * @return array con el estado de la eliminación
     */
    public function delCotizacion($p){
        $this->hasAccess('Cotizaciones');
        $this->c->q("SELECT ven_id FROM ventas WHERE ven_id = '".$p."' AND ven_tipo = '2' LIMIT 1;");
        if ($this->c->nr() == '0')
            return array('denied');
        $this->log($this, __FUNCTION__, 'bitacora', $p);
        $this->c->q("DELETE FROM ventas WHERE ven_id = '".$p."' LIMIT 1;");
        return array('true');
    }

    /**
     * Método que elimina una orden de venta
     *
     * @bitacora Eliminación de una orden de venta con id
     * @param $p number id de la orden de venta a eliminar
     * @return array con el estado de la eliminación
     */
    public function delOrdenVenta($p){
        $this->hasAccess('OrdenDeVenta');
        $this->c->q("SELECT ven_id FROM ventas WHERE ven_id = '".$p."' AND ven_tipo = '3' LIMIT 1;");
        if ($this->c->nr() == '0')
            return array('denied');
        $this->log($this, __FUNCTION__, 'bitacora', $p);
        $this->c->q("DELETE FROM ventas WHERE ven_id = '".$p."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}