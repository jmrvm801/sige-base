<?php
require_once('Core.php');
require_once('Direccion.php');
require_once('Personas.php');
/**
Clase Proveedores, contiene los métodos necesarios para el manejo del módulo proveedores

@author Fernando Carreon
@version 1.0
**/
class Proveedores extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Proveedores
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Proveedores($c = ''){
		$this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un proveedor a través de un id.
    
    @bitacora Acceso a la información de un proveedor
    @param id del proveedor a obtener el resultado
    @return arreglo con los datos del proveedor
    **/
    public function getProveedoresById($id){
        $this->c->q("SELECT * FROM proveedores WHERE pro_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Proveedores'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(2), true);
        $per = new Personas($this->c);
        $data[1] = $per->getPersonaById($data[1]);
        $dir = new Direccion($this->c);
        $data[1][5] = $dir->getDirById($data[1][5]);
        return $data;
    }
    /**
    Método principal de la clase Proveedores
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllProveedores':
                return $this->getAllProveedores();
            break;
            case 'saveProveedor':
                return (!isset($_SESSION['edit-Proveedores'])) ? $this->saveProveedor() : $this->updateProveedor();
            break;
            case 'getProveedoresById':
                return $this->getProveedoresById($_POST['param']);
            break;
            case 'delProveedores':
                return $this->delProveedores($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los proveedores
    
    @bitacora Acceso a la información básica de todos los proveedores
    @param void
    @return arreglo de los proveedores
    **/
    public function getAllProveedores(){
        $v = array();
        unset($_SESSION['edit-Proveedores']);
        $this->c->q("SELECT pro_id, pe_razon, pe_correo, di_telefono, di_movil, di_ciudad, di_estado FROM proveedores LEFT JOIN personas ON pe_id = pro_pe_id LEFT JOIN direcciones ON pe_di_id = di_id WHERE pro_deleted = '0' ORDER BY pe_razon ASC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 5, 6), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene la lista de proveedores en formato para un campo de selección
    
    @bitacora Obtención de datos generales de los proveedores
    @param void
    @return arreglo de proveedores
    **/
    public function getForProveedores(){
        $arr = array();
        $proved = $this->getAllProveedores();
        for($i = 0; $i < count($proved); $i++)
            array_push($arr, array($proved[$i][0], $proved[$i][1].' '.$proved[$i][5].' '.$proved[$i][6]));
        return $arr;
    }
    /**
    Método que crea un proveedor.
    
    @bitacora Guardado de un nuevo proveedor
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveProveedor(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $p[7] = $this->StringDateToNumber($p[7]);
        $dir = new Direccion($this->c);
        $iddir = $dir->createDir(array($p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]));
        $per = new Personas($this->c);
        $idpersona = $per->createPersona(array($p[1], $p[2], $p[3], $p[4], $iddir, $p[6], $p[5], $p[7], $p[8], $p[9]));
        $this->createProveedor(array($idpersona, $p[0], $p[17]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un préstamo en la base de datos
    
    @bitacora Creación de un nuevo proveedor
    @param arreglo con los datos del préstamo
    @return identificador del préstamo agregado en la base de datos
    
    **/
    public function createProveedor($v){
        $v = $this->u8($v, array(1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[1]);
        $this->c->q("INSERT INTO proveedores VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0', '".$v[2]."')");
        return $this->c->last('proveedores');        
    }
    /**
    Método que actualiza la información de un proveedor
    
    @bitacora Actualización de un proveedor
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateProveedor(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Proveedores']))
            return $arr;
        $proveedor = $this->getProveedoresById($_SESSION['edit-Proveedores']);
        $p = $_POST['param'];
        $dir = new Direccion($this->c);
        $dir->updateDirById($proveedor[1][5][0], array($p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]));
        $per = new Personas($this->c);
        $per->updatePersonaById($proveedor[1][0], array($p[1], $p[2], $p[3], $p[4], $proveedor[1][5][0], $p[6], $p[5], $p[7], $p[8], $p[9]));
        $p = $this->u8($p, array(0), false);
        $this->log($this, __FUNCTION__, 'bitacora', $proveedor[1][1].' por '.$p[1]);
        $this->c->q("UPDATE proveedores SET pro_observaciones = '".$p[0]."', pro_fecha = '".$this->d."', pro_su_id = '".$p[17]."' WHERE pro_id = '".$proveedor[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Proveedores']);
        return $arr;
    }
    /**
    Método que marca como eliminada un proveedor
    
    @bitacora Eliminación de un proveedor
    @param identificador de un proveedor en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delProveedores($id){
        $this->hasAccess(get_class($this));
        $proveedor = $this->getProveedoresById($id);
        unset($_SESSION['edit-Proveedores']);
        $this->log($this, __FUNCTION__, 'bitacora', $proveedor[0]);
        $this->c->q("UPDATE proveedores SET pro_deleted = '1' WHERE pro_id = '".$proveedor[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
			$this->c->cl();
    }
}
?>