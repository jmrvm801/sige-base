<?php
require_once('Direccion.php');
require_once('Empresas.php');
require_once('Departamentos.php');
require_once('Core.php');
/**
Clase Empresas, contiene los métodos necesarios para el manejo del módulo empresas

@author Fernando Carreon
@version 1.0
**/
class Sucursales extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Sucursales
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Sucursales($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una sucursal a través de un id.
    
    @bitacora Acceso a la información de la sucursal
    @param id de la sucursal a obtener el resultado
    @return arreglo con los datos de la sucursal
    **/
    public function getSucursalById($id){
        $this->c->q("SELECT * FROM sucursales WHERE su_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Sucursal'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(3, 4), true);
        $dir = new Direccion($this->c);
        $data[2] = $dir->getDirById($data[2]);
        $emp = new Empresas();
        $data[1] = $emp->getEmpresaById($data[1]);
        return $data;
    }
    /**
    Método principal de la clase Sucursales
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllSucursales':
                return $this->getAllSucursales();
            break;
            case 'saveSucursal':
                return (!isset($_SESSION['edit-Sucursal'])) ? $this->saveSucursal() : $this->updateSucursales();
            break;
            case 'getSucursalById':
                return $this->getSucursalById($_POST['param']);
            break;
            case 'delSucursal':
                return $this->deleteSucursal($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las sucursales
    
    @bitacora Acceso a la información básica de todas las sucursales
    @param void
    @return arreglo de sucursales
    **/
    public function getAllSucursales($sm = false){
        $v = array();
        unset($_SESSION['edit-Sucursal']);
		if (!$sm){
			$this->c->q("SELECT su_id, su_em_id, su_nombre FROM sucursales LEFT JOIN empresas ON su_em_id = em_id WHERE em_deleted = '0' AND su_deleted = '0' AND su_id = '".$_SESSION['sucursal']."' ORDER BY su_id DESC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(2), true);
				array_push($v, $row);
			}
		} else {
			$this->c->q("SELECT su_id, su_em_id, em_nombre, su_nombre FROM sucursales LEFT JOIN empresas ON su_em_id = em_id WHERE em_deleted = '0' AND su_deleted = '0' ORDER BY su_id DESC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(2, 3), true);
				array_push($v, $row);
			}
		}
        return $v;
    }
    /**
    Método que crea una sucursal nueva. Tiene relación con la clase Dirección
    
    @bitacora Guardado de una nueva sucursal
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveSucursal(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $dir = new Direccion($this->c);
        $p = $_POST['param'];
        $dir = $dir->createDir(array($p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]));
        $this->createSucursal(array($p[2], $dir, $p[0], $p[1]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la sucursal en la base de datos
    
    @bitacora Creación de una nueva sucursal
    @param arreglo con los datos de la sucursal
    @return identificador de la sucursal agregada en la base de datos
    
    **/
    public function createSucursal($v){
        $this->log($this, __FUNCTION__, 'bitacora', $v[3]);
        $v = $this->u8($v, array(2, 3), false);
        $this->c->q("INSERT INTO sucursales VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$this->d."', '0')");
        return $this->c->last('sucursales');        
    }
    /**
    Método que actualiza la información de la sucursal
    
    @bitacora Actualización de la sucursal
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateSucursales(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Sucursal']))
            return $arr;
        $sucursal = $this->getSucursalById($_SESSION['edit-Sucursal']);
        $dir = new Direccion($this->c);
        $p = $_POST['param'];
        $dir->updateDirById($sucursal[2][0], array($p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]));
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$sucursal[3].' por '.$p[0]);
        $this->c->q("UPDATE sucursales SET su_nombre = '".$p[0]."', su_descr = '".$p[1]."', su_em_id = '".$p[2]."', su_fecha = '".$this->d."' WHERE su_id = '".$sucursal[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Sucursal']);
        return $arr;
    }
    /**
    Método que marca como eliminada una sucursal
    
    @bitacora Eliminación de la sucursal
    @param identificador de la sucursal en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteSucursal($id){
        $this->hasAccess(get_class($this));
        $sucursal = $this->getSucursalById($id);
        $this->log($this, __FUNCTION__, 'bitacora',$sucursal[3]);
        unset($_SESSION['edit-Sucursal']);
        $this->c->q("UPDATE sucursales SET su_deleted = '1' WHERE su_id = '".$sucursal[0]."' LIMIT 1;");
        $dep = new Departamentos();
        $dep->deleteDepartamentoBySucursal($sucursal[0]);
        return array('true');
    }
	/**
    Método que elimina las sucursales asociadas a una empresa
    
    @bitacora Eliminación de sucursales asociadas a una empresa
    @param identificador de la empresa en la base de datos
    @return arreglo con el estado del guardado
    **/
	public function deleteSucursalByEmpresa($id){
        $this->c->q("SELECT su_id FROM sucursales WHERE su_em_id = '".$id."';");
        $dep = new Departamentos();
        while($row = $this->c->fr())
            $dep->deleteDepartamentoBySucursal($row[0]);
        $this->c->q("UPDATE sucursales SET su_deleted = '1' WHERE su_em_id = '".$id."';");
	}
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        if ($this->close)
        	$this->c->cl();
    }
}
?>