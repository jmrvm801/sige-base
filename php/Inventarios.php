<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Familias.php');
/**
Clase Inventarios, contiene los métodos necesarios para el manejo del módulo Inventarios

@author Fernando Carreon
@version 1.0
**/
class Inventarios extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Inventarios
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Inventarios(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Método principal de la clase Inventario
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getMetaPrecio':
                $art = new Articulos();
                $proved = new Proveedores();
                return array($art->getForArticulos(), $proved->getForProveedores());
            break;
            case 'getAllInventarios':
                return $this->getAllInventarios();
            break;
            case 'getInventariosById':
                return $this->getInventariosById($_POST['param']);
            break;
            case 'addManuallyToMovements':
            case 'getAllMovementsFromId':
                $mov = new Movimientos($this->c);
                return $mov->run($method);
            break;
        }
    }
    /**
    Método que obtiene los datos del inventario a través de un id
    
    @bitacora acceso a la información general del inventario
    @param id del inventario
    @return arreglo del inventario
    **/
    public function getInventariosById($id){
        $this->c->q("SELECT in_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, em_nombre, su_nombre, pre_mayoreo, pre_menudeo, in_actual FROM inventarios LEFT JOIN precios ON pre_id = in_pre_id LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON art_uni_id = uni_id LEFT JOIN sucursales ON in_su_id = su_id LEFT JOIN empresas ON em_id = su_em_id WHERE in_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Inventario'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2, 3, 4, 5, 6, 7), true);
        return $data;
    }
    /**
    Método que verifica si existe en inventario un precio para el sucursal y precio
    
    @bitacora Verificación de los datos del inventario
    @param sucursal
    @param precio
    @return número del id en el inventario
    **/
    public function existInventario($sucursal, $precio){
        $this->c->q("SELECT in_id FROM inventarios WHERE in_su_id = '".$sucursal."' AND in_pre_id IN(SELECT pre_id FROM precios WHERE pre_art_id = '".$precio."' AND pre_su_id = '".$sucursal."') LIMIT 1;");
        $d = ($this->c->nr() == 0) ? -1 : $this->c->r(0);
        return $d;
    }
    /**
    Método que verifica si existe en inventario un precio para el sucursal y precio
    
    @bitacora Verificación de los datos del inventario
    @param sucursal
    @param precio
    @return número del id en el inventario
    **/
    public function existInventarioSell($sucursal, $precio){
        $this->c->q("SELECT in_id FROM inventarios WHERE in_su_id = '".$sucursal."' AND in_pre_id = '".$precio."' LIMIT 1;");
        $d = ($this->c->nr() == 0) ? -1 : $this->c->r(0);
        return $d;
    }
    /**
    Método que obtiene toda la información básica de los precios
    
    @bitacora Acceso a la información básica de todos los precios
    @param void
    @return arreglo de los precios
    **/
    public function getAllInventarios(){
        $v = array();
        unset($_SESSION['edit-Inventario']);
		$this->c->q("SELECT in_id, pre_nombre, art_nombre, fa_nombre, in_actual, in_fecha FROM inventarios LEFT JOIN precios ON pre_id = in_pre_id LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id WHERE pre_deleted = '0' AND art_deleted = '0' AND in_su_id = '".$_SESSION['sucursal']."'");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un registro del inventario en la base de datos
    
    @bitacora Creación de un elemento en inventario
    @param id del precio
    @param sucursal de la empresa
    @return boolean
    **/
    public function createInventario($sucursal, $precio){
        $this->c->q("INSERT INTO inventarios VALUES(NULL,'".$sucursal."','".$precio."','0','".$this->d."')");
        return true;
    }
    /**
    Método que obtiene un registro del inventario a través del id
    
    @bitacora Obtención de un inventario en particular
    @param id del inventario
    @return arreglo de inventario
    **/
    public function getInventarioById($id){
        $this->c->q("SELECT * FROM inventarios WHERE in_id = '".$id."' LIMIT 1;");
        return $this->c->fr();
    }
    /**
    Método que obtiene un registro del inventario a través del id de sucursal y del producto
    
    @bitacora Obtención de un inventario en particular
    @param id del inventario
    @return arreglo de inventario
    **/
    public function getInventarioByPrecio($precio){
        $this->c->q("SELECT * FROM inventarios WHERE in_pre_id = '".$precio."' LIMIT 1;");
        return $this->c->fr();
    }
    /**
    Método que obtiene un actualiza el inventario a través del id de sucursal y del producto
    
    @bitacora Actualización de un inventario
    @param id del inventario
    @return arreglo de inventario
    **/
    public function updateInventario($sucursal, $precio){
        $inv = $this->getInventarioByPrecio($precio);
        $this->c->q("UPDATE inventarios SET in_su_id = '".$sucursal."' WHERE in_id = '".$inv[0]."' LIMIT 1;");
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}

/**
Clase DÉBIL Movimientos, contiene los métodos necesarios para el manejo del módulo Inventarios

@author Fernando Carreon
@version 1.0
**/
class Movimientos extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Movimientos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Movimientos($c){
        $this->c = $c;
        $this->d = date('YmdHis');
    }
    /**
    Método principal de la clase Movimientos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'addManuallyToMovements':
                return $this->addManuallyToMovements();
            break;
            case 'getAllMovementsFromId':
                return $this->getAllMovementsFromId($_SESSION['edit-Inventario']);
            break;
        }
    }
    /**
    Método que obtiene todos los movimientos del inventario
    
    @bitacora Obtención de los movimientos de un id
    @param id del movimiento
    **/
    public function getAllMovementsFromId($id){
        $v = array();
        $this->c->q("SELECT mov_id, pe_razon, mov_tipo, mov_unidades, mov_observaciones, mov_fecha FROM movimientos LEFT JOIN usuarios ON us_id = mov_us_id LEFT JOIN personas ON pe_id = us_pe_id WHERE mov_in_id = '".$id."' ORDER BY mov_id DESC LIMIT 100;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 4), true);
            array_push($v, $row);
        }
        $this->c->q("SELECT in_actual FROM inventarios WHERE in_id = '".$id."' LIMIT 1;");
        $actual = $this->c->r(0);
        return array($v, $actual);
    }
    /**
    Método que agrega manualmente los elementos al inventario
    
    @bitacora Movimiento de entrada o salida manual
    @param void
    @return json con datos en función del método ejecutado
    **/
    public function addManuallyToMovements(){
        $this->hasAccess('Inventarios');
        $p = $_POST['param'];
        $p = $this->u8($p, array(2), false);
        $id = $this->createMovimiento($_SESSION['edit-Inventario'], $p[0], $p[1], $p[2]);
        if ($id == -1)
            return array('insuficient');
        $this->log($this, __FUNCTION__, 'bitacora', $id);
        return array('true');
    }
    /**
    Método que inserta el inventario en la base de datos
    
    @bitacora Movimiento de entrada o salida manual
    @param inventario. Id del inventario
    @param tipo. Id del tipo (entrada / salida)
    @param unidades. No de unidades a ingresar
    @param observaciones. Observaciones del movimiento
    @return Id del movimiento insertado
    **/
    public function createMovimiento($id, $tipo, $unidades, $observaciones){ //0 = entrada, 1 = salida
        $this->hasAccess('Inventarios');
        if ($tipo == '1')
            if (!$this->checkForUnitInWarehouse($id, $unidades))
                return -1;
        $this->c->q("INSERT INTO movimientos VALUES(NULL, '".$id."', '".$_SESSION['us_id']."', '".$tipo."', '".$unidades."', '".$observaciones."', '".$this->d."')");
        $this->alterInventario($id, $tipo, $unidades);
        return $this->c->last('movimientos');
    }
    /**
    Método que verifica si la resta de unidades al inventario es suficiente
    
    @bitacora Verificación de disponibilidad de unidades en inventario
    @param id del inventario
    @param unidades a intentar descontar
    @return boolean indicando si se puede o no realizar la operación
    **/
    public function checkForUnitInWarehouse($id, $unidades){
        $this->c->q("SELECT in_actual FROM inventarios WHERE in_id = '".$id."' LIMIT 1;");
        $unidad = $this->c->r(0);
        if (($unidad - $unidades) < 0)
            return false;
        return true;
    }
    /**
    Método que actualiza el No. de productos al inventario del producto.
    
    @bitacora Actualización del inventario para un producto
    @param void
    @return void
    **/
    private function alterInventario($id, $tipo, $unidades){
        $this->c->q("SELECT in_actual FROM inventarios WHERE in_id = '".$id."' LIMIT 1;");
        $unidad = $this->c->r(0);
        $unidad = ($tipo == '0') ? $unidad + $unidades : $unidad - $unidades;
        $this->c->q("UPDATE inventarios SET in_actual = '".$unidad."', in_fecha = '".$this->d."' WHERE in_id = '".$id."' LIMIT 1;");
    }
}
?>