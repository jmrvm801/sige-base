<?php
require_once('Core.php');
/**
Clase Deducciones, contiene los métodos necesarios para el manejo del módulo catalogos

@author Fernando Carreon
@version 1.0
**/
class Deducciones extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Deducciones
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Deducciones($c = ''){
		$this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una unidad a través de un id.
    
    @bitacora Acceso a la información de la unidad
    @param id de la unidad a obtener el resultado
    @return arreglo con los datos de la unidad
    **/
    public function getDeduccionById($id){
        $this->c->q("SELECT * FROM deducciones WHERE ded_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Deducciones'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
    /**
    Método principal de la clase Deducciones
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllDeducciones':
                return $this->getAllDeducciones();
            break;
            case 'saveDeduccion':
                return (!isset($_SESSION['edit-Deducciones'])) ? $this->saveDeduccion() : $this->updateDeducciones();
            break;
            case 'getDeduccionById':
                return $this->getDeduccionById($_POST['param']);
            break;
            case 'delDeducciones':
                return $this->deleteDeducciones($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las deducciones
    
    @bitacora Acceso a la información básica de todos las deducciones
    @param void
    @return arreglo de las deducciones
    **/
    public function getAllDeducciones(){
        $v = array();
        unset($_SESSION['edit-Deducciones']);
        $this->c->q("SELECT ded_id, ded_nombre, ded_monto FROM deducciones WHERE ded_deleted = '0' ORDER BY ded_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea una unidad nueva.
    
    @bitacora Guardado de una nueva unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveDeduccion(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createDeducciones(array($p[0], $p[1]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la unidad en la base de datos
    
    @bitacora Creación de una nueva unidad
    @param arreglo con los datos de la unidad
    @return identificador de la unidad agregada en la base de datos
    
    **/
    public function createDeducciones($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO deducciones VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0')");
        return $this->c->last('deducciones');        
    }
    /**
    Método que actualiza la información de una unidad
    
    @bitacora Actualización de una unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateDeducciones(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Deducciones']))
            return $arr;
        $unidad = $this->getDeduccionById($_SESSION['edit-Deducciones']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$unidad[1].' por '.$p[0]);
        $this->c->q("UPDATE deducciones SET ded_nombre = '".$p[0]."', ded_monto = '".$p[1]."', ded_fecha = '".$this->d."' WHERE ded_id = '".$unidad[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Deducciones']);
        return $arr;
    }
    /**
    Método que marca como eliminada una unidad
    
    @bitacora Eliminación de una unidad
    @param identificador de una unidad en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteDeducciones($id){
        $this->hasAccess(get_class($this));
        $unidad = $this->getDeduccionById($id);
        unset($_SESSION['edit-Deducciones']);
        $this->log($this, __FUNCTION__, 'bitacora', $unidad[1]);
        $this->c->q("UPDATE deducciones SET ded_deleted = '1' WHERE ded_id = '".$unidad[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>