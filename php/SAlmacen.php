<?php
require_once('Core.php');
require_once('Inventarios.php');
require_once('Articulos.php');
require_once('Proveedores.php');
require_once('Core.php');
require_once('Comprobacion.php');
require_once('Precios.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo traspasos

@author Fernando Carreon
@version 1.0
**/
class SAlmacen extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Gastos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function SAlmacen(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un gasto a través de un id.
    
    @bitacora Acceso a la información de un gasto
    @param id del gasto a obtener el resultado
    @return arreglo con los datos del gasto
    **/
    public function getSAlmacenById($id){
        $this->c->q("SELECT * FROM salmacen WHERE sal_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-salmacen'] = $id;
        $data = $this->c->fr();
        $this->c->q("SELECT art_fa_id FROM articulos WHERE art_id = (SELECT pre_art_id FROM precios WHERE pre_id = '".$data[2]."');");
        $data[12] = $this->c->r(0);
        $data = $this->u8($data, array(6), true);
        return $data;
    }
    /**
    Método principal de la clase Traspasos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllSAlmacen':
                return $this->getAllSAlmacen();
            break;
            case 'saveSAlmacen':
                return $this->saveSAlmacen();
            break;
            case 'getSAlmacenById':
                return $this->getSAlmacenById($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los traspasos
    
    @bitacora Acceso a la información básica de todos los traspasos
    @param void
    @return arreglo de los traspasos
    **/
    public function getAllSAlmacen(){
        $v = array();
		$this->c->q("SELECT sal_id, prov.pe_razon, art_nombre, sal_unidades, sal_cantidad, sal_nota, res.pe_razon, sal_fecha, sal_lotebruto FROM salmacen LEFT JOIN precios ON pre_id = sal_pre_id LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN usuarios ON us_id = sal_us_id LEFT JOIN personas AS res ON res.pe_id = us_pe_id LEFT JOIN proveedores ON pro_id = sal_pro_id LEFT JOIN personas AS prov ON prov.pe_id = pro_pe_id WHERE sal_su_origen = '".$_SESSION['sucursal']."' ORDER BY sal_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 5, 6, 8), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un traspaso.
    
    @bitacora Guardado de un nuevo traspaso
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveSAlmacen(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $pre = new Precios();
        $id = $pre->getArticulosBySucursalAndId(array($p[0], $p[1]));
        $inv = new Inventarios();
        $cm = $inv->getInventarioByPrecio($p[1]);
        $mov = new Movimientos($this->c);
        $idm = $this->createSAlmacen(array($p[1], $p[0], $p[3], $p[2], $p[4], $p[5], $p[6], $p[7], $p[8]));
        $mov->createMovimiento($cm[0], 1, $p[2], '[SALIDA] Entrada a inventario con id '.$idm);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un traspaso en la base de datos
    
    @bitacora Creación de un nuevo traspaso
    @param arreglo con los datos del traspaso
    @return identificador del traspaso agregado en la base de datos
    
    **/
    public function createSAlmacen($v){
        $v = $this->u8($v, array(4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO salmacen VALUES(NULL, '".$_SESSION['us_id']."', '".$v[0]."', '".$v[1]."', '".$v[3]."', '".$v[2]."', '".$v[4]."', '".$this->d."', '".$v[5]."', '".$v[6]."', '".$v[7]."', '".$v[8]."')");
        return $this->c->last('salmacen');        
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>