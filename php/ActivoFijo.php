<?php
require_once('Core.php');
/**
* Clase Sueldos, contiene los métodos necesarios para el manejo del módulo sueldos
*
* @author Fernando Carreon
* @version 1.0
*/

class ActivoFijo extends Core{
    /**
     * @var db
     */
    public $c;
    /**
     * @var false|string
     */
    public $d;
    /**
    * Constructor de la clase ActivoFijo
    *
    * @bitacora Constructor ejecutado
    * @param void
    * @return void
    */
    public function __construct(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }

    /**
     * Obtiene los datos de un activo fijo a través de un id.
     *
     * @bitacora Acceso a la información de activos fijos
     * @param $id number del activo fijo a obtener el resultado
     * @return array|bool|mixed|null con los datos del activo fijo
     */
    public function getActivosById($id){
        $this->c->q("SELECT * FROM activosfijos WHERE acf_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-ActivosFijos'] = $id;
        $data = $this->c->fr();
        $data[4] = $this->numberDateToString($data[4]);
        $data = $this->u8($data, array(1, 2, 6), true);
        return $data;
    }

    /**
     * Método principal de la clase ActivoFIjo
     *
     * @bitacora Acceso al menú de opciones
     * @param $method string method Opción a ejecutar
     * @return array|bool|mixed|null con datos en función del método ejecutado
     */
    public function run($method){
        switch($method){
            case 'getAllActivos':
                return $this->getAllActivos();
            case 'saveActivo':
                return (!isset($_SESSION['edit-ActivosFijos'])) ? $this->saveActivo() : $this->updateActivo();
            case 'getActivosById':
                return $this->getActivosById($_POST['param']);
            case 'delActivos':
                return $this->deleteActivos($_POST['param']);
            default:
                return array('null');
        }
    }
    /**
    * Método que obtiene toda la información básica de los activos fijos
    *
    * @bitacora Acceso a la información básica de todos los activos fijos
    * @param void
    * @return array de los activos fijos
    */
    public function getAllActivos(){
        $v = array();
        unset($_SESSION['edit-ActivosFijos']);
		$this->c->q("SELECT acf_id, acf_nombre, acf_monto, acf_fechacompra FROM activosfijos WHERE acf_su_id = '".$_SESSION['sucursal']."';");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            $row[3] = $this->numberDateToString($row[3]);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    * Método que crea un activo fijo.
    *
    * @bitacora Guardado de un nuevo activo fijo
    * @param void
    * @return array con el estado del guardado
    */
    public function saveActivo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createActivo(array($p[0], $p[1], $p[2], $p[3], $p[4], $p[5]));
        $arr[0] = 'true';
        return $arr;
    }

    /**
     * Método que inserta los datos de un activo fijo en la base de datos
     *
     * @bitacora Creación de un nuevo activo fijo
     * @param $v array arreglo con los datos del activo fijo
     * @return number identificador del activo fijo agregado en la base de datos
     */
    public function createActivo($v){
        $v = $this->u8($v, array(0, 1, 5), false);
        $v[3] = $this->stringDateToNumber($v[3]);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO activosfijos VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$this->d."')");
        return $this->c->last('activosfijos');        
    }
    /**
    * Método que actualiza la información de un activo fijo
    *
    * @bitacora Actualización de un activo fijo
    * @param void
    * @return array con el estado del guardado
    */
    public function updateActivo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-ActivosFijos']))
            return $arr;
        $activo = $this->getActivosById($_SESSION['edit-ActivosFijos']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1, 5), false);
        $p[3] = $this->stringDateToNumber($p[3]);
        $this->log($this, __FUNCTION__, 'bitacora',$activo[1].' por '.$p[0]);
        $this->c->q("UPDATE activosfijos SET acf_nombre = '".$p[0]."', acf_descr = '".$p[1]."', acf_monto = '".$p[2]."', acf_fechacompra = '".$p[3]."', acf_su_id = '".$p[4]."', acf_noserie = '".$p[5]."', acf_fecha = '".$this->d."' WHERE acf_id = '".$activo[0]."' LIMIT 1;");
        $arr[0] = 'true';
        //unset($_SESSION['edit-ActivosFijos']);
        return $arr;
    }

    /**
     * Método que marca como eliminado un activo fijo
     *
     * @bitacora Eliminación de un activo fijo
     * @param $id number identificador de un activo fijo en la base de datos
     * @return array con el estado del guardado
     */
    public function deleteActivos($id){
        $this->hasAccess(get_class($this));
        $activo = $this->getActivosById($id);
        unset($_SESSION['edit-ActivosFijos']);
        $this->log($this, __FUNCTION__, 'bitacora', $activo[1]);
        $this->c->q("DELETE FROM activosfijos WHERE acf_id = '".$activo[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}