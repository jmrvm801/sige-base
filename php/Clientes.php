<?php
require_once('Core.php');
require_once('Direccion.php');
require_once('Personas.php');
/**
Clase Clientes, contiene los métodos necesarios para el manejo del módulo clientes

@author Fernando Carreon
@version 1.0
**/
class Clientes extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Clientes
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Clientes($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un cliente a través de un id.
    
    @bitacora Acceso a la información de un cliente
    @param id del cliente a obtener el resultado
    @return arreglo con los datos del cliente
    **/
    public function getClientesById($id){
        $this->c->q("SELECT * FROM clientes WHERE cli_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Clientes'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(2), true);
        $per = new Personas($this->c);
        $data[1] = $per->getPersonaById($data[1]);
        $dir = new Direccion($this->c);
        $data[1][5] = $dir->getDirById($data[1][5]);
        return $data;
    }
    /**
    Método que obtiene la información de los clientes en un select
    **/
    public function getForClientes(){
        $this->c->q("SELECT cli_id, pe_razon FROM clientes LEFT JOIN personas ON cli_pe_id = pe_id WHERE cli_deleted = '0' ORDER BY pe_razon ASC");
        $arr = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($arr, $row);
        }
        return $arr;
    }
    /**
    Método principal de la clase Clientes
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllClientes':
                return $this->getAllClientes();
            break;
            case 'saveCliente':
                return (!isset($_SESSION['edit-Clientes'])) ? $this->saveCliente() : $this->updateCliente();
            break;
            case 'getClientesById':
                return $this->getClientesById($_POST['param']);
            break;
            case 'delClientes':
                return $this->delClientes($_POST['param']);
            break;
            case 'getForClientes':
                return $this->getForClientes();
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los clientes
    
    @bitacora Acceso a la información básica de todos los clientes
    @param void
    @return arreglo de los clientes
    **/
    public function getAllClientes(){
        $v = array();
        unset($_SESSION['edit-Clientes']);
        $this->c->q("SELECT cli_id, pe_razon, pe_correo, di_telefono, di_movil FROM clientes LEFT JOIN personas ON pe_id = cli_pe_id LEFT JOIN direcciones ON pe_di_id = di_id WHERE cli_su_id = '".$_SESSION['sucursal']."' ORDER BY pe_razon ASC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un cliente.
    
    @bitacora Guardado de un nuevo cliente
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveCliente(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $p[7] = $this->StringDateToNumber($p[7]);
        $dir = new Direccion($this->c);
        $iddir = $dir->createDir(array($p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]));
        $per = new Personas($this->c);
        $idpersona = $per->createPersona(array($p[1], $p[2], $p[3], $p[4], $iddir, $p[6], $p[5], $p[7], $p[8], $p[9]));
        $this->createCliente(array($idpersona, $p[0], $p[17]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un préstamo en la base de datos
    
    @bitacora Creación de un nuevo préstamo
    @param arreglo con los datos del préstamo
    @return identificador del préstamo agregado en la base de datos
    
    **/
    public function createCliente($v){
        $v = $this->u8($v, array(1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[1]);
        $this->c->q("INSERT INTO clientes VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0','".$v[2]."')");
        return $this->c->last('clientes');        
    }
    /**
    Método que actualiza la información de un cliente
    
    @bitacora Actualización de un cliente
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateCliente(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Clientes']))
            return $arr;
        $cliente = $this->getClientesById($_SESSION['edit-Clientes']);
        $p = $_POST['param'];
        $dir = new Direccion($this->c);
        $dir->updateDirById($cliente[1][5][0], array($p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]));
        $per = new Personas($this->c);
        $per->updatePersonaById($cliente[1][0], array($p[1], $p[2], $p[3], $p[4], $cliente[1][5][0], $p[6], $p[5], $p[7], $p[8], $p[9]));
        $p = $this->u8($p, array(0), false);
        $this->log($this, __FUNCTION__, 'bitacora', $cliente[1][1].' por '.$p[1]);
        $this->c->q("UPDATE clientes SET cli_observaciones = '".$p[0]."', cli_fecha = '".$this->d."', cli_su_id = '".$p[17]."' WHERE cli_id = '".$cliente[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Clientes']);
        return $arr;
    }
    /**
    Método que marca como eliminada un cliente
    
    @bitacora Eliminación de un cliente
    @param identificador de un cliente en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delClientes($id){
        $this->hasAccess(get_class($this));
        $cliente = $this->getClientesById($id);
        unset($_SESSION['edit-Clientes']);
        $this->log($this, __FUNCTION__, 'bitacora', $cliente[0]);
        $this->c->q("UPDATE clientes SET cli_deleted = '1' WHERE cli_id = '".$cliente[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>