<?php
require_once('Core.php');
require_once('Trabajadores.php');
require_once('Comprobacion.php');
/**
Clase Asistencia, contiene los métodos necesarios para el manejo del módulo nóminas

@author Fernando Carreon
@version 1.0
**/
class Nomina extends Core{
    public $c;
    public $d;
    public $data;
    public function Nomina(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    public function run($method){
        switch($method){
            case 'previsualize':
                return $this->previsualize($_POST['param']);
            break;
            case 'getAllNominas':
                return $this->getAllNominas();
            break;
            case 'formalizar':
                return $this->formalizar();
            break;
            case 'getDetalle':
                return $this->getDetalle($_POST['param']);
            break;
			case 'getBefore':
                return $this->getBefore($_POST['param']);
            break;
			case 'calcularInit':
				return $this->calcularInit($_POST['param']);
			break;
			case 'finishInit':
				return $this->finishInit($_POST['param']);
			break;
			case 'getDetails':
				return $this->getDetails($_POST['param']);
			break;
			case 'getReporteNominaByPeriodo':
				return $this->getReporteNominaByPeriodo($_POST['param']);
			break;
			case 'preFinishInit':
				return $this->preFinishInit($_POST['param']);
			break;
			case 'getSimpleDetailReport':
				return $this->getSimpleDetailReport($_POST['param']);
			break;
			case 'getComplexDetailReport':
				return $this->getComplexDetailReport($_POST['param']);
			break;
        }
    }
	public function getSimpleDetailReport($p){
		$p = explode('|',$p);
		$array = array(array($p[0], $p[1], $_SESSION['nomina']), array());
		$p[0] = date('Ymd',strtotime($p[0]));
		$p[1] = date('Ymd',strtotime($p[1]));
		$this->c->q("SELECT pa_id, pe_razon, pa_subtotal, pa_prestamos, pa_montopagado FROM pagos LEFT JOIN usuarios ON pa_us_id = us_id LEFT JOIN personas ON pe_id = us_pe_id LEFT JOIN departamentos ON dep_id = us_dep_id WHERE pa_tipo = '".$_SESSION['nomina']."' AND pa_inicio = '".$p[0]."' AND pa_fin = '".$p[1]."' AND dep_su_id = '".$_SESSION['sucursal']."' ORDER BY pe_razon;");
		
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(1), true);
			array_push($array[1], $row);
		}
		return $array;
	}
	public function getComplexDetailReport($p){
		$p = explode('|',$p);
		$array = array(array($p[0], $p[1], $_SESSION['nomina']), array());
		$p[0] = date('Ymd',strtotime($p[0]));
		$p[1] = date('Ymd',strtotime($p[1]));
		switch($_SESSION['nomina']){
			case '0':
				$this->c->q("SELECT pa_id, pe_razon, pa_estajos, pa_sueldo, pa_subtotal, pa_multa, pa_montopercepciones, pa_montodeducciones, pa_prestamos, pa_montopagado FROM pagos LEFT JOIN usuarios ON pa_us_id = us_id LEFT JOIN personas ON pe_id = us_pe_id LEFT JOIN departamentos ON dep_id = us_dep_id WHERE pa_tipo = '".$_SESSION['nomina']."' AND pa_inicio = '".$p[0]."' AND pa_fin = '".$p[1]."' AND dep_su_id = '".$_SESSION['sucursal']."' ORDER BY pe_razon;");
			break;
			default:
				$this->c->q("SELECT pa_id, pe_razon, pa_asistencias, (pa_laboral - pa_asistencias), pa_subtotal, pa_multa, pa_montopercepciones, pa_montodeducciones, pa_prestamos, pa_montopagado FROM pagos LEFT JOIN usuarios ON pa_us_id = us_id LEFT JOIN personas ON pe_id = us_pe_id LEFT JOIN departamentos ON dep_id = us_dep_id WHERE pa_tipo = '".$_SESSION['nomina']."' AND pa_inicio = '".$p[0]."' AND pa_fin = '".$p[1]."' AND dep_su_id = '".$_SESSION['sucursal']."' ORDER BY pe_razon;");
			break;
		}
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(1), true);
			array_push($array[1], $row);
		}
		return $array;
	}
	public function getReporteNominaByPeriodo($p){
		$this->c->q("SELECT pa_inicio, pa_fin, pa_tipo, pa_prestamos, pa_montodeducciones, pa_montopercepciones, pa_montopagado FROM pagos WHERE pa_us_id = '".$p[0]."' ORDER BY pa_id DESC LIMIT ".$p[1]);
		$array = array();
		while($row = $this->c->fr())
			array_push($array, $row);
		return $array;
	}
	
	public function preFinishInit($p){
		$p = json_decode($p);
		for($i = 0; $i < count($p[0]); $i++)
			$p[0][$i][0] = base64_encode($p[0][$i][0]);
		for($i = 0; $i < count($p[1]); $i++)
			$p[1][$i][0] = base64_encode($p[1][$i][0]);
		$p = json_encode($p);
		$data = $_SESSION['nominaprecalcular'];
		$data[3] = explode(' al ', $data[3]);
		$finicio = $this->stringDateToNumber($data[3][0]);
		$ffin = $this->stringDateToNumber($data[3][1]);
		$this->c->q("SELECT pres_id FROM presave WHERE pres_inicio = '".$finicio."' AND pres_fin = '".$ffin."' AND pres_us_id = '".$data[0]."' LIMIT 1;");
		if ($this->c->nr() > 0){
			$id = $this->c->r(0);
			$this->c->q("DELETE FROM presave WHERE pres_id = '".$id."' LIMIT 1;");
		}
		$this->c->q("INSERT INTO presave VALUES(NULL, '".$finicio."', '".$ffin."', '".$data[0]."', '".$p."')");
		return array('presaved');
	}
	/**
		Método que formaliza la nómina de un empleado en la base de datos
		@bitacora generación de la nómina para el empleado
		@param datos de percepciones y deducciones (en caso de haber)
		@return array
	*/
	public function finishInit($p){
		$data = $_SESSION['nominaprecalcular'];
		$p = json_decode($p);
		switch($data[2][0]){
			case '0':
				$data[10] = $p[0];
				$data[11] = $p[1];
				$pos = array(10, 11);
			break;
			case '1':
			case '2':
				$data[13] = $p[0];
				$data[14] = $p[1];
				$pos = array(13, 14);
			break;
		}
		$montodeducciones = 0;
		$montopercepciones = 0;
		for($i = 0; $i < count($data[$pos[0]]); $i++){
			$montodeducciones += $data[$pos[0]][$i][1];
			$data[$pos[0]][$i][0] = base64_encode($data[$pos[0]][$i][0]);
		}
		for($i = 0; $i < count($data[$pos[1]]); $i++){
			$montopercepciones += $data[$pos[1]][$i][1];
			$data[$pos[1]][$i][0] = base64_encode($data[$pos[1]][$i][0]);
		}
		$prestamos = 0;
		for($i = 0; $i < count($data[9]); $i++){
			$prestamos += $data[9][$i][2];
		}
		$data[3] = explode(' al ', $data[3]);
		$data[3][0] = $this->stringDateToNumber($data[3][0]);
		$data[3][1] = $this->stringDateToNumber($data[3][1]);
		switch($data[2][0]){
			case '0':
				$factorMulta = 0;
				$multa = 0;
				$estajos = 0;
				$subtotal = 0;
				for($i = 0; $i < count($data[8]); $i++){
					$estajos += $data[8][$i][1];
					$subtotal += $data[8][$i][2];
					$this->c->q("UPDATE estajos SET est_eliminado = '2' WHERE est_id = '".$data[8][$i][4]."' LIMIT 1;");
				}
				$montopagado = $subtotal - $prestamos - $montodeducciones + $montopercepciones;
			break;
			case '1':
			case '2':
				$factorMulta = ($data[2][0] == '1') ? 6 : 13;
				$multa = $data[10];
				$estajos = 0; //se queda en cero
				$subtotal = $data[6] - $multa;
				$montopagado = $subtotal - $prestamos -$montodeducciones + $montopercepciones;
			break;
		}
		$this->abonarPrestamo($data[9]);
		$this->c->q("SELECT pres_id FROM presave WHERE pres_inicio = '".$data[3][0]."' AND pres_fin = '".$data[3][1]."' AND pres_us_id = '".$data[0]."' LIMIT 1;");
		if ($this->c->nr() > 0){
			$id = $this->c->r(0);
			$this->c->q("DELETE FROM presave WHERE pres_id = '".$id."' LIMIT 1;");
		}
		$this->c->q("INSERT INTO pagos VALUES(NULL, '".$data[0]."', '".$data[3][0]."', '".$data[3][1]."', '".$data[2][0]."', '".$data[4]."', '".$factorMulta."', '".$data[5]."', '".$data[6]."', '".$multa."', '".$estajos."', '".$subtotal."', '".$prestamos."', '".json_encode($data[$pos[0]])."', '".$montodeducciones."', '".json_encode($data[$pos[1]])."', '".$montopercepciones."', '".$montopagado."', '".$_SESSION['us_id']."', '".json_encode($data[9])."')");
		return array('true');
	}
	/**
		Método que realiza abonos a los préstamos de un empleado
		@bitacora acceso al registro de abonos
		@param array con los datos de cada préstamo
		@return array
	*/
	public function abonarPrestamo($p){
		$comp = new Comprobacion($this->c, 2);
		for($i = 0; $i < count($p); $i++){
			$_SESSION['edit-Referencia'] = $p[$i][4];
			$this->c->q("SELECT pre_parCubierta, pre_parcialidades, pre_cubierto, pre_parcialidad, pre_estado FROM prestamos WHERE pre_ref = '".$p[$i][4]."' LIMIT 1;");
			$prestamo = $this->c->fr();
			$prestamo[0] += 1;
			$this->c->q("UPDATE prestamos SET pre_parCubierta = '".$prestamo[0]."' WHERE pre_ref = '".$p[$i][4]."' LIMIT 1;");
			$comp->comprobarPrestamo(array($prestamo[3], '7','PAGO POR NOMINA', $_SESSION['us_id'], 'PARCIALIDAD No. '.$prestamo[0].' REALIZADA'));
		}
	}
	/**
		Método que obtiene la información de la situación económica de un empleado para su evaluación
		@bitacora acceso al registro de nómina del empleado
		@param int con el No. de empleado
		@return array
	*/
	public function calcularInit($p){
		$array = array();
		//OBLIGATORIO EL ORDEN id, nombre, [N0, 'Tipo de Nómina'], días laborables, días asistidos, sueldo/precio, hrsextras
		switch($_SESSION['nomina']){
			case '0':
				//id, nombre, tiponomina, fecha de la nomina, dias laborables, dias asistidos, precio estajo, hrsextras, (arreglo de estajos realizados), (arreglo de préstamos)
				$this->c->q("SELECT us_id, us_code, pe_razon, sue_monto FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id LEFT JOIN departamentos ON us_dep_id = dep_id LEFT JOIN sueldos ON us_sue_id = sue_id WHERE dep_su_id = '".$_SESSION['sucursal']."' AND us_jornada = '0' AND us_deleted = '0' AND us_id = '".$p."' LIMIT 1;");
				$row = $this->u8($this->c->fr(), array(2), true);
				array_push($array, $row[0]);
				array_push($array, $row[2]);
				array_push($array, array('0', 'Estajo'));
				$fecha = $_SESSION['fechaNomina'];
				array_push($array, $this->numberDateToString($fecha[0]).' al '.$this->numberDateToString($fecha[1]));
				array_push($array, 6);
				$this->c->q("SELECT SUM(as_extras) FROM asistencias WHERE as_code = '".$row[1]."' AND (as_fecha >= '".$fecha[0]."' AND as_fecha <= '".$fecha[1]."');");
				array_push($array, $this->c->nr());
				array_push($array, $row[3]);
				array_push($array, $this->c->r(0));
				if ($array[7] == null){
					$array[7] = '0';
				}
				$this->c->q("SELECT est_fecha, est_unidades, est_unidades, est_observaciones, est_id FROM estajos WHERE est_empleado = '".$row[0]."' AND (est_fecha >= '".$fecha[0]."' AND est_fecha <= '".$fecha[1]."') AND est_eliminado = '0';");
				$estajo = array();
				while($estajos = $this->c->fr()){
					$estajos = $this->u8($estajos, array(3), true);
					$estajos[2] = $row[3] * $estajos[1];
					array_push($estajo, $estajos);
				}
				array_push($array, $estajo);
				$this->c->q("SELECT pre_fecha, (pre_monto - pre_cubierto) , pre_parcialidad, (pre_monto - pre_cubierto) - pre_parcialidad, pre_ref FROM prestamos WHERE pre_us_responsable = '".$row[0]."' AND pre_estado = '0';");
				$prestamo = array();
				while($prestamos = $this->c->fr())
					array_push($prestamo, $prestamos);
				array_push($array, $prestamo);
				$this->c->q("SELECT pres_json FROM presave WHERE pres_inicio = '".$fecha[0]."' AND pres_fin = '".$fecha[1]."' AND pres_us_id = '".$array[0]."' LIMIT 1;");
				if ($this->c->nr() > 0){
					$json = $this->c->r(0);
					$json = json_decode($json);
					for($i = 0; $i < count($json[0]); $i++)
						$json[0][$i][0] = base64_encode(utf8_decode(base64_decode($json[0][$i][0])));
					for($i = 0; $i < count($json[1]); $i++)
						$json[1][$i][0] = base64_encode(utf8_decode(base64_decode($json[1][$i][0])));
					array_push($array, $json);
				} else
					array_push($array, array(array(), array()));
				$_SESSION['nominaprecalcular'] = $array;
			break;
			case '1':
			case '2':
				//id, nombre, tiponomina, fecha de la nomina, dias laborables, dias asistidos, precio estajo, hrsextras, (arreglo de horarios realizados), (arreglo de préstamos)
				$this->c->q("SELECT us_id, us_code, pe_razon, sue_monto FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id LEFT JOIN departamentos ON us_dep_id = dep_id LEFT JOIN sueldos ON us_sue_id = sue_id WHERE dep_su_id = '".$_SESSION['sucursal']."' AND us_jornada = '".$_SESSION['nomina']."' AND us_deleted = '0' AND us_id = '".$p."' LIMIT 1;");
				$fecha = $_SESSION['fechaNomina'];
				$laboral = 0;
				$dinicio = date('j', strtotime($fecha[0]));
				$finicio = date('Y-m', strtotime($fecha[0]));
				$dfin = date('j', strtotime($fecha[1]));
				if ($_SESSION['nomina'] == '1'){
					$laboral = 6;
				} else {
					for($i = 0; $dinicio <= $dfin; $dinicio++, $i++)
						if (date('w', strtotime($finicio.'-'.$dinicio)) != 0)
							$laboral++;
				}
				if ($_SESSION['nomina'] == '1'){
					$factorMulta = 6;
				} else {
					$factorMulta = 13;
				}
				$row = $this->u8($this->c->fr(), array(2), true);
				array_push($array, $row[0]);
				array_push($array, $row[2]);
				array_push($array, array($_SESSION['nomina'], 'Nómina '.($_SESSION['nomina'] == '1' ? 'Semanal' : 'Quincenal')));
				$ffd = $fecha;
				array_push($array, $this->numberDateToString($fecha[0]).' al '.$this->numberDateToString($fecha[1]));
				array_push($array, $laboral);
				$this->c->q("SELECT DISTINCT as_fecha, as_entrada, as_salida, as_dayWeek, as_extras FROM asistencias WHERE as_code = '".$row[1]."' AND (as_fecha >= '".$fecha[0]."' AND as_fecha <= '".$fecha[1]."');");
				$arrayFechas = array();
				$fecha = array();
				$asistencias = 0;
				$horaextra = 0;
				while($rom = $this->c->fr()){
					$horaextra += $rom[4];
					$rom[1] = $this->numberTimeToString($rom[1]);
					$rom[2] = $this->numberTimeToString($rom[2]);
					array_push($arrayFechas, $rom);
					if (array_search($rom[0], $fecha) !== false)
						continue;
					if ($rom[3] != '7')
						$asistencias++;
					array_push($fecha, $rom[0]);
				}
				array_push($array, $asistencias); //asistencias
				array_push($array, $row[3]); //sueldo
				array_push($array, $horaextra); //horas extras
				array_push($array, $arrayFechas); //horas extras
				$this->c->q("SELECT pre_fecha, (pre_monto - pre_cubierto) , pre_parcialidad, (pre_monto - pre_cubierto) - pre_parcialidad, pre_ref FROM prestamos WHERE pre_us_responsable = '".$row[0]."' AND pre_estado = '0';");
				$prestamo = array();
				while($prestamos = $this->c->fr())
					array_push($prestamo, $prestamos);
				array_push($array, $prestamo);
				$multa = 0;
				if ($asistencias <= $factorMulta){
					if ($asistencias < $laboral){
						$multa = $array[6] - (($array[6] / $factorMulta) * $asistencias);
					}
				}
				$multa = round($multa,2);
				array_push($array, $multa); //multa
				array_push($array, $factorMulta - $asistencias); //multa
				array_push($array, $factorMulta); //multa
				$this->c->q("SELECT pres_json FROM presave WHERE pres_inicio = '".$ffd[0]."' AND pres_fin = '".$ffd[1]."' AND pres_us_id = '".$array[0]."' LIMIT 1;");
				if ($this->c->nr() > 0){
					$json = $this->c->r(0);
					$json = json_decode($json);
					for($i = 0; $i < count($json[0]); $i++)
						$json[0][$i][0] = base64_encode(utf8_decode(base64_decode($json[0][$i][0])));
					for($i = 0; $i < count($json[1]); $i++)
						$json[1][$i][0] = base64_encode(utf8_decode(base64_decode($json[1][$i][0])));
					array_push($array, $json);
				} else
					array_push($array, array(array(), array()));
				$_SESSION['nominaprecalcular'] = $array;
			break;
		}
		return $array;
	}
	/**
		Método que obtiene fechas hacia el pasado en función del tipo de nómica
		
		@bitacora Obtención de fechas anteriores
		@param $p arreglo con el tipo de fechas a obtener [periodo hacia atrás en el pasado], [No de periodos hacia atrás]
	**/
	public function getBefore($p){
		if (!isset($p[1])){
			$p = array($p, 5);
		}
		$_SESSION['nomina'] = $p[0];
		$actual = date('YmdHis');
		$periodos = array();
		switch($p[0]){
			case '0':
			case '1':
				$titlem = ($p[0] == '0') ? 'Estajo' : 'Semana';
				$dactual = date('w');
				if ($dactual == 0)
					$dactual = 7;
				$prestar = $dactual - 1;
				for($i = 0; $i < $p[1]; $i++){
					$inicioSemana = date('d-m-Y', strtotime($actual.' - '.$prestar.' day'));
					$finSemana = date('d-m-Y', strtotime($inicioSemana.' + 5 day'));
					array_push($periodos, array($inicioSemana.'|'.$finSemana, $titlem.' del '.$inicioSemana.' al '.$finSemana));
					$prestar += 7;
				}
			break;
			case '2':
				$dmes = date('j');
				if ($dmes <= 15)
					$dmes = 15;
				else
					$dmes = date('t');
				$dactual = date($dmes.'-m-Y');
				for($i = 0; $i < $p[1]; $i++){
					if ($dmes == 15){
						$inicioQuincena = date('d-m-Y', strtotime($dactual.' - 14 day'));
						array_push($periodos, array($inicioQuincena.'|'.$dactual, 'Quincena del '.$inicioQuincena.' al '.$dactual));
						$dactual = date('d-m-Y', strtotime($dactual.' - 15 day'));
						$dmes = date('t', strtotime($dactual));
					} else {
						$dtrim = $dmes - 15;
						$inicioQuincena = date('d-m-Y', strtotime($dactual.' - '.$dtrim.' day'));
						array_push($periodos, array($inicioQuincena.'|'.$dactual, 'Quincena del '.$inicioQuincena.' al '.$dactual));
						$dactual = $inicioQuincena;
						$dmes = 15;
					}
				}
			break;
		}
		return $periodos;
	}
    /**
		Método que obtiene los detalles de la tabla pagos
		@bitacora acceso a la consulta de nómina del empleado
		@param int con el No. de empleado
		@return array
	*/
    public function getDetails($p){
		$f = $_SESSION['fechaNomina'];
		$this->c->q("SELECT * FROM pagos WHERE pa_us_id = '".$p."' AND pa_tipo = '".$_SESSION['nomina']."' AND pa_inicio = '".$f[0]."' AND pa_fin = '".$f[1]."' LIMIT 1;");
		if ($this->c->nr() == 0)
			return array('notfound');
        $data = $this->c->fr();
		$this->c->q("SELECT us_id, pe_razon, us_code FROM usuarios LEFT JOIN personas ON pe_id = us_pe_id WHERE us_id = '".$p."' LIMIT 1;");
		$data[1] = $this->c->fr();
		$data[1] = $this->u8($data[1], array(1), true);
		$data[13] = json_decode($data[13]);
		for($i = 0; $i < count($data[13]); $i++)
			$data[13][$i][0] = base64_decode($data[13][$i][0]);
		$data[15] = json_decode($data[15]);
		for($i = 0; $i < count($data[15]); $i++)
			$data[15][$i][0] = base64_decode($data[15][$i][0]);
		$data[19] = json_decode($data[19]);
		switch($_SESSION['nomina']){
			case '0':
				$this->c->q("SELECT est_fecha, est_unidades, est_observaciones, est_unidades FROM estajos WHERE est_empleado = '".$p."' AND (est_fecha >= '".$f[0]."' AND est_fecha <= '".$f[1]."');");
				$estajo = array();
				while($estajos = $this->c->fr()){
					$estajos = $this->u8($estajos, array(2), true);
					$estajos[3] = $data[8] * $estajos[1];
					array_push($estajo, $estajos);
				}
				array_push($data, $estajo);
			break;
			case '1':
			case '2':
				$this->c->q("SELECT DISTINCT as_fecha, as_entrada, as_salida, as_dayWeek, as_extras FROM asistencias WHERE as_code = '".$data[1][2]."' AND (as_fecha >= '".$f[0]."' AND as_fecha <= '".$f[1]."');");
				$arrayFechas = array();
				$fecha = array();
				$asistencias = 0;
				$horaextra = 0;
				while($rom = $this->c->fr()){
					$horaextra += $rom[4];
					$rom[1] = $this->numberTimeToString($rom[1]);
					$rom[2] = $this->numberTimeToString($rom[2]);
					array_push($arrayFechas, array($rom[0], $rom[1], $rom[2], $rom[4]));
					if (array_search($rom[0], $fecha) !== false)
						continue;
					if ($rom[3] != '7')
						$asistencias++;
					array_push($fecha, $rom[0]);
				}
				array_push($data, $arrayFechas);
			break;
		}
		return $data;
    }
    /**
    Método que obtiene la información de las nóminas
    
    @bitacora Acceso a la información básica de las nóminas
    @param void
    @return arreglo con la información básica de los nóminas generadas
    **/
    public function getAllNominas(){
        
    }
	/**
    Método que registra y calcula los minutos que registra un empleado
    
    @bitactora registro de acceso en el reloj
    @param code de empleado
    @return estatus del registro así como el tipo de registro que ha realizado
    **/
    public function previsualize($p){
		if ($p == '')
			$p = $_SESSION['fechaNomina'];
		else{
			$p = explode('|',$p);
			$p[0] = date('Ymd', strtotime($p[0]));
			$p[1] = date('Ymd', strtotime($p[1]));
		}
		$_SESSION['fechaNomina'] = $p;
		$result = array(array($this->numberDateToString($p[0]), $this->numberDateToString($p[1]), $_SESSION['nomina'], 6),array());
		$d = new db();
		switch($_SESSION['nomina']){
			case '0':
				//return (id, empleado, dias laborables, dias asistidos, precio estajo, estajos, subtotal, prestamos, a pagar, pagado)
				$this->c->q("SELECT us_id, us_code, pe_razon, sue_monto, dep_nombre FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id LEFT JOIN departamentos ON us_dep_id = dep_id LEFT JOIN sueldos ON us_sue_id = sue_id WHERE dep_su_id = '".$_SESSION['sucursal']."' AND us_jornada = '0' AND us_deleted = '0';");
				while($row = $this->c->fr()){
					$row = $this->u8($row, array(2, 4), true);
					$emp = array($row[0], $row[2]);
					$d->q("SELECT count(*) FROM asistencias WHERE as_code = '".$row[1]."' AND (as_fecha >= '".$p[0]."' AND as_fecha <= '".$p[1]."');");
					array_push($emp, $d->r(0));
					array_push($emp, $row[3]);
					$d->q("SELECT SUM(est_unidades) FROM estajos WHERE est_empleado = '".$row[0]."' AND (est_fecha >= '".$p[0]."' AND est_fecha <= '".$p[1]."') AND est_eliminado = '0';");
					array_push($emp, $d->r(0));
					if ($emp[4] == null)
						$emp[4] = '0';
					array_push($emp, $emp[3] * $emp[4]);
					$d->q("SELECT SUM(pre_parcialidad) FROM prestamos WHERE pre_us_responsable = '".$row[0]."' AND pre_estado = '0';");
					array_push($emp, $d->r(0));
					if ($emp[6] == '')
						$emp[6] = 0;
					$percepcion = 0;
					$deduccion = 0;
					$d->q("SELECT pres_json FROM presave WHERE pres_inicio = '".$p[0]."' AND pres_fin = '".$p[1]."' AND pres_us_id = '".$row[0]."' LIMIT 1;");
					if ($d->nr() > 0){
						$json = $d->r(0);
						$json = json_decode($json);
						for($i = 0; $i < count($json[0]); $i++)
							$deduccion += $json[0][$i][1];
						for($i = 0; $i < count($json[1]); $i++)
							$percepcion += $json[1][$i][1];
					}
					array_push($emp, $emp[5] - $emp[6] + $percepcion - $deduccion);
					$d->q("SELECT pa_montopagado FROM pagos WHERE pa_inicio = '".$p[0]."' AND pa_fin = '".$p[1]."' AND pa_us_id = '".$row[0]."' AND pa_tipo = '0' LIMIT 1;");
					array_push($emp, (($d->nr() > 0) ? $d->r(0) : 0));
					array_push($emp, $row[4]);
					array_push($emp, $row[1]);
					$emp[3] = number_format($emp[3], 2, '.', '');
					$emp[5] = number_format($emp[5], 2, '.', '');
					$emp[6] = number_format($emp[6], 2, '.', '');
					$emp[7] = number_format($emp[7], 2, '.', '');
					$emp[8] = number_format($emp[8], 2, '.', '');
					array_push($result[1], $emp);
				}
			break;
			case '1':
			case '2':
				//return  id | empleado | sueldo | asistencias | falta | multa | subtotal | prestamos | pagar | pagado | detalles
				$laboral = 0;
				if ($_SESSION['nomina'] == '1'){
					$laboral = 6;
					$factorMulta = 6;
				} else {
					$factorMulta = 13;
					$dinicio = date('j', strtotime($p[0]));
					$finicio = date('Y-m', strtotime($p[0]));
					$dfin = date('j', strtotime($p[1]));
					for($i = 0; $dinicio <= $dfin; $dinicio++, $i++){
						if (date('w', strtotime($finicio.'-'.$dinicio)) != 0){
							$laboral++;
						}
					}
				}
				$result[0][3] = $laboral;
				$this->c->q("SELECT us_id, us_code, pe_razon, sue_monto, dep_nombre FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id LEFT JOIN departamentos ON us_dep_id = dep_id LEFT JOIN sueldos ON us_sue_id = sue_id WHERE dep_su_id = '".$_SESSION['sucursal']."' AND us_jornada = '".$_SESSION['nomina']."' AND us_deleted = '0';");
				while($row = $this->c->fr()){
					$row = $this->u8($row, array(2, 4), true);
					$emp = array($row[0], $row[2]);
					array_push($emp, $row[3]);
					$d->q("SELECT DISTINCT as_fecha FROM asistencias WHERE as_code = '".$row[1]."' AND (as_fecha >= '".$p[0]."' AND as_fecha <= '".$p[1]."') AND as_dayWeek != '7';");
					array_push($emp, $d->nr());
					array_push($emp, $laboral - $emp[3]);
					$multa = 0;
					if ($emp[3] <= $factorMulta){
						if ($emp[3] < $laboral){
							$multa = $emp[2] - (($emp[2] / $factorMulta) * $emp[3]);
						}
					}
					array_push($emp, number_format($multa, 2, '.', ''));
					array_push($emp, $emp[2] - $emp[5]);
					$d->q("SELECT SUM(pre_parcialidad) FROM prestamos WHERE pre_us_responsable = '".$row[0]."' AND pre_estado = '0';");
					array_push($emp, $d->r(0));
					if ($emp[7] == '')
						$emp[7] = 0;
					//////////////////////
					$percepcion = 0;
					$deduccion = 0;
					$d->q("SELECT pres_json FROM presave WHERE pres_inicio = '".$p[0]."' AND pres_fin = '".$p[1]."' AND pres_us_id = '".$row[0]."' LIMIT 1;");
					if ($d->nr() > 0){
						$json = $d->r(0);
						$json = json_decode($json);
						for($i = 0; $i < count($json[0]); $i++)
							$deduccion += $json[0][$i][1];
						for($i = 0; $i < count($json[1]); $i++)
							$percepcion += $json[1][$i][1];
					}
					//////////////////////
					array_push($emp, $emp[6] - $emp[7] - $deduccion + $percepcion); //sumar y restar deducciones y percepciones
					$d->q("SELECT pa_montopagado FROM pagos WHERE pa_inicio = '".$p[0]."' AND pa_fin = '".$p[1]."' AND pa_us_id = '".$row[0]."' AND pa_tipo = '".$result[0][2]."' LIMIT 1;");
					array_push($emp, (($d->nr() > 0) ? $d->r(0) : 0));
					array_push($emp, $row[4]);
                    array_push($emp, $row[1]);
					array_push($result[1], $emp);
                    
				}
			break;
		}
		$d->cl();
		return $result;
	}
    /**
    Método que registra y calcula los minutos que registra un empleado
    
    @bitactora registro de acceso en el reloj
    @param code de empleado
    @return estatus del registro así como el tipo de registro que ha realizado
    **/
    public function previsualizeant($p){
        $_SESSION['edit-Nomina1'] = $p;
        $this->hasAccess(get_class($this));
        $salida = array();
        $empleados = array();
        $d = new db();
        $this->c->q("SELECT us_id, us_code, pe_razon, us_jornada, us_retraso, sue_monto FROM usuarios LEFT JOIN sueldos on us_sue_id = sue_id LEFT JOIN personas ON pe_id = us_pe_id WHERE us_jornada = '".$p[1]."' AND us_dep_id IN (SELECT dep_id FROM departamentos WHERE dep_su_id = '".$p[0]."');");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(2), true);
            $row[6] = array();
            $d->q("SELECT ho_dia, ho_entrada, ho_salida FROM horarios WHERE ho_us_id = '".$row[0]."' ORDER BY ho_dia ASC;");
            while($dias = $d->fr()){
                $mins = ceil((strtotime($dias[2]) - strtotime($dias[1])) / 60);
                $dias[3] = $mins; 
                array_push($row[6], $dias);
            }
            if (count($row[6]) > 0)
                array_push($empleados, $row);
        }
        if (count($empleados) == 0)
            return array('zero');
        switch($p[1]){
            case '1':
                $p[2] = $this->stringDateToNumber($p[2]);
                $p[3] = $this->stringDateToNumber($p[3]);
                $dias = ceil((strtotime($p[3]) - strtotime($p[2])) / 86400)+1;
                if ($dias != 7)
                    return array('fecha1');
                $i = 0;
                $fin = 7;
            break;
            case '2':
                if ($p[4] == '01'){
                    $total = date('t', strtotime($p[2].''.$p[3].'01'));
                    $i = 0;
                    $fin = ($total == 28 || $total == 29) ? 14 : 15;
                    $p[2] = $p[2].''.$p[3].'01';
                } else {
                    $total = date('t', strtotime($p[2].''.$p[3].'01'));
                    $fin = ($total == 28 || $total == 29) ? 15 : 16;
                    $p[2] = $p[2].''.$p[3].''.$fin;
                    $i = 0;
                    $fin = ($total - $fin)+1;
                }
            break;
            case '3':
                $fin = date('t', strtotime($p[2].''.$p[3].'01'));
                $p[2] = $p[2].''.$p[3].'01';
                $i = 0;
            break;
        }
        $_SESSION['fechaInicio'] = $p[2];
        for($e = 0; $e < count($empleados); $e++){
            $employ = $empleados[$e];
            $totalmins = 0;
            $minutostrabajados = 0;
            $dias = 0;
            $faltas = 0;
            $minspenalizados = 0;
            $diastrabajados = 0;
            $faltita = 0;
            for(; $i < $fin; $i++){
                $fecha = date('Ymd', strtotime('+'.$i.' day', strtotime($p[2])));
                $N = date('N', strtotime($fecha));
                $dia = false;
                for($k = 0; $k < count($employ[6]); $k++){
                    $t = $employ[6][$k];
                    if ($t[0] == $N){
                        $dia = $t;
                        $totalmins += $t[3];
                        $dias++;
                    }
                }
                if ($dia != false){
                    $minutosdia = 0;
                    $this->c->q("SELECT as_id, as_minutos, as_entrada, as_salida FROM asistencias WHERE as_code = '".$employ[1]."' AND as_fecha = '".$fecha."' AND as_dayWeek = '".$N."';");
                    if ($this->c->nr() > 0){
                        $diastrabajados++;
                        while($horario = $this->c->fr()){
                            if ($horario[3] == ''){
                                $resta = 0;
                                $minutos = ceil((strtotime($fecha.''.$horario[2]) - strtotime($fecha.''.$dia[1])) / 60);
                                if ($minutos < 0)
                                    $resta += ($minutos * -1);
                                $total = ceil((strtotime($fecha.''.$dia[2]) - strtotime($fecha.''.$horario[2])) / 60);
                                $total -= $resta;
                                $horario[1] = $total;
                            }
                            $minutostrabajados += $horario[1];
                            $minutosdia += $horario[1];
                        }
                        switch($employ[4]){
                            case '0': //No hacer nada
                                $minutostrabajados -= $minutosdia;
                                $minutostrabajados += $dia[3];
                            break;
                            case '1': //Tolerar 5 minutos
                                if (($dia[3] - $minutosdia) > 5){
                                    $faltas++;
                                    $minutostrabajados -= $minutosdia;
                                    $minspenalizados += $minutosdia;
                                }
                            break;
                            case '2': //Tolerar 10 minutos
                                if (($dia[3] - $minutosdia) > 10){
                                    $faltas++;
                                    $minutostrabajados -= $minutosdia;
                                    $minspenalizados += $minutosdia;
                                }
                            break;
                            case '3': //Tolerar 15 minutos
                                if (($dia[3] - $minutosdia) > 10){
                                    $faltas++;
                                    $minutostrabajados -= $minutosdia;
                                    $minspenalizados += $minutosdia;
                                }
                            break;
                            case '4': //Cero tolerancia
                                if (($dia[3] - $minutosdia) > 0){
                                    $faltas++;
                                    $minutostrabajados -= $minutosdia;
                                    $minspenalizados += $minutosdia;
                                }
                            break;
                            case '5': //Hacerlo proporcional

                            break;
                        }
                    } else {
                        if ($employ[4] == '0'){
                            $diastrabajados++;
                            $faltita++;
                            $minutostrabajados += $dia[3];
                        }
                    }
                    //Obtener los eventos de trabajo
                }
            }
            if ($employ[4] == '0'){
                $minutostrabajados = 1;
                $totalmins = 1;
            }
            $_SESSION['fechaFin'] = $fecha;
            $faltadia = $dias - $diastrabajados + $faltita;
            $sueldoMerecido = round((($minutostrabajados * $employ[5]) / $totalmins), 2);
            $sueldodescontado = $employ[5] - $sueldoMerecido;
            /*
            echo '<br>';
            echo 'Minutos totales: '.$totalmins.' mins. <br>';
            echo 'dias totales: '.$dias.' dias. <br>';
            echo 'Minutos trabajados: '.$minutostrabajados.' mins. <br>';
            echo 'dias trabajados: '.$diastrabajados.' dias. <br>';
            echo 'DIAS CON FALTAS: '.$faltadia.' dias. <br>';
            echo 'faltas por incumplimiento: '.$faltas.' dias. <br>';
            echo 'Minutos descontados por faltas: '.$minspenalizados.' dias. <br>';
            echo 'Sueldo: '.$employ[5].' pesos. <br>';
            echo 'Sueldo descontado: '.$sueldodescontado.' pesos. <br>';
            echo 'Sueldo Merecido: '.$sueldoMerecido.' pesos. <br>';
            */
            $arr = array($employ[1], $employ[2], $employ[5], $dias, $totalmins, $diastrabajados, $minutostrabajados, $employ[4], $faltadia, $faltas, $minspenalizados, $sueldodescontado, $sueldoMerecido);
            array_push($salida, $arr);
        }
        $d->cl();
        $_SESSION['edit-Nomina2'] = $salida;
        return $salida;
    }
    /**
    Método que formaliza la información de la nómina en la base de datos
    
    @bitacora Registro de nómina en la plataforma
    @param void
    @return arreglo con el estado
    **/
    public function formalizar(){
        $this->hasAccess(get_class($this));
        $p = $_SESSION['edit-Nomina1'];
        $fi = $_SESSION['fechaInicio'];
        $ff = $_SESSION['fechaFin'];
        $data = WebService::$g->json(true, $_SESSION['edit-Nomina2']);
        $this->c->q("INSERT INTO nominas VALUES(NULL,'".$_SESSION['us_id']."','".$p[0]."','".$p[1]."','".$fi."','".$ff."','".$data."','".$this->d."')");
        $id = $this->c->last('nominas');
        $this->log($this, __FUNCTION__, 'bitacora',$id);
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>