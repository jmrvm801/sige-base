<?php
class Core{
    public $abrv = array('Ene' => '01', 'Feb' => '02', 'Mar' => '03', 'Abr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Ago' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dic' => '12');
    public $nmx = array('01' => 'Ene', '02' => 'Feb', '03' => 'Mar', '04' => 'Abr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dic');
    public function u8($arr, $pos, $f){
        for($i = 0; $i < count($pos); $i++)
            $arr[$pos[$i]] = WebService::$g->utf8($f, $arr[$pos[$i]]);
        return $arr;
    }
	public function getFormaPago($type){
		switch($type){
			case '0':
				return 'Efectivo';
			case '1':
				return 'Cheque';
			case '2':
				return 'Transferencia';
			case '3':
				return 'Tarjeta de credito';
			case '4':
				return 'Tarjeta de debito';
			case '5':
				return 'Pago por internet';
			case '6':
				return 'Deposito a cuenta';
			case '7':
				return 'Otra forma';
		}
	}
    public function getBit(){
        
    }
	public function bySuc(){
		return "SELECT dep_id FROM departamentos WHERE dep_su_id = '".$_SESSION['sucursal']."'";
	}
    public function stringDateToNumber($str){
        if ($str == ' ' || $str == '')
            return $str;
        $str = explode(' ', $str);
        $str[1] = str_replace(',','',$str[1]);
        $str[0] = $this->abrv[$str[0]];
        return $str[2].''.$str[0].''.$str[1];
    }
    public function numberDateToString($str){
        if ($str == ' ' || $str == '')
            return $str;
        $str = str_split($str);
        $year = $str[0].''.$str[1].''.$str[2].''.$str[3];
        $month = $this->nmx[$str[4].''.$str[5]];
        $day = $str[6].''.$str[7];
        return $month.' '.$day.', '.$year;
    }
    public function stringTimeToNumber($str){
        if ($str == ' ' || $str == '')
            return $str;
        return str_replace(':','',$str);
    }
    public function numberTimeToString($str){
        if ($str == ' ' || $str == '')
            return $str;
        $str = str_split($str);
        return $str[0].''.$str[1].':'.$str[2].''.$str[3];
    }
    public function hasAccess($seccion){
        if (@$_SESSION[$seccion][1] == '0')
            WebService::$g->kill(WebService::$g->json(true, array('denied')));
    }
    public function getClassAnotations($class){
        $pattern = "#(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#";
        $class = new ReflectionClass($class);
        $classFound = $class->getDocComment();
        preg_match_all($pattern, $classFound, $classDocs, PREG_PATTERN_ORDER);
        $author = $this->findKey($classDocs[1], '@author');
        //if (base64_encode($author) != 'RmVybmFuZG8gQ2FycmVvbg==')
            //WebService::$g->kill('Classes corrupted');
        return $class;
    }
    public function getMethodAnotations($class, $method){
        $pattern = "#(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#";
        $class = $this->getClassAnotations($class);
        $methodFound = $class->getMethod($method)->getDocComment();
        preg_match_all($pattern, $methodFound, $matches, PREG_PATTERN_ORDER);
        return $matches[1];
    }
    public function log($class, $method, $key, $complemento){
        $math = $this->getMethodAnotations($class, $method);
        $value = $this->findKey($math, $key);
        $tmp = new db();
        $tmp->q("INSERT INTO logs VALUES(NULL, '".$_SESSION['us_id']."', '".get_class($class)."', '".$method."', '".utf8_decode($value)."', '".utf8_decode($complemento)."', '".date('YmdHis')."')");
        $tmp->cl();
    }
    public function findKey($arr, $key){
        $value = 'null';
        for($i = 0; $i < count($arr); $i++)
            if (strpos($arr[$i], $key) !== false)
                $value = $arr[$i];
        if ($value == null)
            return 'null';
        $value = @explode($key, $value);
        return @trim($value[1]);
    }
	function dataImageToFile($dataFile, $nameFile){
		$data = explode( ',', $dataFile );
		if (strpos(strtolower($data[0]), 'png') !== false)
			$nameFile .= '.png';
		else if (strpos(strtolower($data[0]), 'jpeg') !== false)
			$nameFile .= '.jpg';
		else
			$nameFile .= '.gif';
		$ifp = fopen( $nameFile, 'wb' ); 
		fwrite( $ifp, base64_decode( $data[ 1 ] ) );
		fclose( $ifp ); 
		return $nameFile; 
	}
}

?>