<?php
require_once('Core.php');
require_once('Trabajadores.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Sueldos extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Sueldos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Sueldos($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un sueldo a través de un id.
    
    @bitacora Acceso a la información de un sueldo
    @param id del sueldo a obtener el resultado
    @return arreglo con los datos del sueldo
    **/
    public function getSueldosById($id){
        $this->c->q("SELECT * FROM sueldos WHERE sue_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Sueldos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
    /**
    Método principal de la clase Sueldos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllSueldos':
                return $this->getAllSueldos();
            break;
            case 'saveSueldo':
                return (!isset($_SESSION['edit-Sueldos'])) ? $this->saveSueldo() : $this->updateSueldo();
            break;
            case 'getSueldosById':
                return $this->getSueldosById($_POST['param']);
            break;
            case 'delSueldos':
                return $this->deleteSueldos($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los sueldos
    
    @bitacora Acceso a la información básica de todos los sueldos
    @param void
    @return arreglo de los sueldos
    **/
    public function getAllSueldos(){
        $v = array();
        unset($_SESSION['edit-Sueldos']);
        $this->c->q("SELECT * FROM sueldos ORDER BY sue_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un sueldo nuev.
    
    @bitacora Guardado de un nuevo sueldos
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveSueldo(){
        $this->hasAccess(get_class($this));
        
        $arr = array('error');
        $p = $_POST['param'];
        $this->createSueldo(array($p[0], $p[1], $p[2]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un sueldo en la base de datos
    
    @bitacora Creación de un nuevo sueldo
    @param arreglo con los datos del sueldo
    @return identificador del sueldo agregado en la base de datos
    
    **/
    public function createSueldo($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO sueldos VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$this->d."')");
        return $this->c->last('sueldos');        
    }
    /**
    Método que actualiza la información de un sueldo
    
    @bitacora Actualización de un sueldo
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateSueldo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Sueldos']))
            return $arr;
        $sueldo = $this->getSueldosById($_SESSION['edit-Sueldos']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$sueldo[1].' por '.$p[0]);
        $this->c->q("UPDATE sueldos SET sue_nombre = '".$p[0]."', sue_descr = '".$p[1]."', sue_monto = '".$p[2]."', sue_fecha = '".$this->d."' WHERE sue_id = '".$sueldo[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Sueldos']);
        return $arr;
    }
    /**
    Método que marca como eliminado un sueldo
    
    @bitacora Eliminación de un sueldo
    @param identificador de un sueldo en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteSueldos($id){
        $this->hasAccess(get_class($this));
        $sueldo = $this->getSueldosById($id);
        unset($_SESSION['edit-Sueldos']);
        $this->log($this, __FUNCTION__, 'bitacora', $sueldo[1]);
        $this->c->q("DELETE FROM sueldos WHERE sue_id = '".$sueldo[0]."' LIMIT 1;");
        $tra = new Trabajadores();
        $tra->limpiarTrabajadoresPorSueldos($sueldo[0]);
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>