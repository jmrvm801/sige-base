<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Asistencia.php');
require_once('Proveedores.php');
require_once('Core.php');
require_once('Gastos.php');
require_once('Comprobacion.php');
/**
Clase Prestamos, contiene los métodos necesarios para el manejo del módulo préstamos

@author Fernando Carreon
@version 1.0
**/
class Prestamos extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Prestamos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Prestamos(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un préstamos a través de un id.
    
    @bitacora Acceso a la información de un préstamos
    @param id del préstamos a obtener el resultado
    @return arreglo con los datos del préstamos
    **/
    public function getPrestamosById($id){
        $this->c->q("SELECT * FROM prestamos WHERE pre_ref = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Prestamos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(3, 7), true);
        return $data;
    }
    /**
    Método principal de la clase Prestamos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllPrestamos':
                return $this->getAllPrestamos();
            break;
            case 'savePrestamo':
                return $this->savePrestamo();
            break;
            case 'getPrestamosById':
                return $this->getPrestamosById($_POST['param']);
            break;
            case 'setIdComprobacion':
            case 'comprobarPrestamo':
            case 'getComprobacionesById':
            case 'delComprobante':
            case 'getComprobanteById':
                $comp = new Comprobacion($this->c, 2);
                return $comp->run($method);
            break;
			case 'delPrestamo':
				return $this->delPrestamo($_POST['param']);
			break;
        }
    }
	/**
    Método que obtiene toda la información básica de los préstamos
    
    @bitacora Se eliminó un préstamo: 
    @param void
    @return arreglo de los préstamos
    **/
	public function delPrestamo($p){
		$this->log($this, __FUNCTION__, 'bitacora', 'Préstamo con id: '.$p);
		$this->c->q("DELETE FROM prestamos WHERE pre_ref = '".$p."' LIMIT 1;");
		return array('true');
	}
    /**
    Método que obtiene toda la información básica de los préstamos
    
    @bitacora Acceso a la información básica de todos los préstamos
    @param void
    @return arreglo de los préstamos
    **/
    public function getAllPrestamos(){
        $v = array();
        unset($_SESSION['edit-Prestamos']);
		$this->c->q("SELECT pre_ref, pe_razon, pre_monto, pre_cubierto, 0 FROM prestamos LEFT JOIN usuarios ON us_id = pre_us_responsable LEFT JOIN personas ON us_pe_id = pe_id WHERE pre_estado = '0' AND pre_su_id = '".$_SESSION['sucursal']."' ORDER BY pre_fecha DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
			$row[4] = ($row[2] - $row[3]);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un préstamo.
    
    @bitacora Guardado de un nuevo préstamo
    @param void
    @return arreglo con el estado del guardado
    **/
    public function savePrestamo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
		if ($_POST['restriccion'] == 1){
			$this->c->q("SELECT (sum(pre_monto) - sum(pre_cubierto)) FROM prestamos WHERE pre_us_responsable = '".$p[1]."' AND pre_estado = '0' GROUP BY pre_us_responsable");
			$prestado = $this->c->r(0);
			$prestado = ($prestado == '') ? 0 : $prestado;
			$asis = new Asistencia($this->c);
			$monto = $asis->getStatics($p[1]);
			if ($monto == 0)
				return array('falta', $monto);
			if ($p[2]+$prestado > $monto)
				return array('asistencia', $monto, $prestado, $monto-$prestado);
			$this->c->q("SELECT us_limite FROM usuarios WHERE us_id = '".$p[1]."' LIMIT 1;");
			$limite = $this->c->r(0);
			$limit = $limite - $prestado;
			if ($p[2] > $limit)
				return array('limite', $limit, $limite, $prestado);
		} else {
			$this->log($this, __FUNCTION__, 'bitacora', 'Préstamo sin restricciones con id: '.$p[0]);
		}
		$this->createPrestamos($p);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un préstamo en la base de datos
    
    @bitacora Creación de un nuevo préstamo
    @param arreglo con los datos del préstamo
    @return identificador del préstamo agregado en la base de datos
    
    **/
    public function createPrestamos($v){
        $v = $this->u8($v, array(4, 6), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
		$parcialidades = $v[2]/$v[5];
		$this->c->q("INSERT INTO prestamos VALUES('".$v[0]."', '".$_SESSION['sucursal']."', '".$_SESSION['us_id']."', '".$v[6]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$parcialidades."', '0', '".$this->d."', '0', '0', '0')");
        return $this->c->last('prestamos');        
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>