<?php
require_once('Direccion.php');
require_once('Empresas.php');
require_once('Sucursales.php');
require_once('Core.php');
/**
Clase departamentos, contiene los métodos necesarios para el manejo del módulo departamentos

@author Fernando Carreon
@version 1.0
**/
class Departamentos extends Core{
    public $c;
    public $d;
	public $close;
	/**
    Constructor de la clase Departamentos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Departamentos($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
	/**
    Obtiene los datos de un departamento a través de un id.
    
    @bitacora Acceso a la información del departamento
    @param id del departamento a obtener el resultado
    @return arreglo con los datos del departamento
    **/
    public function getDepartamentoById($id){
        $this->c->q("SELECT * FROM departamentos WHERE dep_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Departamento'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(2, 3), true);
        return $data;
    }
	/**
    Método principal de la clase Departamentos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllDepartamentos':
                return $this->getAllDepartamentos();
            break;
            case 'saveDepartamento':
                return (!isset($_SESSION['edit-Departamento'])) ? $this->saveDepartamento() : $this->updateDepartamento();
            break;
            case 'getDepartamentoById':
                return $this->getDepartamentoById($_POST['param']);
            break;
            case 'delDepartamento':
                return $this->deleteDepartamento($_POST['param']);
            break;
        }
    }
	/**
    Método que obtiene toda la información básica de los departamentos
    
    @bitacora Acceso a la información básica de todos los departamentos
    @param void
    @return arreglo de departamentos
    **/
    public function getAllDepartamentos($str = false){
        $v = array();
        unset($_SESSION['edit-Departamento']);
		if (!$str){
			$this->c->q("SELECT dep_id, dep_nombre FROM departamentos WHERE dep_su_id = '".$_SESSION['sucursal']."' ORDER BY dep_nombre;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(1), true);
				array_push($v, $row);
			}
		} else {
			$this->c->q("SELECT dep_id, dep_su_id, em_nombre, su_nombre, dep_nombre FROM departamentos LEFT JOIN sucursales ON dep_su_id = su_id LEFT JOIN empresas ON su_em_id = em_id WHERE su_deleted = '0' AND su_deleted = '0' AND dep_deleted = '0' ORDER BY dep_id DESC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(2, 3, 4), true);
				array_push($v, $row);
			}
		}
        return $v;
    }
	/**
    Método que crea un departamento nueva. Tiene relación con la clase Dirección
    
    @bitacora Guardado de un nuevo departamento
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveDepartamento(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createDepartamento(array($p[2], $p[0], $p[1]));
        $arr[0] = 'true';
        return $arr;
    }
	/**
    Método que inserta los datos de la sucursal en la base de datos
    
    @bitacora Creación de un nuevo departamento
    @param arreglo con los datos de la sucursal
    @return identificador de la sucursal agregada en la base de datos
    
    **/
    public function createDepartamento($v){
        $v = $this->u8($v, array(1, 2), false);
		$this->log($this, __FUNCTION__, 'bitacora', $v[1]);
        $this->c->q("INSERT INTO departamentos VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$this->d."', '0')");
        return $this->c->last('sucursales');        
    }
	/**
    Método que actualiza la información de un departamento
    
    @bitacora Actualización de un departamento
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateDepartamento(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Departamento']))
            return $arr;
        $departamento = $this->getDepartamentoById($_SESSION['edit-Departamento']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
		$this->log($this, __FUNCTION__, 'bitacora',$departamento[2].' por '.$p[0]);
        $this->c->q("UPDATE departamentos SET dep_nombre = '".$p[0]."', dep_descr = '".$p[1]."', dep_su_id = '".$p[2]."', dep_fecha = '".$this->d."' WHERE dep_id = '".$departamento[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Departamento']);
        return $arr;
    }
	/**
    Método que marca como eliminada un departamento
    
    @bitacora Eliminación de un departamento
    @param identificador de un departamento en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteDepartamento($id){
        $this->hasAccess(get_class($this));
        $departamento = $this->getDepartamentoById($id);
		$this->log($this, __FUNCTION__, 'bitacora',$departamento[3]);
        unset($_SESSION['edit-Departamento']);
        $this->c->q("UPDATE departamentos SET dep_deleted = '1' WHERE dep_id = '".$departamento[0]."' LIMIT 1;");
        return array('true');
    }
	/**
    Método que elimina todos los departamentos asociados a una sucursal
    
    @bitacora Eliminación de departamentos asociados a una sucursal
    @param identificador de un departamento en la base de datos
    @return arreglo con el estado del guardado
    **/
	public function deleteDepartamentoBySucursal($sucursal){
        $this->c->q("UPDATE departamentos SET dep_deleted = '1' WHERE dep_su_id = '".$sucursal."';");
	}
	/**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        if ($this->close)
        	$this->c->cl();
    }
}
?>