<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Inventarios.php');
require_once('Proveedores.php');
require_once('Precios.php');
require_once('Core.php');
require_once('Comprobacion.php');
/**
Clase OrdenDeCompra, contiene los métodos necesarios para el manejo del módulo OrdenDeCompra

@author Fernando Carreon
@version 1.0
**/
class OrdenDeCompra extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase OrdenDeCompra
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function OrdenDeCompra(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un precio a través de un id.
    
    @bitacora Acceso a la información de un precio
    @param id del precio a obtener el resultado
    @return arreglo con los datos de la orden
    **/
    public function getOrdenesById($id){
        $this->c->q("SELECT * FROM ordendecompra WHERE orc_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-OrdenCompra'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(11), true);
        $data[5] = $this->numberDateToString($data[5]);
        $data[6] = $this->numberDateToString($data[6]);
        $comp = new OrdenArticulos($this->c, 1);
        $data[14] = $comp->getOrdenArticulosById($id);
        $comp = new Comprobacion($this->c, 3);
        $data[15] = $comp->getComprobacionesById();
        return $data;
    }
    /**
    Método que obtiene los artículos en función de la orden
    
    @bitacora Obtención de de artículos a través de una orden
    @param id de la orden
    @return arreglo con los datos de los artículos de una orden
    **/
    public function getOrdenesArticulosById($id){
        $this->c->q("SELECT orc_id FROM ordendecompra WHERE orc_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-OrdenCompra'] = $id;
        
        
        $comp = new OrdenArticulos($this->c, 1);
        $data = $comp->getOrdenArticulosByIdAll($id);
        return $data;
    }
    /**
    Método principal de la clase Precios
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getMetaOrdenDeCompra':
                $proved = new Proveedores();
                return array($proved->getForProveedores());
            break;
            case 'getAllOrden':
                return $this->getAllOrden();
            break;
            case 'getAllOrdenFormalizada':
                return $this->getAllOrden(true);
            break;
            case 'getAllOrdenCredito':
                return $this->getAllOrdenCredito();
            break;
            case 'saveOrden':
                return (!isset($_SESSION['edit-OrdenCompra'])) ? $this->saveOrden() : $this->updateOrden();
            break;
            case 'getOrdenesById':
                return $this->getOrdenesById($_POST['param']);
            break;
            case 'getOrdenesArticulosById':
                return $this->getOrdenesArticulosById($_POST['param']);
            break;
            case 'delOrden':
                return $this->delOrden($_POST['param']);
            break;
            case 'getArticulosBySucursalAndFamilia':
                return $this->getArticulosBySucursalAndFamilia($_POST['param']);
            break;
            case 'getArticulosByPrecioId':
                return $this->getArticulosByPrecioId($_POST['param']);
            break;
            case 'formalizeOrden':
                return $this->formalizeOrden($_POST['param']);
            break;
            case 'setIdComprobacion':
            case 'comprobarOrdenCompra':
                $comp = new Comprobacion($this->c, 3);
                return $comp->run($method);
            break;
        }
    }
    /**
    Método
    **/
    public function formalizeOrden($p){
        $id = $_SESSION['edit-OrdenCompra'];
        $mov = new Movimientos($this->c);
        for($i = 0; $i < count($p); $i++){
            $this->c->q("SELECT orda_id, in_id, orda_unidades FROM ordenarticulos LEFT JOIN inventarios ON orda_pre_id = in_pre_id WHERE orda_id = '".$p[$i][0]."' LIMIT 1;");
            $dato = $this->c->fr();
            $dif = $p[$i][1] - $dato[2];
            if ($dif == 0)
                $observ = 'No se presentó diferencia en la orden número'.$id;
            else if ($dif > 0)
                $observ = 'Hubo una diferencia de '.$dif.' unidades MÁS con respecto a lo establecido en la orden de compra número '.$id;
            else
                $observ = 'Hubo una diferencia de '.$dif.' unidades MENOS con respecto a lo establecido en la orden de compra número '.$id;
            $mov->createMovimiento($dato[1], 0, $p[$i][1], WebService::$g->utf8(false, $observ));
        }
        $this->c->q("UPDATE ordendecompra SET orc_formalizado = '1', orc_fechaLlegada = '".date('Ymd')."', orc_fecha = '".$this->d."' WHERE orc_id = '".$id."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que obtiene toda la información básica de los precios
    
    @bitacora Acceso a la información básica de todos los precios
    @param void
    @return arreglo de los precios
    **/
    public function getAllOrdenCredito(){
        $v = array();
        unset($_SESSION['edit-OrdenCompra']);
        $this->c->q("SELECT orc_id, CONCAT_WS(' ', pe_razon, di_ciudad, di_estado), orc_monto, orc_cubierto FROM ordendecompra LEFT JOIN departamentos ON dep_id = orc_dep_id LEFT JOIN sucursales ON su_id = dep_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN proveedores ON pro_id = orc_pro_id LEFT JOIN personas ON pe_id = pro_pe_id LEFT JOIN direcciones ON di_id = pe_di_id WHERE orc_estado = '0';");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene toda la información básica de los precios
    
    @bitacora Acceso a la información básica de todos los precios
    @param void
    @return arreglo de los precios
    **/
    public function getAllOrden($value = false){
        $v = array();
        unset($_SESSION['edit-OrdenCompra']);
        $value = ($value) ? " AND orc_formalizado = '0'" : "";
        $this->c->q("SELECT orc_id, dep_nombre, CONCAT_WS(' ', pe_razon, di_ciudad, di_estado), orc_fechaEmision, orc_fechaLlegada, orc_monto, orc_tipopago FROM ordendecompra LEFT JOIN departamentos ON dep_id = orc_dep_id LEFT JOIN sucursales ON su_id = dep_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN proveedores ON pro_id = orc_pro_id LEFT JOIN personas ON pe_id = pro_pe_id LEFT JOIN direcciones ON di_id = pe_di_id WHERE su_id = '".$_SESSION['sucursal']."' ".$value." ORDER BY orc_id DESC");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un precio.
    
    @bitacora Guardado de un nuevo precio
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveOrden(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createOrdenCompra($p);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un precio en la base de datos
    
    @bitacora Creación de una orden de compra
    @param arreglo con los datos del precio
    @return identificador del precio agregado en la base de datos
    
    **/
    public function createOrdenCompra($v){
        $v = $this->u8($v, array(5), false);
        $v[3] = $this->stringDateToNumber($v[3]);
        $v[4] = $this->stringDateToNumber($v[4]);
        $this->c->q("INSERT INTO ordendecompra VALUES(NULL, '".$_SESSION['us_id']."', '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[7]."', '".$v[6]."', '0', '0', '".$v[5]."', '0', '".$this->d."')");
        $superid = $this->c->last('ordendecompra');
        $this->log($this, __FUNCTION__, 'bitacora', $superid);
        $comp = new Comprobacion($this->c, 3);
        if (isset($v[9]))
            for($i = 0; $i < count($v[9]); $i++)
                $comp->comprobarOrdenDeCompra($superid, $v[9][$i][0], $v[9][$i][1], $v[9][$i][2], $_SESSION['us_id'], $v[9][$i][3]);
        $orda = new OrdenArticulos($this->c, 1);
        for($i = 0; $i < count($v[8]); $i++)
            $orda->createOrdenArticulo($superid, $v[8][$i]);
        return $superid;       
    }
    /**
    Método que actualiza la información de una orden de compra
    
    @bitacora Actualización de una orden de compra
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateOrden(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-OrdenCompra']))
            return $arr;
        $orden = $this->getOrdenesById($_SESSION['edit-OrdenCompra']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(5), false);
        $p[3] = $this->stringDateToNumber($p[3]);
        $p[4] = $this->stringDateToNumber($p[4]);
        if ($orden[12] != '0')
            return array('formalized');
        $comp = new Comprobacion($this->c, 3);
        $comp->delComprobanteByRef($orden[0]);
        $orda = new OrdenArticulos($this->c, 1);
        $orda->delOrdenArticulosByRef($orden[0]);
        $this->log($this, __FUNCTION__, 'bitacora',$orden[0]);
        $this->c->q("UPDATE ordendecompra SET orc_dep_id = '".$p[0]."', orc_us_responsable = '".$p[1]."', orc_pro_id = '".$p[2]."', orc_fechaEmision = '".$p[3]."', orc_fechaLlegada = '".$p[4]."', orc_monto = '".$p[7]."', orc_tipopago = '".$p[6]."', orc_estado = '0', orc_cubierto = '0', orc_observaciones = '".$p[5]."', orc_fecha = '".$this->d."' WHERE orc_id = '".$orden[0]."' LIMIT 1;");
        if (isset($v[9]))
            for($i = 0; $i < count($p[9]); $i++)
                $comp->comprobarOrdenDeCompra($orden[0], $p[9][$i][0], $p[9][$i][1], $p[9][$i][2], $_SESSION['us_id'], $p[9][$i][3]);
        for($i = 0; $i < count($p[8]); $i++)
            $orda->createOrdenArticulo($orden[0], $p[8][$i]);
        unset($_SESSION['edit-OrdenCompra']);
        return array('true');
    }
    /**
    Método que elimina una orden de compra
    
    @bitacora Eliminación de una orden de compra con id
    @param identificador de una orden de compra en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delOrden($id){
        $this->hasAccess(get_class($this));
        $orden = $this->getOrdenesById($id);
        if ($orden[12] != '0')
            return array('formalized');
        unset($_SESSION['edit-OrdenCompra']);
        $comp = new Comprobacion($this->c, 3);
        $comp->delComprobanteByRef($orden[0]);
        $orda = new OrdenArticulos($this->c, 1);
        $orda->delOrdenArticulosByRef($orden[0]);
        $this->log($this, __FUNCTION__, 'bitacora', $orden[0]);
        $this->c->q("DELETE FROM ordendecompra WHERE orc_id = '".$orden[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que obtiene los artículos filtrados por sucursal y familia
    
    @bitacora acceso a la información de artículos filtrados por sucursal y familia
    @param arreglo con la información del artículo
    @return arreglo de artículos coincidentes
    **/
    public function getArticulosBySucursalAndFamilia($p){ //P[0] = sucursal, p[1] = familia
        $this->c->q("SELECT pre_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, art_codigoSat, art_unidadSat FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE pre_su_id = '".$p[0]."' AND art_fa_id = '".$p[1]."' ORDER BY art_nombre;");
        $list = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7), true);
            array_push($list, $row);
        }
        return $list;
    }
    /**
    Método que obtiene los artículos filtrados por id de precio
    
    @bitacora acceso a la información de artículos filtrados por id de precio
    @param arreglo con la información del artículo
    @return arreglo del artículo coincidente
    **/
    public function getArticulosByPrecioId($p){
        $this->c->q("SELECT pre_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, art_codigoSat, art_unidadSat FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE pre_id = '".$p."' ORDER BY art_nombre;");
        $list = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7), true);
            array_push($list, $row);
        }
        return $list;
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
/**
Clase débil OrdenArticulos, contiene los métodos necesarios para el manejo del módulo OrdenDeCompra, Cotizaciones y OrdenDeVenta

@author Fernando Carreon
@version 1.0
**/
class OrdenArticulos{
    public $c;
    public $d;
    public $tipo;
    /**
    Constructor de la clase OrdenArticulos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function OrdenArticulos($c, $tipo){
        $this->c = $c;
        $this->d = date('YmdHis');
        $this->tipo = $tipo;
    }
    /**
    Método que elimina los artículos de una orden de compra, venta o cotización a través de un id
    
    @bitacora Eliminación de un artículo de una orden.
    @param referencia de la orden o cotización
    @return void
    **/
    public function delOrdenArticulosByRef($ref){ //Sacar a una clase común
        $this->c->q("DELETE FROM ordenarticulos WHERE orda_tipo = '".$this->tipo."' AND orda_ref = '".$ref."';");
    }
    /**
    Método que inserta los artículos de una orden a la base de datos
    
    @bitacora Agregación de un producto en la base de datos
    @param referencia de la orden o cotización
    @param arreglo de los datos del artículo
    @return void
    **/
    public function createOrdenArticulo($ref, $v){
        $this->c->q("INSERT INTO ordenarticulos VALUES(NULL, '".$this->tipo."', '".$ref."', '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."','".$this->d."')");
    }
    /**
    Método que obtiene los artículos de una orden por id
    
    @bitacora Obtención de artículos de una orden
    @param ref de la orden
    @return arreglo de los artículos de la orden
    **/
    public function getOrdenArticulosById($ref){
        $this->c->q("SELECT * FROM ordenarticulos WHERE orda_tipo = '".$this->tipo."' AND orda_ref = '".$ref."' ORDER BY orda_id;");
        $arr = array();
        while($row = $this->c->fr())
            array_push($arr, $row);
        return $arr;
    }
    /**
    Método que obtiene los artículos de una orden por id
    
    @bitacora Obtención de artículos de una orden
    @param ref de la orden
    @return arreglo de los artículos de la orden
    **/
    public function getOrdenArticulosByIdAll($ref){
        $this->c->q("SELECT * FROM ordenarticulos WHERE orda_tipo = '".$this->tipo."' AND orda_ref = '".$ref."' ORDER BY orda_id;");
        $arr = array();
        $art = new Precios();
        while($row = $this->c->fr()){
            $row[3] = $art->getArticulosByPrecioId($row[3]);
            array_push($arr, $row);
        }
        return $arr;
    }
}
?>