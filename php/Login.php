<?php
require_once('Core.php');
/**
Clase Login, contiene los métodos necesarios para el manejo del módulo de inicio de sesión

@author Fernando Carreon
@version 1.0
**/
class Login extends Core{
    public $c;
    public $data = array('us_id', 'username', 'admin', 'nombre', 'departamento', 'google', 'sucursal', 'empresa','accesoTotal');
    public function dj($a){
        WebService::$g->kill(WebService::$g->json(true,$a));
    }
    public function Login($method){
        $this->c = new db();
        $this->run($method);
    }
    public function run($method){
        switch($method){
            case 'verify':
                $this->verify($_POST['param']);
            break;
            case 'exit':
                $this->unsetVars();
            break;
        }
    }
    /**
    Método que verifica el inicio de sesión
    
    @bitacora Acceso al SIGE
    @param param de los datos de inicio de sesión
    @return arreglo con el resultado de la verificación
    **/
    public function verify($param){
        $param['user'] = WebService::$g->crypto(false, $param['user']);
        $ret = array();
        $user = new Usuarios($this->c);
        $v = $user->getUsuarioPorCorreo($param['user']);
        if (!$v)
            $this->dj(array('error'));
        $pdata = $user->getUsuarioTable();
		$this->c->q("SELECT ac_su_id, su_em_id FROM accesos LEFT JOIN sucursales ON ac_su_id = su_id WHERE ac_us_id = '".$pdata[0]."';");
		if ($this->c->nr() == 0)
			$this->dj(array('nosuc'));
		elseif ($this->c->nr() == 1){
			$suc = $this->c->fr();
			$sucursal = $suc[0];
			$empresa = $suc[1];
		} else{
			$suc = $this->c->fr();
			$sucursal = $suc[0];
			$empresa = $suc[1];
			$this->c->q("SELECT dep_su_id FROM departamentos WHERE dep_id = '".$pdata[2]."' LIMIT 1;");
			$sucx = $this->c->r(0);
			$this->c->q("SELECT ac_su_id, su_em_id FROM accesos LEFT JOIN sucursales ON ac_su_id = su_id WHERE ac_us_id = '".$pdata[0]."' AND ac_su_id = '".$sucx."' LIMIT 1;");
			if ($this->c->nr() > 0){
				$suc = $this->c->fr();
				$sucursal = $suc[0];
				$empresa = $suc[1];
			}
			
		}
        if ($pdata[6] != $param['pass'])
            $this->dj(array('error'));
        $pper = $user->getPersonaTable();
        $google = (($pdata[5] == 'no') ? 'passed' : 'needle');
        $k = array($pdata[0], $param['user'], $pdata[9], $pper[1], $pdata[2], $google, $sucursal, $empresa, $pdata[7]);
        $this->setVars($k);
        $p = new Permisos($this->c);
        $permisos = $p->regPermisos();
        $this->log($this, __FUNCTION__, 'bitacora','Inicio de sesión');
		//print_r($_SESSION);
		//echo 'logerror';
        $this->dj(array($google, $permisos));
    }
    public function unsetVars(){
        for($i = 0; $i < count($this->data); $i++){
            WebService::$g->cook($this->data[$i], '', 1);
            $_SESSION[$this->data[$i]] = '';
        }
        session_destroy();
        $this->dj(array('true'));
    }
    public function setVars($pairs){
        for($i = 0; $i < count($this->data); $i++){
            WebService::$g->cook($this->data[$i], $pairs[$i], 7200);
            $_SESSION[$this->data[$i]] = $pairs[$i];
        }
    }
    function __destruct(){
        //$this->c->cl();
    }
}

class Modulos{
    public $c;
    public function Modulos($c){
        $this->c = $c;
    }
    public function getModulos(){
        $arr = array();
        $this->c->q("SELECT mo_id FROM modulos ORDER BY mo_fecha;");
        while($row = $this->c->fr())
            array_push($arr, $row[0]);
        return $arr;
    }
}
class Permisos extends Core{
    public $c;
    public function Permisos($c){
        $this->c = $c;
        $this->c->q("SELECT mo_id FROM modulos;");
        $d = new db();
        while($row = $this->c->fr()){
            $d->q("SELECT per_id FROM permisos WHERE per_us_id = '".$_SESSION['us_id']."' AND per_mo_id = '".$row[0]."' LIMIT 1;");
            if ($d->nr() == 0)
                $d->q("INSERT INTO permisos VALUES(NULL,'".$_SESSION['us_id']."','".$row[0]."','0','".date('YmdHis')."')");
        }
        $d->cl();
    }
    public function run($method){
        switch($method){
            case 'regPermisos':
                return $this->regPermisos();
            break;
			case 'getBitacora':
				return $this->getBitacora();
			break;
        }
    }
	public function getBitacora(){
		$this->c->q("SELECT log_id, pe_razon, log_clase, log_texto, log_complemento, log_fecha FROM logs LEFT JOIN usuarios ON log_us_id = us_id LEFT JOIN personas ON us_pe_id = pe_id ORDER BY log_id DESC LIMIT 100");
		$arr = array();
		while($row = $this->c->fr()){
			$row = $this->u8($row, array(1, 3, 4), true);
			array_push($arr, $row);
		}
		return $arr;
	}
    public function setPermiso($user, $modulo, $value){
        $this->c->q("SELECT per_id FROM permisos WHERE per_us_id = '".$user."' AND per_mo_id = '".$modulo."' LIMIT 1;");
        if ($this->c->nr() == 0){
            $this->c->q("INSERT INTO permisos VALUES(NULL,'".$user."','".$modulo."','".$value."','".date('YmdHis')."')");
        } else
            $this->c->q("UPDATE permisos SET per_auth = '".$value."', per_fecha = '".date('YmdHis')."' WHERE per_id = '".$this->c->r(0)."' LIMIT 1;");
    }
    public function regPermisos(){
        $this->c->q("SELECT per_id, per_mo_id, per_auth FROM permisos WHERE per_us_id = '".$_SESSION['us_id']."';");
        $result = array();
        while($row = $this->c->fr()){
            array_push($result, $row);
            $_SESSION[$row[1]] = array($row[0], $row[2]);
        }
        return $result;
    }
    public function getPermisosById($id){
        $arr = array();
        $this->c->q("SELECT per_mo_id, per_auth FROM permisos WHERE per_us_id = '".$id."';");
        while($row = $this->c->fr())
            array_push($arr, $row);
        return $arr;
    }
}
?>