<?php
class Usuarios{
    public $c;
    public $data;
    public function Usuarios($c){
        $this->c = new db();
    }
    public function getUsuarioPorCorreo($correo){
        $this->c->q("SELECT * FROM personas LEFT JOIN usuarios ON pe_id = us_pe_id LEFT JOIN direcciones ON di_id = pe_di_id WHERE pe_correo = '".$correo."' AND us_deleted = '0' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $this->data = $this->c->fr();
        return true;
    }
    public function getPersonaTable(){
        $v = array();
        for($i = 0; $i <= 11; $i++)
            array_push($v, WebService::$g->utf8(true, $this->data[$i]));
        return $v;
    }
    public function getUsuarioTable(){
        $v = array();
        for($i = 12; $i <= 24; $i++)
            array_push($v, WebService::$g->utf8(true, $this->data[$i]));
        return $v;
    }
    public function getDireccionTable(){
        $v = array();
        for($i = 25; $i <= 32; $i++)
            array_push($v, WebService::$g->utf8(true, $this->data[$i]));
        return $v;
    }
}
?>