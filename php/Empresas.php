<?php
require_once('Direccion.php');
require_once('Sucursales.php');
require_once('Departamentos.php');
require_once('Trabajadores.php');
require_once('Core.php');
/**
Clase Empresas, contiene los métodos necesarios para el manejo del módulo empresas

@author Fernando Carreon
@version 1.0
**/
class Empresas extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Empresas
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Empresas($c = ''){
		$this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una empresa a través de un id.
    
    @bitacora Acceso a la información de la empresa
    @param id de la empresa a obtener el resultado
    @return arreglo con los datos de la empresa
    **/
    public function getEmpresaById($id){
        $this->c->q("SELECT * FROM empresas WHERE em_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Empresa'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2, 4), true);
        $dir = new Direccion($this->c);
        $data[3] = $dir->getDirById($data[3]);
        return $data;
    }
    /**
    Método principal de la clase Empresas
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllEmpresas':
                return $this->getAllEmpresas();
            break;
            case 'saveEmpresa':
                return (!isset($_SESSION['edit-Empresa'])) ? $this->saveEmpresa() : $this->updateEmpresaById();
            break;
            case 'getEmpresaById':
                return $this->getEmpresaById($_POST['param']);
            break;
            case 'deleteEmpresa':
                return $this->deleteEmpresa($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las empresas
    
    @bitacora Acceso a la información básica de todas las empresas
    @param void
    @return arreglo de empresas
    **/
    public function getAllEmpresas($str = false){
        $v = array();
        unset($_SESSION['edit-Empresa']);
		if (!$str){
			$this->c->q("SELECT em_id, em_nombre, em_descr, em_di_id, em_correo, em_fecha, em_deleted FROM empresas LEFT JOIN sucursales ON su_em_id = em_id WHERE em_deleted = '0' AND su_id = '".$_SESSION['sucursal']."' ORDER BY em_id DESC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(1, 2, 4), true);
				array_push($v, $row);
			}
		} else{
			$this->c->q("SELECT * FROM empresas WHERE em_deleted = '0' ORDER BY em_id DESC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(1, 2, 4), true);
				array_push($v, $row);
			}
		}
        return $v;
    }
    /**
    Método que crea una empresa nueva. Tiene relación con la clase Dirección
    
    @bitacora Guardado de una nueva empresa
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveEmpresa(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $dir = new Direccion($this->c);
        $p = $_POST['param'];
        $dir = $dir->createDir(array($p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]));
        $this->createEmpresa(array($p[0], $p[1], $dir, $p[2]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la empresa en la base de datos
    
    @bitacora Creación de una nueva empresa
    @param arreglo con los datos de la empresa
    @return identificador de la empresa agregada en la base de datos
    
    **/
    public function createEmpresa($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$v[1]);
        $this->c->q("INSERT INTO empresas VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$this->d."', '0')");
        return $this->c->last('empresas');        
    }
    /**
    Método que actualiza la información de la empresa
    
    @bitacora Actualización de la empresa
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateEmpresaById(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Empresa']))
            return $arr;
        $empresa = $this->getEmpresaById($_SESSION['edit-Empresa']);
        $dir = new Direccion($this->c);
        $p = $_POST['param'];
        $this->log($this, __FUNCTION__, 'bitacora',$empresa[1].' por '.$p[0]);
        $dir->updateDirById($empresa[3][0], array($p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]));
        $p = $this->u8($p, array(0, 1, 2), false);
        $this->c->q("UPDATE empresas SET em_nombre = '".$p[0]."', em_descr = '".$p[1]."', em_correo = '".$p[2]."', em_fecha = '".$this->d."' WHERE em_id = '".$empresa[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Empresa']);
        return $arr;
    }
    /**
    Método que marca como eliminada una empresa
    
    @bitacora Eliminación de la empresa
    @param identificador de la empresa en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteEmpresa($id){
        $this->hasAccess(get_class($this));
        $empresa = $this->getEmpresaById($id);
        $this->log($this, __FUNCTION__, 'bitacora',$empresa[1]);
        $this->c->q("UPDATE empresas SET em_deleted = '1' WHERE em_id = '".$empresa[0]."' LIMIT 1;");
        unset($_SESSION['edit-Empresa']);
        $suc = new Sucursales();
        $suc->deleteSucursalByEmpresa($empresa[0]);
		$trab = new Trabajadores($this->c);
		$trab->setNewDeptByEmpresa($empresa[0]);
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>