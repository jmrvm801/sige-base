<?php
require_once('Core.php');
require_once('Familias.php');
/**
* Clase Artículos, contiene los métodos necesarios para el manejo del módulo artículos
*
* @author Fernando Carreon
* @version 1.0
*/
class Articulos extends Core{
    public $c;
    public $d;
    /**
    * Constructor de la clase Artículos
    *
    * @bitacora Constructor ejecutado
    * @param void
    * @return void
    */
    public function __construct(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    * Obtiene los datos de un artículo a través de un id.
    *
    * @bitacora Acceso a la información de un artículo
    * @param number|string del artículo a obtener el resultado
    * @return array|bool con los datos del artículo
    */
    public function getArticulosById($id){
        $this->c->q("SELECT * FROM articulos WHERE art_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Articulos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2, 5, 6), true);
        return $data;
    }

    /**
     * Método principal de la clase Artículos
     *
     * @bitacora Acceso al menú de opciones
     * @param $method string method Opción a ejecutar
     * @return array con datos en función del método ejecutado
     */
    public function run($method){
        switch($method){
            case 'getAllArticulos':
                return $this->getAllArticulos();
            case 'saveArticulo':
                return (!isset($_SESSION['edit-Articulos'])) ? $this->saveArticulo() : $this->updateArticulo();
            case 'getArticulosById':
                return $this->getArticulosById($_POST['param']);
            case 'delArticulos':
                return $this->delArticulos($_POST['param']);
            default:
                return array('null');
        }
    }
    /**
    * Método que obtiene toda la información básica de los artículos
    *
    * @bitacora Acceso a la información básica de todos los artículos
    * @param void
    * @return array de los artículos
    */
    public function getAllArticulos($flagx = false){
        $v = array();
        unset($_SESSION['edit-Articulos']);
		  //$this->c->q("SELECT art_id, art_nombre, uni_nombre, fa_nombre, art_code FROM articulos LEFT JOIN unidades ON art_uni_id = uni_id LEFT JOIN familias ON art_fa_id = fa_id LEFT JOIN precios ON pre_art_id = art_id WHERE art_deleted = '0' AND pre_su_id = '".$_SESSION['sucursal']."' ORDER BY art_id DESC;");
      
      $this->c->q("SELECT DISTINCT art_id, art_nombre, uni_nombre, fa_nombre, art_code FROM articulos LEFT JOIN unidades ON art_uni_id = uni_id LEFT JOIN familias ON art_fa_id = fa_id LEFT JOIN precios ON pre_art_id = art_id WHERE art_deleted = '0' ORDER BY art_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3), true);
            array_push($v, $row);
        }
        return $v;
    }

    /**
     * Método que obtiene toda la información básica de los artículos
     *
     * @bitacora Acceso a la información básica de todos los artículos
     * @param $id string id del artículo
     * @return array|mixed|null de los artículos
     */
    public function getAllArticulosById($id){
        unset($_SESSION['edit-Articulos']);
        $this->c->q("SELECT art_id, art_nombre, uni_nombre, fa_nombre, art_code FROM articulos LEFT JOIN unidades ON art_uni_id = uni_id LEFT JOIN familias ON art_fa_id = fa_id WHERE art_id = '".$id."' ORDER BY art_id DESC;");
        $row = $this->c->fr();
        $row = $this->u8($row, array(1, 2, 3), true);
        return $row;
    }
    /**
    * Método que obtiene la lista de artículos en formato para un campo de selección
    *
    * @bitacora Obtención de datos generales de los artículos
    * @param void
    * @return array de artículos
    */
    public function getForArticulos(){
		$arts = array();
		$this->c->q("SELECT DISTINCT art_id, art_nombre, uni_nombre, fa_nombre, art_code FROM articulos LEFT JOIN unidades ON art_uni_id = uni_id LEFT JOIN familias ON art_fa_id = fa_id LEFT JOIN precios ON pre_art_id = art_id WHERE art_deleted = '0' ORDER BY art_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3), true);
            array_push($arts, $row);
        }
        $arr = array();
        for($i = 0; $i < count($arts); $i++)
            array_push($arr, array($arts[$i][0], $arts[$i][4].'. '.$arts[$i][3].': '.$arts[$i][1].' ('.$arts[$i][2].')'));
        return $arr;
    }
    /**
    * Método que crea un artículo.
    *
    * @bitacora Guardado de un nuevo artículo
    * @param void
    * @return array con el estado del guardado
    */
    public function saveArticulo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createArticulo(array($p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    * Método que inserta los datos de un préstamo en la base de datos
    *
    * @bitacora Creación de un nuevo préstamo
    * @param $v array con los datos del préstamo
    * @return number identificador del préstamo agregado en la base de datos
    *
    */
    public function createArticulo($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO articulos VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."', '".$this->d."','0')");
        return $this->c->last('articulos');        
    }
    /**
    * Método que actualiza la información de un artículo
    *
    * @bitacora Actualización de un artículo
    * @param void
    * @return array con el estado del guardado
    */
    public function updateArticulo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Articulos']))
            return $arr;
        $articulo = $this->getArticulosById($_SESSION['edit-Articulos']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $articulo[1].' por '.$p[0]);
        $this->c->q("UPDATE articulos SET art_nombre = '".$p[0]."', art_descr = '".$p[1]."', art_uni_id = '".$p[2]."', art_fa_id = '".$p[3]."', art_codigoSat = '".$p[4]."', art_unidadSat = '".$p[5]."', art_code = '".$p[6]."', art_fecha = '".$this->d."' WHERE art_id = '".$articulo[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Articulos']);
        return $arr;
    }

    /**
     * Método que marca como eliminada un artículo
     *
     * @bitacora Eliminación de un artículo
     * @param $id string de un artículo en la base de datos
     * @return array con el estado del guardado
     */
    public function delArticulos($id){
        $this->hasAccess(get_class($this));
        $articulo = $this->getArticulosById($id);
        unset($_SESSION['edit-Articulos']);
        $this->log($this, __FUNCTION__, 'bitacora', $articulo[1].' por '.$articulo[0]);
        $this->c->q("UPDATE articulos SET art_deleted = '1' WHERE art_id = '".$articulo[0]."' LIMIT 1;");
        return array('true');
    }

    /**
     * Método que limpia a los artículos por familia cuando se elimina en su origen
     *
     * @bitacora
     * @param $id string id del artículo
     * @return void
     */
    public function limpiarArticulosPorFamilia($id){
        $this->c->q("UPDATE articulos SET art_fa_id = '0' WHERE art_fa_id = '".$id."';");
    }

    /**
     * Método que limpia a los artículos por unidad cuando se elimina en su origen
     *
     * @bitacora
     * @param $id string id del artículo
     * @return void
     */
    public function limpiarArticulosPorUnidad($id){
        $this->c->q("UPDATE articulos SET art_uni_id = '0' WHERE art_uni_id = '".$id."';");
    }
    /**
    * Método que cierra la conexión con la base de datos
    *
    * @bitacora Cierre de conexión con la base de datos
    * @param void
    * @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}