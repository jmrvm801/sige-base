<?php
require_once('Core.php');
require_once('Trabajadores.php');
/**
Clase Asistencia, contiene los métodos necesarios para el manejo del módulo asistencias

@author Fernando Carreon
@version 1.0
**/
class Asistencia extends Core{
    public $c;
    public $d;
    public $data;
	public $close;
    public function __construct($c = ''){
		$this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    public function run($method){
        switch($method){
            case 'make':
                return $this->make($_POST['param']);
            break;
            case 'getByCode':
                return $this->getByCode($_POST['param']);
            break;
            case 'getAllAsistencias':
                return $this->getAllAsistencias();
            break;
			case 'finishExtraHous':
				return $this->finishExtraHous($_POST['param']);
			break;
        }
    }
    public function getAllAsistencias(){
        $v = array();
        $this->c->q("SELECT as_id, p2.pe_razon, dep_nombre, p1.pe_razon, em_nombre, su_nombre, as_fecha, as_entrada, as_salida, as_comentario FROM asistencias LEFT JOIN usuarios AS us1 ON us1.us_code = as_code LEFT JOIN personas AS p1 ON us1.us_pe_id = p1.pe_id LEFT JOIN usuarios AS us2 ON us2.us_id = as_session LEFT JOIN personas AS p2 ON us2.us_pe_id = p2.pe_id LEFT JOIN departamentos ON dep_id = us1.us_dep_id LEFT JOIN sucursales ON su_id = dep_su_id LEFT JOIN empresas ON em_id = su_em_id WHERE su_id = '".$_SESSION['sucursal']."' ORDER BY as_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7, 9), true);
			$newtext = '';
			$row[9] = explode(' ', $row[9]);
			for($i = 0, $j = 0; $i < count($row[9]); $i++, $j++){
				$newtext .= $row[9][$i].' ';
				if ($j == 2){
					$newtext .= '<br>';
					$j = 0;
				}
			}
			$row[9] = $newtext;
			$row[7] = $this->numberTimeToString($row[7]);
            $row[8] = $this->numberTimeToString($row[8]);
            array_push($v, $row);
        }
        return $v;
    }
    public function getByCode($code){
        $this->c->q("SELECT pe_razon FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id WHERE us_code = '".$code."' LIMIT 1;");
        $n = ($this->c->nr() == 0) ? array(-1, 'No hay registros') : array(0, WebService::$g->utf8(true, $this->c->r(0)));
        return $n;
    }
    /**
    Método que registra y calcula los minutos que registra un empleado
    
    @bitactora registro de acceso en el reloj
    @param code de empleado
    @return estatus del registro así como el tipo de registro que ha realizado
    **/
    public function make($code){
        $N = date('N');
        $access = date('Hi');
        $f = date('Ymd');
        $this->c->q("SELECT us_id FROM usuarios WHERE us_code = '".$code."' AND us_deleted = '0' LIMIT 1;");
        if ($this->c->nr() == 0)
            return array('error');
        $us_id = $this->c->r(0);
        $this->c->q("SELECT ho_entrada, ho_salida FROM horarios WHERE ho_us_id = '".$us_id."' AND ho_dia = '".$N."' LIMIT 1;");
        $horario = $this->c->fr(); //[0] = entrada, [1] = salida
        $style = 0;
        $this->c->q("SELECT * FROM asistencias WHERE as_fecha = '".$f."' AND as_salida = '' AND as_code = '".$code."';");
        if ($this->c->nr() == 0){
			$this->c->q("SELECT * FROM asistencias WHERE as_fecha = '".$f."' AND as_code = '".$code."';");
			if ($this->c->nr() > 0){
				return array('twice');
			}
            $usr = (isset($_SESSION['us_id'])) ? $_SESSION['us_id'] : '-1';
            $suc = (isset($_SESSION['sucursal'])) ? $_SESSION['sucursal'] : '0';
            $this->c->q("INSERT INTO asistencias VALUES(NULL,'".$usr."','".$code."', '".$f."', '".$N."', '".$access."', '', '0', '".$this->d."', '".$suc."', '0','')");
            $style = 1;
        } else {
            $style = 2;
            $actual = $this->c->fr();
            $resta = 0;
            $minutos = ceil((strtotime($f.''.$actual[5]) - strtotime($f.''.$horario[0])) / 60);
            if ($minutos < 0)
                $resta += ($minutos * -1);
            $minutos = ceil((strtotime($f.''.$access) - strtotime($f.''.$horario[1])) / 60);
            if ($minutos > 0)
                $resta += $minutos;
            $total = ceil((strtotime($f.''.$access) - strtotime($f.''.$actual[5])) / 60);
            $total -= $resta;
            if ($total < 0)
                $total = 0;
			$_SESSION['as_id'] = $actual[0];
            $this->c->q("UPDATE asistencias SET as_salida = '".$access."', as_minutos = '".$total."' WHERE as_id = '".$actual[0]."' LIMIT 1;");
        }
        return array('true', $style);
    }
	public function finishExtraHous($p){
		$p = $this->u8($p, array(1), false);
		$trid = (isset($_SESSION['as_id'])) ? $_SESSION['as_id'] : $p[2];
		$this->c->q("UPDATE asistencias SET as_extras = '".$p[0]."', as_comentario = '".$p[1]."' WHERE as_id = '".$trid."' LIMIT 1;");
		if ($p[2] != ''){
			$this->c->q("UPDATE asistencias SET as_salida = '".date('Hi')."' WHERE as_id = '".$p[2]."' LIMIT 1;");
		} else
			unset($_SESSION['as_id']);
		return array('true');
	}
	public function getAsistenciasByFecha($tipo, $empleado, $code, $monto){
		$asistencias = 0;
		$inasistencias = 0;
		$total = 0;
		$costoDia = 0;
		switch($tipo){
			case '0':
				$total = 6;
				$dactual = date('w');
				$prestar = $dactual - 1;
				$inicioSemana = date('Y-m-d H:i:s', strtotime(date('YmdHis').' - '.$prestar.'day'));
				for($i = 0; $i < 6; $i++){
					$fechaBuscar = date('Ymd', strtotime($inicioSemana.' + '.$i.'day'));
					$this->c->q("SELECT SUM(est_unidades) FROM estajos WHERE est_fecha = '".$fechaBuscar."' AND est_empleado = '".$empleado."';");
					if ($this->c->nr() > 0)
						$asistencias += $this->c->r(0);
				}
				return array($asistencias, $inasistencias, $total, $dactual);
			break;
			case '1':
				$total = 6;
				$dactual = date('w');
				$prestar = $dactual - 1;
				$inicioSemana = date('Y-m-d H:i:s', strtotime(date('YmdHis').' - '.$prestar.'day'));
				for($i = 0; $i < 6; $i++){
					$fechaBuscar = date('Ymd', strtotime($inicioSemana.' + '.$i.'day'));
					$this->c->q("SELECT as_id FROM asistencias WHERE as_fecha = '".$fechaBuscar."' AND as_entrada != '' AND as_code = '".$code."' LIMIT 1;");
					if ($this->c->nr() == 0)
						$inasistencias++;
					else
						$asistencias++;
				}
				return array($asistencias, $inasistencias, $total, $dactual);
			break;
			case '2':
				$diames = date('j');
				if ($diames < 16){ //16
					$inicio = 0;
					$total = 15;
				}else {
					$inicio = 15;
					$total = date('t');
				}
				$diahabil = 0;
				$inicioquincena = date('Y-m').'-01 00:00:00';
				for(;$inicio < $total; $inicio++){
					$fechaBuscar = date('Ymd', strtotime($inicioquincena.' + '.$inicio.'day'));
					$diasemana = date('w', strtotime($fechaBuscar));
					if ($diasemana != '0'){
						$diahabil++;
						$this->c->q("SELECT as_id FROM asistencias WHERE as_fecha = '".$fechaBuscar."' AND as_entrada != '' AND as_code = '".$code."' LIMIT 1;");
						if ($this->c->nr() == 0)
							$inasistencias++;
						else
							$asistencias++;
					}
				}
				return array($asistencias, $inasistencias, $diahabil, $diames);
			break;
		}
	}
	/**
	Método que obtiene las asistencias en el periodo determinado
	
	@bitacora Obtención del comportamiento de asistencia del trabajador
	@param id del trabajador
	@return array
	**/
	public function getStatics($trabajador){
		$this->c->q("SELECT us_id, us_jornada, us_sue_id, sue_monto, us_code, us_limite FROM usuarios LEFT JOIN sueldos ON sue_id = us_sue_id WHERE us_id = '".$trabajador."' LIMIT 1;");
		$datos = $this->c->fr();
		switch($datos[1]){
			case '0': //Estajo
				$x = $this->getAsistenciasByFecha('0', $datos[0], $datos[4], $datos[3]);
				$montoPrestar = round(($datos[3] * $x[0]) / 2, 2);
				if ($montoPrestar > $datos[5])
					$montoPrestar = $datos[5];
				return $montoPrestar;
			break;
			case '1': //Semanal
				$x = $this->getAsistenciasByFecha('1', $datos[0], $datos[4], $datos[3]);
				$montoPrestar = $datos[3] / $x[2];
				$montoPrestar = round(($montoPrestar * $x[0]) / 2, 2);
				if ($montoPrestar > $datos[5])
					$montoPrestar = $datos[5];
				if ($x[0] < $x[3])
					$montoPrestar = 0;
				return $montoPrestar;
			break;
			case '2': //Quincenal
				$x = $this->getAsistenciasByFecha('2', $datos[0], $datos[4], $datos[3]);
				$x[3] = $x[3] / 2;
				$montoPrestar = $datos[3] / $x[2];
				$montoPrestar = round(($montoPrestar * $x[0]) / 2, 2);
				if ($montoPrestar > $datos[5])
					$montoPrestar = $datos[5];
				if ($x[0] < $x[3])
					$montoPrestar = 0;
				return $montoPrestar;
			break;
			case '3': //No aplica
				return $datos[5] / 2;
			break;
		}
	}
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>