<?php
require_once('Core.php');
require_once('Inventarios.php');
require_once('Articulos.php');
require_once('Proveedores.php');
require_once('Core.php');
require_once('Comprobacion.php');
require_once('Precios.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo traspasos

@author Fernando Carreon
@version 1.0
**/
class Traspasos extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Gastos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Traspasos(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un gasto a través de un id.
    
    @bitacora Acceso a la información de un gasto
    @param id del gasto a obtener el resultado
    @return arreglo con los datos del gasto
    **/
    public function getTraspasosById($id){
        $this->c->q("SELECT * FROM traspasos WHERE tra_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Traspasos'] = $id;
        $data = $this->c->fr();
        $this->c->q("SELECT art_fa_id FROM articulos WHERE art_id = (SELECT pre_art_id FROM precios WHERE pre_id = '".$data[2]."');");
        $data[8] = $this->c->r(0);
        $data = $this->u8($data, array(6), true);
        return $data;
    }
    /**
    Método principal de la clase Traspasos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllTraspasos':
                return $this->getAllTraspasos();
            break;
            case 'saveTraspaso':
                return $this->saveTraspaso();
            break;
            case 'getTraspasosById':
                return $this->getTraspasosById($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los traspasos
    
    @bitacora Acceso a la información básica de todos los traspasos
    @param void
    @return arreglo de los traspasos
    **/
    public function getAllTraspasos(){
        $v = array();
        unset($_SESSION['edit-Traspasos']);
		$this->c->q("SELECT tra_id, em_nombre, su_nombre, art_nombre, tra_unidades, pe_razon, tra_su_origen FROM traspasos LEFT JOIN sucursales ON su_id = tra_su_destino LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN precios ON pre_id = tra_pre_id LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN usuarios ON us_id = tra_us_id LEFT JOIN personas ON pe_id = us_pe_id WHERE tra_su_origen = '".$_SESSION['sucursal']."' OR tra_su_destino = '".$_SESSION['sucursal']."' ORDER BY tra_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 5), true);
			$row[6] = ($row[6] == $_SESSION['sucursal']) ? '[Hacia dónde fue ]' : '[Dónde vino]';
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un traspaso.
    
    @bitacora Guardado de un nuevo traspaso
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveTraspaso(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $pre = new Precios();
        $id = $pre->getArticulosBySucursalAndId(array($p[0], $p[1]));
        $inv = new Inventarios();
        $inv2 = $inv->existInventario($p[3], $id[0][1]);
        if ($inv2 == -1){
			$precioant = $pre->getPreciosById($id[0][0]);
			$pre->createPrecio(array($precioant[1], $p[3], $precioant[3], $precioant[4], $precioant[5], $precioant[6], $precioant[9], $precioant[10]));
			$inv2 = $inv->existInventario($p[3], $id[0][1]);
		}
        $cm = $inv->getInventarioByPrecio($p[1]);
        $mov = new Movimientos($this->c);
        $v = $mov->checkForUnitInWarehouse($cm[0], $p[2]);
        if (!$v)
            return array('insufficient');
        $idm = $this->createTraspaso(array($p[1], $p[0], $p[3], $p[2], $p[4]));
        $mov->createMovimiento($cm[0], 1, $p[2], '[SALIDA] Traspaso de inventario con id '.$idm);
        $mov->createMovimiento($inv2, 0, $p[2], '[ENTRADA] Traspaso de inventario con id '.$idm);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un traspaso en la base de datos
    
    @bitacora Creación de un nuevo traspaso
    @param arreglo con los datos del traspaso
    @return identificador del traspaso agregado en la base de datos
    
    **/
    public function createTraspaso($v){
        $v = $this->u8($v, array(4), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO traspasos VALUES(NULL, '".$_SESSION['us_id']."', '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$this->d."')");
        return $this->c->last('traspasos');        
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>