<?php
require_once('Core.php');
/**
Clase Percepciones, contiene los métodos necesarios para el manejo del módulo catalogos

@author Fernando Carreon
@version 1.0
**/
class Percepciones extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Percepciones
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Percepciones($c = ''){
		$this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una unidad a través de un id.
    
    @bitacora Acceso a la información de la unidad
    @param id de la unidad a obtener el resultado
    @return arreglo con los datos de la unidad
    **/
    public function getPercepcionById($id){
        $this->c->q("SELECT * FROM percepciones WHERE per_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Percepciones'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
    /**
    Método principal de la clase Percepciones
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllPercepciones':
                return $this->getAllPercepciones();
            break;
            case 'savePercepcion':
                return (!isset($_SESSION['edit-Percepciones'])) ? $this->savePercepcion() : $this->updatePercepciones();
            break;
            case 'getPercepcionById':
                return $this->getPercepcionById($_POST['param']);
            break;
            case 'delPercepciones':
                return $this->deletePercepciones($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las percepciones
    
    @bitacora Acceso a la información básica de todos las percepciones
    @param void
    @return arreglo de las percepciones
    **/
    public function getAllPercepciones(){
        $v = array();
        unset($_SESSION['edit-Percepciones']);
        $this->c->q("SELECT per_id, per_nombre, per_monto FROM percepciones WHERE per_deleted = '0' ORDER BY per_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea una unidad nueva.
    
    @bitacora Guardado de una nueva unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function savePercepcion(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createPercepcion(array($p[0], $p[1]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la unidad en la base de datos
    
    @bitacora Creación de una nueva unidad
    @param arreglo con los datos de la unidad
    @return identificador de la unidad agregada en la base de datos
    
    **/
    public function createPercepcion($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO percepciones VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0')");
        return $this->c->last('percepciones');        
    }
    /**
    Método que actualiza la información de una unidad
    
    @bitacora Actualización de una unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updatePercepciones(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Percepciones']))
            return $arr;
        $unidad = $this->getPercepcionById($_SESSION['edit-Percepciones']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$unidad[1].' por '.$p[0]);
        $this->c->q("UPDATE percepciones SET per_nombre = '".$p[0]."', per_monto = '".$p[1]."', per_fecha = '".$this->d."' WHERE per_id = '".$unidad[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Percepciones']);
        return $arr;
    }
    /**
    Método que marca como eliminada una unidad
    
    @bitacora Eliminación de una unidad
    @param identificador de una unidad en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deletePercepciones($id){
        $this->hasAccess(get_class($this));
        $unidad = $this->getPercepcionById($id);
        unset($_SESSION['edit-Percepciones']);
        $this->log($this, __FUNCTION__, 'bitacora', $unidad[1]);
        $this->c->q("UPDATE percepciones SET per_deleted = '1' WHERE per_id = '".$unidad[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>