<?php
require_once('Core.php');
require_once('Articulos.php');
/**
Clase Unidades, contiene los métodos necesarios para el manejo del módulo unidades

@author Fernando Carreon
@version 1.0
**/
class Unidades extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Unidades
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Unidades($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de una unidad a través de un id.
    
    @bitacora Acceso a la información de la unidad
    @param id de la unidad a obtener el resultado
    @return arreglo con los datos de la unidad
    **/
    public function getUnidadById($id){
        $this->c->q("SELECT * FROM unidades WHERE uni_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Unidades'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
    /**
    Método principal de la clase Unidades
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllUnidades':
                return $this->getAllUnidades();
            break;
            case 'saveUnidad':
                return (!isset($_SESSION['edit-Unidades'])) ? $this->saveUnidad() : $this->updateUnidades();
            break;
            case 'getUnidadById':
                return $this->getUnidadById($_POST['param']);
            break;
            case 'delUnidades':
                return $this->deleteUnidades($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de las unidades
    
    @bitacora Acceso a la información básica de todos las unidades
    @param void
    @return arreglo de las unidades
    **/
    public function getAllUnidades(){
        $v = array();
        unset($_SESSION['edit-Unidades']);
        $this->c->q("SELECT uni_id, uni_nombre FROM unidades WHERE uni_deleted = '0' ORDER BY uni_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea una unidad nueva.
    
    @bitacora Guardado de una nueva unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveUnidad(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createUnidad(array($p[0], $p[1]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de la unidad en la base de datos
    
    @bitacora Creación de una nueva unidad
    @param arreglo con los datos de la unidad
    @return identificador de la unidad agregada en la base de datos
    
    **/
    public function createUnidad($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO unidades VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0')");
        return $this->c->last('unidades');        
    }
    /**
    Método que actualiza la información de una unidad
    
    @bitacora Actualización de una unidad
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateUnidades(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Unidades']))
            return $arr;
        $unidad = $this->getUnidadById($_SESSION['edit-Unidades']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora',$unidad[1].' por '.$p[0]);
        $this->c->q("UPDATE unidades SET uni_nombre = '".$p[0]."', uni_descr = '".$p[1]."', uni_fecha = '".$this->d."' WHERE uni_id = '".$unidad[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Unidades']);
        return $arr;
    }
    /**
    Método que marca como eliminada una unidad
    
    @bitacora Eliminación de una unidad
    @param identificador de una unidad en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteUnidades($id){
        $this->hasAccess(get_class($this));
        $unidad = $this->getUnidadById($id);
        unset($_SESSION['edit-Unidades']);
        $this->log($this, __FUNCTION__, 'bitacora', $unidad[1]);
        $this->c->q("UPDATE unidades SET uni_deleted = '1' WHERE uni_id = '".$unidad[0]."' LIMIT 1;");
        $art = new Articulos();
        $art->limpiarArticulosPorUnidad($unidad[0]);
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
			$this->c->cl();
    }
}
?>