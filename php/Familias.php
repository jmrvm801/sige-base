<?php
require_once('Core.php');
require_once('Articulos.php');
/**
Clase Familias, contiene los métodos necesarios para el manejo del módulo familias

@author Fernando Carreon
@version 1.0
**/
class Familias extends Core{
    public $c;
    public $d;
	public $close;
	/**
    Constructor de la clase Familias
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Familias($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
	/**
    Obtiene los datos de una familia a través de un id.
    
    @bitacora Acceso a la información de la familia
    @param id de la familia a obtener el resultado
    @return arreglo con los datos del familia
    **/
    public function getFamiliaById($id){
        $this->c->q("SELECT * FROM familias WHERE fa_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Familias'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(1, 2), true);
        return $data;
    }
	/**
    Método principal de la clase Familias
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllFamilias':
                return $this->getAllFamilias();
            break;
            case 'saveFamilia':
                return (!isset($_SESSION['edit-Familias'])) ? $this->saveFamilia() : $this->updateFamilias();
            break;
            case 'getFamiliaById':
                return $this->getFamiliaById($_POST['param']);
            break;
            case 'delFamilias':
                return $this->deleteFamilias($_POST['param']);
            break;
        }
    }
	/**
    Método que obtiene toda la información básica de las familias
    
    @bitacora Acceso a la información básica de todos las familias
    @param void
    @return arreglo de las familias
    **/
    public function getAllFamilias(){
        $v = array();
        unset($_SESSION['edit-Familias']);
        $this->c->q("SELECT fa_id, fa_nombre FROM familias WHERE fa_deleted = '0' ORDER BY fa_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
        return $v;
    }
	/**
    Método que crea una familia nueva.
    
    @bitacora Guardado de una nueva familia
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveFamilia(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createFamilia($p);
        $arr[0] = 'true';
        return $arr;
    }
	/**
    Método que inserta los datos de la familia en la base de datos
    
    @bitacora Creación de una nueva familia
    @param arreglo con los datos de la familia
    @return identificador de la familia agregada en la base de datos
    
    **/
    public function createFamilia($v){
        $v = $this->u8($v, array(0, 1), false);
		$this->log($this, __FUNCTION__, 'bitacora',$v[0]);
        $this->c->q("INSERT INTO familias VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$this->d."', '0')");
        return $this->c->last('familias');        
    }
	/**
    Método que actualiza la información de una familia
    
    @bitacora Actualización de un familia
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateFamilias(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Familias']))
            return $arr;
        $familia = $this->getFamiliaById($_SESSION['edit-Familias']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(0, 1), false);
		$this->log($this, __FUNCTION__, 'bitacora',$familia[1].' por '.$p[0]);
        $this->c->q("UPDATE familias SET fa_nombre = '".$p[0]."', fa_descr = '".$p[1]."', fa_fecha = '".$this->d."' WHERE fa_id = '".$familia[0]."' LIMIT 1;");
        $arr[0] = 'true';
        unset($_SESSION['edit-Familias']);
        return $arr;
    }
	/**
    Método que marca como eliminada una familia
    
    @bitacora Eliminación de una familia
    @param identificador de una familia en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deleteFamilias($id){
        $this->hasAccess(get_class($this));
        $familia = $this->getFamiliaById($id);
        unset($_SESSION['edit-Familias']);
		$this->log($this, __FUNCTION__, 'bitacora',$familia[3]);
        $this->c->q("UPDATE familias SET fa_deleted = '1' WHERE fa_id = '".$familia[0]."' LIMIT 1;");
        $art = new Articulos();
        $art->limpiarArticulosPorFamilia($familia[0]);
        return array('true');
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
        	$this->c->cl();
    }
}
?>