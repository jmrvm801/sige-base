<?php
require_once('Core.php');
require_once('Direccion.php');
require_once('Personas.php');
require_once('Login.php');
/**
Clase Trabajadores, contiene los métodos necesarios para el manejo del módulo trabajadores

@author Fernando Carreon
@version 1.0
**/
class Trabajadores extends Core{
    public $c;
    public $d;
	public $close;
    /**
    Constructor de la clase Trabajadores
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Trabajadores($c = ''){
        $this->close = ($c == '');
        $this->c = ($c == '') ? new db() : $c;
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un trabajador a través de un id.
    
    @bitacora Acceso a la información de un trabajador
    @param id del trabajador a obtener el resultado
    @return arreglo con los datos del trabajador
    **/
    public function getTrabajadorById($id){
        $this->c->q("SELECT * FROM usuarios WHERE us_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Trabajadores'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(3), true);
        $per = new Personas($this->c);
        $data[1] = $per->getPersonaById($data[1]);
        $data[6] = '';
        $dir = new Direccion($this->c);
        $data[1][5] = $dir->getDirById($data[1][5]);
        $per = new Permisos($this->c);
        $data[12] = $per->getPermisosById($data[0]);
        return $data;
    }
    /**
    Método principal de la clase Trabajadores
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllTrabajadores':
                return $this->getAllTrabajadores();
            break;
            case 'saveTrabajador':
                return (!isset($_SESSION['edit-Trabajadores'])) ? $this->saveTrabajador() : $this->updateTrabajador();
            break;
            case 'getTrabajadorById':
                return $this->getTrabajadorById($_POST['param']);
            break;
            case 'getEmpleadosByDepId':
                return $this->getEmpleadosByDepId($_POST['param']);
            break;
            case 'getForEmpleadosByDepId':
                return $this->getForEmpleadosByDepId($_POST['param']);
            break;
            case 'delTrabajador':
                return $this->delTrabajador($_POST['param']);
            break;
            case 'saveConfTarde':
                return $this->saveConfTarde($_POST['param']);
            break;
			case 'getTrabajadorByEstajos':
				return $this->getTrabajadorByEstajos();
			break;
			case 'getPermissionById':
				return $this->getPermissionById($_POST['param']);
			break;
			case 'setPermissionById':
				return $this->setPermissionById($_POST['param']);
			break;
            case 'getHorariosById':
            case 'saveHorario':
            case 'getUniqueHorarioById':
            case 'delHorario':
                if ($method == 'getHorariosById')
                    $_SESSION['trabajador-id'] = $_POST['param'];
                $horario = new Horarios($this->c);
                return $horario->run($method);
            break;
        }
    }
	public function setPermissionById($p){
		$this->c->q("DELETE FROM accesos WHERE ac_us_id = '".$_SESSION['permissionId']."';");
		for($i = 0; $i < count($p); $i++){
			if ($p[$i][1] == '1'){
				$this->c->q("INSERT INTO accesos VALUES(NULL,'".$_SESSION['permissionId']."','".$p[$i][0]."','".date('YmdHis')."')");
			}
		}
		unset($_SESSION['permissionId']);
		return array('true');
	}
	public function getPermissionById($id){
		$d = new db();
		/*
		$this->c->q("SELECT us_admin FROM usuarios WHERE us_id = '".$id."' LIMIT 1;");
		if ($this->c->r(0) != '1')
			return array('notpermisible');
		*/
		$_SESSION['permissionId'] = $id;
		$this->c->q("SELECT su_id, em_nombre, su_nombre FROM empresas LEFT JOIN sucursales ON em_id = su_em_id WHERE em_deleted = '0' AND su_deleted = '0' ORDER BY em_nombre, su_nombre;");
		$v = array();
		while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2), true);
			$d->q("SELECT ac_fecha FROM accesos WHERE ac_us_id = '".$id."' AND ac_su_id = '".$row[0]."' LIMIT 1;");
			$row[3] = ($d->nr() > 0) ? $d->r(0) : '0';
            array_push($v, $row);
        }
		$d->cl();
		return $v;
	}
	public function getTrabajadorByEstajos(){
		$this->c->q("SELECT us_id, pe_razon FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id WHERE us_jornada = '0' ORDER BY pe_razon;");
		//AND us_dep_id IN (SELECT dep_id FROM departamentos WHERE dep_su_id = '".$_SESSION['sucursal']."')
		$v = array();
		while($row = $this->c->fr()){
            $row = $this->u8($row, array(1), true);
            array_push($v, $row);
        }
		return $v;
	}
    /**
    Método que guarda la configuración de los ratardos para la nómina
    
    @bitacora Cambio en la configuración de acción de retardos del trabajo
    @param identificador del trabajador
    @return arreglo con el estado del guardado
    **/
    public function saveConfTarde($id){
        $this->c->q("UPDATE usuarios SET us_retraso = '".$id."' WHERE us_id = '".$_SESSION['trabajador-id']."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que obtiene todos los empleados por departamento
    
    @bitacora Acceso a los datos generales de los trabajadores por departamento
    @param identificador del departamento
    @return arreglo de empleados
    **/
    public function getEmpleadosByDepId($id){
        $v = array();
        $this->c->q("SELECT us_id, us_code, pe_razon, em_nombre, su_nombre, dep_nombre, us_puesto FROM usuarios LEFT JOIN departamentos ON dep_id = us_dep_id LEFT JOIN sucursales ON su_id = dep_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN personas ON pe_id = us_pe_id WHERE us_deleted = '0' AND us_dep_id = '".$id."' ORDER BY pe_razon ASC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(2, 3, 4, 5, 6), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que obtiene la lista de trabajadores en formato para un campo de selección
    
    @bitacora Obtención de datos generales de los trabajadores
    @param void
    @return arreglo de trabajadores
    **/
    public function getForEmpleadosByDepId($id){
        $arr = array();
        $emp = $this->getEmpleadosByDepId($id);
        for($i = 0; $i < count($emp); $i++)
            array_push($arr, array($emp[$i][0], $emp[$i][2].'. '.$emp[$i][6]));
        return $arr;
    }
    /**
    Método que obtiene toda la información básica de los trabajadores
    
    @bitacora Acceso a la información básica de todos los trabajadores
    @param void
    @return arreglo de los trabajadores
    **/
    public function getAllTrabajadores($sm = false){
        $v = array();
		unset($_SESSION['edit-Trabajadores']);
		if (!$sm && !isset($_POST['login'])){
			$this->c->q("SELECT us_id, us_code, pe_razon, us_puesto FROM usuarios LEFT JOIN personas ON pe_id = us_pe_id LEFT JOIN departamentos ON dep_id = us_dep_id LEFT JOIN sucursales ON su_id = dep_su_id WHERE us_deleted = '0' AND (su_id = '".$_SESSION['sucursal']."' OR us_dep_id = '0') ORDER BY pe_razon;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(2, 3), true);
				array_push($v, $row);
			}
		} else {
			$this->c->q("SELECT us_id, us_code, pe_razon, em_nombre, su_nombre, dep_nombre, us_puesto FROM usuarios LEFT JOIN departamentos ON dep_id = us_dep_id LEFT JOIN sucursales ON su_id = dep_su_id LEFT JOIN empresas ON em_id = su_em_id LEFT JOIN personas ON pe_id = us_pe_id WHERE us_deleted = '0' ORDER BY pe_razon ASC;");
			while($row = $this->c->fr()){
				$row = $this->u8($row, array(2, 3, 4, 5, 6), true);
				array_push($v, $row);
			}
		}
        return $v;
    }
    /**
    Método que crea un trabajador.
    
    @bitacora Guardado de un nuevo trabajador
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveTrabajador(){
        $this->hasAccess(get_class($this));
		$p = $_POST['param'];
		$this->c->q("SELECT pe_correo FROM personas WHERE pe_id IN(SELECT us_pe_id FROM usuarios WHERE us_deleted = '0') AND pe_correo = '".$p[0][5]."' LIMIT 1;");
		if ($this->c->nr() > 0)
			return array('same');
        $arr = array('error');
        $p[0][6] = $this->StringDateToNumber($p[0][6]);
        $dir = new Direccion($this->c);
        $iddir = $dir->createDir($p[1]);
        $per = new Personas($this->c);
        $idpersona = $per->createPersona(array($p[0][0], $p[0][1], $p[0][2], $p[0][3], $iddir, $p[0][5], $p[0][4], $p[0][6], $p[0][7], $p[0][8]));
        $idtrabajador = $this->createTrabajador(array($idpersona, $p[2][0], $p[2][1], $p[2][2], $p[2][3], $p[2][4], $p[2][6], $p[2][7], $p[2][8]));
        $per = new Permisos($this->c);
        for($i = 0; $i < count($p[3]); $i++)
            $per->setPermiso($idtrabajador, $p[3][$i][0], $p[3][$i][1]);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que genera un código para el trabajador. Este código se utilizará en el reloj checador
    
    @bitacora Generación de un nuevo código de empleado
    @param void
    @eturn código del empleado generado
    **/
    public function getCode(){
        do{
            $key1 = rand(1000,9999);
            $this->c->q("SELECT us_code FROM usuarios WHERE us_code = '".$key1."' LIMIT 1;");
        } while($this->c->nr() > 0);
        return $key1;
    }
    /**
    Método que inserta los datos de un préstamo en la base de datos
    
    @bitacora Creación de un nuevo préstamo
    @param arreglo con los datos del préstamo
    @return identificador del préstamo agregado en la base de datos
    
    **/
    public function createTrabajador($v){
        $v[5] = WebService::$g->crypto(true, $v[5]);
        $v = $this->u8($v, array(2), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO usuarios VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."', '".$_SESSION['us_id']."', '".$this->getCode()."', '".$v[7]."', '".$this->d."', '0', '0', '".$v[8]."', '0')");
        return $this->c->last('usuarios');        
    }
    /**
    Método que actualiza la información de un trabajador
    
    @bitacora Actualización de un trabajador
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateTrabajador(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Trabajadores']))
            return $arr;
        $trabajador = $this->getTrabajadorById($_SESSION['edit-Trabajadores']);
        $p = $_POST['param'];
        $dir = new Direccion($this->c);
        @$dir->updateDirById($trabajador[1][5][0], $p[1]);
        $per = new Personas($this->c);
        @$per->updatePersonaById($trabajador[1][0], array($p[0][0], $p[0][1], $p[0][2], $p[0][3], $trabajador[1][5][0], $p[0][5], $p[0][4], $p[0][6], $p[0][7], $p[0][8]));
        $p = $this->u8($p, array(2), false);
        $this->log($this, __FUNCTION__, 'bitacora', $trabajador[1][1].' por '.$p[0][1]);
		$p[2][4] = ($p[2][4] != '') ? "us_password = '".WebService::$g->crypto(true, $p[2][4])."', " : "";
        $this->c->q("UPDATE usuarios SET us_dep_id = '".$p[2][0]."', us_puesto = '".$p[2][1]."', us_jornada = '".$p[2][2]."', us_google = '".$p[2][3]."', ".$p[2][4]." us_admin = '".$p[2][6]."', us_sue_id = '".$p[2][7]."', us_limite = '".$p[2][8]."', us_fecha = '".$this->d."' WHERE us_id = '".$trabajador[0]."' LIMIT 1;");
        $per = new Permisos($this->c);
        for($i = 0; $i < count($p[3]); $i++)
            $per->setPermiso($trabajador[0], $p[3][$i][0], $p[3][$i][1]);
        $arr[0] = 'true';
        unset($_SESSION['edit-Trabajadores']);
        return $arr;
    }
    /**
    Método que marca como eliminada un trabajador
    
    @bitacora Eliminación de un trabajador
    @param identificador de un trabajador en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delTrabajador($id){
        $this->hasAccess(get_class($this));
        $trabajador = $this->getTrabajadorById($id);
        unset($_SESSION['edit-Trabajadores']);
        $this->log($this, __FUNCTION__, 'bitacora', $trabajador[1][1]);
        $this->c->q("UPDATE usuarios SET us_deleted = '1' WHERE us_id = '".$trabajador[0]."' LIMIT 1;");
        return array('true');
    }
	public function setNewDeptByEmpresa($id){
		$this->c->q("UPDATE usuarios SET us_dep_id = '0' WHERE us_dep_id IN (SELECT dep_id FROM departamentos WHERE dep_su_id IN (SELECT su_id FROM sucursales WHERE su_em_id = '".$id."'));");
	}
    /**
    Método que limpia los sueldos de los trabajadores cuyo origen se ha eliminado
    **/
    public function limpiarTrabajadoresPorSueldos($id){
        $this->c->q("UPDATE usuarios SET us_sue_id = '0' WHERE us_sue_id = '".$id."';");
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
		if ($this->close)
			$this->c->cl();
    }
}
/**
Clase DÉBIL Horarios, contiene los métodos necesarios para el manejo del módulo trabajadores

@author Fernando Carreon
@version 1.0
**/
class Horarios extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Horarios
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Horarios($c){
        $this->d = date('YmdHis');
        $this->c = $c;
    }
    /**
    Método principal de la clase Horarios
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getHorariosById':
                return array($this->getRetraso(), $this->getHorariosById());
            break;
            case 'saveHorario':
                return (!isset($_SESSION['edit-Horarios'])) ? $this->saveHorario() : $this->updateHorario();
            break;
            case 'getUniqueHorarioById':
                return $this->getUniqueHorarioById($_POST['param']);
            break;
            case 'delHorario':
                return $this->delHorario($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene la configuración del retraso de tiempo para un trabajador
    
    @bitacora Acceso a la información de retraso de tiempo para un trabajador
    **/
    public function getRetraso(){
        $this->c->q("SELECT us_retraso FROM usuarios WHERE us_id = '".$_SESSION['trabajador-id']."' LIMIT 1;");
        return $this->c->r(0);
    }
    /**
    Obtiene los datos de un horario a través de un id.
    
    @bitacora Acceso a la información de un horario
    @param id del horario a obtener el resultado
    @return arreglo con los datos del horario
    **/
    public function getHorariosById(){
        unset($_SESSION['edit-Horarios']);
        $arr = array();
        $this->c->q("SELECT ho_id, ho_dia, ho_entrada, ho_salida FROM horarios WHERE ho_us_id = '".$_SESSION['trabajador-id']."' ORDER BY ho_id");
        while($row = $this->c->fr()){
            $row[2] = $this->numberTimeToString($row[2]);
            $row[3] = $this->numberTimeToString($row[3]);
            array_push($arr, $row);
        }
        return $arr;
    }
    /**
    Obtiene los datos de un horario específicio a través de un id
    
    @bitacora Acceso a la información de un horario
    @param id del horario a obtener el resultado
    @return arreglo con los datos del proveedor
    **/
    public function getUniqueHorarioById($id){
        $this->c->q("SELECT ho_id, ho_dia, ho_entrada, ho_salida FROM horarios WHERE ho_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Horarios'] = $id;
        $data = $this->c->fr();
        $data[2] = $this->numberTimeToString($data[2]);
        $data[3] = $this->numberTimeToString($data[3]);
        return $data;
    }
    /**
    Método que crea un horario.
    
    @bitacora Guardado de un nuevo horario
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveHorario(){
        $this->hasAccess('Trabajadores');
        $this->createHorario();
        return array('true', $_SESSION['trabajador-id']);
    }
    /**
    Método que inserta los datos de un préstamo en la base de datos
    
    @bitacora Creación de un nuevo préstamo
    @param arreglo con los datos del préstamo
    @return identificador del préstamo agregado en la base de datos
    
    **/
    public function createHorario(){
        $p = $_POST['param'];
        $p[1] = $this->stringTimeToNumber($p[1]);
        $p[2] = $this->stringTimeToNumber($p[2]);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['trabajador-id']);
        $this->c->q("INSERT INTO horarios VALUES(NULL,'".$_SESSION['trabajador-id']."','".$p[0]."','".$p[1]."','".$p[2]."','".$this->d."')");
        return $this->c->last('horarios');
    }
    /**
    Método que actualiza la información de un horario
    
    @bitacora Actualización de un horario
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateHorario(){
        $this->hasAccess('Trabajadores');
        $arr = array('error');
        if (!isset($_SESSION['edit-Horarios']))
            return $arr;
        $p = $_POST['param'];
        $p[1] = $this->stringTimeToNumber($p[1]);
        $p[2] = $this->stringTimeToNumber($p[2]);
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['trabajador-id']);
        $this->c->q("UPDATE horarios SET ho_dia = '".$p[0]."', ho_entrada = '".$p[1]."', ho_salida = '".$p[2]."', ho_fecha = '".$this->d."' WHERE ho_id = '".$_SESSION['edit-Horarios']."' LIMIT 1;");
        unset($_SESSION['edit-Horarios']);
        $arr[0] = 'true';
        $arr[1] = $_SESSION['trabajador-id'];
        return $arr;
    }
    /**
    Método que marca como eliminada un horario
    
    @bitacora Eliminación de un horario
    @param identificador de un horario en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function delHorario($id){
        $this->hasAccess('Trabajadores');
        $this->log($this, __FUNCTION__, 'bitacora', $_SESSION['trabajador-id']);
        $this->c->q("DELETE FROM horarios WHERE ho_id = '".$id."' LIMIT 1;");
        unset($_SESSION['edit-Horarios']);
        return array('true', $_SESSION['trabajador-id']);
    }
}
?>