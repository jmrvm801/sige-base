<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Proveedores.php');
require_once('Core.php');
require_once('Comprobacion.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Gastos extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Gastos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Gastos(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un gasto a través de un id.
    
    @bitacora Acceso a la información de un gasto
    @param id del gasto a obtener el resultado
    @return arreglo con los datos del gasto
    **/
    public function getGastosById($id){
        $this->c->q("SELECT * FROM gastos WHERE gas_ref = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Gastos'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(4, 5, 7), true);
		if (file_exists('../data/'.$data[0].'.png'))
			$data[13] = 'png';
		else if (file_exists('../data/'.$data[0].'.jpg'))
			$data[13] = 'jpg';
		else if (file_exists('../data/'.$data[0].'.gif'))
			$data[13] = 'gif';
		else
			$data[13] = 'null';
        return $data;
    }
    /**
    Método principal de la clase Gastos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getAllGastos':
                return $this->getAllGastos();
            break;
            case 'saveGasto':
                return $this->saveGasto();
            break;
            case 'getGastosById':
                return $this->getGastosById($_POST['param']);
            break;
            case 'setIdComprobacion':
            case 'comprobarGasto':
            case 'getComprobacionesById':
            case 'delComprobante':
            case 'getComprobanteById':
                $comp = new Comprobacion($this->c, 1);
                return $comp->run($method);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los gastos
    
    @bitacora Acceso a la información básica de todos los gastos
    @param void
    @return arreglo de los gastos
    **/
    public function getAllGastos(){
        $sql = (@$_SESSION['BancoSocios'][1] == '0') ? "gas_us_creator = '".$_SESSION['us_id']."'" : "gas_dep_id = '".$_SESSION['sucursal']."'";
        $v = array();
        unset($_SESSION['edit-Gastos']);
		$this->c->q("SELECT gas_ref, gas_pro_nombre, gas_monto, gas_formaPago, gas_us_res, gas_cuenta, gas_descr, cat_nombre, sub_nombre FROM gastos LEFT JOIN subcatalogos ON gas_sub_id = sub_id LEFT JOIN catalogos ON cat_id = sub_cat_id WHERE ".$sql." ORDER BY gas_ref DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 4, 5, 6, 7, 8), true);
			$row[3] = $this->getFormaPago($row[3]);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un gasto.
    
    @bitacora Guardado de un nuevo gasto
    @param void
    @return arreglo con el estado del guardado
    **/
    public function saveGasto(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $this->createGasto($p);
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $saldoorigen = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$p[0]', '$_SESSION[us_id]', '$p[8]', '7', '$p[8]', '$p[4]', '$p[1]', '$p[2]', '$p[3]', '', '', '$saldoorigen', '".($saldoorigen - $p[1])."', '0', '0', '".date('YmdHis')."')");
        $saldoorigen -= $p[1];
        $this->c->q("UPDATE usuarios SET us_amount = '$saldoorigen' WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
		if ($p[9] != 'nophoto')
			$this->dataImageToFile($p[9], '../data/'.$p[0]);
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que inserta los datos de un gasto en la base de datos
    
    @bitacora Creación de un nuevo gasto
    @param arreglo con los datos del gasto
    @return identificador del gasto agregado en la base de datos
    
    **/
    public function createGasto($v){
        $v = $this->u8($v, array(4, 5, 6), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO gastos VALUES('".$v[0]."', '".$_SESSION['us_id']."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$v[6]."', '".$this->d."', '0','0', '".$v[7]."', '".$v[8]."')");
        return $this->c->last('gastos');        
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>