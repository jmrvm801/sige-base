<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Inventarios.php');
require_once('Proveedores.php');
require_once('Core.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Precios extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Precios
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Precios(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Obtiene los datos de un precio a través de un id.
    
    @bitacora Acceso a la información de un precio
    @param id del precio a obtener el resultado
    @return arreglo con los datos del precio
    **/
    public function getPreciosById($id){
        $this->c->q("SELECT * FROM precios WHERE pre_id = '".$id."' LIMIT 1;");
        if ($this->c->nr() == 0)
            return false;
        $_SESSION['edit-Precios'] = $id;
        $data = $this->c->fr();
        $data = $this->u8($data, array(3, 6), true);
        return $data;
    }
    /**
    Método principal de la clase Precios
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getMetaPrecio':
                $art = new Articulos();
                $proved = new Proveedores();
                return array($art->getForArticulos(), $proved->getForProveedores());
            break;
            case 'getAllPrecios':
                return $this->getAllPrecios();
            break;
            case 'savePrecio':
                return (!isset($_SESSION['edit-Precios'])) ? $this->savePrecio() : $this->updateActivo();
            break;
            case 'getPreciosById':
                return $this->getPreciosById($_POST['param']);
            break;
            case 'delPrecios':
                return $this->deletePrecios($_POST['param']);
            break;
            case 'getArticulosBySucursalAndFamilia':
                return $this->getArticulosBySucursalAndFamilia($_POST['param']);
            break;
            case 'getArticulosByPrecioId':
                return $this->getArticulosByPrecioId($_POST['param']);
            break;
        }
    }
    /**
    Método que obtiene toda la información básica de los precios
    
    @bitacora Acceso a la información básica de todos los precios
    @param void
    @return arreglo de los precios
    **/
    public function getAllPrecios(){
        $v = array();
        unset($_SESSION['edit-Precios']);
        $this->c->q("SELECT pre_id, pre_nombre, art_nombre, pre_mayoreo, pre_precioc, pe_razon FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN proveedores ON pre_pro_id = pro_id LEFT JOIN personas ON pro_pe_id = pe_id WHERE pre_deleted = '0' AND pre_su_id = '".$_SESSION['sucursal']."' ORDER BY pre_id DESC;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 5), true);
            array_push($v, $row);
        }
        return $v;
    }
    /**
    Método que crea un precio.
    
    @bitacora Guardado de un nuevo precio
    @param void
    @return arreglo con el estado del guardado
    **/
    public function savePrecio(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        $p = $_POST['param'];
        $v = $this->isEquals($p[0], $p[1]);
        if ($v != '0')
            return array('equals');
        $this->createPrecio(array($p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7]));
        $arr[0] = 'true';
        return $arr;
    }
    /**
    Método que determina si existe un precio con los el mismo ide de artículo y sucursal
    
    @bitacora Acceso al método de verificación de precios
    @param identificador del artículo
    @param identificador de la sucursal
    @return Id del precio si es que existe o 0 en caso contrario.
    **/
    public function isEquals($articulo, $sucursal){
        $this->c->q("SELECT pre_id FROM precios WHERE pre_art_id = '".$articulo."' AND pre_su_id = '".$sucursal."' AND pre_deleted = '0' LIMIT 1;");
        return ($this->c->nr() > 0) ? $this->c->r(0) : 0;
    }
    /**
    Método que inserta los datos de un precio en la base de datos
    
    @bitacora Creación de un nuevo precio
    @param arreglo con los datos del precio
    @return identificador del precio agregado en la base de datos
    
    **/
    public function createPrecio($v){
        $v = $this->u8($v, array(0, 1), false);
        $this->log($this, __FUNCTION__, 'bitacora', $v[0]);
        $this->c->q("INSERT INTO precios VALUES(NULL, '".$v[0]."', '".$v[1]."', '".$v[2]."', '".$v[3]."', '".$v[4]."', '".$v[5]."', '".$this->d."', '0', '".$v[6]."', '".$v[7]."')");
        $precio = $this->c->last('precios');
        $inv = new Inventarios();
        $inv->createInventario($v[1], $precio);
        return $this->c->last('precios');        
    }
    /**
    Método que actualiza la información de un precio
    
    @bitacora Actualización de un precio
    @param void
    @return arreglo con el estado del guardado
    **/
    public function updateActivo(){
        $this->hasAccess(get_class($this));
        $arr = array('error');
        if (!isset($_SESSION['edit-Precios']))
            return $arr;
        $precios = $this->getPreciosById($_SESSION['edit-Precios']);
        $p = $_POST['param'];
        $p = $this->u8($p, array(2, 5), false);
        $v = $this->isEquals($p[0], $p[1]);
        if ($v != '0' && $v != $precios[0])
            return array('equals');
        $this->log($this, __FUNCTION__, 'bitacora',$precios[3].' por '.$p[3]);
        $this->c->q("UPDATE precios SET pre_art_id = '".$p[0]."', pre_su_id = '".$p[1]."', pre_nombre = '".$p[2]."', pre_mayoreo = '".$p[3]."', pre_menudeo = '".$p[4]."', pre_observaciones = '".$p[5]."', pre_pro_id = '".$p[6]."', pre_precioc = '".$p[7]."', pre_fecha = '".$this->d."' WHERE pre_id = '".$precios[0]."' LIMIT 1;");
        $arr[0] = 'true';
        $inv = new Inventarios();
        $inv->updateInventario($p[1], $precios[0]);
        
        unset($_SESSION['edit-Precios']);
        return $arr;
    }
    /**
    Método que marca como eliminado un precio
    
    @bitacora Eliminación de un precio
    @param identificador de un precio en la base de datos
    @return arreglo con el estado del guardado
    **/
    public function deletePrecios($id){
        $this->hasAccess(get_class($this));
        $precio = $this->getPreciosById($id);
        unset($_SESSION['edit-Precios']);
        $this->log($this, __FUNCTION__, 'bitacora', $precio[3]);
        $this->c->q("UPDATE precios SET pre_deleted = '1' WHERE pre_id = '".$precio[0]."' LIMIT 1;");
        return array('true');
    }
    /**
    Método que obtiene los artículos filtrados por sucursal y familia
    
    @bitacora acceso a la información de artículos filtrados por sucursal y familia
    @param arreglo con la información del artículo
    @return arreglo de artículos coincidentes
    **/
    public function getArticulosBySucursalAndFamilia($p){ //P[0] = sucursal, p[1] = familia
        $this->c->q("SELECT pre_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, art_codigoSat, art_unidadSat, in_actual FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON uni_id = art_uni_id LEFT JOIN inventarios ON in_pre_id = pre_id WHERE pre_su_id = '".$p[0]."' AND art_fa_id = '".$p[1]."' AND pre_deleted = '0' ORDER BY art_nombre;"); //AND in_actual > 0
        $list = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7), true);
            array_push($list, $row);
        }
        return $list;
    }
    /**
    Método que obtiene los artículos filtrados por sucursal y familia
    
    @bitacora acceso a la información de artículos filtrados por sucursal e id
    @param arreglo con la información del artículo
    @return arreglo de artículos coincidentes
    **/
    public function getArticulosBySucursalAndId($p){ //P[0] = sucursal, p[1] = id
        $this->c->q("SELECT pre_id, art_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, art_codigoSat, art_unidadSat FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE pre_su_id = '".$p[0]."' AND pre_id = '".$p[1]."' AND pre_deleted = '0' ORDER BY art_nombre;");
        $list = array();
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7), true);
            array_push($list, $row);
        }
        return $list;
    }
    /**
    Método que obtiene los artículos filtrados por id de precio
    
    @bitacora acceso a la información de artículos filtrados por id de precio
    @param arreglo con la información del artículo
    @return arreglo del artículo coincidente
    **/
    public function getArticulosByPrecioId($p){
        $this->c->q("SELECT pre_id, pre_nombre, art_code, art_nombre, fa_nombre, uni_nombre, art_codigoSat, art_unidadSat FROM precios LEFT JOIN articulos ON art_id = pre_art_id LEFT JOIN familias ON fa_id = art_fa_id LEFT JOIN unidades ON uni_id = art_uni_id WHERE pre_id = '".$p."' AND pre_deleted = '0' ORDER BY art_nombre LIMIT 1;");
        $row = $this->c->fr();
        $row = $this->u8($row, array(1, 2, 3, 4, 5, 6, 7), true);
        return $row;
    }
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>