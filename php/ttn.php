<?php
date_default_timezone_set("America/Mexico_City");

include_once('db.php');
include_once('gral.php');
include_once('Login.php');

		$m = array("accesos", "activosfijos", "articulos", "asistencias", "bitacora", "catalogos", "clientes", "comprobaciones", "deducciones", "departamentos", "direcciones", "ealmacen", "embarques", "empresas", "estajos", "familias", "gastos", "horarios", "inventarios", "logs", "modulos", "movimientos", "nominas", "notas", "ordenarticulos", "ordendecompra", "pagos", "percepciones", "permisos", "personas", "precios", "presave", "prestamos", "proveedores", "salmacen", "subcatalogos", "sucursales", "sueldos", "traspasos", "unidades", "usuarios", "ventas");
		$v = true;
		$x = array(4, 8, 10, 12, 5, 5, 6, 10, 5, 6, 8, 12, 10, 7, 8, 5, 13, 6, 5, 7, 2, 7, 8, 6, 11, 14, 20, 5, 5, 12, 11, 5, 15, 6, 12, 6, 7, 5, 8, 5, 15, 21);
		$c = new db();
		for($i = 0; $i < count($m); $i++){
			$c->q("DESCRIBE ".$m[$i]);
			if ($c->nr() != $x[$i])
				$v = false;
		}
		$c->cl();
		if (!$v){
			die('PROGRAMA ALTERADO');
		}
?>