<?php
require_once('Core.php');
require_once('Articulos.php');
require_once('Proveedores.php');
require_once('Core.php');
require_once('Comprobacion.php');
/**
Clase Sueldos, contiene los métodos necesarios para el manejo del módulo sueldos

@author Fernando Carreon
@version 1.0
**/
class Bancos extends Core{
    public $c;
    public $d;
    /**
    Constructor de la clase Gastos
    
    @bitacora Constructor ejecutado
    @param void
    @return void
    **/
    public function Bancos(){
        $this->c = new db();
        $this->d = date('YmdHis');
    }
    /**
    Método principal de la clase Gastos
    
    @bitacora Acceso al menú de opciones
    @param method Opción a ejecutar
    @return json con datos en función del método ejecutado
    **/
    public function run($method){
        switch($method){
            case 'getBancoDetails':
                return $this->getBancoDetails();
            case 'getEmployDetails':
                return $this->getEmployDetails();
            case 'nuevaVenta':
                return $this->nuevaVenta($_POST['param']);
            case 'nuevaVentaEmpleado':
                return $this->nuevaVentaEmpleado($_POST['param']);
            case 'nuevaOtros':
                return $this->nuevaOtros($_POST['param']);
            case 'getFinantialData':
                return $this->getFinantialData($_POST['id']);
            case 'getFinantialEmployeeData':
                return $this->getFinantialEmployeeData($_POST['id']);
            case 'saveTraspaso':
                return $this->saveTraspaso($_POST['param']);
            case 'saveTrapasoEmployeed':
                return $this->saveTrapasoEmployeed($_POST['param']);
            case 'saveProveedorEmployeed':
                return $this->saveProveedorEmployeed($_POST['param']);
            case 'getMovementById':
                return $this->getMovementById($_POST['id']);
            case 'getEdoCuenta':
                return $this->getEdoCuenta($_POST['param']);
            case 'getIngresosPara':
                return $this->getIngresosPara($_POST['param']);
            case 'getEgresosPara':
                return $this->getEgresosPara($_POST['param']);
            case 'getTraspasosPara':
                return $this->getTraspasosPara($_POST['param']);
            case 'getSaldos':
                return $this->getSaldos();
            case 'getAllMovementsById':
                return $this->getAllMovementsById($_POST['param']);
            case 'getSPayments':
                return $this->getSPayments();
            case 'cancelPaymentData':
                return $this->cancelPaymentData($_POST['id'], $_POST['obs']);
            case 'AceptarPago':
                return $this->AceptarPago($_POST['id']);
            case 'RechazarPago':
                return $this->RechazarPago($_POST['id']);
            case 'CubiertoPago':
                return $this->CubiertoPago($_POST['id']);
            case 'exportData':
                return $this->exportData($_POST['param']);
            case 'setIdComprobacion':
            case 'comprobarProveedor':
            case 'getComprobacionesById':
            case 'delComprobante':
            case 'getComprobanteById':
                $comp = new Comprobacion($this->c, 6);
                return $comp->run($method);
        }
    }
    public function exportData($param){
        // $param = '<table>'.base64_decode($param).'</table>';
        $_SESSION['cookie_export'] = $param;
        return array('true');
    }
    public function AceptarPago($id){
        $this->c->q("UPDATE bancomovimientos SET be_comprobado = '1' WHERE be_id = '$id' LIMIT 1;");
        return array('true');
    }
    public function RechazarPago($id){
        $this->c->q("UPDATE bancomovimientos SET be_comprobado = '2' WHERE be_id = '$id' LIMIT 1;");
        return array('true');
    }
    public function CubiertoPago($id){
        $this->c->q("UPDATE bancomovimientos SET be_comprobado = '3' WHERE be_id = '$id' LIMIT 1;");
        return array('true');
    }
    public function cancelPaymentData($id, $obs){
        $this->c->q("SELECT com_ref, com_monto FROM comprobaciones WHERE com_id = '$id' LIMIT 1;");
        $data = $this->c->fr();
        $this->c->q("SELECT be_cubierto FROM bancomovimientos WHERE be_id = '$data[0]' LIMIT 1;");
        $cubierto = $this->c->r(0);
        $cubierto -= $data[1];
        $this->c->q("UPDATE bancomovimientos SET be_cubierto = '$cubierto' WHERE be_id = '$data[0]' LIMIT 1;");
        $this->c->q("DELETE FROM comprobaciones WHERE com_id = '$id' LIMIT 1;");
        // $this->c->q("INSERT INTO cancelaciones VALUES(NULL, '$id', '$_SESSION[us_id]', '$obs', '".date('YmdHis')."')");
        return array('true');
    }
    public function getSPayments(){
        $this->c->q("SELECT be_id, be_fecha, pe_razon, be_title, be_amount, be_cubierto FROM bancomovimientos LEFT JOIN usuarios ON us_id = be_us_creator LEFT JOIN personas ON pe_id = us_pe_id WHERE be_type = '7' ORDER BY be_fecha DESC");
        $array = array();
        while($row = $this->c->fr()){
            $row[2] = utf8_encode($row[2]);
            array_push($array, $row);
        }
        return $array;
    }
    public function getAllMovementsById($id){
        $v = array(array(), array(), '', array(0, 0, 0, 0, 0, 0, 0, 0));
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$id';");
        $v[0] = $this->c->fr(0);
        $c = new db();
        $this->c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$id' LIMIT 1;");
        $v[2] = utf8_encode($this->c->r(0));
        $this->c->q("SELECT be_id, be_fecha, be_type, pe_razon, be_antescuenta, be_amount, be_despuescuenta FROM bancomovimientos LEFT JOIN usuarios ON us_id = be_us_res LEFT JOIN personas ON pe_id = us_pe_id WHERE be_us_res = '$id' OR be_us_creator = '$id' AND be_type NOT IN('1', '2', '3') ORDER BY be_fecha DESC");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(3), false);
            $row[1] = date('d-m-Y H:i', strtotime($row[1]));
            if ($row[2] == '5'){
                $c->q("SELECT be_us_creator, be_us_res, be_antes, be_ahora FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                $res = $c->fr();
                if ($res[0] == $id){
                    $row[4] = $res[2];
                    $row[6] = $res[3];
                } else {
                    $row[2] = '6';
                    $c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$res[0]' lIMIT 1;");
                    $row[3] = utf8_decode($c->r(0));
                }
            }
            if ($row[2] == '7'){
                $c->q("SELECT be_us_creator, be_us_res, be_antes, be_ahora, be_cubierto, be_comprobado, be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                $res = $c->fr();
                $c->q("SELECT be_comprobado FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                $status = $c->r(0);
                $arrayss = array('[Pendiente]', '[Autorizado]', '[Invalidado]', '[Cubierto]');
                // $d->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$row[3]' LIMIT 1;");
                $row[3] = $arrayss[$status].' '.utf8_encode($res[6]);
                // $c->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$res[1]' lIMIT 1;");
                // $row[3] = utf8_decode($res[1]);
                if ($res[5] == '2')
                    $v[3][5] += $row[5];
            }
            switch($row[2]){
                case '3':
                    $v[3][0] += $row[5];
                break;
                case '4':
                    $v[3][1] += $row[5];
                break;
                case '5':
                    $v[3][7] += $row[5];
                break;
                case '6':
                    $v[3][2] += $row[5];
                break;
                case '7':
                    $v[3][4] += $row[5];
                break;
            }
            array_push($v[1], $row);
        }
        $v[3][6] = $v[3][4] - $v[3][5];
        $v[3][3] = $v[3][0] + $v[3][1] + $v[3][2];
        $c->cl();
        return $v;
    }
    public function getSaldos(){
        $array = array();
        $this->c->q("SELECT us_id, pe_razon, us_amount FROM usuarios LEFT JOIN personas ON us_pe_id = pe_id LEFT JOIN permisos ON us_id = per_us_id WHERE us_deleted = '0' AND per_mo_id = 'Gastos' AND per_auth = '1' ORDER BY us_amount DESC");
        while($row = $this->c->fr()){
            $row[1] = utf8_encode($row[1]);
            array_push($array, $row);
        }
        return $array;
    }
    public function getTraspasosPara($p){
        $p[0] = $this->stringDateToNumber($p[0]).'000000';
        $p[1] = $this->stringDateToNumber($p[1]).'235959';
        if ($p[2] == '0'){
            $filter = false;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('3') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') ORDER BY be_fecha DESC";
        } else {
            $filter = true;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('3','5') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') AND (be_us_res = '$p[2]' OR be_us_creator = '$p[2]') ORDER BY be_fecha DESC";
        }
        $array = array();
        $this->c->q($query);
        $d = new db();
        while($row = $this->c->fr()){
            $admmit = true;
            $appear = true;
            switch($row[2]){
                case '3':
                    if ($filter){
                        if ($row[3] != $p[2]) {
                            $appear = false;
                        }
                    }
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                    $row[5] = 'Banco socios';
                break;
                case '5':
                    if ($row[3] == $p[2]){
                        $row[2] = '6';
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_res = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                        $row[5] = utf8_encode($d->r(0));
                    } else{
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                        $row[5] = utf8_encode($d->r(0));
                    }
                break;
            }
            if ($admmit && $appear)
                array_push($array, $row);
        }
        $d->cl();
        return $array;
    }
    public function getEgresosPara($p){
        $p[0] = $this->stringDateToNumber($p[0]).'000000';
        $p[1] = $this->stringDateToNumber($p[1]).'235959';
        if ($p[2] == '0'){
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('3') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') ORDER BY be_fecha DESC";
        } else {
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('5', '7') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') AND (be_us_res = '$p[2]' OR be_us_creator = '$p[2]') ORDER BY be_fecha DESC";
        }
        $array = array();
        $this->c->q($query);
        $d = new db();
        while($row = $this->c->fr()){
            $admmit = true;
            switch($row[2]){
                case '3':
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '5':
                    if ($row[3] == $p[2]){
                        $admmit = false;
                    } else{
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                    }
                break;
                case '7':
                    $d->q("SELECT be_comprobado, be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                    $status = $d->fr();
                    $arrayss = array('[Pendiente]', '[Autorizado]', '[Invalidado]', '[Cubierto]');
                    // $d->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$row[3]' LIMIT 1;");
                    $row[3] = $arrayss[$status[0]].' '.utf8_encode($status[1]);
                    // $d->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$row[3]' LIMIT 1;");
                    // $row[3] = utf8_encode($row[3]);
                break;
            }
            if ($admmit)
                array_push($array, $row);
        }
        $d->cl();
        return $array;
    }
    public function getIngresosPara($p){
        $p[0] = $this->stringDateToNumber($p[0]).'000000';
        $p[1] = $this->stringDateToNumber($p[1]).'235959';
        if ($p[2] == '0'){
            $filter = false;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('1', '2') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') ORDER BY be_fecha DESC";
        } else {
            $filter = true;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_amount FROM bancomovimientos WHERE be_type IN('3', '4', '5') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') AND (be_us_res = '$p[2]' OR be_us_creator = '$p[2]') ORDER BY be_fecha DESC";
        }
        $array = array();
        $this->c->q($query);
        $d = new db();
        while($row = $this->c->fr()){
            $appear = true;
            $admmit = true;
            switch($row[2]){
                case '1':
                case '2':
                    $d->q("SELECT be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '3':
                    if ($filter){
                        if ($row[3] != $p[2]) {
                            $appear = false;
                        }
                    }
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '4':
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '5':
                    if ($row[3] == $p[2]){
                        $row[2] = '6';
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                    } else{
                        $admmit = false;
                    }
                break;
            }
            if ($admmit && $appear)
                array_push($array, $row);
        }
        $d->cl();
        return $array;
    }
    public function getEdoCuenta($p){
        $p[0] = $this->stringDateToNumber($p[0]).'000000';
        $p[1] = $this->stringDateToNumber($p[1]).'235959';
        if ($p[2] == '0'){
            $filter = false;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_antes, be_amount, be_ahora FROM bancomovimientos WHERE be_type IN('1', '2', '3') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') ORDER BY be_fecha DESC";
        } else {
            $filter = true;
            $query = "SELECT be_id, be_fecha, be_type, be_us_res, be_antescuenta, be_amount, be_despuescuenta FROM bancomovimientos WHERE be_type NOT IN('1', '2') AND (be_fecha >= '$p[0]' AND be_fecha <= '$p[1]') AND (be_us_res = '$p[2]' OR be_us_creator = '$p[2]') ORDER BY be_fecha DESC";
        }
        $array = array();
        $this->c->q($query);
        $d = new db();
        while($row = $this->c->fr()){
            $row[7] = 'No aplica';
            $appear = true;
            switch($row[2]){
                case '1':
                case '2':
                    $d->q("SELECT be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '3':
                    if ($filter){
                        if ($row[3] != $p[2]) {
                            $appear = false;
                        }
                    }
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
                case '5':
                    if ($row[3] == $p[2]){
                        $row[2] = '6';
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                    } else{
                        $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[3]' LIMIT 1;");
                        $row[3] = utf8_encode($d->r(0));
                    }
                break;
                case '7':
                    $d->q("SELECT be_comprobado, be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                    $status = $d->fr();
                    $arrayss = array('Pendiente', 'Autorizado', 'Invalidado', 'Cubierto');
                    // $d->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$row[3]' LIMIT 1;");
                    $row[3] = utf8_encode($status[1]);
                    $row[7] = $arrayss[$status[0]];
                break;
                case '4':
                    $d->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id LEFT JOIN bancomovimientos ON be_us_creator = us_id WHERE be_id = '$row[0]' LIMIT 1;");
                    $row[3] = utf8_encode($d->r(0));
                break;
            }
            if ($appear)
                array_push($array, $row);
        }
        $d->cl();
        return $array;
    }
    public function getMovementById($id){
        $this->c->q("SELECT * FROM bancomovimientos WHERE be_id = '$id' LIMIT 1;");
        $row = $this->c->fr();
        $row[7] = $this->getFormaPago($row[7]);
        $this->c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[1]' LIMIT 1;");
        $row[1] = $this->c->r(0);
        $row[16] = array();
        $row[17] = 'nophoto';
        $row[15] = date('d-m-Y H:i', strtotime($row[15]));
        switch($row[3]){
            case '3':
                $this->c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[2]' LIMIT 1;");
                $row[2] = $this->c->r(0);
            break;
            case '5':
                $this->c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$row[2]' LIMIT 1;");
                $row[2] = $this->c->r(0);
            break;
            case '7':
                // $this->c->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$row[2]' LIMIT 1;");
                $row[2] = $row[2];
                $this->c->q("SELECT * FROM comprobaciones WHERE com_ref = '$row[0]' ORDER BY com_id DESC; ");
                while($res = $this->c->fr()){
                    $res = $this->u8($res, array(6, 7, 8), true);
                    $res[9] = date('d-m-Y H:i', strtotime($res[9]));
                    $res[5] = $this->getFormaPago($res[5]);
                    array_push($row[16], $res);
                }
                if (file_exists('../data/'.$row[0].'.png'))
                    $row[17] = 'png';
                else if (file_exists('../data/'.$row[0].'.jpg'))
                    $row[17] = 'jpg';
                else if (file_exists('../data/'.$row[0].'.gif'))
                    $row[17] = 'gif';
                else
                    $row[17] = 'null';
            break;
            default:
                $row[2] = 'No aplica';
            break;
        }
        $row = $this->u8($row, array(2, 4, 5, 8), true);
        return $row;
    }
    public function getFinantialEmployeeData($id = 0){
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $saldo = $this->c->r(0);
        return array($saldo);
    }
    public function getFinantialData($id){
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$id' LIMIT 1;");
        $saldo = $this->c->r(0);
        $this->c->q("SELECT bs_saldo FROM bancosaldos WHERE bs_id = '1' LIMIT 1;");
        $saldoBanco = $this->c->r(0);
        return array($saldo, $saldoBanco);
    }

    public function saveProveedorEmployeed($data){
        $data = $this->u8($data, array(2, 5, 6), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $saldoorigen = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[1]', '$_SESSION[us_id]', '$data[0]', '7', '$data[2]', '$data[6]', '$data[3]', '$data[4]', '$data[5]', '', '', '$saldoorigen', '".($saldoorigen - $data[3])."', '0', '0', '".date('YmdHis')."')");
        $saldoorigen -= $data[3];
        $this->c->q("UPDATE usuarios SET us_amount = '$saldoorigen' WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }
    public function saveTrapasoEmployeed($data){
        $data = $this->u8($data, array(2, 5, 6), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $saldoorigen = $this->c->r(0);
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$data[0]' LIMIT 1;");
        $saldodestino = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[1]', '$_SESSION[us_id]', '$data[0]', '5', '$data[2]', '$data[6]', '$data[3]', '$data[4]', '$data[5]', '$saldoorigen', '".($saldoorigen - $data[3])."', '$saldodestino', '".($saldodestino + $data[3])."', '', '0', '".date('YmdHis')."')");
        $saldoorigen -= $data[3];
        $saldodestino += $data[3];
        $this->c->q("UPDATE usuarios SET us_amount = '$saldoorigen' WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $this->c->q("UPDATE usuarios SET us_amount = '$saldodestino' WHERE us_id = '$data[0]' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }
    public function saveTraspaso($data){
        $data = $this->u8($data, array(2, 5, 6), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT bs_saldo FROM bancosaldos WHERE bs_id = '1' LIMIT 1;");
        $saldobanco = $this->c->r(0);
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$data[0]' LIMIT 1;");
        $saldoempleado = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[1]', '$_SESSION[us_id]', '$data[0]', '3', '$data[2]', '$data[6]', '$data[3]', '$data[4]', '$data[5]', '$saldobanco', '".($saldobanco - $data[3])."', '$saldoempleado', '".($saldoempleado + $data[3])."', '', '0', '".date('YmdHis')."')");
        $saldobanco -= $data[3];
        $saldoempleado += $data[3];
        $this->c->q("UPDATE bancosaldos SET bs_last = '".date('YmdHis')."', bs_saldo = '$saldobanco' WHERE bs_id = '1' LIMIT 1;");
        $this->c->q("UPDATE usuarios SET us_amount = '$saldoempleado' WHERE us_id = '$data[0]' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }
    public function nuevaOtros($data){
        $data = $this->u8($data, array(1, 4, 5), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT bs_saldo FROM bancosaldos WHERE bs_id = '1' LIMIT 1;");
        $saldo = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[0]', '$_SESSION[us_id]', '', '2', '$data[1]', '$data[5]', '$data[2]', '$data[3]', '$data[4]', '$saldo', '".($saldo + $data[2])."', '', '', '', '0', '".date('YmdHis')."')");
        $saldo += $data[2];
        $this->c->q("UPDATE bancosaldos SET bs_last = '".date('YmdHis')."', bs_saldo = '$saldo' WHERE bs_id = '1' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }

    public function nuevaVenta($data){
        $data = $this->u8($data, array(1, 4, 5), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT bs_saldo FROM bancosaldos WHERE bs_id = '1' LIMIT 1;");
        $saldo = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[0]', '$_SESSION[us_id]', '', '1', '$data[1]', '$data[5]', '$data[2]', '$data[3]', '$data[4]', '$saldo', '".($saldo + $data[2])."', '', '', '', '0', '".date('YmdHis')."')");
        $saldo += $data[2];
        $this->c->q("UPDATE bancosaldos SET bs_last = '".date('YmdHis')."', bs_saldo = '$saldo' WHERE bs_id = '1' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }
    public function nuevaVentaEmpleado($data){
        $data = $this->u8($data, array(1, 4, 5), false);
        $this->hasAccess(get_class($this));
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        $saldo = $this->c->r(0);
        $this->c->q("INSERT INTO bancomovimientos VALUES('$data[0]', '$_SESSION[us_id]', '$_SESSION[us_id]', '4', '$data[1]', '$data[5]', '$data[2]', '$data[3]', '$data[4]', '', '', '$saldo', '".($saldo + $data[2])."', '', '0', '".date('YmdHis')."')");
        $saldo += $data[2];
        $this->c->q("UPDATE usuarios SET us_amount = '$saldo' WHERE us_id = '$_SESSION[us_id]' LIMIT 1;");
        return array('true', $this->c->last('bancomovimientos'));
    }
    /**
    Método que obtiene toda la información básica de los gastos
    
    @bitacora Acceso a la información básica de todos los gastos
    @param void
    @return arreglo de los gastos
    **/
    public function getBancoDetails(){
        $v = array(array(), array());
        $this->c->q("SELECT * FROM bancosaldos WHERE bs_id = '1';");
        $v[0] = $this->c->fr(0);
        $v[0][2] = date('d-m-y H:i', strtotime($v[0][2]));
        $this->c->q("SELECT be_id, be_fecha, be_type, pe_razon, be_antes, be_amount, be_ahora FROM bancomovimientos LEFT JOIN usuarios ON us_id = be_us_res LEFT JOIN personas ON pe_id = us_pe_id WHERE be_type IN('1', '2', '3') ORDER BY be_fecha DESC LIMIT 250;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(3), false);
            $row[1] = date('d-m-Y H:i', strtotime($row[1]));
            array_push($v[1], $row);
        }
        return $v;
    }
    public function getEmployDetails(){
        $v = array(array(), array());
        $this->c->q("SELECT us_amount FROM usuarios WHERE us_id = '$_SESSION[us_id]';");
        $v[0] = $this->c->fr(0);
        $c = new db();
        $this->c->q("SELECT be_id, be_fecha, be_type, pe_razon, be_antescuenta, be_amount, be_despuescuenta FROM bancomovimientos LEFT JOIN usuarios ON us_id = be_us_res LEFT JOIN personas ON pe_id = us_pe_id WHERE be_us_res = '$_SESSION[us_id]' OR be_us_creator = '$_SESSION[us_id]' AND be_type NOT IN('1', '2', '3') ORDER BY be_fecha DESC LIMIT 250;");
        while($row = $this->c->fr()){
            $row = $this->u8($row, array(3), false);
            $row[7] = 'Realizado';
            $row[1] = date('d-m-Y H:i', strtotime($row[1]));
            if ($row[2] == '5'){
                $c->q("SELECT be_us_creator, be_us_res, be_antes, be_ahora FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                $res = $c->fr();
                if ($res[0] == $_SESSION['us_id']){
                    $row[4] = $res[2];
                    $row[6] = $res[3];
                } else {
                    $row[2] = '6';
                    $c->q("SELECT pe_razon FROM personas LEFT JOIN usuarios ON us_pe_id = pe_id WHERE us_id = '$res[0]' lIMIT 1;");
                    $row[3] = utf8_decode($c->r(0));
                }
            }
            if ($row[2] == '7'){
                $c->q("SELECT be_us_creator, be_us_res, be_antes, be_ahora, be_comprobado, be_title FROM bancomovimientos WHERE be_id = '$row[0]' LIMIT 1;");
                $res = $c->fr();
                // $c->q("SELECT pe_razon FROM personas LEFT JOIN proveedores ON pro_pe_id = pe_id WHERE pro_id = '$res[1]' lIMIT 1;");
                $row[3] = utf8_encode($res[5]);
                $status = array('Pendiente', 'Autorizado', 'Invalidado', 'Cubierto');
                $row[7] = $status[$res[4]];
            }
            array_push($v[1], $row);
        }
        $c->cl();
        return $v;
    }
    
    /**
    Método que cierra la conexión con la base de datos
    
    @bitacora Cierre de conexión con la base de datos
    @param void
    @return void
    **/
    function __destruct(){
        $this->c->cl();
    }
}
?>