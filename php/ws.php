<?php
date_default_timezone_set("America/Mexico_City");

include_once('db.php');
include_once('gral.php');
include_once('Login.php');

include_once('ActivoFijo.php');
include_once('Articulos.php');
include_once('Asistencia.php');
include_once('Clientes.php');
include_once('Catalogos.php');
include_once('Comprobacion.php');
include_once('Core.php');
include_once('Deducciones.php');
include_once('Departamentos.php');
include_once('Direccion.php');
include_once('EAlmacen.php');
include_once('Embarques.php');
include_once('Empresas.php');
include_once('Estajos.php');
include_once('Familias.php');
include_once('Gastos.php');
include_once('Inventarios.php');
include_once('Nomina.php');
include_once('OrdenDeCompra.php');
include_once('Personas.php');
include_once('Percepciones.php');
include_once('Precios.php');
include_once('Prestamos.php');
include_once('Proveedores.php');
include_once('PuntoVenta.php');
include_once('SAlmacen.php');
include_once('SubCatalogo.php');
include_once('Sucursales.php');
include_once('Trabajadores.php');
include_once('Traspasos.php');
include_once('Unidades.php');
include_once('Usuarios.php');
include_once('Sueldos.php');
include_once('Bancos.php');





class WebService{
    /** @var $g gral**/
    public static $g = '';

    public function __construct(){
        WebService::$g = new Gral();
        $this->reUpdate();
		$this->checkDB();
    }
	
	public function checkDB(){
		// $m = array("accesos", "activosfijos", "articulos", "asistencias", "bitacora", "catalogos", "clientes", "comprobaciones", "deducciones", "departamentos", "direcciones", "ealmacen", "embarques", "empresas", "estajos", "familias", "gastos", "horarios", "inventarios", "logs", "modulos", "movimientos", "nominas", "notas", "ordenarticulos", "ordendecompra", "pagos", "percepciones", "permisos", "personas", "precios", "presave", "prestamos", "proveedores", "salmacen", "subcatalogos", "sucursales", "sueldos", "traspasos", "unidades", "usuarios", "ventas");
		// $v = true;
		// $x = array(4, 8, 10, 12, 5, 5, 6, 10, 5, 6, 8, 12, 10, 7, 8, 5, 13, 6, 5, 7, 2, 7, 8, 6, 11, 14, 20, 5, 5, 12, 11, 5, 15, 6, 12, 6, 7, 5, 8, 5, 16, 21);
		// $c = new db();
		// for($i = 0; $i < count($m); $i++){
		// 	$c->q("DESCRIBE ".$m[$i]);
		// 	if ($c->nr() != $x[$i])
		// 		$v = false;
		// }
		// $c->cl();
		// if (!$v)
		// 	WebService::$g->kill('Classes corrupted');
	}
    public function reUpdate(){
        foreach($_COOKIE as $clave => $valor){
            WebService::$g->cook($clave, $valor, 7200);
        }
    }
    public function run($class){
        switch($class){
            case 'login':
                new Login($_POST['method']);
            break;
			case 'security':
				$c = new db();
				$id = WebService::$g->xss($_POST['id']);
				if ($_SESSION['accesoTotal'] != '1')
					WebService::$g->kill(WebService::$g->json(true, array('denied')));
				$c->q("SELECT ac_su_id FROM accesos WHERE ac_us_id = '".$_SESSION['us_id']."' AND ac_su_id = '".$id."' LIMIT 1;");
				if ($c->nr() == '0'){
					$c->cl();
					WebService::$g->kill(WebService::$g->json(true, array('notpermisible')));
				}
				$_SESSION['su_id'] = $id;
				$_SESSION['sucursal'] = $id;
				WebService::$g->cook('sucursal', $id, 7200);
				$c->cl();
				WebService::$g->kill(WebService::$g->json(true, array('true')));
			break;
            case 'BasicData':
                $c = new db();
                $array = array();
                $empresa = new Empresas($c);
                array_push($array, $empresa->getAllEmpresas(true));
                $sucursales = new Sucursales($c);
                array_push($array, $sucursales->getAllSucursales(true));
                $departamentos = new Departamentos($c);
                array_push($array, $departamentos->getAllDepartamentos(true));
                $familia = new Familias($c);
                array_push($array, $familia->getAllFamilias());
                $unidad = new Unidades($c);
                array_push($array, $unidad->getAllUnidades());
                $modulo = new Modulos($c);
                array_push($array, $modulo->getModulos());
                $sueldo = new Sueldos($c);
                array_push($array, $sueldo->getAllSueldos());
				$catalogo = new Catalogos($c);
                array_push($array, $catalogo->getAllCatalogos());
				$subcatalogo = new SubCatalogo($c);
				array_push($array, $subcatalogo->getAllSubcatalogos());
				$trabajadores = new Trabajadores($c);
				array_push($array, $trabajadores->getAllTrabajadores(true));
				$proveedores = new Proveedores($c);
				array_push($array, $proveedores->getAllProveedores());
				$clientes = new Clientes($c);
				array_push($array, $clientes->getAllClientes());
				$percepciones = new Percepciones($c);
				array_push($array, $percepciones->getAllPercepciones());
				$deducciones = new Deducciones($c);
				array_push($array, $deducciones->getAllDeducciones());
                $c->cl();
				//print_r($array);
                WebService::$g->kill(WebService::$g->json(true, $array));
            break;
            case 'permisos':
                $c = new db();
                $perm = new Permisos($c);
                $run = $perm->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
                $c->cl();
            break;
            case 'asistencia':
                $emp = new Asistencia();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'empresas':
                $emp = new Empresas();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'nominas':
                $emp = new Nomina();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'sucursales':
                $emp = new Sucursales();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'departamentos':
                $emp = new Departamentos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'familias':
                $emp = new Familias();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'clientes':
                $emp = new Clientes();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'proveedores':
                $emp = new Proveedores();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'sueldos':
                $emp = new Sueldos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'activoFijo':
                $emp = new ActivoFijo();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'precios':
                $emp = new Precios();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'unidades':
                $emp = new Unidades();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'articulos':
                $emp = new Articulos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'trabajadores':
                $emp = new Trabajadores();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'embarques':
                $emp = new Embarques();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'gastos':
                $emp = new Gastos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'prestamos':
                $emp = new Prestamos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'inventarios':
                $emp = new Inventarios();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'traspasos':
                $emp = new Traspasos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'ealmacen':
                $emp = new EAlmacen();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'salmacen':
                $emp = new SAlmacen();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'ordendecompra':
                $emp = new OrdenDeCompra();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            case 'PuntoVenta':
                $emp = new PuntoVenta();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'catalogos':
                $emp = new Catalogos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'percepciones':
                $emp = new Percepciones();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'deducciones':
                $emp = new Deducciones();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'estajos':
                $emp = new Estajos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'subcatalogos':
                $emp = new SubCatalogo();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
			case 'bancos':
                $emp = new Bancos();
                $run = $emp->run($_POST['method']);
                WebService::$g->kill(WebService::$g->json(true, $run));
            break;
            default:
                WebService::$g->kill(WebService::$g->json(true, array('not-registered')));
            break;
        }
    }
}

$w = new WebService();
$w->run($_POST['class']);