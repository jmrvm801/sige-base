/* globals Core, M, Form, ui, Fecha, Ax, u, Ic, Dropdown, z */
/*hola*/
Object.defineProperty(String.prototype, 'replaceByClassName', {
    value() {
        let t = this;
        switch(t.toString()){
            case 'Prestamos':
                return 'Préstamos';
            case 'Nominas':
                return 'Nóminas';
            case 'PuntoVenta':
                return 'Ventas realizadas';
            case 'FormalizarOrdenCompra':
                return 'Formalizar compras';
			case 'Catalogos':
                return 'Catálogos';
			case 'SubCatalogo':
                return 'Subcátalogo';
			case 'EntradaAlmacen':
                return 'Entradas';
			case 'SalidaAlmacen':
                return 'Salidas';
			case 'BancoSocios':
                return 'Banco socios';
			default:
                return t;
        }
    }
});
let B;
const version = '1.0025 (10 abr 2019 22:58)';
$('document').ready(() => {
	// if (location.href.indexOf('http://www.fruttega.com.mx/sige') != 0 && location.href.indexOf('https://dev.armesis.gov.mx/sige') != 0){
	// 	Core.toast('Este sistema no está disponible para esta dirección URL. Un mensaje ha sido enviado a Grupo Marssoft.','red');
	// 	return false;
	// }
    B = new Base();
    B.run();
    B.scroll();
});

class Base{
    constructor(){
        this.sige = {
            und : undefined,
            ui : new ui(),
            fecha : new Fecha(),
            asyn : new Ax('php/ws.php'),
            validator : new Validator(),
            login : new Login(),
            creator : new Creator(),
            access : new Access(),
            uf : () => {
                M.updateTextFields();
                $('input:not(.select-dropdown), textarea').characterCounter();
            }
        };
        this.data = {};
    }
    scroll(){
        $(window).scroll(function(){
            let av = $(this).scrollTop();
            let me = $('.H-fixed');
            if (av > 217){
                if (!me.hasClass('z-depth-1'))
                    me.addClass('z-depth-1');
            } else{
                if (me.hasClass('z-depth-1'))
                    me.removeClass('z-depth-1');
            }
        });
    }
    run(){
        B.sige.validator.isLoged();
    }
    prop(target, value){
        $('select.'+target).find('option[value="'+value+'"]').prop('selected', true);
    }
    ref(){
        return B.sige.fecha.getDate(8)+'-'+Core.rand(10000,99999)+'-'+Core.rand(100,999);
    }
    msn(t){
        switch(t){
            case 'error':
                Core.toast('Se presentó un error al guardar, intente nuevamente.','red');
            break;
            case 'denied':
                Core.toast('Acceso denegado. Verifique sus permisos con el administrador','red');
            break;
            case 'true':
                Core.toast('Datos guardados con exito.','green');
            break;
            case 'empty':
                Core.toast('Error, debe completar todos los datos para guardar','red');
            break;
            case 'insuficient':
                Core.toast('Error, No dispone de muchas unidades para retirar de inventario.','red');
            break;
            case 'formalized':
                Core.toast('Error, No se puede eliminar una orden formalizada.','red');
            break;
            case 'inv':
                Core.toast('Ningún campo debe superar los caracteres permitidos. Los campos erróneos se encuentran en color rojo.','red');
            break;
            case 'comprobado':
                Core.toast('No se puede eliminar ningún registro de una operación ya comprobada','red');
            break;
        }
    }
}
class Login{
    constructor(){
        this.clicks();
    }
    ui(){
        $('body').html('<div class="login intro"><div class="l-logos row"><div class="l-l-rc col s4 logotipo" id="recover">'+Form.image('media/recover.png', 80)+'</div><div class="l-l-rc col s4 logotipo grande" id="principal">'+Form.image('media/logo.png', 80)+'</div><div class="l-l-rc col s4 logotipo" id="reloj">'+Form.image('media/reloj.png', 80)+'</div></div><div class="l-content"></div><div class="nombred tg-search"></div></div><div class="bottom">Sistema Integral de Gestión Empresarial. Vr. '+version+'</div>');
        this.principal();
        setTimeout(() => {
            $('.login').removeClass('intro');
            $('.l-content').slideDown(300);
        },200);
    }
    principal(){
        $('.l-content').html('<div class="l-c-input input-field">'+Form.input('username','text','Correo electrónico','')+'</div><div class="l-c-input input-field">'+Form.input('password','password','Contraseña','')+'</div><div class="l-c-login">'+Form.btn(false,'indigo','b-login','Iniciar sesión')+'</div>');
    }
    recover(){
        $('.l-content').html('<div class="l-c-input input-field">'+Form.input('username','text','Correo electrónico','')+'</div><div class="l-c-login">'+Form.btn(false,'green','b-recover','Enviar correo con contraseña')+'</div>');
    }
    reloj(){
		B.sige.asyn.post({class:'trabajadores', method:'getAllTrabajadores', login:'true'}, r => {
            r = Core.json(false, r, false);
            B.data.trabajadores = r;
			$('.l-content').html('<div class="l-c-input input-field">'+Form.input('codeEmploy','text','Nombre del empleado','')+'</div><div class="l-c-login">'+Form.btn(false,'blue','b-register','Marcar acceso')+'</div>');
        });
    }
    clicks(){
        u.click('.logotipo', v => {
            let id;
            if (!v.hasClass('.grande')){
                let d = $('.logotipo.grande');
                let c = v.children('img').attr('src');
                id = v.attr('id');
                let n = d.children('img').attr('src');
                let id2 = d.attr('id');
                v.children('img').attr('src', n);
                v.attr('id', id2);
                d.children('img').attr('src', c);
                d.attr('id', id);
            }
            switch(id){
                case 'recover':
                    B.sige.login.recover();
                break;
                case 'principal':
                    B.sige.login.principal();
                break;
                case 'reloj':
                    B.sige.login.reloj();
                break;
            }
        });
        u.keyup('.username, .password', (v, e) => {
            if (e.keyCode === 13)
                $('.b-login').click();
        });
        u.click('.b-login', () => {
            let u = $('.username').val();
            let p = $('.password').val();
            if (u === '' || p === ''){
                Core.toast('Error, debe completar sus datos de inicio de sesión','red');
                return false;
            }
            u = Core.en(u);
            p = Core.en(p);
            B.sige.asyn.post({class:'login', method: 'verify', param : {user: u, pass: p}}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                        Core.toast('Error, sus credenciales no son correctas','red');
                    break;
					case 'nosuc':
                        Core.toast('Acceso denegado. Su cuenta no está asociada con ninguna empresa. Solicite el acceso a empresas con su administrador.','red');
                    break;
                    case 'passed':
                        Ls.set('us_id', $.cookie('us_id'), 7200);
                        B.sige.validator.regPermisos(r[1]);
                        B.sige.creator.build();
                        B.sige.validator.getBasicData();
						if ($.cookie('accesoTotal') == '1'){
							$('body').append('<div class="smblack"><div class="container loader z-depth-5">Cargando espacios de trabajo...</div></div>');
							B.sige.login.sloading();
						}
                    break;
                }
            });
        });
        u.click('.b-register', () => {
            let u = $('.codeEmploy').val();
            if (u === '')
                return false;
            B.sige.login.employ(u);
        });
		u.change('select.SelectedEmpresaII', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalII').html(Form.initSelect(filter));
            $('.SelectedSucursalII').formSelect();
        });
    }
	sloading(){
		let sint = setInterval(function(){
			if (B.sige.validator.getForSelect('empresas',0, 1).length > 0){
				clearInterval(sint);
				$('.smblack').fadeOut(100,function(){$(this).remove();});
				B.sige.login.sucursal();
			}
		}, 100);
	}
	sucursal(){
		let SelectedEmpresaII = Form.select('SelectedEmpresaII','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let SelectedSucursalII = Form.select('SelectedSucursalII','Seleccione primero una empresa', []);
		B.sige.ui.question('Seleccione un espacio de trabajo','<div class="row"><div class="col s12 input-field">'+SelectedEmpresaII+'</div><div class="col s12 input-field">'+SelectedSucursalII+'</div></div>','Seleccionar', () => {
			let nsucursal = $('select.SelectedSucursalII').val();
			if (nsucursal == null){
				Core.toast('No seleccionó una sucursal para establecerla como su espacio de trabajo.','red');
				return false;
			}
			B.sige.asyn.post({class:'security', method: 'cambiarSucursal', id:nsucursal}, r => {
				r = Core.json(false, r, false);
				switch(r[0]){
					case 'true':
						Core.toast('Espacio de trabajo cambiado exitósamente, recargando.','Green');
						location.reload();
					break;
					case 'notpermisible':
						Core.toast('Acceso denegado. No tiene acceso a esta empresa, solicite acceso con su administrador','red');
					break;
					default:
						Core.toast('Acceso denegado. No dispone del acceso total para cambiar entre sucursales o empresas','red');
					break;
				}
			});
		}, () => {
			$('.SelectedEmpresaII, .SelectedSucursalII').formSelect();
		});
	}
	updateworkspace(){
		z.observer('.workingon', function(){
			let wrkon = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'),  0);
			$('.workingon').html(wrkon[0][2]+' - '+wrkon[0][3]);
		});
	}
    logout(){
        B.sige.asyn.post({class:'login', method: 'exit'}, () => {
            location.reload();
        });
    }
    employ(code){
		let nxm = $('.nombred').html();
        B.sige.asyn.post({class:'asistencia', method: 'make', param: code}, r => {
            r = Core.json(false, r, false);
			//let m;
            switch(r[0]){
                case 'error':
                case 'denied':
                    B.msn(r[0]);
                break;
				case 'twice':
					Core.toast('Este empleado ya tiene un registro de entrada para el día de hoy. Intente nuevamente mañana.','red rounded');
				break;
                case 'true':
				default:
                    //m = (r[1] === 1) ? 'ENTRADA' : 'SALIDA';
					if (r[1] === 1)
						Core.toast('Acceso correcto. Se ha registrado su ENTRADA correctamente.','green rounded');
					else {
						Core.toast('Acceso correcto. Se ha registrado su SALIDA correctamente.','green rounded');
						B.sige.login.msg(nxm);
					}
					$('.codeEmploy').val('');
                break;
            }
        });
    }
	msg(nxm, idAsis = '', fn = function(){}){
		let v1 = Form.input('horaExtra', 'number', '¿Horas extra?', '', '', undefined, 1, '0');
		let v2 = Form.input('comentarios', 'text', '¿Comentarios?', '', '', undefined, 150, '');
		B.sige.ui.question('¿El trabajador '+nxm+' tiene algo adicional?','<div class="row col s12 input-field">'+v1+'</div><div class="row col s12 input-field">'+v2+'</div><div class="row col s12 tg-search"></div><div class="row col s12 nombred tg-title"></div>','Aceptar',() => {
			let code = $('.horaExtra').val();
			let come = $('.comentarios').val();
			B.sige.asyn.post({class:'asistencia', method: 'finishExtraHous', param: [code, come, idAsis]}, r => {
				r = Core.json(false, r, false);
				switch(r[0]){
					case 'error':
					case 'denied':
						B.msn(r[0]);
					break;
					case 'true':
						Core.toast('Se marcaron '+code+' horas extras en el registro con observación: '+come+'.','green rounded');
						fn();
					break;
				}
			});
		}, () => {
			$('.horaExtra').characterCounter();
			B.sige.uf();
		});
	}
}
class Creator{
    constructor(){
        this.mods = {};
        this.clicks();
        this.active = null;
        this.pos = 0;
		this.sret = null;
    }
    getInstance(title){
        let clx = '';
        $.each(this.mods, (i, e) =>{
            if (e.title === title){
                clx = e;
                return false;
            }
        });
        return clx;
    }
    build(){
        let b = $('body');
		let sm = ($.cookie('accesoTotal') == '1') ? '<div class="c-perfil btnchange circle" data-tooltip="Cambiar de Empresa y/o sucursal" data-position="left">'+Ic.c(2,'','autorenew')+'</div>' : '';
        b.html('<header class="grey lighten-3"><div class="H-fixed white"><div class="container"><div class="workingon center-align truncate">Cargando espacio de trabajo...</div><div class="c-menu show-on-small hide-on-med-and-up sidenav-trigger" data-target="slide-out">'+Ic.c(0,'','menu')+'</div><div class="c-logo">'+Form.image('media/logo.png', 40)+'</div><div class="c-title">S.I.G.E.</div>'+sm+'<div class="c-perfil circle" data-target="notify">'+Form.image('media/person.jpg', 40)+'</div><div class="c-nombre">'+$.cookie('nombre').split(' ')[0]+'</div></div></div><div class="z-depth-1 hgedt"><div class="grey lighten-3 container posventa row"></div><div class="grey lighten-3 container searchEngine row"><div class="c-search col s12 input-field">'+Form.input('looking','text','Hola '+$.cookie('nombre')+', ¿Qué estás buscando?','')+'</div><div class="pos-actual container col s12"></div></div></div></header><ul id="slide-out" class="head-over sidenav"><li>Olita de mar</li></ul><div class="base container"></div><div class="bottom no-fixed">SIGE. Vr. '+version+'. &copy; Grupo Marssoft 2019</div>');
        $('.searchEngine').prepend('<div class="col s12 center-align c-search"><div class="col s6 strcrd rchecadorb"><div class="col s12 tg-title">Reloj checador</div><div class="col s12 tg-title">'+Ic.c('3','','access_time')+'</div></div><div class="col s6 strcrd rpuntovb"><div class="col s12 tg-title">Punto de venta</div><div class="col s12 tg-title">'+Ic.c('3','','receipt')+'</div></div></div>');
        b.append(Dropdown.create('opciones', 'notify'));
        if (B.sige.validator.hasAccessTo('Usuarios'))
            Dropdown.add('.opciones', 'Bitácora');
		if (!B.sige.validator.hasAccessTo('PuntoVenta')){
			$('.rpuntovb').remove();
			$('.rchecadorb').removeClass('s6').addClass('s12');
		}
        Dropdown.add('.opciones', 'Cerrar sesión');
        $('.c-perfil:not(.btnchange)').dropdown({constrainWidth: false});
        $('.sidenav').sidenav();
		$('.btnchange').tooltip();
        this.createMenu();
    }
    createMenu(){
        $.each(this.mods, (i, e) => {
            if (e.seccion === '')
                return;
            if ($('.b-categoria.'+e.seccion).length === 0)
                $('.base').append('<div class="b-categoria '+e.seccion+'"><div class="b-title">'+e.seccion+'</div><div class="b-partes row '+e.seccion+'"></div></div>');
            $('.b-partes.'+e.seccion).append('<div class="b-b-left col s12 m6 scale-transition scale-out" search="'+e.descr+'"><div class="openCard card hoverable center-align" target="'+e.title+'"><div class="card-content"><div class="c-c-icon '+e.color+'-text">'+Ic.c(3,'icon',e.icon)+'</div><div class="card-title">'+e.title.replaceByClassName().splitByUpper()+'</div></div></div></div>');
        });
        setTimeout(() => {
            $('.b-b-left').addClass('scale-in');
        },150);
    }
    addCreator(title, cls, permiso){
		if (permiso == '1')
        	this.mods[title] = cls;
    }
    clicks(){
		u.click('.b-initReporte', v => {
			$('.m-content').slideUp(200);
			$('.cPart').html('');
			B.sige.creator.active.reportes(v);
		});
		u.click('.btnchange', function(){
			B.sige.login.sucursal();
		});
        u.click('ul.opciones > li', v => {
            switch(v.html()){
                case 'Bitácora':
					B.sige.asyn.post({class:'permisos', method: 'getBitacora'}, r => {
						r = Core.json(false, r, false);
						let vldate = [];
						$.each(r, (i, e) => {
							e[5] = B.sige.fecha.date(e[5],'-',':');
							vldate.push(e);
						});
						let value = Form.table('datosStatus', 'datosStatusC cfm','striped',['Id', 'Usuario', 'Módulo', 'Acción realizada', 'Complemento', 'Fecha'], vldate);
						B.sige.ui.question('Bitácora de movimientos',value,'Cerrar', () => {}, () => {});
					});
                break;
                case 'Cerrar sesión':
                    B.sige.login.logout();
                break;
            }
        });
        u.click('.rchecadorb', () => {
            B.sige.ui.isquestion();
            let v1 = Form.input('codeEmploy', 'text', 'Nombre completo del empleado', '', '', undefined, 50, '');
            B.sige.ui.question('Registro de empleados','<div class="row col s12 input-field">'+v1+'</div><div class="row col s12 tg-search"></div><div class="row col s12 nombred tg-title"></div>','Aceptar',() => {
                let code = $('.codeEmploy').val();
                if (code === '')
                    return false;
                B.sige.login.employ(code);
            }, () => {
                $('.codeEmploy').characterCounter();
            });
        });
        u.click('.rpuntovb', () => {
            B.sige.creator.getInstance('PuntoVenta').window(() => {
                //$('.b-cambiarCliente').click();
                $('.ref').focus();
			});
        });
        u.keyup('.codeEmploy', v => {
			var val = v.val();
			var rs = B.sige.validator.searchNoSensitive('trabajadores', val, 2, 5);
			if (rs.length == 0){
				$('.nombred').html('Ninguna coincidencia');
			} else {
				$('.nombred').html('Haga clic sobre un empleado y luego dé clic en aceptar');
			}
			$('.tg-search').html('');
			$.each(rs, function(i, e){
				$('.tg-search').append('<div class="s12 card resultadoPersona" target="'+e[1]+'" nombre="'+e[2]+'"><b>'+e[2]+'</b> de la empresa <b>' + e[3] + '</b> y sucursal <b>'+e[4]+'</b></div>');
			});
			/*
            if (v.val().length > 4)
                v.val('');
            else if (v.val().length === 4){
                B.sige.asyn.post({class:'asistencia', method: 'getByCode', param: v.val()}, r => {
                    r = Core.json(false, r, false);
                    if (r[0] === -1)
                        $('.codeEmploy').val('');
                    $('.nombred').html('Nombre: '+r[1]);
                });
            } else
                $('.nombred').html('');
			*/
        });
		u.click('.resultadoPersona', v =>{
			$('.tg-search').html('');
			let code = v.attr('target');
			let nombre = v.attr('nombre');
			$('.codeEmploy').val(code);
			$('.nombred').html(nombre);
		});
        u.keyup('.looking', v => {
            let t = v.val().toLowerCase(), bl = $('.b-b-left');
            if (t !== ''){
                bl.hide();
                bl.removeClass('scale-in');
                bl.each(function(){
                    if ($(this).attr('search').toLowerCase().indexOf(t) >= 0){
                        $(this).show();
                        $(this).addClass('scale-in');
                    }
                });
            } else{
                bl.show();
                bl.addClass('scale-in');
            }
        });
        u.click('.openCard', v => {
            B.sige.creator.pos = $(window).scrollTop();
            let c = v.attr('target');
            let cl = B.sige.creator.getInstance(c);
            B.sige.creator.active = cl;
            cl.build();
            $('.c-search, .c-bitacora').slideUp(100);
            $('.pos-actual').slideDown(100);
        });
        u.click('.b-return', () => {
            let pc = $('.pos-actual');
            $('.metainfo, .cPart').remove();
            $('.base').html('');
            B.sige.creator.createMenu();
            pc.html('');
            $('.c-search, .c-bitacora').slideDown(100);
            pc.slideUp(100, function(){
                $(window).scrollTop(B.sige.creator.pos);
            });
			B.sige.creator.active = null;
			if (B.sige.creator.sret != null){
				$('.openCard[target="'+B.sige.creator.sret.title+'"]').click();
				B.sige.creator.sret = null;
			}
        });
        u.keyup('.buscandoPor', v => {
            let tr = v.attr('look'), input = v.val().toLowerCase(), s = $('tr.'+tr);
            if (tr === 'undefined' || input === ''){
                s.show();
                return false;
            }
            s.hide().each(function(){
                let search = $(this).attr('search').toLowerCase().indexOf(input);
                if (search >= 0)
                    $(this).show();
            });
        });
		u.click('.b-repExportarExcel', function(){
			B.sige.creator.printer();
		});
    }
    breturn(){
        let btn = Form.btn(false,'blue-grey','b-return','Regresar','keyboard_arrow_left');
        $('.base').html('<div class="metainfo row"><div class="m-return col s12 offset-m9 m3 smnb">'+btn+'</div><div class="m-content"></div><div class="m-filtros"></div></div><div class="cPart row"></div>');
    }
    simpliest(title, where){
        return '<div class="buscando col s12 input-field">'+Form.input('buscandoPor','text',title,'', false, undefined, undefined, '', 'look="'+where+'"')+'</div>';
    }
    simbtn(klass, btnclass, texto, color = 'green'){
        return '<div class="col '+klass+' smnb">'+Form.btn(false, color, 'b-'+btnclass, texto)+'</div>';
    }
    atrs(cl, color = 'blue-grey', title = 'Regresar'){
        return Form.btn(false, color, 'b-'+cl, title, 'keyboard_arrow_left');
    }
	printer(){
		$('.contenidoReporte').printArea({mode : "popup", popClose : true, extraHead : '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'});
	}
}
class Validator {
    isLoged(){
        if ($.cookie('us_id') === undefined)
            B.sige.login.ui();
        else{
            B.sige.validator.getBasicData();
            B.sige.validator.regPermisosByLS();
        }
    }
    getBasicData(){
        B.sige.asyn.post({class:'BasicData'}, r => {
            r = Core.json(false, r, false);
            B.data.empresas = r[0];
            B.data.sucursales = r[1];
            B.data.departamentos = r[2];
            B.data.familias = r[3];
            B.data.unidades = r[4];
            B.data.modulos = r[5];
            B.data.sueldos = r[6];
            B.data.catalogos = r[7];
            B.data.subcatalogos = r[8];
            B.data.trabajadores = r[9];
            B.data.proveedores = r[10];
            B.data.clientes = r[11];
            B.data.percepciones = r[12];
            B.data.deducciones = r[13];
			B.sige.login.updateworkspace();
        });
    }
    getForSelect(pos, v1, v2){
        let select = [];
        $.each(B.data[pos], (i, e) => {
            let t = '';
            if (typeof v2 === "number")
                t = e[v2];
            else {
                $.each(v2, function(kk, mm){
                   t +=  e[mm]+' ';
                });
            }
            select.push([e[v1], t]);
        });
        return select;
    }
    forSelect(array, v1, v2){
        let select = [];
        $.each(array, (i, e) => {
            select.push([e[v1], e[v2]]);
        });
        return select;
    }
    searchIn(pos, search, v2, limit = -1){
        let select = [], s = 0;
        $.each(B.data[pos], (i, e) => {
            if (e[v2] === search)
                select.push(e);
            if (limit !== -1){
                s++;
                if(s === limit)
                    return false;
            }
        });
        return select;
    }
	searchSensitive(pos, search, v2, limit = -1, r){
        let select = [], s = 0;
        $.each(((r === undefined) ? B.data[pos] : r), (i, e) => {
            if (e[v2].indexOf(search) >= 0){
                select.push(e);
				if (limit !== -1){
					s++;
					if(s === limit)
						return false;
				}
			}
        });
        return select;
    }
	searchNoSensitive(pos, search, v2, limit = -1, r){
        let select = [], s = 0;
        $.each(((r === undefined) ? B.data[pos] : r), (i, e) => {
			e[v2] = e[v2].toLowerCase();
			search = search.toLowerCase();
            if (e[v2].indexOf(search) >= 0){
                select.push(e);
				if (limit !== -1){
					s++;
					if(s === limit)
						return false;
				}
			}
        });
        return select;
    }
    hasAccessTo(loc){
        return (Ls.get(loc,true) === '1');
    }
    regPermisosByLS(){
        B.sige.asyn.post({class:'permisos', method: 'regPermisos'}, r => {
            r = Core.json(false, r, false);
			B.data.permisos = r;
            B.sige.validator.regPermisos(r);
            B.sige.creator.build();
        });
    }
    regPermisos(p){
        let shr = B.sige.creator;
        $.each(p, (i, e) => {
            if (e[2] === 0)
                return;
            switch(e[1]){
                case 'Clientes':
                    shr.addCreator(e[1], new Clientes(e[0], e[1]), e[2]);
                break;
                case 'Asistencias':
                    shr.addCreator(e[1], new Asistencias(e[0], e[1]), e[2]);
                break;
                case 'Nominas':
                    shr.addCreator(e[1], new Nominas(e[0], e[1]), e[2]);
                break;
                case 'Articulos':
                    shr.addCreator(e[1], new Articulos(e[0], e[1]), e[2]);
                break;
                case 'Familias':
                    shr.addCreator(e[1], new Familias(e[0], e[1]), e[2]);
                break;
                case 'Proveedores':
                    shr.addCreator(e[1], new Proveedores(e[0], e[1]), e[2]);
                break;
                case 'Empresas':
                    shr.addCreator(e[1], new Empresas(e[0], e[1]), e[2]);
                break;
                case 'Sucursales':
                    shr.addCreator(e[1], new Sucursales(e[0], e[1]), e[2]);
                break;
                case 'Departamentos':
                    shr.addCreator(e[1], new Departamentos(e[0], e[1]), e[2]);
                break;
                case 'Trabajadores':
                    shr.addCreator(e[1], new Trabajadores(e[0], e[1]), e[2]);
                break;
                case 'Sueldos':
                    shr.addCreator(e[1], new Sueldos(e[0], e[1]), e[2]);
                break;
                case 'ActivoFijo':
                    shr.addCreator(e[1], new ActivoFijo(e[0], e[1]), e[2]);
                break;
                case 'Unidades':
                    shr.addCreator(e[1], new Unidades(e[0], e[1]), e[2]);
                break;
                case 'Precios':
                    shr.addCreator(e[1], new Precios(e[0], e[1]), e[2]);
                break;
                case 'Gastos':
                    shr.addCreator(e[1], new Gastos(e[0], e[1]), e[2]);
                break;
                case 'Prestamos':
                    shr.addCreator(e[1], new Prestamos(e[0], e[1]), e[2]);
                break;
                case 'Inventarios':
                    shr.addCreator(e[1], new Inventarios(e[0], e[1]), e[2]);
                break;
                case 'Traspasos':
                    shr.addCreator(e[1], new Traspasos(e[0], e[1]), e[2]);
                break;
                case 'OrdenDeCompra':
                    shr.addCreator(e[1], new OrdenDeCompra(e[0], e[1]), e[2]);
                break;
                case 'FormalizarOrdenCompra':
                    shr.addCreator(e[1], new FormalizarOrdenCompra(e[0], e[1]), e[2]);
                break;
                case 'CuentasPorPagar':
                    shr.addCreator(e[1], new CuentasPorPagar(e[0], e[1]), e[2]);
                break;
                case 'PuntoVenta':
                    shr.addCreator(e[1], new PuntoVenta(e[0], e[1]), e[2]);
                break;
                case 'Cotizaciones':
                    shr.addCreator(e[1], new Cotizaciones(e[0], e[1]), e[2]);
                break;
                case 'OrdenDeVenta':
                    shr.addCreator(e[1], new OrdenDeVenta(e[0], e[1]), e[2]);
                break;
                case 'CuentasPorCobrar':
                    shr.addCreator(e[1], new CuentasPorCobrar(e[0], e[1]), e[2]);
                break;
				case 'Catalogos':
                    shr.addCreator(e[1], new Catalogos(e[0], e[1]), e[2]);
                break;
				case 'Percepciones':
                    shr.addCreator(e[1], new Percepciones(e[0], e[1]), e[2]);
                break;
				case 'Deducciones':
                    shr.addCreator(e[1], new Deducciones(e[0], e[1]), e[2]);
                break;
				case 'Estajos':
                    shr.addCreator(e[1], new Estajos(e[0], e[1]), e[2]);
                break;
				case 'SubCatalogo':
                    shr.addCreator(e[1], new SubCatalogo(e[0], e[1]), e[2]);
                break;
				case 'EntradaAlmacen':
                    shr.addCreator(e[1], new EntradaAlmacen(e[0], e[1]), e[2]);
                break;
				case 'SalidaAlmacen':
                    shr.addCreator(e[1], new SalidaAlmacen(e[0], e[1]), e[2]);
                break;
				case 'BancoSocios':
                    shr.addCreator(e[1], new BancoSocios(e[0], e[1]), e[2]);
                break;
				case 'Embarques':
                    shr.addCreator(e[1], new Embarques(e[0], e[1]), e[2]);
                break;
            }
            Ls.set(e[1], e[2], 7200);
        });
    }
    vForm(v){
        let f = true;
        if ($('input.invalid, textarea.invalid').length > 0){
            B.msn('inv');
            return false;
        }
        $.each(v,(i, e) => {
            if (e === '' || e == null || e === undefined)
                f = false;
        });
        if (!f)
            B.msn('empty');
        return f;
    }
    vTable(v, c1, c2){
        $('.'+c1).removeClass('active');
        v.addClass('active');
        let id = v.attr('id');
        $(c2).removeClass('disabled').attr('target', id);
    }
}
/* INICIO DE CLASES DE USUARIO */
class OrdenDeVenta{
    constructor(permiso, title){
        this.clicks();
        this.title = title;
        this.seccion = 'Ventas';
        this.icon = 'insert_drive_file';
        this.color = 'green';
        this.descr = 'ver ordenes de venta';
        this.idPermiso = permiso;
    }
    window(r, edit){
        B.sige.creator.getInstance('PuntoVenta').window(() => {
            if (typeof edit === 'undefined'){
                $('.margintop10.tg-title').html('Ordenes de venta');
                $('.ref').addClass('ordenventa');
                $('.b-pagarPunto').html('generar orden de venta').addClass('ordenventa');
                $('.m9.adicional').slideUp(150);
            }
            if (r !== undefined){
                if (r[2][8] !== ''){
                    B.sige.creator.getInstance('PuntoVenta').cambiarCliente(r[2][8], r => {
                        r = Core.json(false, r, false);
                        $('.texter.cliente').html('Cliente: '+r[0]);
                    });
                }
                $.each(r[4], (i, e) => {
                    e[5][8] = e[5][8].toSimpleNumber().formatMoney();
                    e[5][9] = e[5][9].toSimpleNumber().formatMoney();
                    let ndc = (typeof edit === 'undefined') ? 'ordenventa' : '';
                    $('.strproductos').append('<div class="product '+ndc+' col s12 m4 center-align" id="'+e[0]+'" code="'+e[5][4]+'" nombre="'+e[5][0]+'" unidad="'+e[5][1]+'" familia="'+e[5][2]+'" mayoreo="'+e[5][8]+'" menudeo="'+e[5][9]+'" existencia="'+e[5][10]+'"><div class="card z-depth-2 hoverable"><div class="card-content row npadding"><div class="col s12 npadding tg-title">'+e[5][0]+'</div><div class="col s12 npadding">'+e[5][4]+' - '+e[5][2]+'</div><div class="col s6 npadding tg-subtitle">Mayoreo</div><div class="col s6 npadding tg-subtitle">Menudeo</div><div class="col s6 npadding">'+e[5][8]+'</div><div class="col s6 npadding">'+e[5][9]+'</div></div></div></div>');
                    $('.product[id="'+e[0]+'"]').click();
                    let clsid = $('.producted[id="'+e[0]+'"]').attr('clsid');
                    $('.unidades.'+clsid).val(e[1]);
                    $('.preciou.'+clsid).val(e[2]);
                    let prc = (e[4] === e[3]) ? '0' : '1';
                    B.prop('impuestoS.'+clsid, prc);
                    $('.impuestoS').formSelect();
                });
                $('.unidades').keyup();
            }
        });
    }
    build(fn){
        B.sige.creator.breturn();
        let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s6 m3','generarOventa', 'Nueva'));
        mc.append(B.sige.creator.simbtn('s6 m3','gestionarOventa', 'editar'));
        mc.append(B.sige.creator.simbtn('s6 m3','detalleOventa', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s6 m3','delOventa', 'Eliminar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en ordenes de venta', 'datosOrdenesVentaC'));
        this.get(fn);
    }
    get(fn){
        $('.b-gestionarOventa, .b-delOventa, .b-detalleOventa').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Ordenes de venta</a>');
        B.sige.asyn.post({class:'PuntoVenta', method: 'getAllOrdenesVenta'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = e[2].toSimpleNumber().formatMoney();
                e[3] = B.sige.fecha.date(e[3],'-');
                vldate.push(e);
            });
            let value = Form.table('datosOrdenesVenta', 'datosOrdenesVentaC cfm','striped',['id', 'Vendedor', 'Monto', 'Fecha'], vldate);
            $('.cPart').html(value);
            if (typeof fn === "function")
                fn();
        });
    }
    clicks(){
        u.click('.b-detalleOventa', v => {
            let id = v.attr('target');
            $('.pos-actual').html('<a class="breadcrumb">Ordenes de venta</a><a class="breadcrumb">Detalles</a>');
            B.sige.creator.getInstance('PuntoVenta').getComplexTicket(id);
            z.observer('.coverjm2', () => {
                let btn2 = Form.btn(false, 'green', 'b-convertVenta', 'Convertir a venta');
                $('.cPart').append('<div class="col s12 smnb">'+btn2+'</div>');
                $('.b-convertVenta').attr('target', id);
            });
        });
        u.click('.b-convertVenta', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','Está a punto de convertir una orden de venta en una venta. Podrá modificar los datos de la orden para su comodidad. Esta venta descontará los productos del inventario por lo que asegúrese de contar con suficientes unidades para realizar la venta.','Abrir interfaz de venta',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'validateOrdenVenta', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'insufficient':
                            Core.toast('Se requieren '+r[1][4]+' '+r[1][2]+' de '+r[1][1]+' con código de sucursal '+r[1][0]+' para iniciar la venta. Actualmente hay en existencia '+r[1][3]+' '+r[1][2]+'.','red rounded');
                        break;
                        default:
                            B.sige.asyn.post({class:'PuntoVenta', method: 'getComplexTicket', param: id}, r => {
                                r = Core.json(false, r, false);
                                B.sige.creator.getInstance('OrdenDeVenta').window(r, false);
                                z.observer('.b-pagarPunto', () => {
                                    $('.b-pagarPunto').html('Actualizar a venta').addClass('crearVentaM');
                                });
                            });
                        break;
                    }
                });
            });
        });
        u.click('.b-generarOventa', () => {
            B.sige.creator.getInstance('OrdenDeVenta').window();
        });
        u.click('.b-delOventa', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta orden de venta? Esta acción es irreversible','Aceptar',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'delOrdenVenta', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('OrdenDeVenta').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-gestionarOventa', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'PuntoVenta', method: 'getComplexTicket', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('OrdenDeVenta').window(r);
                z.observer('.b-pagarPunto', () => {
                    $('.b-pagarPunto').html('Actualizar orden de venta').addClass('b-updateOrdenDeVenta');
                });
            });
        });
        u.click('.datosOrdenesVentaC', v => {
            B.sige.validator.vTable(v, 'datosOrdenesVentaC', '.b-gestionarOventa, .b-delOventa, .b-detalleOventa');
        });
        u.click('.b-getOrdenesVenta', () => {
            B.sige.creator.getInstance('OrdenDeVenta').get();
        });
    }
}
class Cotizaciones{
    constructor(permiso, title){
        this.clicks();
        this.title = title;
        this.seccion = 'Ventas';
        this.icon = 'assignment';
        this.color = 'teal';
        this.descr = 'ver cotizaciones';
        this.idPermiso = permiso;
    }
    window(r){
        B.sige.creator.getInstance('PuntoVenta').window(() => {
            $('.margintop10.tg-title').html('Centro de cotizaciones');
            $('.ref').addClass('cotizacion');
            $('.b-pagarPunto').html('generar cotización').addClass('cotizacion');
            $('.camion, .marca, .placas, .operador').parent().slideUp(150);
            if (r !== undefined){
                if (r[2][8] !== ''){
                    B.sige.creator.getInstance('PuntoVenta').cambiarCliente(r[2][8], r => {
                        r = Core.json(false, r, false);
                        $('.texter.cliente').html('Cliente: '+r[0]);
                    });
                }
                $.each(r[4], (i, e) => {
                    e[5][8] = e[5][8].toSimpleNumber().formatMoney();
                    e[5][9] = e[5][9].toSimpleNumber().formatMoney();
                    $('.strproductos').append('<div class="product cotizacion col s12 m4 center-align" id="'+e[0]+'" code="'+e[5][4]+'" nombre="'+e[5][0]+'" unidad="'+e[5][1]+'" familia="'+e[5][2]+'" mayoreo="'+e[5][8]+'" menudeo="'+e[5][9]+'" existencia="'+e[5][10]+'"><div class="card z-depth-2 hoverable"><div class="card-content row npadding"><div class="col s12 npadding tg-title">'+e[5][0]+'</div><div class="col s12 npadding">'+e[5][4]+' - '+e[5][2]+'</div><div class="col s6 npadding tg-subtitle">Mayoreo</div><div class="col s6 npadding tg-subtitle">Menudeo</div><div class="col s6 npadding">'+e[5][8]+'</div><div class="col s6 npadding">'+e[5][9]+'</div></div></div></div>');
                    $('.product[id="'+e[0]+'"]').click();
                    let clsid = $('.producted[id="'+e[0]+'"]').attr('clsid');
                    $('.unidades.'+clsid).val(e[1]);
                    $('.preciou.'+clsid).val(e[2]);
                    let prc = (e[4] === e[3]) ? '0' : '1';
                    B.prop('impuestoS.'+clsid, prc);
                    $('.impuestoS').formSelect();
                });
                $('.unidades').keyup();
            }
        });
    }
    build(fn){
        B.sige.creator.breturn();
        let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s6 m3','generarCot', 'Nueva'));
        mc.append(B.sige.creator.simbtn('s6 m3','gestionarCot', 'editar'));
        mc.append(B.sige.creator.simbtn('s6 m3','detalleCot', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s6 m3','delCot', 'Eliminar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en ventas', 'datosCotizacionesC'));
        this.get(fn);
    }
    get(fn){
        $('.b-gestionarCot, .b-delCot, .b-detalleCot').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Cotizaciones</a>');
        B.sige.asyn.post({class:'PuntoVenta', method: 'getAllCotizaciones'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = e[2].toSimpleNumber().formatMoney();
                e[3] = B.sige.fecha.date(e[3],'-');
                vldate.push(e);
            });
            let value = Form.table('datosCotizaciones', 'datosCotizacionesC cfm','striped',['id', 'Vendedor', 'Monto', 'Fecha'], vldate);
            $('.cPart').html(value);
            if (typeof fn === "function")
                fn();
        });
    }
    clicks(){
        u.click('.b-detalleCot', v => {
            let id = v.attr('target');
            $('.pos-actual').html('<a class="breadcrumb">Cotizaciones</a><a class="breadcrumb">Detalles</a>');
            B.sige.creator.getInstance('PuntoVenta').getComplexTicket(id);
            z.observer('.coverjm2', () => {
                let btn2 = Form.btn(false, 'green', 'b-convertOrdenVenta', 'Convertir a orden de venta');
                $('.cPart').append('<div class="col s12 smnb">'+btn2+'</div>');
                $('.b-convertOrdenVenta').attr('target', id);
            });
        });
        u.click('.b-convertOrdenVenta', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','Esta cotización se convertirá en una orden de venta. Al realizar esto, se compromete a cubrir con el inventario que contiene dicha cotización. El inventario no se descontará hasta que la orden de venta se convierta en venta. Es decir, se entregue al cliente.','Entiendo y acepto cambio',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'convertIntoOrdenVenta', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('OrdenDeVenta').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-generarCot', () => {
            B.sige.creator.getInstance('Cotizaciones').window();
        });
        u.click('.b-delCot', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta cotización? Esta acción es irreversible','Aceptar',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'delCotizacion', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Cotizaciones').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-gestionarCot', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'PuntoVenta', method: 'getComplexTicket', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Cotizaciones').window(r);
                z.observer('.b-pagarPunto', () => {
                    $('.b-pagarPunto').html('Actualizar cotización').addClass('b-updateCotizacion');
                });
            });
        });
        u.click('.datosCotizacionesC', v => {
            B.sige.validator.vTable(v, 'datosCotizacionesC', '.b-gestionarCot, .b-delCot, .b-detalleCot');
        });
        u.click('.b-getCotizaciones', () => {
            B.sige.creator.getInstance('Cotizaciones').get();
        });
    }
}
class PuntoVenta{
    constructor(permiso, title){
        this.clicks();
        this.title = title;
        this.seccion = 'Ventas';
        this.icon = 'receipt';
        this.color = 'indigo';
        this.descr = 'ver faltas, ver asistencias';
        this.idPermiso = permiso;
    }
    window(fn){
        let pv = $('.posventa'), bs = $('.base');
        $('.searchEngine').slideUp(0);
        pv.slideDown(300);
        let bt1 = Form.btn(false, 'blue-grey', 'b-retroceder b-return', 'Regresar','keyboard_arrow_left');
        //let bt2 = Form.btn(false, 'indigo', 'b-cambiarSucursal', '','swap_horiz','data-tooltip="Cambiar de sucursal y empresa" data-position="bottom"');
        let bt3 = Form.btn(false, 'indigo', 'b-cambiarCliente', '','person','data-tooltip="Seleccione un cliente (opcional)" data-position="bottom"');
        let bt4 = Form.btn(false, 'green', 'b-pagarPunto disabled', 'Pagar');
        let ref = Form.input('ref', 'text', 'Escriba el producto por nombre, código o familia', '', '');
        let camion = Form.input('camion', 'text', 'Camión', '', '', undefined, 40);
        let marca = Form.input('marca', 'text', 'Marca', '', '', undefined, 20);
        let placas = Form.input('placas', 'text', 'Placas', '', '', undefined, 20);
        let operador = Form.input('operador', 'text', 'Operador', '', '', undefined, 50);
		
        let fecha = Form.input('fechaVenta', 'text', 'Fecha', '', '', undefined, 50);
		
        let observ = Form.area('observ','Observaciones finales.', 150);
        //<div class="col s3 m1 smnb">'+bt2+'</div> Para cambiar de sucursal
        pv.html('<div class="margintop10 col s12 m9 tg-title">Punto de venta</div><div class="margintop10 col s12 m3' +
            ' smnb">'+bt1+'</div><div class="col s12 m6 texter empresa">...</div><div class="col s9 m5 texter cliente right-align">Seleccione cliente (opcional)</div><div class="col s3 m1 smnb">'+bt3+'</div><div class="col s12 texter vendedor">...</div>');
        $('.b-cambiarSucursal, .b-cambiarCliente').tooltip();
        //Agregar código QR en extra
        bs.html('<div class="row mainpos z-depth-1"><div class="col s12 m9 white"><div class="col s12 npadding"><div' +
            ' class="col s12 m10 input-field noinput refe npadding">'+ref+'</div><div class="col s12 m2 extra"></div></div><div class="col s12 strproductos npadding"></div><div class="col s12 mainProducts"></div></div><div class="col s12 m3 center-align"><div class="col s12 tg-title t2">Resumen</div><div class="col s12 npadding"><div class="col s12 npadding n-bold input-field">'+fecha+'</div></div><div class="col s12 npadding"><div class="col s6 npadding tr1">Subtotal</div><div class="col s6 npadding n-bold subt">$0.00</div></div><div class="col s12 npadding"><div class="col s6 npadding tr1">I.V.A. 16%</div><div class="col s6 npadding n-bold ivaa">$0.00</div></div><div class="col s12 npadding"><div class="col s6 npadding tr1">A PAGAR</div><div class="col s6 npadding n-bold xtotal">$0.00</div></div></div><div class="col s12 m9 adicional"><div class="col s12 m4 input-field">'+camion+'</div><div class="col s6 m4 input-field">'+marca+'</div><div class="col s6 m4 input-field">'+placas+'</div><div class="col s12 input-field">'+operador+'</div><div class="col s12 input-field">'+observ+'</div></div><div class="col s12 m3 smnb marginbottom20">'+bt4+'<div></div>');
        bs.addClass('ovhidden');
		B.sige.fecha.Dtpicker('.fechaVenta');
        B.sige.creator.getInstance('PuntoVenta').cambiarSucursal('');
        if (typeof fn === "function")
            fn();
    }
    build(fn){
        B.sige.creator.breturn();
        let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s6 m3','detallePos', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s6 m3','printTicket', 'Imprimir ticket'));
		if (B.sige.validator.hasAccessTo('Cancelaciones'))
			mc.append(B.sige.creator.simbtn('s6 m3','deleteTicket', 'Cancelar', 'red'));
		mc.append(B.sige.creator.simbtn('s12 m3','initReporte', 'Reportes','blue'));
        mc.append(B.sige.creator.simpliest('Buscar en ventas', 'datosPuntoVentaC'));
        this.get(fn);
    }
	reportes(){
		let btn_1 = Form.btn(false, 'green', 'b-PVventas', 'Reporte de ventas');
		$('.cPart').append('<div class="col s12 m4 smnb">'+btn_1+'</div>');
		$('.cPart').append('<div class="col s12 FilterData"></div>');
		$('.cPart').append('<div class="col s12 contenidoReporte"></div>');
	}
    get(fn){
		B.sige.creator.getInstance('PuntoVenta').from = '';
        $('.b-detallePos, .b-printTicket, .b-deleteTicket').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Punto de venta</a>');
        B.sige.asyn.post({class:'PuntoVenta', method: 'getAllPuntoVenta'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
				if (e[2] == '0')
					e[2] = 'CANCELADO';
				else
                	e[2] = e[2].toSimpleNumber().formatMoney();
                e[3] = B.sige.fecha.date(e[3],'-');
                e[4] = (e[4] === '') ? 'Público general' : e[4];
				e[5] = (e[5] == '0') ? 'Contado' : 'Crédito';
                vldate.push([e[0], e[1], e[4], e[2], e[3], e[5]]);
            });
            let value = Form.table('datosPuntoVenta', 'datosPuntoVentaC cfm','striped',['id', 'Vendedor', 'Cliente', 'Monto', 'Fecha', 'Tipo'], vldate);
            $('.cPart').html(value);
            if (typeof fn === "function")
                fn();
        });
    }
    cambiarSucursal(v,fn){
        B.sige.asyn.post({class:'PuntoVenta', method: 'getBasic', param: v}, r => {
            r = Core.json(false, r, false);
            $('.texter.empresa').html(r[1]+' '+r[3]);
            $('.texter.vendedor').html('Vendedor: '+r[4]);
            if (typeof fn === "function")
                fn();
        });
    }
    cambiarCliente(v,fn){
        B.sige.asyn.post({class:'PuntoVenta', method: 'setCliente', param: v}, r => {
            if (typeof fn === "function")
                fn(r);
        });
    }
    calcularTotales(){
        let sbtotal = 0, st;
        $('input.subtotal').each(function(){
            st = $(this).val().toSimpleNumber();
            sbtotal += st;
        });
        $('.subt').html(sbtotal.formatMoney());
        let total = 0;
        $('input.total').each(function(){
            st = $(this).val().toSimpleNumber();
            total += st;
        });
        $('.xtotal').html(total.formatMoney());
        $('.ivaa').html((total - sbtotal).formatMoney());
    }
    getComplexTicket(id, fn = '.cPart'){
        B.sige.asyn.post({class:'PuntoVenta', method: 'getComplexTicket', param: id}, r => {
            r = Core.json(false, r, false);
			if (r[18] != '0')
				r[18] = B.sige.fecha.date(r[18], '-');
            let ret = (r[10] === '1') ? 'getPuntosVenta' : (r[10] === '2') ? 'getCotizaciones' : 'getOrdenesVenta';
            let btn1 = B.sige.creator.atrs(ret);
            let btn2 = Form.btn(false, 'green', 'b-printDetail', 'Imprimir');
			if (fn == '.cPart')
            	$('.metainfo').hide();
			else {
				btn1 = '';
			}
            let cmx = ' style="text-align: center;"';
            let cmy = ' style="text-align: right;"';
            r[2][2] = (r[2][2] !== '') ? r[2][2]+',' : '';
            r[2][3] = (r[2][3] !== '') ? r[2][3]+',' : '';
            r[2][6] = (r[2][6] !== '') ? r[2][6]+',' : '';
            r[2][4] = (r[2][4] !== '') ? 'CP: '+r[2][4]+',' : '';
            let ttsl = (r[10] === '1') ? 'VENTA' : (r[10] === '2') ? 'COTIZACIÓN' : 'ORDEN DE VENTA';
            $(fn).html('<div class="col s6 m3 smnb">'+btn2+'</div><div class="col s6 offset-m6 m3 smnb">'+btn1+'</div><div class="col s12 coverjm2"><table style="width: 100%"><thead><tr><td colspan="6" '+cmx+'><img src="media/frugtega.png" height="60"><br><b>'+ttsl+' No. '+r[0]+'<br>'+r[18]+'</b></td></tr><tr><td colspan="3"><b>'+r[1][0]+'</b><br><br>'+r[1][1]+'<br>'+r[1][2]+'<br>'+r[1][3]+', '+r[1][4]+', '+r[1][5]+'<br>'+r[1][7]+', '+r[1][8]+'</td><td colspan="3"'+cmy+'><b>'+r[2][0]+'</b><br>'+r[2][1]+'<br>'+r[2][2]+' '+r[2][3]+' '+r[2][4]+' '+r[2][5]+'<br>'+r[2][6]+' '+r[2][7]+'</td></tr><tr><td colspan="12" '+cmx+'>Atendido por <b>'+r[3]+'</b></td></tr></thead><tbody class="articles"></tbody></table></div>');
			$('tbody.articles').append('<tr><td'+cmx+'><b>COD. SUC.</b></td><td'+cmx+'><b>CANTIDAD</b></td><td'+cmx+'><b>NOMBRE</b></td><td'+cmx+'><b>C. UNIT.</b></td><td'+cmx+'><b>SUBTOTAL</b></td><td'+cmx+'><b>IVA</b></td></tr>');
            $.each(r[4], (i, e) => {
                e[2] = e[2].toSimpleNumber();
                e[3] = e[3].toSimpleNumber();
                e[4] = e[4].toSimpleNumber();
                let iva = (e[4] - e[3]);
                e[2] = e[2].formatMoney();
                e[3] = e[3].formatMoney();
                iva = iva.formatMoney();
                $('tbody.articles').append('<tr><td'+cmx+'>'+e[5][4]+'</td><td'+cmx+'>'+e[1]+'</td><td'+cmx+'>'+e[5][0]+'</td><td'+cmy+'>'+e[2]+'</td><td'+cmy+'>'+e[3]+'</td><td'+cmy+'>'+iva+'</td></tr>');

            });
            r[7] = r[7].toSimpleNumber().formatMoney();
            r[8] = r[8].toSimpleNumber().formatMoney();
			if (r[9] == '0')
				r[9] = 'CANCELADO';
			else
				r[9] = r[9].toSimpleNumber().formatMoney();
            let xa = $('tbody.articles');
            xa.append('<tr><td colspan="5"'+cmy+'><b>SUBTOTAL</b></td><td'+cmy+'><b>'+r[7]+'</b></td></tr>');
            xa.append('<tr><td colspan="5"'+cmy+'><b>IVA</b></td><td'+cmy+'><b>'+r[8]+'</b></td></tr>');
            xa.append('<tr class="totalCover"><td colspan="5"'+cmy+'><b>TOTAL</b></td><td'+cmy+'><b>'+r[9]+'</b></td></tr>');
            if (r[10] === '1')
                $('tbody.articles').append('<tr><td'+cmx+'><b>Camión</b></td><td'+cmx+'><b>Marca</b></td><td'+cmx+'><b>Placas</b></td><td colspan="3"'+cmx+'><b>Operador</b></td></tr><tr><td'+cmx+'>'+r[13]+'</td><td'+cmx+'>'+r[14]+'</td><td'+cmx+'>'+r[15]+'</td><td colspan="3"'+cmx+'>'+r[16]+'</td></tr><tr><td colspan="6"><b>Obnservaciones</b></td></tr><tr><td colspan="6">'+r[17]+'</td></tr><tr><td colspan="6"'+cmx+'>Gracias por su compra</td></tr>');
			else
				$('tbody.articles').append('<tr><td colspan="6"><b>Obnservaciones</b></td></tr><tr><td colspan="6">'+r[17]+'</td></tr><tr><td colspan="6"'+cmx+'>Gracias por su compra</td></tr>');
        });
    }
    clicks(){
		u.click('.b-PVventas', () => {
			$('.FilterData').html();
			let j = B.sige.und;
			let nota = Form.input('PVnota', 'text', 'No. de nota', '', '', j, 200, '');
			let tipo = Form.select('PVtipo','Tipo de venta', [[0,'Contado'], [1, 'Crédito'], [2, 'Ambos']]);
			
			let ca = Access.reportes('PVventasx', true, true, false, true, true, true, ['<div class="col s12 m6 input-field">'+nota+'</div><div class="col s12 m6 input-field">'+tipo+'</div>', function(){
				$('.PVtipo').formSelect();
			}]);
			$('.FilterData').html(ca[0]);
			ca[1]();

		});
		u.click('.b-PVventasx', function(){
			let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sCliente').val(),
				$('select.rep_sVendedor').val(),
				$('.PVnota').val(),
				$('select.PVtipo').val()
			];
			B.sige.asyn.post({class:'PuntoVenta', method: 'getPVventas', param: v}, r => {
				r = Core.json(false, r, false);
				let vldate = [], totales = 0;
				$.each(r, (i, e) => {
					e[4] = B.sige.fecha.date(e[4],'-',':');
					if (e[3] == '0')
						e[3] = 'CANCELADO';
					else{
						e[3] = e[3].toSimpleNumber();
						totales += e[3];
						e[3] = e[3].formatMoney();
					}
					e[5] = (e[5] == '0') ? 'CONTADO' : 'CRÉDITO';
					vldate.push(e);
				});
				let value = Form.table('reporteX xxx', 'reporteX cfm','striped',['Nota', 'Vendedor', 'Cliente', 'Monto', 'Fecha', 'Tipo'], vldate);
				$('.contenidoReporte').html(value);
				$('.reporteX > thead').prepend('<tr><th colspan="6" class="encabezadoReporte"></th></tr>');
				$('.encabezadoReporte').html(Access.headTable('Reporte de ventas', '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
				$('tbody.reporteX').append('<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>TOTAL DE VENTAS DEL PERIODO</b></td><td><b>'+totales.formatMoney()+'</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');
			});
		});	
        u.click('.datosPuntoVentaC', v => {
            B.sige.validator.vTable(v, 'datosPuntoVentaC', '.b-detallePos, .b-printTicket, .b-deleteTicket');
        });
		u.click('.b-deleteTicket', v => {
			let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de cancelar este ticket? Los productos regresarán a su inventario y no es posible deshacer esta acción. Sin embargo podrá ver dicha cancelación.','Aceptar',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'cancelTicketVenta', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
						case 'false':
							Core.toast('No se puede cancelar un ticket ya cancelado.','red');
						break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('PuntoVenta').get();
                        break;
                    }
                });
            });
		});
        u.click('.b-printTicket', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'PuntoVenta', method: 'getSimpleTicket', param: id}, r => {
                r = Core.json(false, r, false);
                r[18] = B.sige.fecha.date(r[18],'-');
                B.sige.ui.question('Impresión del ticket','<div class="coverjm center-align"><table style="font-size: 18px;width:225px;font-family:sans-serif;"><thead><tr><th style="text-align: center;"><img src="media/logoc.jpg" width="140" /><br>'+r[18]+'<br>'+r[1][0]+'<br><br>'+r[1][1]+'<br>'+r[1][2]+'<br>'+r[1][3]+', '+r[1][4]+', '+r[1][5]+'<br>'+r[1][7]+', '+r[1][8]+'<br><br>Fue atendido por: '+r[3]+'<br>Folio: '+r[0]+'</th></tr></thead><tbody><table style="font-size: 18px;width:225px;font-family:sans-serif;"><thead><tr><th>Cant.</th><th>Unit.</th><th style="text-align:right;">Total</th></tr></thead><tbody class="articulos"></tbody></table></tbody></table></div>','Imprimir',() => {
                    $('.coverjm').printArea({mode : "popup", popClose : true, extraHead : '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'});
                }, () => {
                    $.each(r[4], (i, e) => {
                        e[2] = e[2].toSimpleNumber();
                        e[3] = e[3].toSimpleNumber();
                        e[4] = e[4].toSimpleNumber();
                        e[2] = e[2].formatMoney();
                        e[3] = e[3].formatMoney();
                        $('.articulos').append('<tr><td colspan="3"><b>'+e[5][1]+', '+e[5][0]+'</b></td></tr><tr><td><b>'+e[1]+'</b></td><td><b>'+e[2]+'</b></td><td style="text-align: right;"><b>'+e[3]+'</b></td></tr>');
                    });
                    r[7] = r[7].toSimpleNumber().formatMoney();
                    r[8] = r[8].toSimpleNumber().formatMoney();
					if (r[9] == '0')
						r[9] = 'CANCELADO';
					else
						r[9] = r[9].toSimpleNumber().formatMoney();
					r[19] = (r[19] == '0') ? 'CONTADO' : 'CRÉDITO';
                    let art = $('.articulos');
                    art.append('<tr><td colspan="2" style="font-weight: bold;">&nbsp;</td><td style="text-align: right; font-weight: bold;">&nbsp;</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;">SUBTOTAL</td><td style="text-align: right; font-weight: bold;">'+r[7]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;">IVA</td><td style="text-align: right;font-weight: bold;">'+r[8]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;">TOTAL</td><td style="text-align: right;font-weight: bold;">'+r[9]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;">FORMA DE PAGO</td><td style="text-align: right;font-weight: bold;">'+r[19]+'</td></tr>');
					r[12] = r[12].toSimpleNumber().formatMoney();
					if (r[9].toSimpleNumber() < r[12].toSimpleNumber()){
						art.append('<tr><td colspan="2" style="font-weight: bold;">PAGO</td><td style="text-align:' +
							' right;' +
						 ' font-weight: bold;">'+r[12]+'</td></tr>');
						art.append('<tr><td colspan="2" style="font-weight: bold;">CAMBIO</td><td style="text-align:' +
							' right;' +
						 ' font-weight: bold;">'+(r[12].toSimpleNumber() - r[9].toSimpleNumber()).formatMoney()+'</td></tr>');
					}
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">&nbsp;</td><td>&nbsp;</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">Cliente</td><td>'+r[2]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">Camion</td><td>'+r[13]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">Marca</td><td>'+r[14]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">Placas</td><td>'+r[15]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;vertical-align:top;">Operador</td><td>'+r[16]+'<br><br><br><br><br><br><br><br><br></td></tr>'); //
                    art.append('<tr><td colspan="2" style="font-weight: bold; text-align: right;">Observ.</td><td>'+r[17]+'</td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;vertical-align:top;">Entregó</td><td><br><br><br><br><br><br><br><br><br></td></tr>');
                    art.append('<tr><td colspan="2" style="font-weight: bold;vertical-align:top;">Recibió</td><td><br><br><br><br><br><br><br><br><br></td></tr>');
                    art.append('<tr><td class="finalPage" colspan="3" style="font-weight: bold; text-align:' +
                     ' center;">Gracias por su compra</td></tr>');
                });
            });
            
        });
        u.click('.b-printDetail', () => {
            $('.coverjm2').printArea({mode : "popup", popClose : true, extraHead : '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'});
        });
        u.click('.b-detallePos', v => {
            let id = v.attr('target');
			B.sige.creator.getInstance('PuntoVenta').from = $('.breadcrumb').html();
            $('.pos-actual').html('<a class="breadcrumb">Punto de venta</a><a class="breadcrumb"Detalles</a>');
            B.sige.creator.getInstance('PuntoVenta').getComplexTicket(id);
        });
        u.click('.b-getPuntosVenta', () => {
			if (B.sige.creator.getInstance('PuntoVenta').from == "Cuentas por cobrar"){
				B.sige.creator.getInstance('PuntoVenta').get();
				$('.b-return').click();
				$('.openCard[target="CuentasPorCobrar"]').click();
			} else
				B.sige.creator.getInstance('PuntoVenta').get();
        });
        u.keyup('.ref', (v, e) => {
            let val = v.val();
            if (e.keyCode == 13){
                $('.product[code="'+val+'"]').click();
                return false;
            }
            
            let cot = (v.hasClass('cotizacion')) ? 'cotizacion' : (v.hasClass('ordenventa')) ? 'ordenventa' : '';
            if (val.length < 3)
                return false;
            B.sige.asyn.post({class:'PuntoVenta', method: 'getProductsByParam', param: [val, cot]}, r => {
                r = Core.json(false, r, false);
                $('.strproductos').html('');
                $.each(r, (i, e) => {
                    // if ($('.producted[id="'+e[0]+'"]').length > 0)
                    //     return;
                    e[5] = e[5].toSimpleNumber().formatMoney();
                    e[6] = e[6].toSimpleNumber().formatMoney();
                    $('.strproductos').append('<div class="product '+cot+' col s12 m4 center-align" id="'+e[0]+'" code="'+e[1]+'" nombre="'+e[2]+'" unidad="'+e[3]+'" familia="'+e[4]+'" mayoreo="'+e[5]+'" menudeo="'+e[6]+'" existencia="'+e[7]+'"><div class="card z-depth-2 hoverable"><div class="card-content row npadding"><div class="col s12 npadding tg-title">'+e[2]+'</div><div class="col s12 npadding">'+e[1]+' - '+e[4]+'</div><div class="col s6 npadding tg-subtitle">Mayoreo</div><div class="col s6 npadding tg-subtitle">Menudeo</div><div class="col s6 npadding">'+e[5]+'</div><div class="col s6 npadding">'+e[6]+'</div></div></div></div>');
                });
                if ($('.strproductos').html() !== '')
                    $('.strproductos').slideDown(150);
                else{
                    Core.toast('Nada que coincida con '+val,'blue');
                    $('.strproductos').slideUp(150);
                }
            });
        });
        u.click('.product', v => {
            let cot = (v.hasClass('cotizacion')) ? 'cotizacion' : (v.hasClass('ordenventa')) ? 'ordenventa' : '';
            let id = v.attr('id');
            let code = v.attr('code');
            let nombre = v.attr('nombre');
            let unidad = v.attr('unidad');
            let familia = v.attr('familia');
            let mayoreo = v.attr('mayoreo');
            let menudeo = v.attr('menudeo');
            let existencia = v.attr('existencia');
            let clsid = z.clsid();
            let producs = Form.select('tipoPrecio '+clsid,'Tipo de precio', [[mayoreo.toSimpleNumber(), 'Mayoreo: '+mayoreo], [menudeo.toSimpleNumber(), 'Menudeo: '+menudeo]]);
            let close = Form.btn(false, 'red', 'b-closeData', 'X');
            let unidades = Form.input('unidades '+clsid+' '+cot, 'number', 'Unidades: '+unidad, '', '', undefined, 12, '1', 'max="'+existencia+'"');
            let preciou = Form.input('preciou '+clsid+' '+cot, 'number', 'Precio unitario', '', '', undefined, 12, '', 'max="'+existencia+'"');
            let subtotal = Form.input('subtotal notdisabled bolder '+clsid, 'text', 'Subtotal', '', '');
            let impuesto = Access.impuesto(clsid);
            let total = Form.input('total notdisabled bolder '+clsid, 'text', 'Total', '', '');
            if ($('.producted[code="'+code+'"]').length == 0){
                console.log('select.tipoPrecio.'+clsid);
                $('.mainProducts').append('<div class="col s12 npadding producted z-depth-1" id="'+id+'" existencia="'+existencia+'" clsid="'+clsid+'" code="'+code+'"><div class="col s12 npadding heads"><div class="col s4 m1 cmxd code">'+code+'</div><div class="col s8 m2 cmxd familia">'+familia+'</div><div class="col s7 m5 cmxd nombre">'+nombre+'</div><div class="col s5 m3 producs">'+producs+'</div><div class="col offset-s10 s2 m1 cmxd close smnb">'+close+'</div></div><div class="col s12 npadding contents"><div class="col s6 m3 unidades input-field">'+unidades+'</div><div class="col s6 m2 preciou input-field">'+preciou+'</div><div class="col s12 m2 subtotal input-field">'+subtotal+'</div><div class="col s6 m3 impuestosgr input-field">'+impuesto+'</div><div class="col s6 m2 total input-field">'+total+'</div></div></div>');
                $('.tipoPrecio.'+clsid).val(mayoreo.toSimpleNumber());
                $('.tipoPrecio.'+clsid+', .impuestoS').formSelect();
                $('.tipoPrecio.'+clsid).change();
                $('.total.'+clsid+', .subtotal.'+clsid).attr('disabled','disabled');
            } else {
                var codem = $('.producted[code="'+code+'"]').children('.contents').children('.unidades').children('input').val();
                codem = parseInt(codem) + 1;
                $('.producted[code="'+code+'"]').children('.contents').children('.unidades').children('input').val(codem);
                var clsid2 = $('.producted[code="'+code+'"]').attr('clsid');
                $('.tipoPrecio.'+clsid2).change();
            }
            $('.ref').val('');
            v.hide(0, () => {
                $(this).remove();
            });
            M.updateTextFields();
        });
        u.keyup('.unidades', v => {
            let cot = (v.hasClass('cotizacion') || v.hasClass('ordenventa'));
            let clsid = v.attr('class').split(' ')[1];
            let uni = v.val();
            if (uni === '')
                return false;
            uni = uni.toSimpleNumber();
            if (!cot){
                let existencia = $('.producted[clsid="'+clsid+'"]').attr('existencia').toSimpleNumber();
                if (uni > existencia){
                    v.val(existencia);
                    $('.unidades.'+clsid).keyup();
                    Core.toast('Error, no hay suficientes unidades para completar la compra. Se ha puesto la cantidad máxima disponible.','red');
                    return false;
                }
            }
            let tipoprecio = $('.preciou.'+clsid).val();
            tipoprecio = (tipoprecio == null || tipoprecio === undefined) ? 0 : tipoprecio.toSimpleNumber();
            let sbtotal = uni * tipoprecio;
            $('.subtotal.'+clsid).val(sbtotal.formatMoney());
            let iva = $('select.impuestoS.'+clsid).val();
            let imp = 0, factor = 0;
            switch(iva){
                case '1':
                    factor = 0.16;
                    imp = sbtotal * factor;
                break;
                default:
                    factor = 0;
                    imp = sbtotal * factor;
                break;
            }
            let total = sbtotal + imp;
            $('.total.'+clsid).val(total.formatMoney());
            B.sige.creator.getInstance('PuntoVenta').calcularTotales();
            $('.b-pagarPunto').removeClass('disabled');
            B.sige.uf();
        });
        u.change('select.tipoPrecio', v => {
            let clsid = v.attr('class').split(' ')[1];
            $('.preciou.'+clsid).val(v.val());
            $('.unidades.'+clsid).keyup();
            B.sige.uf();
        });
        u.keyup('.preciou', v => {
            let clsid = v.attr('class').split(' ')[1];
            $('.unidades.'+clsid).keyup();
        });
        u.change('select.impuestoS', v => {
            let clsid = v.attr('class').split(' ')[1];
            $('.unidades.'+clsid).keyup();
        });
        u.click('.b-closeData', v => {
            v.parent().parent().parent().slideUp(150, function(){
                B.sige.creator.getInstance('PuntoVenta').calcularTotales();
                $(this).remove();
            });
        });
        u.click('.b-pagarPunto', v => {
            let addPayment = Form.btn(false, 'green', 'b-addPayment', 'Agregar método de pago');
            let xtotal = $('.xtotal').html();
            let productos = [[],[],[]];
            $('.producted').each(function(){
                let t = $(this);
                let id = t.attr('id');
                let clsid = t.attr('clsid');
                let unidades = $('input.unidades.'+clsid).val().toSimpleNumber();
                let tipoprecio = $('.preciou.'+clsid).val().toSimpleNumber();
                let subtotal = $('input.subtotal.'+clsid).val().toSimpleNumber();
                let total = $('input.total.'+clsid).val().toSimpleNumber();
                if (unidades !== 0 && subtotal !== 0 && total !== 0)
                    productos[0].push([id, unidades, tipoprecio, subtotal, total]);
            });
            productos[1].push($('.subt').html().toSimpleNumber());
            productos[1].push($('.ivaa').html().toSimpleNumber());
            productos[1].push($('div.xtotal').html().toSimpleNumber());
            productos[1].push($('input.camion').val());
            productos[1].push($('input.marca').val());
            productos[1].push($('input.placas').val());
            productos[1].push($('input.operador').val());
            productos[1].push($('textarea.observ').val());
            if (productos[0].length === 0){
                Core.toast('Complete cada información de los artículos de la lista. Ningún artículo debe llevar un 0.','red');
                return false;
            }
            let xx = $('.texter.cliente').html();
            if (xx === 'Seleccione cliente (opcional)')
                xx = '';
            else
                xx = '<div class="col s12 tg-formaPago">'+Form.select('tipoForma','Forma de pago', [[0, 'Contado'], [1, 'Crédito (vea después cuentas por cobrar)']])+'</div>';
            if (v.hasClass('cotizacion')){
				productos[1].push($('select.tipoForma').val());
				productos[1].push($('.fechaVenta').val());
                let act = $('.b-pagarPunto').hasClass('b-updateCotizacion') ? 'updateCotizacion' : 'saveCotizacion';
                B.sige.asyn.post({class:'PuntoVenta', method: act, param : productos}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'noexist':
                            Core.toast('El artículo seleccionado en rojo no existe para la sucursal seleccionada. Elimínelo de la lista para continuar','red');
                            $('.producted[id="'+r[1]+'"]').addClass('error');
                        break;
                        case 'true':
                            B.msn(r[0]);
                            $('.b-retroceder').click();
                            B.sige.creator.getInstance('Cotizaciones').build(() => {
                                $('.datosCotizacionesC[id="'+r[1]+'"]').click();
                                $('.b-detalleCot').click();
                                z.observer('.totalCover', () => {
                                    $('.b-printDetail').click();
                                });
                            });
                            $('.c-search, .c-bitacora').slideUp(100);
                            $('.pos-actual').slideDown(100);
                        break;
                    }
                });

            } else if(v.hasClass('ordenventa')){ //updateOrdenDeVenta
				productos[1].push($('select.tipoForma').val());
				productos[1].push($('.fechaVenta').val());
                let act = $('.b-pagarPunto').hasClass('b-updateOrdenDeVenta') ? 'updateOrdenVenta' : 'saveOrdenVenta';
                B.sige.asyn.post({class:'PuntoVenta', method: act, param : productos}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'noexist':
                            Core.toast('El artículo seleccionado en rojo no existe para la sucursal seleccionada. Elimínelo de la lista para continuar','red');
                            $('.producted[id="'+r[1]+'"]').addClass('error');
                        break;
                        case 'true':
                            
                            B.msn(r[0]);
                            $('.b-retroceder').click();
                            B.sige.creator.getInstance('OrdenDeVenta').build(() => {
                                $('.datosOrdenesVentaC[id="'+r[1]+'"]').click();
                                $('.b-detalleOventa').click();
                                z.observer('.totalCover', () => {
                                    $('.b-printDetail').click();
                                });
                            });
                            $('.c-search, .c-bitacora').slideUp(100);
                            $('.pos-actual').slideDown(100);
                        break;
                    }
                });
            } else if(v.hasClass('crearVentaM')){
				productos[1].push($('select.tipoForma').val());
				productos[1].push($('.fechaVenta').val());
                B.sige.asyn.post({class:'PuntoVenta', method: 'savePos', param : productos}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'noexist':
                            Core.toast('El artículo seleccionado en rojo no existe para la sucursal seleccionada. Elimínelo de la lista para continuar','red');
                            $('.producted[id="'+r[1]+'"]').addClass('error');
                        break;
                        case 'true':
                            B.msn(r[0]);
                            $('.b-retroceder').click();
                            B.sige.creator.getInstance('PuntoVenta').build(() => {
                                $('.datosPuntoVentaC[id="'+r[1]+'"]').click();
                                $('.b-printTicket').click();
                                z.observer('.finalPage', () => {
                                    $('.coverjm').printArea({mode : "popup", popClose : true, extraHead : '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>' } );
                                });
                            });
                            $('.c-search, .c-bitacora').slideUp(100);
                            $('.pos-actual').slideDown(100);
                        break;
                    }
                });
            } else {
                B.sige.ui.question('Elija la forma de pago','<div class="row">'+xx+'<div class="col s4 tg-title">Total a pagar</div><div class="col s8 tg-title right-align">'+xtotal+'</div><div class="col s12 smnb">'+addPayment+'</div><div class="col s12 paymentsFrame"></div><div class="col s4 tg-title">Cubierto</div><div class="col s8 tg-title right-align xcubierto">$0.00</div><div class="col s4 tg-title rpcb">Resta por cubrir</div><div class="col s8 tg-title right-align xresta">'+xtotal+'</div></div>','Confirmar pago',() => {
                    $('.methodPago').each(function(){
                        let m = $(this).children('.a002').children('.command').children('.cDir-content');
                        let monto = m.children('div:nth-child(1)').children('input').val().toSimpleNumber();
                        let formaPago = m.children('div:nth-child(2)').children('.select-wrapper').children('select').val();
                        let cuenta = m.children('div:nth-child(3)').children('input').val();
                        let observ = m.children('div:nth-child(4)').children('input').val();
                        if (monto > 0 && formaPago != null)
                            productos[2].push([monto, formaPago, cuenta, observ]);
                    });
                    if ($('select.tipoForma').val() === '0'){
                        if (productos[2].length === 0){
                            Core.toast('No especificó correctamente los montos de pago. Verifique e intente nuevamente','red');
                            return false;
                        }
                    }
					productos[1].push($('select.tipoForma').val());
					productos[1].push($('.fechaVenta').val());
                    B.sige.asyn.post({class:'PuntoVenta', method: 'savePos', param : productos}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'insufficient':
                                Core.toast('No hay suficientes unidades para completar la venta. Quizás se haya vendido en otro punto de venta o se haya alterado el inventario mientras realizaba la venta','red');
                                $('.producted[id="'+r[1]+'"]').addClass('error');
                            break;
                            case 'noexist':
                                Core.toast('El artículo seleccionado en rojo no existe para la sucursal seleccionada. Elimínelo de la lista para continuar','red');
                                $('.producted[id="'+r[1]+'"]').addClass('error');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                $('.b-retroceder').click();
                                B.sige.creator.getInstance('PuntoVenta').build(() => {
                                    $('.datosPuntoVentaC[id="'+r[1]+'"]').click();
                                    $('.b-printTicket').click();
                                    z.observer('.finalPage', () => {
                                        $('.coverjm').printArea({mode : "popup", popClose : true, extraHead : '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>' } );
                                    });
                                });
                                $('.c-search, .c-bitacora').slideUp(100);
                                $('.pos-actual').slideDown(100);
                            break;
                        }
                    });
                }, () => {
                    B.prop('tipoForma', '0');
                    $('.SelectedEmpresaI, .SelectedSucursalI, .tipoForma').formSelect();
                    $('.cntxdnd').addClass('disabled');
                });
            }
        });
        u.change('select.tipoForma',v => {
            if(v.val() === '1'){
                $('.cntxdnd').removeClass('disabled');
            } else {
                $('.cntxdnd').addClass('disabled');
            }
        });
        u.click('.b-addPayment', () => {
            let a = Access.miniComprobacion();
            let clsid = z.clsid();
            let btn1 = Form.btn(false,'red','b-remPago','Eliminar','','target="'+clsid+'"');
            $('.paymentsFrame').append('<div class="methodPago row col s12 '+clsid+'" clsid="'+clsid+'"><div class="col s12 offset-m8 m4 smnb">'+btn1+'</div><div class="col s12 a002">'+a[0]+'</div></div>');
            a[1]();
            
            $('.monto').addClass('montoStrix');
        });
        u.keyup('.montoStrix', () => {
            let cubierto = 0;
            let xtotal = $('.xtotal').html().toSimpleNumber();
            $('.montoStrix').each(function(){
                let val = $(this).val().toSimpleNumber();
                cubierto += val;
                xtotal -= val;
            });
            if (xtotal <= 0){
                $('.cntxdnd').removeClass('disabled');
                $('.rpcb').html('Cambio');
                xtotal = xtotal * -1;
            } else{
                $('.cntxdnd').addClass('disabled');
                $('.rpcb').html('Resta por cubrir');
            }
            $('.xcubierto').html(cubierto.formatMoney());
            $('.xresta').html(xtotal.formatMoney());
        });
        u.click('.b-cambiarSucursal', () => {
            let SelectedEmpresaI = Form.select('SelectedEmpresaI','¿A qué empresa aplica?', B.sige.validator.getForSelect('empresas',0, 1));
            let SelectedSucursalI = Form.select('SelectedSucursalI','¿A qué sucursal aplica?', []);
            B.sige.ui.question('Seleccione nuevo almacén para el punto de venta','<div class="col s12 input-field">'+SelectedEmpresaI+'</div><div class="col s12 input-field">'+SelectedSucursalI+'</div>','Establecer',() => {
                let v = $('select.SelectedSucursalI').val();
                if (v == null || v === undefined)
                    return false;
                B.sige.creator.getInstance('PuntoVenta').cambiarSucursal(v, () => {
                    Core.toast('Cambio de almacén realizado con éxito','green');
                });
            }, () => {
                $('.SelectedEmpresaI, .SelectedSucursalI').formSelect();
            });
        });
        u.click('.b-cambiarCliente', () => {
            B.sige.asyn.post({class:'clientes', method: 'getForClientes'}, r => {
                r = Core.json(false, r, false);
                let SelectClienteI = Form.input('SelectClienteI', 'text', 'Busque un cliente y presione enter', '', '');
                B.sige.ui.question('Seleccione un cliente (opcional)','<div class="col s12 input-field clientx">'+SelectClienteI+'</div><div class="col s12 height250"></div>','Establecer',() => {
                    let us = $('.SelectClienteI').val();
                    let v = $('.SelectClienteI').data('id');
                    B.sige.creator.getInstance('PuntoVenta').cambiarCliente(v, () => {
                        Core.toast('Cambio de cliente realizado con éxito','green');
                        us = (us === 'Seleccione un cliente') ? 'Seleccione cliente (opcional)' : 'Cliente: '+us;
                        $('.texter.cliente').html(us);
                    });
                }, () => {
                    Form.autocomplete('.SelectClienteI', r);
                });
                
            });
        });
        u.click('.b-retroceder', () => {
            $('.searchEngine').slideDown(300);
            $('.posventa').slideUp(0);
            $('.base').removeClass('ovhidden');
        });
    }
}
class Embarques{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'directions_bus';
        this.color = 'green';
        this.descr = 'agregar embarque, modificar embarque, eliminar embarque, crear embarque';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoEmbarque', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','AgregarNota', 'agregar status'));
        mc.append(B.sige.creator.simbtn('s8 m3','verHistorial', 'Historial'));
        mc.append(B.sige.creator.simbtn('s12 m3','finalizarEstado', 'Finalizar embarque'));
        mc.append(B.sige.creator.simbtn('s12 offset-m9 m3','eliminarEmbarques', 'Eliminar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en embarques', 'datosEmbarquesC'));
        this.get();
    }
    get(){
        $('.b-AgregarNota, .b-finalizarEstado, .b-verHistorial, .b-eliminarEmbarques').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Embarques</a>');
        B.sige.asyn.post({class:'embarques', method: 'getAllEmbarques'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
				switch(e[6][0]){
					case '-1':
						e[6][0] = 'Sin reporte'
					break;
					case '0':
						e[6][0] = 'Sin problema'
					break;
					case '1':
						e[6][0] = 'Con problema'
					break;
					case '2':
						e[6][0] = 'Por llegar'
					break;
				}
                e[5] = B.sige.fecha.date(e[5],'-',':');
				if (e[6][1] != '')
					e[6][1] = B.sige.fecha.date(e[6][1],'-',':');
                vldate.push([e[0], e[1], e[2], e[3], e[4], e[5], e[6][0], e[6][1]]);
            });
            let value = Form.table('datosPrestamos', 'datosEmbarquesC cfm','striped',['Id', 'Embarque', 'Chofer', 'Origen', 'Destino', 'Salida', 'Estado actual', 'Último reporte'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Embarques</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle del')+' embarque</a>');
        let btn1 = B.sige.creator.atrs('getEmbarques');
        let btn2 = Form.btn(false, 'green', 'b-saveEmbarque', 'Guardar');
        v = (v === j) ? ['','','','','','','','','','',[]] : v;
        $('.metainfo').hide();
		let embarque = Form.input('embarque', 'text', 'Embarque', '', '', j, 200, v[2]);
		let chofer = Form.input('chofer', 'text', 'Chofer', '', '', j, 200, v[3]);
		let origen = Form.input('origen', 'text', 'Origen', '', '', j, 100, v[4]);
		let destino = Form.input('destino', 'text', 'Destino', '', '', 100, j, v[5]);
		let observ = Form.area('observ','Observaciones del embarque.', 200, j, v[6]);	
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s6 input-field">'+embarque+'</div><div class="col s6 input-field">'+chofer+'</div><div class="col s6 input-field">'+origen+'</div><div class="col s6 input-field">'+destino+'</div><div class="col s12 input-field">'+observ+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div><div class="col s12 tg-title">Registro de status</div><div class="col s12 cPart2"></div>');
		if (v[2] !== ''){
			$('.b-saveEmbarque').html('Actualizar datos');
			let vldate = [];
			$.each(v[10], (i, e) => {
				switch(e[3]){
					case '-1':
						e[3] = 'Sin reporte'
					break;
					case '0':
						e[3] = 'Sin problema'
					break;
					case '1':
						e[3] = 'Con problema'
					break;
					case '2':
						e[3] = 'Por llegar'
					break;
				}
                e[5] = B.sige.fecha.date(e[5],'-',':');
                vldate.push([e[0], e[5], e[3], e[4]]);
            });
			let value = Form.table('datosStatus', 'datosStatusC cfm','striped',['Id', 'Fecha', 'Status registrado', 'Observaciones'], vldate);
            $('.cPart2').html(value);
		}
        B.sige.uf();
    }
    getComprobaciones(){
        $('.b-verHistorial, .b-borrarComprobante').addClass('disabled').attr('target', '');
        B.sige.asyn.post({class:'embarques', method: 'getComprobacionesById'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = B.sige.fecha.date(e[2],'-',':');
                e[3] = parseFloat(e[3]).formatMoney();
                e[4] = B.sige.access.formas[e[4]][1];
                vldate.push([e[0], e[1], e[2], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosComprobantes', 'datosComprobantesC cfm','striped',['id', 'Referencia', 'Fecha', 'Monto', 'Forma de pago', 'Responsable'], vldate);
            $('.tableGatos').html(value);
        });
    }
    clicks(){
        u.click('.b-verHistorial', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'embarques', method: 'getEmbarqueById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Embarques').nuovo(r);
            });
        });
        u.click('.b-nuevoEmbarque', () => {
            B.sige.creator.getInstance('Embarques').nuovo();
        });
        u.click('.b-getEmbarques', () => {
            B.sige.creator.getInstance('Embarques').get();
        });
        u.click('.datosEmbarquesC', v => {
            B.sige.validator.vTable(v, 'datosEmbarquesC', '.b-AgregarNota, .b-finalizarEstado, .b-verHistorial, .b-eliminarEmbarques');
        });
        u.click('.b-AgregarNota', v => {
			let id = v.attr('target');
			let j = B.sige.und;
			let status = Form.select('status','Seleccione el estado del viaje', [[0, 'Sin problemas'], [1, 'Con problemas'], [2, 'Por llegar']]);
			let observ = Form.area('observ','Observaciones del chofer.', 200, j, v[4]);	
			B.sige.ui.question('Agregar Status','<div class="row"><div class="col s12 input-field">'+status+'</div><div class="col s12 input-field">'+observ+'</div></div>','Guardar',() => {
				let v = [id, $('select.status').val(), $('.observ').val()], flag = true;
				$.each(v, function(o, u){
					if (u === '' || u == null || u === undefined)
						flag = false;
				});
				if (!flag){
					B.msn('empty');
					return false;
				}
				B.sige.asyn.post({class:'embarques', method: 'saveEstado', param: v}, r => {
					r = Core.json(false, r, false);
					switch(r[0]){
						case 'error':
						case 'denied':
							B.msn(r[0]);
						break;
						case 'true':
							B.msn(r[0]);
							B.sige.creator.getInstance('Embarques').get();
						break;
					}
				});
			}, () => {
				$('.status').formSelect();
			});
        });
        u.click('.b-finalizarEstado', v => {
            let id = v.attr('target');
			B.sige.ui.question('¿Está seguro de finalizar este embarque?','Se marcará como finalizado y desaparecerá de esta lista.','Continuar',() => {
				B.sige.asyn.post({class:'embarques', method: 'finalizarEmbarque', param: id}, r => {
					r = Core.json(false, r, false);
					switch(r[0]){
						case 'error':
						case 'denied':
							B.msn(r[0]);
						break;
						case 'true':
							B.msn(r[0]);
							B.sige.creator.getInstance('Embarques').get();
						break;
					}
				});
			}, () => {});
        });
		u.click('.b-eliminarEmbarques', v => {
            let id = v.attr('target');
			B.sige.ui.question('¿Está seguro de eliminar este embarque?','Se eliminarán todos los status relacionados de forma permanente.','Continuar',() => {
				B.sige.asyn.post({class:'embarques', method: 'eliminarEmbarque', param: id}, r => {
					r = Core.json(false, r, false);
					switch(r[0]){
						case 'error':
						case 'denied':
							B.msn(r[0]);
						break;
						case 'true':
							B.msn(r[0]);
							B.sige.creator.getInstance('Embarques').get();
						break;
					}
				});
			}, () => {});
        });
        u.click('.b-saveEmbarque', () => {
            let v = [$('.embarque').val(), $('.chofer').val(), $('.origen').val(), $('.destino').val(), $('.observ').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'embarques', method: 'saveEmbarque', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Embarques').get();
                    break;
                }
            });
        });
    }
}
class Asistencias{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Nómina';
        this.icon = 'access_time';
        this.color = 'amber';
        this.descr = 'ver faltas, ver asistencias';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content'); //B.sige.creator.simbtn('s4','buscarEmpleado', 'Buscar por empleado')
        mc.append('<div class="col s4 stridnm"></div>');
        mc.append(B.sige.creator.simpliest('Buscar en asistencias', 'datosAsistenciasC'));
        this.get();
    }
    get(){
        $('.b-buscarEmpleado').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Asistencias</a>');
        B.sige.asyn.post({class:'asistencia', method: 'getAllAsistencias'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
			let select = [['todos','Todos']], c = true;
            $.each(r, (i, e) => {
				c = true
				$.each(select, (j, k) => {
					if (k[0] == e[2])
						c = false;
				});
				if (c)
					select.push([e[2], e[2]]);
                e[1] = (e[1] === '') ? 'Sin registro' : e[1];
				e[6] = B.sige.fecha.date(e[6],'-');
				e[8] = (e[8] == '') ? '<div class="finishInitTime z-depth-1 " target="'+e[0]+'">'+Ic.c(2,'','access_time')+'</div>' : e[8];
                vldate.push([e[0], e[3], e[9], e[4], e[5], e[2], e[6], e[7], e[8]]);
            });
            let value = Form.table('datosAsistencias', 'datosAsistenciasC cfm','striped',['id', 'Trabajador', 'Observaciones', 'Empresa', 'Sucursal', 'Departamento', 'Fecha', 'Entrada', 'Salida'], vldate);
            $('.cPart').html(value);
			let periodoNomina = Form.select('selectDeptoAsistencia','Seleccione departamento', select); //v
			$('.stridnm').html(periodoNomina);
			$('.selectDeptoAsistencia').formSelect();
        });
    }
    clicks(){
        u.click('.datosAsistenciasC', v => {
            B.sige.validator.vTable(v, 'datosAsistenciasC', '.b-buscarEmpleado');
        });
		u.click('.finishInitTime', v => {
			let target = v.attr('target'), sss = $('.datosAsistenciasC[id="'+target+'"]').attr('search');
            B.sige.ui.question('Marcar salida','¿Está a punto de marcar salida al registro <i>'+sss+'</i> de trabajador?','Confirmar',() => {
                B.sige.login.msg(sss, target, function(){
					B.sige.creator.getInstance('Asistencias').get();
				});
            });
        });
		u.change('select.selectDeptoAsistencia', v => {
			let ser = v.val();
			$('tr.datosAsistenciasC').hide();
			if (ser == 'todos'){
				$('tr.datosAsistenciasC').show();
			} else {
				$('tr.datosAsistenciasC').each(function(){
					if ($(this).attr('search').indexOf(ser) != -1)
						$(this).show();
				});
			}
		});
    }
}
class Nominas{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Nómina';
        this.icon = 'insert_chart';
        this.color = 'blue-grey';
        this.descr = 'generar nómina';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		//let mc = $('.m-content');
        this.get();
    }
    get(){
        $('.b-detalleNomina').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Nóminas</a>');
		B.sige.creator.getInstance('Nominas').nuovo();
    }
    nuovo(){
        $('.pos-actual').html('<a class="breadcrumb">Nóminas</a><a class="breadcrumb">Nueva nómina</a>');
        let btn1 = '';
		$('.cPart').html('<div class="col s12 accessTo">'+B.sige.creator.simbtn('s12 m4','openPrestamos', 'Préstamos')+' '+B.sige.creator.simbtn('s12 m4','openPercepciones', 'Percepciones')+' '+B.sige.creator.simbtn('s12 m4','openDeducciones', 'Deducciones')+'</div><div class="opts"><div class="col s12 m4 genNomina" id="estajo"><div class="card center-align hoverable"><div class="card-content">'+Ic.c(3,'','layers')+'<br>Nómina de estajo</div></div> </div><div class="col s12 m4 genNomina" id="semana"><div class="card center-align hoverable"><div class="card-content">'+Ic.c(3,'','view_week')+'<br>Nómina semanal</div></div> </div><div class="col s12 m4 genNomina" id="quince"><div class="card center-align hoverable"><div class="card-content">'+Ic.c(3,'','date_range')+'<br>Nómina quincenal</div></div></div></div><div class="prepareTo"></div><div class="detailTo row"></div>');
    }
    vldate(r){
        let vldate = [];
        $.each(r, (i, e) => {
            e[2] = e[2].toSimpleNumber().formatMoney();
            e[11] = e[11].formatMoney();
            e[12] = e[12].formatMoney();
            switch(e[7]){
                case '0':
                    e[7] = 'No hacer nada';
                break;
                case '1':
                    e[7] = 'Descontar a los 5 minutos';
                break;
                case '2':
                    e[7] = 'Descontar a los 10 minutos';
                break;
                case '3':
                    e[7] = 'Descontar a los 15 minutos';
                break;
                case '4':
                    e[7] = 'Cero tolerancia';
                break;
                case '5':
                    e[7] = 'Descuento proporcional';
                break;
            }
            vldate.push(e);
        });
        return Form.table('datosNominas', 'datosNominasC cfm','striped centered',['Código', 'Empleado', 'Sueldo asignado', 'dias laborales', 'minutos laborales','dias trabajados','minutos a pagar trabajados','acción en multa', 'faltas', 'multas', 'mins descontados', 'dscto por multas','total a pagar'], vldate);
    }
	updateRowable(){
		let calcular = 0;
		$('.calculate:not([type="novalid"]):not([type="totalapagar"])').each(function(){
			let type = $(this).attr('type');
			let ammount = parseFloat($(this).attr('ammount'));
			if (type == 'suma')
				calcular += ammount;
			else
				calcular -= ammount;
		});
		$('.calculate[type="totalapagar"]').html('Total a pagar: '+calcular.formatMoney());
	}
	updatededuperc(){
		let deducibe = 0;
		$('input.deducible').each(function(){
			let v = $(this).val();
			v = (v == '') ? 0 : parseFloat(v);
			deducibe += v;
		});
		$('.calculate.deduccion').html('Total de deducciones: - '+deducibe.formatMoney()).attr('ammount', deducibe);
		let percep = 0;
		$('input.percepcionable').each(function(){
			let v = $(this).val();
			v = (v == '') ? 0 : parseFloat(v);
			percep += v;
		});
		$('.calculate.percepcion').html('Total de percepciones: + '+percep.formatMoney()).attr('ammount', percep);
		B.sige.creator.getInstance('Nominas').updateRowable();
	}
	createTable(v = ''){
		B.sige.asyn.post({class:'nominas', method: 'previsualize', param:v}, r => {
			r = Core.json(false, r, false);
			let vldate = [];
			let value = '';
			let select = [['todos','Todos']], c = true;
			switch(r[0][2]){
				case '0':
					let est_estajos = 0, est_subtotal = 0, est_prestamos = 0, est_apagar = 0, est_pagado = 0;
					$.each(r[1], (i, e) => {
						c = true;
						$.each(select, (j, k) => {
							if (k[0] == e[9])
								c = false;
						});
						if (c)
							select.push([e[9], e[9]]);
						e[3] = parseFloat(e[3]);
						e[3] = e[3].formatMoney();
						e[4] = parseFloat(e[4]);
						est_estajos += e[4];
						e[5] = parseFloat(e[5]);
						est_subtotal += e[5];
						e[5] = e[5].formatMoney();
						e[6] = parseFloat(e[6]);
						est_prestamos += e[6];
						e[6] = e[6].formatMoney();
						e[7] = parseFloat(e[7]);
						est_apagar += e[7];
						e[7] = e[7].formatMoney();
						e[10] = e[9];
						if (e[8] == 0){
							e[8] = parseFloat(e[8]);
							est_pagado += e[8];
							e[8] = e[8].formatMoney();
							e[9] = Form.btn(false, 'green', 'b-estcreateCalculo', 'Calcular','','target="'+e[0]+'"');
						} else {
							e[8] = parseFloat(e[8]);
							est_pagado += e[8];
							e[8] = e[8].formatMoney();
							e[9] = Form.btn(false, 'green', 'b-estmodifyCalculo', 'consultar','','target="'+e[0]+'"');
						}
						vldate.push(e);
					});
					est_subtotal = est_subtotal.formatMoney();
					est_prestamos = est_prestamos.formatMoney();
					est_apagar = est_apagar.formatMoney();
					est_pagado = est_pagado.formatMoney();
					r[0][2] = 'Estajo'; // Días lab/totales
					value = Form.table('datosNominasX', 'datosNominasXC cfm','striped',['id', 'Empleado', 'Asistencias', 'Precio estajo', 'Estajos', 'Subtotal', 'Préstamos', 'Pagar', 'PAGADO', 'Detalles', 'Departamento'], vldate);
					$('.prepareTo').html('<div class="col s12 sjtitle">Nómina para empleados de tipo: '+r[0][2]+' del '+r[0][0]+' al '+r[0][1]+'.<br>Días laborables de este periodo: '+r[0][3]+'</div><div class="col s12 ">'+value+'</div>');
					$('.datosNominasX > thead > tr > th:nth-child(11)').remove();
					$('.datosNominasX > tbody > tr > td:nth-child(11)').remove();
					let periodoNomina = Form.select('selectDeptoNomina','Seleccione departamento', select); //v
					$('.prepareTo').prepend('<div class="col s12 input-field">'+periodoNomina+'</div>');
					$('.selectDeptoNomina').formSelect();
					$('tbody.datosNominasX').append('<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+est_estajos+'</b></td><td><b>'+est_subtotal+'</b></td><td><b>'+est_prestamos+'</b></td><td><b>'+est_apagar+'</b></td><td><b>'+est_pagado+'</b></td><td></td></tr>');
				break;
				case '1':
				case '2':
					let sueldos = 0, asistencias = 0, faltas = 0, multas = 0, subtotal = 0, prestamos = 0, pagar = 0, pagado = 0;
					let sjor = (r[0][2] == '1') ? 'Jornada semanal' : 'Jornada quincenal';
					$.each(r[1], (i, e) => {
						c = true;
						$.each(select, (j, k) => {
							if (k[0] == e[10])
								c = false;
						});
						if (c)
							select.push([e[10], e[10]]);
						e[2] = parseFloat(e[2]);
						sueldos += e[2];
						e[2] = e[2].formatMoney();
						e[3] = parseInt(e[3]);
						asistencias += e[3];
						e[4] = parseInt(e[4]);
						faltas += e[4];
						e[5] = parseFloat(e[5]);
						multas += e[5];
						e[5] = e[5].formatMoney();
						e[6] = parseFloat(e[6]);
						subtotal += e[6];
						e[6] = e[6].formatMoney();
						e[7] = parseFloat(e[7]);
						prestamos += e[7];
						e[7] = e[7].formatMoney();
						e[8] = parseFloat(e[8]);
						pagar += e[8];
						e[8] = e[8].formatMoney();
						e[11] = e[10];
						if (e[9] == 0){
							e[9] = parseFloat(e[9]);
							pagado += e[9];
							e[9] = e[9].formatMoney();
							e[10] = Form.btn(false, 'green', 'b-estcreateCalculo', 'Calcular','','target="'+e[0]+'"');
						} else {
							e[9] = parseFloat(e[9]);
							pagado += e[9];
							e[9] = e[9].formatMoney();
							e[10] = Form.btn(false, 'green', 'b-estmodifyCalculo', 'consultar','','target="'+e[0]+'"');
						}
						vldate.push(e);
					});
					sueldos = sueldos.formatMoney();
					multas = multas.formatMoney();
					subtotal = subtotal.formatMoney();
					prestamos = prestamos.formatMoney();
					pagar = pagar.formatMoney();
					pagado = pagado.formatMoney();
					value = Form.table('datosNominasX', 'datosNominasXC cfm','striped',['id', 'Empleado', 'Sueldo', 'Asistencias', 'Faltas', 'Multa', 'Subtotal', 'Préstamos', 'Pagar', 'PAGADO', 'Detalles'], vldate);
					$('.prepareTo').html('<div class="col s12 sjtitle">Nómina para empleados de tipo: '+sjor+' del '+r[0][0]+' al '+r[0][1]+'.<br>Días laborables de este periodo: '+r[0][3]+'</div><div class="col s12 ">'+value+'</div>');
					$('.datosNominasX > tbody > tr > td:nth-child(12)').remove();
					let periodoNomina2 = Form.select('selectDeptoNomina','Seleccione departamento', select); //v
					$('.prepareTo').prepend('<div class="col s12 input-field">'+periodoNomina2+'</div>');
					$('.selectDeptoNomina').formSelect();
					$('tbody.datosNominasX').append('<tr><td>&nbsp;</td><td>TOTALES</td><td><b>'+sueldos+'</b></td><td><b>'+asistencias+'</b></td><td><b>'+faltas+'</b></td><td><b>'+multas+'</b></td><td><b>'+subtotal+'</b></td><td><b>'+prestamos+'</b></td><td><b>'+pagar+'</b></td><td><b>'+pagado+'</b></td><td></td></tr>');
				break;
			}
		});
	}
	createEdoCuenta(r){
		r[5] = parseInt(r[5]);
		r[7] = parseInt(r[7]);
		r[10] = parseInt(r[10]);
		r[11] = parseFloat(r[11]).formatMoney();
		r[8] = parseFloat(r[8]).formatMoney();
		r[9] = parseFloat(r[9]).formatMoney();
		let strtable = '<tr><td colspan="5">Trabajador: '+r[1][2]+' - '+r[1][1]+'</td><td colspan="3" class="right-align">'+B.sige.fecha.date(r[2],'-',':')+' al '+B.sige.fecha.date(r[3],'-',':')+'</td></tr>', type;
		switch(r[4]){
			case '0':
				type = 'ESTAJO';
				strtable += '<tr><td colspan="3">Laborados: '+r[5]+'</td><td colspan="2">Asistidos: '+r[7]+'</td><td colspan="3" class="right-align">Costo por estajo: '+r[8]+'</td></tr>';
				strtable += '<tr><td colspan="8"><b>Estajos realizados</b></td></tr>';
				strtable += '<tr><td colspan="2"><b>Fecha</b></td><td colspan="2"><b>Unidades</b></td><td colspan="2"><b>Observaciones</b></td><td colspan="2" class="right-align"><b>Total</b></td></tr>';
				$.each(r[20], function(i, e){
					e[0] = B.sige.fecha.date(e[0],'-',':');
					e[3] = e[3].formatMoney();
					strtable += '<tr><td colspan="2">'+e[0]+'</td><td colspan="2">'+e[1]+'</td><td colspan="2">'+e[2]+'</td><td colspan="2" class="right-align">'+e[3]+'</td></tr>';
				});
				strtable += '<tr><td colspan="2"><b>UNIDADES REALIZADAS</b></td><td colspan="2"><b>'+r[10]+'</b></td><td colspan="2" class="right-align"><b>TOTAL DE ESTAJOS</b></td><td colspan="2" class="right-align"><b>'+r[11]+'</b></td></tr>';
			break;
			case '1':
			case '2':
				type = 'NÓMINA '+((r[4] == '1') ? 'SEMANAL' : 'QUINCENAL');
				strtable += '<tr><td colspan="2">Laborados: '+r[5]+'</td><td colspan="2">Asistidos: '+r[7]+'</td><td colspan="2">Faltados: '+(r[5]-r[7])+'</td><td colspan="2" class="right-align">Sueldo: '+r[8]+'</td></tr>';
				strtable += '<tr><td colspan="8"><b>ASISTENCIAS REGISTRADAS</b></td></tr>';
				strtable += '<tr><td colspan="2"><b>Fecha</b></td><td colspan="2"><b>Hora entrada</b></td><td colspan="2"><b>Hora salida</b></td><td colspan="2" class="right-align"><b>Horas extras</b></td></tr>';
				let hextra = 0;
				$.each(r[20], function(i, e){
					e[0] = B.sige.fecha.date(e[0],'-',':');
					e[3] = parseInt(e[3]);
					hextra += e[3]
					strtable += '<tr><td colspan="2">'+e[0]+'</td><td colspan="2">'+e[1]+'</td><td colspan="2">'+e[2]+'</td><td colspan="2" class="right-align">'+e[3]+'</td></tr>';
				});
				strtable += '<tr><td colspan="7" class="right-align"><b>HORAS EXTRA</b></td><td colspan="1" class="right-align"><b>'+hextra+'</b></td></tr>';
				strtable += '<tr><td colspan="6"><b>¿Cómo se calculó el monto de faltas?</b></td><td colspan="1" class="right-align"><b>SUELDO</b></td><td colspan="1" class="right-align"><b>'+r[8]+'</b></td></tr>';
				strtable += '<tr><td colspan="6">'+r[8]+' -(('+r[8]+' / '+r[5]+') * '+r[7]+')</td><td colspan="1" class="right-align"><b>DESC. POR FALTAS</b></td><td colspan="1" class="right-align"><b>'+r[9]+'</b></td></tr>';
				strtable += '<tr><td colspan="7" class="right-align"><b>TOTAL ASISTENCIA</b></td><td colspan="1" class="right-align"><b>'+r[11]+'</b></td></tr>';
			break;
		}
		strtable += '<tr><td colspan="8"><b>PRÉSTAMOS ABONADOS</b></td></tr>';
		strtable += '<tr><td colspan="2"><b>Fecha</b></td><td colspan="2" class="right-align"><b>Deuda actual</b></td><td colspan="2" class="right-align"><b>Abonado</b></td><td colspan="2" class="right-align"><b>Resta</b></td></tr>';
		$.each(r[19], function(i, e){
			e[0] = B.sige.fecha.date(e[0],'-',':');
			e[1] = parseFloat(e[1]).formatMoney();
			e[2] = parseFloat(e[2]).formatMoney();
			e[3] = parseFloat(e[3]).formatMoney();
			strtable += '<tr><td colspan="2">'+e[0]+'</td><td colspan="2" class="right-align">'+e[1]+'</td><td colspan="2" class="right-align"><b>'+e[2]+'</b></td><td colspan="2" class="right-align">'+e[3]+'</td></tr>';
		});
		r[12] = parseFloat(r[12]).formatMoney();
		strtable += '<tr><td colspan="7" class="right-align"><b>MENOS PRÉSTAMOS</b></td><td colspan="1" class="right-align"><b>'+r[12]+'</b></td></tr>';
		strtable += '<tr><td colspan="8"><b>DEDUCCIONES APLICADAS</b></td></tr>';
		strtable += '<tr><td colspan="7"><b>Descripción de la deducción</b></td><td colspan="1" class="right-align"><b>Monto</b></td></tr>';
		$.each(r[13], function(i, e){
			e[1] = parseFloat(e[1]).formatMoney();
			strtable += '<tr><td colspan="7">'+e[0]+'</td><td colspan="1" class="right-align">'+e[1]+'</td></tr>';
		});
		r[14] = parseFloat(r[14]).formatMoney();
		r[16] = parseFloat(r[16]).formatMoney();
		r[17] = parseFloat(r[17]).formatMoney();
		strtable += '<tr><td colspan="7" class="right-align"><b>MENOS DEDUCCIONES</b></td><td colspan="1" class="right-align"><b>'+r[14]+'</b></td></tr>';
		strtable += '<tr><td colspan="8"><b>PERCEPCIONES APLICADAS</b></td></tr>';
		strtable += '<tr><td colspan="7"><b>Descripción de la percepción</b></td><td colspan="1" class="right-align"><b>Monto</b></td></tr>';
		$.each(r[15], function(i, e){
			e[1] = parseFloat(e[1]).formatMoney();
			strtable += '<tr><td colspan="7">'+e[0]+'</td><td colspan="1" class="right-align">'+e[1]+'</td></tr>';
		});
		strtable += '<tr><td colspan="7" class="right-align"><b>MAS PERCEPCIONES</b></td><td colspan="1" class="right-align"><b>'+r[16]+'</b></td></tr>';
		strtable += '<tr><td colspan="7" class="right-align"><b>TOTAL PAGADO</b></td><td colspan="1" class="right-align"><b>'+r[17]+'</b></td></tr>';
		return '<table><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td colspan="8" class="center-align"><b>REPORTE DE NÓMINA DE TIPO: '+type+'</b></td></tr>'+strtable+'</table>';
	}
    clicks(){
		u.click('.b-openPrestamos', () => {
			let act = B.sige.creator.active;
			$('.b-return').click();
			B.sige.creator.sret = act;
			$('.openCard[target="Prestamos"]').click();
		});
		u.click('.b-openPercepciones', () => {
			let act = B.sige.creator.active;
			$('.b-return').click();
			B.sige.creator.sret = act;
			$('.openCard[target="Percepciones"]').click();
		});
		u.click('.b-openDeducciones', () => {
			let act = B.sige.creator.active;
			$('.b-return').click();
			B.sige.creator.sret = act;
			$('.openCard[target="Deducciones"]').click();
		});
		u.click('.b-estmodifyCalculo', v=> {
			let worker = v.attr('target');
			B.sige.asyn.post({class:'nominas', method: 'getDetails', param:worker}, r => {
                r = Core.json(false, r, false);
				switch(r[0]){
					case 'notfound':
						Core.toast('No se pudo cargar este estado de cuenta','red');
					break;
					default:
						let table = B.sige.creator.getInstance('Nominas').createEdoCuenta(r);
						$('.detailTo').html('<div class="col s12 offset-m6 m3 smnb marginbottom20">'+Form.btn(false, 'green', 'b-repExportarExcel', 'Exportar')+'</div><div class="col s12 m3 smnb marginbottom20">'+Form.btn(false, 'green', 'b-regresarALaNomina', 'Regresar a la lista')+'</div><div class="col s12 contenidoReporte">'+table+'</div>');
						$('.detailTo').slideDown();
						$('.prepareTo').slideUp();
					break;
				}
            });
		});
		u.click('.genNomina', v => {
			let id = v.attr('id'), p;
			switch(id){
				case 'estajo':
					p = 0;
				break;
				case 'semana':
					p = 1;
				break;
				case 'quince':
					p = 2;
					id = 'quincena';
				break;
			}
			B.sige.asyn.post({class:'nominas', method: 'getBefore', param:p}, r => {
                r = Core.json(false, r, false);
				let periodoNomina = Form.select('periodoNomina','Seleccione periodo de '+id, r); //v[2]
				let jrk = Form.btn(false, 'green', 'b-obtenerNomina', 'Consultar');
				let jrq = Form.btn(false, 'blue', 'b-reporteSimple', 'Obtener reporte simple');
				let jrw = Form.btn(false, 'blue', 'b-ReporteDetallado', 'Obtener reporte detallado');
				$('.prepareTo').html('<div class="col s12 m8">'+periodoNomina+'</div><div class="col s12 m4 smnb">'+jrk+'</div><div class="col s12 m6 smnb">'+jrq+'</div><div class="col s12 m6 smnb">'+jrw+'</div>');
				$('.periodoNomina').formSelect();
            });
			$('.detailTo').slideUp();
			$('.prepareTo').slideDown();
		});
        u.click('.b-obtenerNomina', () => {
            let v = $('select.periodoNomina').val();
			if (v == null){
				Core.toast('Seleccione un periodo para continuar','red');
				return false;
			}
			B.sige.creator.getInstance('Nominas').createTable(v);
        });
		u.click('.b-estcreateCalculo', v => {
			let target = v.attr('target');
			B.sige.asyn.post({class:'nominas', method: 'calcularInit', param:target}, r => {
				r = Core.json(false, r, false);
				let value, vldate;
				r[6] = parseFloat(r[6]);
				let sueldo = r[6].formatMoney();
				let sld = (r[2][0]== '0') ? 'Precio por estajo' : (r[2][0]== '1') ? 'Sueldo semanal' : 'Sueldo quincenal';
				$('.detailTo').html('<div class="col s12 offset-m9 m3 smnb marginbottom20">'+Form.btn(false, 'green', 'b-regresarALaNomina', 'Regresar a la lista')+'</div><div class="col s12 rtg-title">Nombre del empleado: '+r[1]+'</div><div class="col s12 m6 rtg-title">'+r[3]+'</div><div class="col s12 m6 rtg-title">Días laborables: '+r[4]+'</div><div class="col s12 m6 rtg-title">Días asistidos: '+r[5]+'</div><div class="col s12 m6 rtg-title">'+sld+': '+sueldo+'</div><div class="col s12 m6 rtg-title">Horas extras: '+r[7]+'</div>');
				switch(r[2][0]){
					case '0':
						vldate = [];
						let estajos = 0, unidades = 0;
						$.each(r[8], (i, e) => {
							e[1] = parseFloat(e[1]);
							unidades += e[1];
							e[2] = parseFloat(e[2]);
							estajos += e[2];
							e[2] = e[2].formatMoney();
							e[0] = B.sige.fecha.date(e[0],'-',':');
							vldate.push(e);
						});
						value = Form.table('datosEstajo', 'datosEstajoC cfm','striped',['Fecha', 'Unidades', 'Total', 'Observaciones', 'id'], vldate);
						$('.detailTo').append('<div class="col s12 tg-title">Estajos realizados</div><div class="col s12">'+value+'</div><div class="col s12 m6 right-align calculate" type="novalid">Unidades hechas: '+unidades+'</div><div class="col s12 m6 right-align calculate" type="suma" ammount="'+estajos+'">Total de estajos: + '+estajos.formatMoney()+'</div>');
					break;
					case '1':
					case '2':
						vldate = [];
						let days = {'0' : 'Dom (no laborable)', '1':'LUN', '2':'MAR', '3':'MIE', '4':'JUE', '5':'VIE', '6':'SAB'};
						$.each(r[8], (i, e) => {
							e[3] = days[e[3]] || days['0'];
							e[0] = B.sige.fecha.date(e[0],'-',':');
							vldate.push(e);
						});
						value = Form.table('datosEstajo', 'datosEstajoC cfm','striped',['Fecha', 'Hora entrada', 'Hora salida', 'Día de la semana', 'Hrs extra'], vldate);
						$('.detailTo').append('<div class="col s12 tg-title">Asistencias durante el periodo</div><div class="col s12">'+value+'</div><div class="col s12 offset-m6 m6 right-align calculate" type="suma" ammount="'+r[6]+'">Sueldo normal: + '+sueldo+'</div><div class="col s12 notas">Para calcular estas faltas, se aplicó '+sueldo+' - (('+sueldo+' / '+r[12]+') * '+r[5]+')</div><div class="col s12 offset-m6 m6 right-align calculate" type="resta" ammount="'+r[10]+'">Descuento por faltas ('+r[11]+' faltas): - '+r[10].formatMoney()+'</div>');
					break;
				}
				vldate = [];
				let prestamos = 0;
				$.each(r[9], (i, e) => {
					e[1] = parseFloat(e[1]).formatMoney();
					e[2] = parseFloat(e[2]);
					prestamos += e[2];
					e[2] = e[2].formatMoney();
					e[3] = parseFloat(e[3]).formatMoney();
					e[0] = B.sige.fecha.date(e[0],'-',':');
					vldate.push(e);
				});
				value = Form.table('datosPresta', 'datosPrestaC cfm','striped',['Fecha', 'Deuda actual', 'Parcialidad a pagar', 'Resta', 'Referencia'], vldate);
				$('.detailTo').append('<div class="col s12 tg-title">Préstamos efectuados</div><div class="col s12">'+value+'</div><div class="col s12 right-align calculate" type="resta" ammount="'+prestamos+'">Total de préstamos: - '+prestamos.formatMoney()+'</div>');
				$('.detailTo').append('<div class="col s12 m9 tg-title">Deducciones aplicables</div><div class="col s12 m3 smnb">'+Form.btn(false, 'green', 'b-addDeduccion', 'Deducción', 'add')+'</div><div class="col s12 dedutable"></div><div class="col s12 right-align calculate deduccion" type="resta" ammount="0">Total de deducciones: - $0.00</div>');
				$('.detailTo').append('<div class="col s12 m9 tg-title">Percepciones aplicables</div><div class="col s12 m3 smnb">'+Form.btn(false, 'green', 'b-addPercepcion', 'Percepción', 'add')+'</div><div class="col s12 pertable"></div><div class="col s12 right-align calculate percepcion" type="suma" ammount="0">Total de percepciones: + $0.00</div><div class="col s12 right-align calculate" type="totalapagar" ammount="0">Total a pagar: $423.00</div><div class="col s12 m6 smnb">'+Form.btn(true, 'green', 'b-preGuardarInformacion', 'Preguardar información')+'</div><div class="col s12 m6 smnb">'+Form.btn(true, 'indigo', 'b-finalizarCalculo', 'Generar Nómina y guardar')+'</div>');
				B.sige.creator.getInstance('Nominas').updatededuperc();
				$('.detailTo').slideDown();
				$('.prepareTo').slideUp();
				let n = (r[2][0] == '0') ? 10 : 13;
				$.each(r[n][0], (i, e) => {
					v = [$.blurhouse.decode(e[0]), e[1]];
					$('.dedutable').append('<div class="col s12 deducible"><div class="col m6">'+v[0]+'</div><div class="col m5 input-field">'+Form.input('strmonto deducible', 'number', 'Monto', '', '', undefined, 200, v[1].toSimpleNumber())+'</div><div class="col m1 smnb">'+Form.btn(false, 'red', 'b-xRowNomina', 'x')+'</div></div>');
					B.sige.creator.getInstance('Nominas').updatededuperc();
					B.sige.uf();
				});
				$.each(r[n][1], (i, e) => {
					v = [$.blurhouse.decode(e[0]), e[1]];
					$('.pertable').append('<div class="col s12 percepcionable"><div class="col m6">'+v[0]+'</div><div class="col m5 input-field">'+Form.input('strmonto percepcionable', 'number', 'Monto', '', '', undefined, 200, v[1].toSimpleNumber())+'</div><div class="col m1 smnb">'+Form.btn(false, 'red', 'b-xRowNomina', 'x')+'</div></div>');
					B.sige.creator.getInstance('Nominas').updatededuperc();
					B.sige.uf();
				});
			});
		});
		u.click('.b-addDeduccion, .b-addPercepcion', v => {
			let opt = v.hasClass('b-addDeduccion') ? ['deducciones', 'deducción', 'deducible', 'dedutable'] : ['percepciones', 'percepción', 'percepcionable', 'pertable'];
			let vldate = [], value;
			$.each(B.sige.validator.getForSelect(opt[0],1, 2), (i, e) => {
				e[1] = parseFloat(e[1]).formatMoney();
				vldate.push(e);
			});
			value = Form.table('datosSelectablx', 'datosSelectablxC cfm','striped',['Descripción', 'Monto'], vldate);
			B.sige.ui.question('Seleccione una '+opt[1],value,'Seleccionar', () => {
				let v = $('tr.selectablable');
				if (v.length > 0){
					v = [v.children('td:nth-child(1)').html(), v.children('td:nth-child(2)').html()];
					$('.'+opt[3]).append('<div class="col s12 '+opt[2]+'"><div class="col m6">'+v[0]+'</div><div class="col m5 input-field">'+Form.input('strmonto '+opt[2], 'number', 'Monto', '', '', undefined, 200, v[1].toSimpleNumber())+'</div><div class="col m1 smnb">'+Form.btn(false, 'red', 'b-xRowNomina', 'x')+'</div></div>');
					B.sige.creator.getInstance('Nominas').updatededuperc();
					B.sige.uf();
				}
			}, () => {
				$('.modal-close.cntxdnd').addClass('disabled');
			});
		});
		u.click('.b-finalizarCalculo, .b-preGuardarInformacion', v => {
			let kind = v.hasClass('b-finalizarCalculo') ? 'finishInit' : 'preFinishInit';
			let vv = [[],[]];
			$('div.deducible').each(function(){
				vv[0].push([$(this).children('div:nth-child(1)').html(), $(this).children('div:nth-child(2)').children('input').val()]);
			});
			$('div.percepcionable').each(function(){
				vv[1].push([$(this).children('div:nth-child(1)').html(), $(this).children('div:nth-child(2)').children('input').val()]);
			});
			B.sige.asyn.post({class:'nominas', method: kind, param:Core.json(true, vv, false)}, r => {
				r = Core.json(false, r, false);
				switch(r[0]){
					case 'true':
						Core.toast('Nómina calculada con éxito para este empleado','green');
						$('.b-regresarALaNomina').click();
						B.sige.creator.getInstance('Nominas').createTable();
					break;
					case 'presaved':
						Core.toast('Datos del empleado preguardados con éxito. Aún puede modificar esta información hasta generar la nómina','green');
						$('.b-regresarALaNomina').click();
						B.sige.creator.getInstance('Nominas').createTable();
					break;
					default:
						Core.toast('Error al generar la nómina para este empleado. Intente nuevamente.','red');
					break;
				}
			});
		});
		u.keyup('.strmonto', () => {
			B.sige.creator.getInstance('Nominas').updatededuperc();
		});
		u.click('.b-xRowNomina', v => {
			v.parent().parent().remove();
			B.sige.creator.getInstance('Nominas').updatededuperc();
		});
		u.click('tr.datosSelectablxC', v => {
			$('tr.datosSelectablxC').removeClass('selectablable');
			v.addClass('selectablable');
			$('.modal-close.cntxdnd').removeClass('disabled');
		});
		u.click('.b-regresarALaNomina', () => {
			$('.detailTo').slideUp();
			$('.prepareTo').slideDown();
		});
		u.click('.b-nuevaNomina', () => {
            B.sige.creator.getInstance('Nominas').nuovo();
        });
        u.click('.b-getNominas', () => {
            B.sige.creator.getInstance('Nominas').get();
        });
		u.click('.b-reporteSimple', () => {
            let periodo = $('select.periodoNomina').val();
			if (periodo == null){
				Core.toast('Seleccione un periodo para continuar','red');
				return false;
			}
			B.sige.asyn.post({class:'nominas', method: 'getSimpleDetailReport', param: periodo}, r => {
				r = Core.json(false, r, false);
				let vldate = [];
				$.each(r[1], (i, e) => {
					e[2] = parseFloat(e[2]).formatMoney();
					e[3] = parseFloat(e[3]).formatMoney();
					e[4] = parseFloat(e[4]).formatMoney();
					vldate.push([e[0], e[1], e[2], e[3], e[4], '']);
				});
				let value = Form.table('datosSDhuebjq', 'datosSDhuebjqC cfm','striped',['Nómina No.', 'Empleado', 'Subtotal', 'Préstamos', 'Total pagado', 'Firma de recibido'], vldate);
				let tipo = (r[0][2] == '0') ? 'Estajo' : (r[0][2] == '0') ? 'Nómina semanal' : 'Nómina quincenal';
				
				$('.detailTo').html('<div class="col s12 offset-m8 m4 smnb">'+Form.btn(false, 'green', 'b-repExportarExcel', 'Exportar')+'</div><div class="col s12 desrjd">En esta lista aparecen las nóminas que ya han sido calculadas, es decir, las que están disponibles para consultas. En caso de no ver un nombre, asegúrese de que el empleado tenga su nómina generada.</div><div class="col s12 contenidoReporte">'+value+'</div>');
				$('.datosSDhuebjq > thead').prepend('<tr><th colspan="6" class="encabezadoReporte center-align">Reporte simple de tipo <b>'+tipo+'</b> para la fecha de entre '+r[0][0]+' y '+r[0][1]+'</th></tr>');
				$('.detailTo').fadeIn(200);
			});
        });
		u.click('.b-ReporteDetallado', () => {
            let periodo = $('select.periodoNomina').val();
			if (periodo == null){
				Core.toast('Seleccione un periodo para continuar','red');
				return false;
			}
			B.sige.asyn.post({class:'nominas', method: 'getComplexDetailReport', param: periodo}, r => {
				r = Core.json(false, r, false);
				let vldate = [];
				$.each(r[1], (i, e) => {
					if (r[0][2] == '0'){
						e[3] = parseFloat(e[3]).formatMoney();
					}
					e[4] = parseFloat(e[4]).formatMoney();
					e[5] = parseFloat(e[5]).formatMoney();
					e[6] = parseFloat(e[6]).formatMoney();
					e[7] = parseFloat(e[7]).formatMoney();
					e[8] = parseFloat(e[8]).formatMoney();
					e[9] = parseFloat(e[9]).formatMoney();
					vldate.push([e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7], e[8], e[9], '']);
				});
				let uwqhn = '';
				if (r[0][2] == '0'){
					uwqhn = ['Nómina No.', 'Empleado', 'No. de estajos', 'Costo / Estajo', 'Subtotal', 'Multas','Percepciones','Deducciones','Préstamos','Total Pagado', 'Firma de recibido'];
				} else {
					uwqhn = ['Nómina No.', 'Empleado', 'Asistencias', 'Faltas', 'Subtotal', 'Multas','Percepciones','Deducciones','Préstamos','Total Pagado', 'Firma de recibido'];
				}
				
				let value = Form.table('datosSDhuebjq', 'datosSDhuebjqC cfm','striped',uwqhn, vldate);
				let tipo = (r[0][2] == '0') ? 'Estajo' : (r[0][2] == '0') ? 'Nómina semanal' : 'Nómina quincenal';
				
				$('.detailTo').html('<div class="col s12 offset-m8 m4 smnb">'+Form.btn(false, 'green', 'b-repExportarExcel', 'Exportar')+'</div><div class="col s12 desrjd">En esta lista aparecen las nóminas que ya han sido calculadas, es decir, las que están disponibles para consultas. En caso de no ver un nombre, asegúrese de que el empleado tenga su nómina generada.</div><div class="col s12 contenidoReporte">'+value+'</div>');
				$('.datosSDhuebjq > thead').prepend('<tr><th colspan="11" class="encabezadoReporte center-align">Reporte simple de tipo <b>'+tipo+'</b> para la fecha de entre '+r[0][0]+' y '+r[0][1]+'</th></tr>');
				$('.detailTo').fadeIn(200);
			});
        });
		u.change('select.selectDeptoNomina', v => {
			let ser = v.val();
			$('tr.datosNominasXC').hide();
			if (ser == 'todos'){
				$('tr.datosNominasXC').show();
			} else {
				$('tr.datosNominasXC').each(function(){
					if ($(this).attr('search').indexOf(ser) != -1)
						$(this).show();
				});
			}
		});
    }
}
class Prestamos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'directions_walk';
        this.color = 'cyan';
        this.descr = 'agregar préstamos, modificar préstamos, eliminar préstamos, crear préstamos';
        this.idPermiso = permiso;
        this.clicks();
		this.tipoPrestamo = '';
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s12 m4','nuevoPrestamo v1', 'Nuevo restringido'));
        mc.append(B.sige.creator.simbtn('s12 m4','nuevoPrestamo v2', 'Nuevo Libre'));
        mc.append(B.sige.creator.simbtn('s12 m4','comprobarPrestamos', 'Comprobar'));
		if (B.sige.validator.hasAccessTo('Cancelaciones'))
			mc.append(B.sige.creator.simbtn('s12 m4','cancelarPrestamos', 'Cancelar', 'red'));
        mc.append(B.sige.creator.simbtn('s12 offset-m4 m4','detallePrestamos', 'Detalles'));
        mc.append(B.sige.creator.simpliest('Buscar en préstamos', 'datosPrestamosC'));
        this.get();
    }
    get(){
        $('.b-comprobarPrestamos, .b-detallePrestamos, .b-cancelarPrestamos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Préstamos</a>');
        B.sige.asyn.post({class:'prestamos', method: 'getAllPrestamos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = parseFloat(e[2]).formatMoney();
                e[3] = parseFloat(e[3]).formatMoney();
                e[4] = parseFloat(e[4]).formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosPrestamos', 'datosPrestamosC cfm','striped',['Referencia', 'Empleado', 'Deuda', 'Cubierto', 'Saldo'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
		let tipoP = (this.tipoPrestamo) ? 'restringido' : 'sin restricciones (libre)';
        $('.pos-actual').html('<a class="breadcrumb">Préstamos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle')+' préstamo '+tipoP+'</a>');
        let btn1 = B.sige.creator.atrs('getPrestamos');
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-savePrestamo', 'Guardar') : '';
		let edocuenta = Form.btn(false, 'blue', 'b-edoCuenta', 'Estado de cuenta');
		
        v = (v === j) ? [B.ref(),'','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
		let SelectedEmpleadoE = Form.input('SelectedEmpleadoE', 'text', 'Busque un empleado y presione enter', '', '');
        let monto = Form.input('monto', 'number', 'Monto del préstamo', '', '', j, 12, v[5]);
        let formaPago = Access.formaPago(); //v[6]
		let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[7]);
		let parcialidad = Form.input('parcialidad', 'number', 'Parcialidad (se descontará en función de la jornada registrada)', '', '', j, 12, v[8]);
		//let quienAutoriza = Form.select('quienAutoriza','¿Quién autoriza?', B.sige.validator.getForSelect('trabajadores',0, 2)); //v[6]
		let descr = Form.area('descr','Descripción o propósito del préstamo.', 200, j, v[3]);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s4 input-field">'+ref+'</div><div class="col s8 input-field">'+SelectedEmpleadoE+'</div><div class="col s6 input-field">'+monto+'</div><div class="col s6 input-field">'+formaPago+'</div><div class="col s12 m6 input-field">'+cuenta+'</div><div class="col s12 m6 input-field">'+parcialidad+'</div><div class="col s12 input-field">'+descr+'</div><div class="col s12 m3 smnb">'+edocuenta+'</div><div class="col s12 offset-m6 m3 smnb">'+btn2+'</div>');
		Form.autocomplete('.SelectedEmpleadoE', B.sige.validator.getForSelect('trabajadores',0, 2));
        if (v[1] !== ''){
			let trabajo = B.sige.validator.searchSensitive('trabajadores', v[4], 0);
			$('.SelectedEmpleadoE').val(trabajo[0][2]);
            B.prop('formaPago', v[6]);
		}
        $('.formaPago').formSelect();
        if (v[1] !== ''){
			$('.b-edoCuenta').hide();
            $('.SelectedEmpleadoE, select.formaPago, .monto, .cuenta, .descr, .parcialidad').attr('disabled','disabled');
            $('.formaPago').formSelect();
            let bt1 = Form.btn(false,'green','disabled b-detalleComprobante','Detalles');
            let bt2 = Form.btn(false,'red','disabled b-borrarComprobante','Borrar');
            $('.cPart').append('<div class="margintop20 divider col s12"></div><div class="tg-title col s12">Comprobantes disponibles</div><div class="tg-title col offset-m6 s6 m3 smnb">'+bt1+'</div><div class="tg-title col s6 m3 smnb">'+bt2+'</div><div class="tableGatos col s12"></div>');
            B.sige.creator.getInstance('Prestamos').getComprobaciones();
        }
        B.sige.uf();
    }
    getComprobaciones(){
        $('.b-detalleComprobante, .b-borrarComprobante').addClass('disabled').attr('target', '');
        B.sige.asyn.post({class:'prestamos', method: 'getComprobacionesById'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = B.sige.fecha.date(e[2],'-',':');
                e[3] = parseFloat(e[3]).formatMoney();
                e[4] = B.sige.access.formas[e[4]][1];
                vldate.push([e[0], e[1], e[2], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosComprobantes', 'datosComprobantesC cfm','striped',['id', 'Referencia', 'Fecha', 'Monto', 'Forma de pago', 'Responsable'], vldate);
            $('.tableGatos').html(value);
        });
    }
    clicks(){
		u.click('.b-edoCuenta', () => {
			let v = $('.SelectedEmpleadoE').data('id');
			if (v == undefined){
				Core.toast('Primero busque un empleado.','red');
				return false;
			}
			B.sige.ui.question('Consultar estado de cuenta de empleado','<div class="row"><div class="col s12 m8">'+Form.input('periodos','text','Periodos a obtener','')+'</div><div class="col s12 m4 smnb">'+Form.btn(false, 'green', 'b-getReporteNominaByPeriodo', 'Obtener')+'</div><div class="col s12 ibusdf"></div></div>','Cerrar',() => {
				
			}, () => {
				
			});
		});
		u.click('.b-getReporteNominaByPeriodo', () => {
			let v = $('.b-getReporteNominaByPeriodo').val();
			if (v == '')
				v = 3;
			B.sige.asyn.post({class:'nominas', method: 'getReporteNominaByPeriodo', param:[$('.SelectedEmpleadoE').data('id'), v]}, r => {
                r = Core.json(false, r, false);
				let vldate = [];
				$.each(r, (i, e) => {
					e[0] = B.sige.fecha.date(e[0],'-',':');
					e[1] = B.sige.fecha.date(e[1],'-',':');
					e[2] = (e[2] == '0') ? 'Estajo' : (e[2] == '1') ? 'Semanal' : 'Quincenal';
					e[3] = parseFloat(e[3]).formatMoney();
					e[4] = parseFloat(e[4]).formatMoney();
					e[5] = parseFloat(e[5]).formatMoney();
					e[6] = parseFloat(e[6]).formatMoney();
					vldate.push(e);
				});
				let value = Form.table('datosPrestamos', 'datosPrestamosC cfm','striped',['Inicio', 'Fin', 'Tipo', 'Préstamos', 'Deducciones', 'Percepciones', 'Pagado'], vldate);
				$('.ibusdf').html(value);
            });
		});
        u.click('.datosComprobantesC', v => {
            B.sige.validator.vTable(v, 'datosComprobantesC', '.b-detalleComprobante, .b-borrarComprobante, .b-borrarComprobanteP');
        });
        u.click('.b-detalleComprobante', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'prestamos', method: 'getComprobanteById', param: id}, r => {
                r = Core.json(false, r, false);
                let ac = Access.comprobacion();
                B.sige.ui.question('Detalle del comprobante de prestamos','<div class="row"><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Cerrar',() => {}, () => {
                    $('.monto, select.formaPago, .cuenta, .SelectedEmpleadoD, .observ, select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled','disabled');
                    B.sige.creator.getInstance('Trabajadores').editing = true;
					/*
                    let sucursal = B.sige.validator.searchIn('departamentos', r[5], 0);
                    let empresa = B.sige.validator.searchIn('sucursales', sucursal[0][1], 0);
                    B.prop('SelectedEmpresaD', empresa[0][1]);
                    $('.SelectedEmpresaD').formSelect();
                    $('select.SelectedEmpresaD').change();
                    B.prop('SelectedSucursalD', sucursal[0][1]);
                    $('select.SelectedSucursalD').change();
					*/
					/*
                    B.prop('SelectedDepartamentoD', r[5]);
                    $('.SelectedDepartamentoD').formSelect();
                    Access.getEmpleadosByDepId(r[5], () => {
                        B.prop('SelectedEmpleadoD',r[3]);
                        $('.SelectedEmpleadoD').formSelect();
                    },'D');
					*/
					$('.cDir-content > .col.s12.m6').hide();
					$('.SelectedEmpleadoD').val(r[3]);
                    $('.monto').val(r[0]);
                    B.prop('formaPago', r[1]);
                    $('.cuenta').val(r[2]);
                    $('.observ').val(r[4]);
                    ac[1]();
                    B.sige.uf();
                    
                });
            });
        });
        u.click('.b-borrarComprobante', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este comprobante? Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'prestamos', method: 'delComprobante', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                        case 'comprobado':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Prestamos').getComprobaciones();
                        break;
                    }
                });
            });
        });
		u.click('.b-cancelarPrestamos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este préstamo? Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'prestamos', method: 'delPrestamo', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Prestamos').get();
                        break;
                    }
                });
            });
        });
        u.change('select.SelectedEmpresaE', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalE').html(Form.initSelect(filter));
            $('.SelectedSucursalE').formSelect();
            $('select.SelectedSucursalE').change();
        });
        u.change('select.SelectedSucursalE', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('departamentos', v.val(), 1), 0, 4);
            $('select.SelectedDepartamentoE').html(Form.initSelect(filter));
            $('.SelectedDepartamentoE').formSelect();
            $('select.SelectedDepartamentoE').change();
        });
        u.change('select.SelectedDepartamentoE', v => {
            Access.getEmpleadosByDepId(v.val(), () => {
                $('select.SelectedEmpleadoE').change();
                $('.SelectedEmpleadoE').formSelect();
            }, 'E');
        });
        u.click('.b-nuevoPrestamo', v => {
			B.sige.creator.getInstance('Prestamos').tipoPrestamo = v.hasClass('v1');
            B.sige.creator.getInstance('Prestamos').nuovo();
        });
        u.click('.b-getPrestamos', () => {
            B.sige.creator.getInstance('Prestamos').get();
        });
        u.click('.datosPrestamosC', v => {
            B.sige.validator.vTable(v, 'datosPrestamosC', '.b-comprobarPrestamos, .b-detallePrestamos, .b-cancelarPrestamos');
        });
        u.click('.b-comprobarPrestamos', v => {
            let id = v.attr('target');
            let ac = Access.comprobacion();
            B.sige.asyn.post({class:'prestamos', method: 'setIdComprobacion', param: id}, r => {
                r = Core.json(false, r, false);
                r[1][0] = parseFloat(r[1][0]);
                r[1][1] = parseFloat(r[1][1]);
                r[1][2] = r[1][0] - r[1][1];
                if (r[1][0] === r[1][1]){
                    Core.toast('Este préstamo se ha comprobado en su totalidad. Si desea modificar un registro, de clic en detalles','blue rounded');
                    return false;
                }
                B.sige.ui.question('Comprobar prestamos','<div class="row"><div class="col s12 m8">Monto pendiente por comprobar</div><div class="col s12 m4 totalPorCubrir" target="'+r[1][2]+'">'+r[1][2].formatMoney()+'</div><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Guardar',() => {
                    let v = [
                        $('.monto').val(),
                        $('select.formaPago').val(),
                        $('.cuenta').val(),
                        $('.SelectedEmpleadoD').val(),
                        $('.observ').val(),
                    ], flag = true;
                    $.each(v, function(o, u){
                        if (u === '' || u == null || u === undefined)
                            flag = false;
                    });
                    if (!flag){
                        B.msn('empty');
                        return false;
                    }
                    B.sige.asyn.post({class:'prestamos', method: 'comprobarPrestamo', param: v}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'mayor':
                                Core.toast('El monto que está comprobando es mayor al monto que resta para este préstamo','red rounded');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                B.sige.creator.getInstance('Prestamos').get();
                            break;
                        }
                    });
                }, () => {
                    ac[1]();
					let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
					B.prop('SelectedEmpresaD', empresa[0][1]);
					$('select.SelectedEmpresaD').change();
					B.prop('SelectedSucursalD', $.cookie('sucursal'));
					$('select.SelectedSucursalD').change();
					$('select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled', 'disabled');
					$('.SelectedEmpresaD').formSelect();
					$('.SelectedSucursalD').formSelect();
                    $('.monto').addClass('gastoKeyUp');
                });
            });
        });
        u.keyup('.gastoKeyUp', v => {
            let m = v.val();
            m = (m === '') ? 0 : parseFloat(m);
            let t = parseFloat($('.totalPorCubrir').attr('target'));
            let r = (t - m);
            if (r < 0){
                Core.toast('No puede comprobar más dinero ('+m.formatMoney()+') del que resta por cubrir ('+t.formatMoney()+')','blue rounded');
                v.val(t);
            }
            r = (r < 0) ? 0 : r;
            $('.totalPorCubrir').html(r.formatMoney());
            
        });
        u.click('.b-detallePrestamos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'prestamos', method: 'getPrestamosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Prestamos').nuovo(r);
            });
        });
        u.click('.b-savePrestamo', () => {
            let v = [
                $('.ref').val(),
                $('.SelectedEmpleadoE').data('id'),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.parcialidad').val(),
                $('.descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
			if (v[5] == '' || v[5] == '0'){
				Core.toast('Seleccione una parcialidad','red');
				return false;
			}
			v[2] = parseInt(v[2]);
			v[5] = parseInt(v[5]);
			if (v[2]%v[5] != 0){
				Core.toast('Las parcialidades deben ser iguales para cobrar lo mismo cada semana hasta llegar a 0.','red');
				return false;
			}
			let rest = (B.sige.creator.getInstance('Prestamos').tipoPrestamo) ? '1' : '0';
            B.sige.asyn.post({class:'prestamos', method: 'savePrestamo', param: v, restriccion : rest}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
					case 'limite':
						r[1] = parseFloat(r[1]).formatMoney();
						r[2] = parseFloat(r[2]).formatMoney();
						r[3] = parseFloat(r[3]).formatMoney();
                        Core.toast('Error. Su límite de crédito es de '+r[2]+' y ha utilizado '+r[3]+' por lo que únicamente se le podría prestar '+r[1]+'','red', 10000);
                    break;
					case 'asistencia':
						let saldo = r[1];
						r[1] = parseFloat(r[1]).formatMoney();
						r[2] = parseFloat(r[2]).formatMoney();
						r[3] = parseFloat(r[3]).formatMoney();
                        Core.toast('Límite del préstamo (por asistencia o límite de crédito) es de '+r[1]+' menos préstamos por '+r[2]+', se le puede prestar sólo '+r[3],'red', 10000);
                    break;
					case 'falta':
						r[1] = parseFloat(r[1]).formatMoney();
                        Core.toast('No se ha cumplido el mínimo de asistencia requerida (50%) para otorgarle un préstamo.','red');
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Prestamos').get();
                    break;
                }
            });
        });
    }
}
class Gastos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'money_off';
        this.color = 'red';
        this.descr = 'agregar gastos, modificar gastos, eliminar gastos, crear gastos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoGasto', 'Nuevo'));
        // mc.append(B.sige.creator.simbtn('s8 m3','comprobarGastos', 'Comprobar'));
        mc.append(B.sige.creator.simbtn('s8 m3','cancelarGastos', 'Cancelar', 'red'));
        mc.append(B.sige.creator.simbtn('s12 m3','detalleGastos', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s12 m3','gestCuenta', 'Mi cuenta'));
        mc.append(B.sige.creator.simpliest('Buscar en gastos', 'datosGastosC'));
        this.get();
    }
    get(){
        $('.b-comprobarGastos, .b-detalleGastos, .b-cancelarGastos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a>');
        B.sige.asyn.post({class:'gastos', method: 'getAllGastos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = parseFloat(e[2]).formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosGastos', 'datosGastosC cfm','striped',['Referencia', 'Proveedor', 'Monto', 'Forma de pago', 'Responsable', 'No. de cheque', 'Descripción', 'Catálogo', 'Sub catálogo'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(amount, v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle')+' gasto</a>');
        let btn1 = B.sige.creator.atrs('getGastos');
		let uploadPhoto = Form.btn(false, 'green', 'b-uploadPhoto', 'Subir foto')
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-saveGasto', 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto mtnGasto', 'number', 'Monto del gasto', '', '', j, 12, v[2]);
        let formaPago = Access.formaPago(); //v[3]
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('descr','Descripción o propósito del gasto.', 200, j, v[5]);
        let SelectedEmpresaD = Form.select('SelectedEmpresaD','¿A qué empresa aplica?', B.sige.validator.getForSelect('empresas',0, 1));
        let SelectedSucursalD = Form.select('SelectedSucursalD','¿A qué sucursal aplica?', []);
		/*
        let SelectedDepartamentoD = Form.select('SelectedDepartamentoD','Seleccione un departamento', []); //v[6]
        let SelectedEmpleadoD = Form.select('SelectedEmpleadoD','Responsable del gasto', B.sige.validator.getForSelect('trabajadores',0, 2)); //v[7]
		*/
		let SelectedEmpleadoD = Form.input('SelectedEmpleadoD', 'text', 'Responsable del gasto', '', '', j, j, v[7],'"');
		let selectCatalagos = Form.select('SelectedCatalogosB','Seleccione un catálogo', B.sige.validator.getForSelect('catalogos',0, 1));
		let selectsCatalagos = Form.select('SelectedSCatalogosB','Seleccione un catálogo primero', []); //v[7]
		let dlproveedor = [];
		$.each(B.sige.validator.getForSelect('proveedores',1), function(i, e){
			dlproveedor.push(e[0]);
		});
		let proveedor = Form.datalist('proveedorD','text','Seleccione o escriba un proveedor','',90,v[12],dlproveedor)
		//let proveedor = Form.select('proveedorD','Seleccione un proveedor', B.sige.validator.getForSelect('proveedores',0, 1)); //v[8]
		amount = (amount >= 0) ? '<div class="col s12"><h4 class="titleamount" data-amount="'+amount+'">'+parseFloat(amount).formatMoney()+'</h4><h5 class="grey-text">Saldo actual</h5></div>' : '';
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div>'+amount+'<div class="col s3 input-field">'+ref+'</div><div class="col s9 input-field">'+proveedor+'</div><div class="col s4 input-field">'+monto+'</div><div class="col s12 m8 input-field">'+formaPago+'</div><div class="col s12 input-field">'+cuenta+'</div><div class="col s12 input-field">'+descr+'</div><div class="col s12 m6 input-field">'+SelectedEmpresaD+'</div><div class="col s12 m6 input-field">'+SelectedSucursalD+'</div><div class="col s12 input-field">'+SelectedEmpleadoD+'</div><div class="col s12 m6 input-field">'+selectCatalagos+'</div><div class="col s12 m6 input-field">'+selectsCatalagos+'</div><div class="col s12 input-field toPicture">'+Form.file('fotoGasto', 'Cargar foto','Foto jpg, png o gif')+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        if (v[1] !== ''){
            B.prop('formaPago', v[3]);
			$('.toPicture').html('');
			switch(v[13]){
				case 'jpg':
				case 'gif':
				case 'png':
					$('.toPicture').html('<img width="100%" class="reponsive" src="data/'+v[0]+'.'+v[13]+'" />');
				break;
			}
			//uploadPhoto = '';
		} else {
			Form.fnimage('fotoGasto',1500000,['jpg','png','gif'])
		}
        if (v[6] !== ''){
			B.sige.creator.getInstance('Trabajadores').editing = true;
			/*
            let sucursal = B.sige.validator.searchIn('departamentos', v[6], 0);
            let empresa = B.sige.validator.searchIn('sucursales', sucursal[0][1], 0);
            B.prop('SelectedEmpresaD', empresa[0][1]);
            $('.SelectedEmpresaD').formSelect();
            $('select.SelectedEmpresaD').change();
            B.prop('SelectedSucursalD', sucursal[0][1]);
            $('select.SelectedSucursalD').change();
			$('.SelectedSucursalD').formSelect();
            B.prop('SelectedDepartamentoD', v[6]);
            $('.SelectedDepartamentoD').formSelect();
			*/
			let catalogos = B.sige.validator.searchIn('subcatalogos', v[11], 0);
			B.prop('SelectedCatalogosB', catalogos[0][1]);
            $('.SelectedCatalogosB').formSelect();
            $('select.SelectedCatalogosB').change();
			B.prop('SelectedSCatalogosB', v[11]);
            $('.SelectedSCatalogosB').formSelect();
        } else{
            let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
            B.prop('SelectedEmpresaD', empresa[0][1]);
			//$('select.SelectedEmpresaD').attr('disabled','disabled');
            $('.SelectedEmpresaD').formSelect();
            $('select.SelectedEmpresaD').change();
            B.prop('SelectedSucursalD', $.cookie('sucursal'));
            $('select.SelectedSucursalD').change();
			//$('select.SelectedSucursalD').attr('disabled','disabled');
			$('.SelectedSucursalD').formSelect();
            //$('.SelectedDepartamentoD').formSelect();
            $('.SelectedSCatalogosB, .SelectedCatalogosB').formSelect();
		}
        $('.formaPago').formSelect();
        if (v[1] !== ''){
            $('select.SelectedEmpresaD, select.SelectedSucursalD, .SelectedEmpleadoD, select.SelectedCatalogosB, select.SelectedSCatalogosB, select.formaPago, .monto, .cuenta, .descr, .proveedorD').attr('disabled','disabled');
            $('.SelectedEmpresaD, .SelectedSucursalD, .SelectedCatalogosB, .SelectedSCatalogosB, .formaPago').formSelect();
            let bt1 = Form.btn(false,'green','disabled b-detalleComprobanteC','Detalles');
            let bt2 = Form.btn(false,'red','disabled b-borrarComprobanteC','Borrar');
            //<div class="tg-title col s6 m3 smnb">'+bt2+'</div>
            $('.cPart').append('<div class="margintop20 divider col s12"></div><div class="tg-title col s12">Comprobaciones disponibles</div><div class="tg-title col offset-m9 s12 m3 smnb">'+bt1+'</div><div class="tableGatos col s12"></div>');
            B.sige.creator.getInstance('Gastos').getComprobaciones();
        }
        B.sige.uf();
    }
    nuovoEmp(type, v){
        let j = B.sige.und;
        let sm = {1 : 'la venta', 2 : 'los otros ingresos'};
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Detalle de')+' ventas</a>');
        let btn1 = B.sige.creator.atrs('gestCuenta');
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-saveNuevoVentaE', 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto', 'number', 'Monto de la venta', '', '', j, 12, v[2]);
        let title = Form.input('title', 'text', 'Nombre de la venta', '', '', j, 150, v[2]);
        let formaPago = Access.formaPago();
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('be_descr','Observaciones adicionales.', 200, j, v[5]);
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s3 input-field">${ref}</div>
            <div class="col s6 input-field">${title}</div>
            <div class="col s3 input-field">${monto}</div>
            <div class="col s12 m4 input-field">${formaPago}</div>
            <div class="col s12 m8 input-field">${cuenta}</div>
            <div class="col s12 input-field">${descr}</div>
            <div class="col s12 offset-m9 m3 smnb">${btn2}</div>
        `);
        $('.formaPago').formSelect();
        B.sige.uf();
    }
    gestCuenta(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a><a class="breadcrumb">Mi cuenta </a>');
        let btn1 = B.sige.creator.atrs('getGastos');
        let btn2 = Form.btn(false, 'green', 'b-nuevoIngresoG', 'Ingresos');
        let btn3 = Form.btn(false, 'green', 'b-nuevoTraspasoG', 'Traspasos');
        let btn4 = Form.btn(false, 'green', 'b-nuevoProveedoresG', 'Pagos');
        let btn5 = Form.btn(false, 'green', 'b-nuevoReportesG', 'Reportes');
        $('.metainfo').hide();
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s12 m6 smnb">${btn2}</div>
            <div class="col s12 m6 smnb">${btn3}</div>
            <div class="col s12 offset-m8 m4 sSaldoAmount">$10,000.00</div>
            <div class="col s12 offset-m8 m4 sSaldolastedit grey-text right-align">Saldo al día de hoy</div>
            <div class="col s12 cTableBanco2"></div>
        `);
        B.sige.asyn.post({class:'bancos', method: 'getEmployDetails'}, r => {
            r = Core.json(false, r, false);
            r[0][0] = parseFloat(r[0][0]).formatMoney();
            $('.sSaldoAmount').html(r[0][0]);
            let vldate = [];
            let vg = {3:'Trapaso de banco', 4: 'Ingreso por venta', 5: 'Traspaso para', 6: 'Traspaso de', 7: 'Gastos'};
            let vt = {3:'+', 4: '+', 5: '-', 6: '+', 7: '-'};
            $.each(r[1], (i, e) => {
                e[4] = parseFloat(e[4]).formatMoney();
                e[5] = parseFloat(e[5]).formatMoney();
                e[5] = vt[e[2]] + ' ' + e[5];
                e[6] = parseFloat(e[6]).formatMoney();
                e[2] = vg[e[2]];
                vldate.push(e);
            });
            let value = Form.table('datosGastos', 'datosReportesC cfm','striped',['Mov', 'Fecha', 'Tipo', 'Responsable', 'Anterior', 'Monto', 'Actual', 'Estado'], vldate);
            $('.cTableBanco2').html(value);
            $('.datosReportesC > td:nth-child(5)').addClass('right-align');
            $('.datosReportesC > td:nth-child(6)').addClass('right-align');
            $('.datosReportesC > td:nth-child(7)').addClass('right-align');
            $('.datosReportesC > td:nth-child(1)').hide();
            $('.datosReportesC').parent().parent().children('thead').children('tr').children('th:nth-child(1)').hide();
        });
    }
    getComprobaciones(){
        $('.b-detalleComprobanteC, .b-borrarComprobanteC').addClass('disabled').attr('target', '');
        B.sige.asyn.post({class:'gastos', method: 'getComprobacionesById'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = B.sige.fecha.date(e[2],'-',':');
                e[3] = parseFloat(e[3]).formatMoney();
                e[4] = B.sige.access.formas[e[4]][1];
                vldate.push([e[0], e[1], e[2], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosComprobantes', 'datosComprobantesC cfm','striped',['id', 'Referencia', 'Fecha', 'Monto', 'Forma de pago', 'Responsable'], vldate);
            $('.tableGatos').html(value);
        });
    }
    nuevoTraspaso(v){
        let j = B.sige.und;
        let sm = {1 : 'la venta', 2 : 'los otros ingresos'};
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a><a class="breadcrumb">Mi cuenta</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle de')+' traspaso</a>');
        let btn1 = B.sige.creator.atrs('gestCuenta');
        let vb = {1 : 'b-saveNuevoVenta', 2 : 'b-saveNuevoOtros'};
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-saveTrapasoEmployeed disabled', 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto mtotraspasoe', 'number', 'Monto del traspaso', '', '', j, 12, v[2]);
        let title = Form.input('title', 'text', 'Nombre del traspaso', '', '', j, 150, v[2]);
        let formaPago = Access.formaPago();
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('be_descr','Observaciones adicionales.', 200, j, v[5]);
        let SelectedEmpleadoE = Form.input('SelectedEmpleadoQ', 'text', 'Busque un empleado y presione enter', '', '');
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s12 m9 input-field">${SelectedEmpleadoE}</div>
            <div class="col s12 m3 smvalue smnb">${Form.btn(false, 'green', 'b-getEmployeeZ', 'Buscar')}</div>
            <div class="jmnhide">
                <div class="col s12 saldoPdte bancosocio">$0.00</div>
                <div class="col s12 grey-text">Mi saldo actual</div>
                
                <div class="col s3 input-field">${ref}</div>
                <div class="col s6 input-field">${title}</div>
                <div class="col s3 input-field">${monto}</div>
                <div class="col s12 m4 input-field">${formaPago}</div>
                <div class="col s12 m8 input-field">${cuenta}</div>
                <div class="col s12 input-field">${descr}</div>
                <div class="col s12 offset-m9 m3 smnb">${btn2}</div>
            </div>
        `);
        $('.formaPago').formSelect();
        Form.autocomplete('.SelectedEmpleadoQ', B.sige.validator.getForSelect('trabajadores',0, 2));
        B.sige.uf();
    }
    nuevoProveedor(v){
        let j = B.sige.und;
        let sm = {1 : 'la venta', 2 : 'los otros ingresos'};
        $('.pos-actual').html('<a class="breadcrumb">Gastos</a><a class="breadcrumb">Mi cuenta</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle de')+' pago a proveedores</a>');
        let btn1 = B.sige.creator.atrs('gestCuenta');
        let vb = {1 : 'b-saveNuevoVenta', 2 : 'b-saveNuevoOtros'};
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-saveProveedorEmployeed disabled', 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto mtotraspasoe', 'number', 'Monto de la nota', '', '', j, 12, v[2]);
        let title = Form.input('title', 'text', 'Nombre de la nota', '', '', j, 150, v[2]);
        let formaPago = Access.formaPago();
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('be_descr','Observaciones adicionales.', 200, j, v[5]);
        let SelectedEmpleadoE = Form.input('SelectedEmpleadoQ', 'text', 'Busque un proveedor y presione enter', '', '');
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s12 m9 input-field">${SelectedEmpleadoE}</div>
            <div class="col s12 m3 smvalue smnb">${Form.btn(false, 'green', 'b-getEmployeeZ', 'Buscar')}</div>
            <div class="jmnhide">
                <div class="col s12 saldoPdte bancosocio">$0.00</div>
                <div class="col s12 grey-text">Mi saldo actual</div>
                <div class="col s3 input-field">${ref}</div>
                <div class="col s6 input-field">${title}</div>
                <div class="col s3 input-field">${monto}</div>
                <div class="col s12 m4 input-field">${formaPago}</div>
                <div class="col s12 m8 input-field">${cuenta}</div>
                <div class="col s12 input-field">${descr}</div>
                <div class="col s12 offset-m9 m3 smnb">${btn2}</div>
            </div>
        `);
        $('.formaPago').formSelect();
        Form.autocomplete('.SelectedEmpleadoQ', B.sige.validator.getForSelect('proveedores', 0, 1));
        B.sige.uf();
    }
    clicks(){
        u.keyup('.mtnGasto', (v) => {
            let amount = parseFloat($('.titleamount').attr('data-amount'));
            let val = v.val();
            val = (val == '') ? 0 : parseFloat(val);
            if (amount - val < 0){
                v.val('');
                v.keyup();
                Core.toast('Fondos insuficientes','red');
                return false;
            }
            $('.titleamount').html((amount - val).formatMoney());
        });
        u.click('.b-nuevoProveedoresG', () => {
            B.sige.creator.getInstance('Gastos').nuevoProveedor();
        });
        u.click('.b-nuevoTraspasoG', () => {
            B.sige.creator.getInstance('Gastos').nuevoTraspaso();
        });
        u.click('.b-getEmployeeZ', () => {
            if ($('.SelectedEmpleadoQ').data('id') == undefined){
                Core.toast('Seleccione un empleado para continuar.','red');
                return false;
            }
            B.sige.asyn.post({class:'bancos', method: 'getFinantialEmployeeData', id: $('.SelectedEmpleadoQ').data('id')}, r => {
                r = Core.json(false, r, false);
                $('.saldoPdte.bancosocio').html(parseFloat(r[0]).formatMoney()).attr('data-amount', r[0]);
                $('.b-saveTrapasoEmployeed, .b-saveProveedorEmployeed').removeClass('disabled');
                $('.jmnhide').slideDown(200);
            });
        });
        u.keyup('.mtotraspasoe', () => {
            let banco = parseFloat($('.saldoPdte.bancosocio').attr('data-amount'));
            let value = $('.mtotraspasoe').val();
            if (value == '')
                value = '0';
            value = parseFloat(value);
            if (banco - value < 0 ){
                Core.toast('Fondos insuficientes','red');
                $('.mtotraspasoe').val('').keyup();
                return false;
            }
            let nuevobanco = parseFloat((banco - value).toFixed(2));
            $('.saldoPdte.bancosocio').html(nuevobanco.formatMoney());
        });
        u.click('.b-saveProveedorEmployeed', () => {
            let v = [
                $('.ref').val(),
                $('.title').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.be_descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            if ($('.SelectedEmpleadoQ').data('id') == undefined){
                Core.toast('Seleccione un empleado para continuar.','red');
                return false;
            }
            v.unshift($('.SelectedEmpleadoQ').data('id'));
            B.sige.asyn.post({class:'bancos', method: 'saveProveedorEmployeed', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        $('.b-gestCuenta').click();
                    break;
                }
            });
        });
        u.click('.b-saveTrapasoEmployeed', () => {
            let v = [
                $('.ref').val(),
                $('.title').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.be_descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            if ($('.SelectedEmpleadoQ').data('id') == undefined){
                Core.toast('Seleccione un empleado para continuar.','red');
                return false;
            }
            v.unshift($('.SelectedEmpleadoQ').data('id'));
            B.sige.asyn.post({class:'bancos', method: 'saveTrapasoEmployeed', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('BancoSocios').get();
                    break;
                }
            });
        });
        u.click('.b-saveNuevoVentaE', (r) => {
            let v = [
                $('.ref').val(),
                $('.title').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.be_descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'bancos', method: 'nuevaVentaEmpleado', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Gastos').gestCuenta();
                    break;
                }
            });
        });
        u.click('.b-nuevoIngresoG', () => {
            B.sige.creator.getInstance('Gastos').nuovoEmp(1);
        });
        u.click('.datosComprobantesC', v => {
            B.sige.validator.vTable(v, 'datosComprobantesC', '.b-detalleComprobanteC, .b-borrarComprobanteC');
        });
        u.click('.b-detalleComprobanteC', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'gastos', method: 'getComprobanteById', param: id}, r => {
                r = Core.json(false, r, false);
                let ac = Access.comprobacion();
                B.sige.ui.question('Detalle del comprobante de gastos','<div class="row"><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Cerrar',() => {}, () => {
                    $('.monto, select.formaPago, .cuenta, .SelectedEmpleadoD, .observ, select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled','disabled');
                    B.sige.creator.getInstance('Trabajadores').editing = true;
					/*
                    let sucursal = B.sige.validator.searchIn('departamentos', r[5], 0);
                    let empresa = B.sige.validator.searchIn('sucursales', sucursal[0][1], 0);
                    B.prop('SelectedEmpresaD', empresa[0][1]);
                    $('.SelectedEmpresaD').formSelect();
                    $('select.SelectedEmpresaD').change();
                    B.prop('SelectedSucursalD', sucursal[0][1]);
                    $('select.SelectedSucursalD').change();
					*/
					/*
                    B.prop('SelectedDepartamentoD', r[5]);
                    $('.SelectedDepartamentoD').formSelect();
                    Access.getEmpleadosByDepId(r[5], () => {
                        B.prop('SelectedEmpleadoD',r[3]);
                        $('.SelectedEmpleadoD').formSelect();
                    },'D');
					*/
					$('.cDir-content > .col.s12.m6').hide();
                    $('.monto').val(r[0]);
                    $('.SelectedEmpleadoD').val(r[3]);
                    B.prop('formaPago', r[1]);
                    $('.cuenta').val(r[2]);
                    $('.observ').val(r[4]);
                    ac[1]();
                    B.sige.uf();
                    
                });
            });
        });
        u.click('.b-borrarComprobanteC', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este comprobante? Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'gastos', method: 'delComprobante', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                        case 'comprobado':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Gastos').getComprobaciones();
                        break;
                    }
                });
            });
        });
        u.change('select.SelectedCatalogosB', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('subcatalogos', v.val(),  1), 0, 3);
            $('select.SelectedSCatalogosB').html(Form.initSelect(filter));
            $('.SelectedSCatalogosB').formSelect();
            $('select.SelectedSCatalogosB').change();
        });
		u.change('select.SelectedEmpresaD', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalD').html(Form.initSelect(filter));
            $('.SelectedSucursalD').formSelect();
            $('select.SelectedSucursalD').change();
        });
        u.change('select.SelectedSucursalD', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('departamentos', v.val(), 1), 0, 4);
            $('select.SelectedDepartamentoD').html(Form.initSelect(filter));
            $('.SelectedDepartamentoD').formSelect();
            $('select.SelectedDepartamentoD').change();
        });
        u.change('select.SelectedDepartamentoD', v => {
            Access.getEmpleadosByDepId(v.val(), () => {
                $('select.SelectedEmpleadoD').change();
                $('.SelectedEmpleadoD').formSelect();
            },'D');
        });
        u.click('.b-gestCuenta', () => {
            B.sige.creator.getInstance('Gastos').gestCuenta();
        });
        u.click('.b-nuevoGasto', () => {
            B.sige.asyn.post({class:'bancos', method: 'getFinantialEmployeeData', id: ''}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Gastos').nuovo(r);
            });
            
        });
        u.click('.b-getGastos', () => {
            B.sige.creator.getInstance('Gastos').get();
        });
        u.click('.datosGastosC', v => {
            B.sige.validator.vTable(v, 'datosGastosC', '.b-comprobarGastos, .b-detalleGastos');
        });
        u.click('.b-ComprobarPagoMov', v => {
            let id = v.attr('target');
            let ac = Access.comprobacion();
            B.sige.asyn.post({class:'bancos', method: 'setIdComprobacion', param: id}, r => {
                r = Core.json(false, r, false);
                r[1][0] = parseFloat(r[1][0]);
                r[1][1] = parseFloat(r[1][1]);
                r[1][2] = r[1][0] - r[1][1];
                if (r[1][0] === r[1][1]){
                    Core.toast('Este pago a proveedores se ha comprobado en su totalidad. Si desea modificar un registro, de clic en detalles','blue rounded');
                    return false;
                }
                B.sige.ui.question('Comprobar proveedor','<div class="row"><div class="col s12 m8">Monto pendiente por comprobar</div><div class="col s12 m4 totalPorCubrir" target="'+r[1][2]+'">'+r[1][2].formatMoney()+'</div><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Guardar',() => {
                    let v = [
                        $('.monto').val(),
                        $('select.formaPago').val(),
                        $('.cuenta').val(),
                        $('.SelectedEmpleadoD').val(),
                        $('.observ').val(),
                    ];
                    let flag = B.sige.validator.vForm(v);
                    if (!flag)
                        return false;
                    B.sige.asyn.post({class:'bancos', method: 'comprobarProveedor', param: v}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'mayor':
                                Core.toast('El monto que está comprobando es mayor al monto que resta para este gasto','red rounded');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                B.sige.creator.getInstance('Gastos').gestCuenta();
                            break;
                        }
                    });
                }, () => {
                    ac[1]();
					let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
					B.prop('SelectedEmpresaD', empresa[0][1]);
					$('select.SelectedEmpresaD').change();
					B.prop('SelectedSucursalD', $.cookie('sucursal'));
					$('select.SelectedSucursalD').change();
					$('select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled', 'disabled');
					$('.SelectedEmpresaD').formSelect();
					$('.SelectedSucursalD').formSelect();
                    $('.monto').addClass('gastoKeyUp');
                });
            });
        });
        u.click('.b-AutorizarPagoMov', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'bancos', method: 'AceptarPago', id: id}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        $('tr#'+id).click();
                    break;
                }
            });
        });
        u.click('.b-RechazarPagoMov', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'bancos', method: 'RechazarPago', id: id}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        $('tr#'+id).click();
                    break;
                }
            });
        });
        u.click('.b-CubiertoPagoMov', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'bancos', method: 'CubiertoPago', id: id}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        $('tr#'+id).click();
                    break;
                }
            });
        });
        u.click('.b-comprobarGastos', v => {
            let id = v.attr('target');
            let ac = Access.comprobacion();
            B.sige.asyn.post({class:'gastos', method: 'setIdComprobacion', param: id}, r => {
                r = Core.json(false, r, false);
                r[1][0] = parseFloat(r[1][0]);
                r[1][1] = parseFloat(r[1][1]);
                r[1][2] = r[1][0] - r[1][1];
                if (r[1][0] === r[1][1]){
                    Core.toast('Este gasto se ha comprobado en su totalidad. Si desea modificar un registro, de clic en detalles','blue rounded');
                    return false;
                }
                B.sige.ui.question('Comprobar gastos','<div class="row"><div class="col s12 m8">Monto pendiente por comprobar</div><div class="col s12 m4 totalPorCubrir" target="'+r[1][2]+'">'+r[1][2].formatMoney()+'</div><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Guardar',() => {
                    let v = [
                        $('.monto').val(),
                        $('select.formaPago').val(),
                        $('.cuenta').val(),
                        $('.SelectedEmpleadoD').val(),
                        $('.observ').val(),
                    ];
                    let flag = B.sige.validator.vForm(v);
                    if (!flag)
                        return false;
                    B.sige.asyn.post({class:'gastos', method: 'comprobarGasto', param: v}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'mayor':
                                Core.toast('El monto que está comprobando es mayor al monto que resta para este gasto','red rounded');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                B.sige.creator.getInstance('Gastos').get();
                            break;
                        }
                    });
                }, () => {
                    ac[1]();
					let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
					B.prop('SelectedEmpresaD', empresa[0][1]);
					$('select.SelectedEmpresaD').change();
					B.prop('SelectedSucursalD', $.cookie('sucursal'));
					$('select.SelectedSucursalD').change();
					$('select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled', 'disabled');
					$('.SelectedEmpresaD').formSelect();
					$('.SelectedSucursalD').formSelect();
                    $('.monto').addClass('gastoKeyUp');
                });
            });
        });
        u.keyup('.gastoKeyUp', v => {
            let m = v.val();
            m = (m === '') ? 0 : parseFloat(m);
            let t = parseFloat($('.totalPorCubrir').attr('target'));
            let r = (t - m);
            if (r < 0){
                Core.toast('No puede comprobar más dinero ('+m.formatMoney()+') del que resta por cubrir ('+t.formatMoney()+')','blue rounded');
                v.val(t);
            }
            r = (r < 0) ? 0 : r;
            $('.totalPorCubrir').html(r.formatMoney());
        });
        u.click('.b-detalleGastos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'gastos', method: 'getGastosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Gastos').nuovo(-1, r);
            });
        });
        u.click('.b-saveGasto', () => {
            let v = [
                $('.ref').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.descr').val(),
				$('select.SelectedSucursalD').val(),
                $('.SelectedEmpleadoD').val(),
                $('select.SelectedSCatalogosB').val(),
                $('.proveedorD').val(),
				$('.iterbium').attr('data')
            ];
			if (v[9] == undefined)
				v[9] = 'nophoto';
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'gastos', method: 'saveGasto', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Gastos').get();
                    break;
                }
            });
        });
    }
}
class BancoSocios{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'account_balance';
        this.color = 'blue';
        this.descr = 'banco socios';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s2','nuevaVenta', 'venta', 'blue'));
        mc.append(B.sige.creator.simbtn('s2','nuevoIngreso', 'otros', 'blue'));
        mc.append(B.sige.creator.simbtn('s3','nuevoTraspasos', 'Traspasos'));
        mc.append(B.sige.creator.simbtn('s3','nuevoComprobacionesGcm', 'Comprobaciones'));
        mc.append(B.sige.creator.simbtn('s2','nuevoReportes', 'Reportes'));
        mc.append(B.sige.creator.simpliest('Buscar en banco socios', 'datosReportesC'));
        this.get();
    }
    get(){
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Banco socios</a>');
        $('.cPart').html(`
            <div class="col s12 offset-m8 m4 sSaldoAmount">$10,000.00</div>
            <div class="col s12 offset-m8 m4 sSaldolastedit grey-text right-align"></div>
            <div class="col s12 cTableBanco"></div>
        `);
        B.sige.asyn.post({class:'bancos', method: 'getBancoDetails'}, r => {
            r = Core.json(false, r, false);
            r[0][1] = parseFloat(r[0][1]).formatMoney();
            $('.sSaldoAmount').html(r[0][1]);
            $('.sSaldolastedit').html('Ult mov: '+r[0][2]);
            let vldate = [];
            let vg = {1: 'Ingreso venta', 2: 'Ingreso otros', 3:'Trapaso a', 4: 'Venta a empleado', 5: 'Traspaso de', 6: 'Traspaso para', 7: 'Pago a proveedores'};
            let vt = {1: '+', 2: '+', 3:'-', 4: '+', 5: '+', 6: '-', 7: '-'};
            $.each(r[1], (i, e) => {
                e[4] = parseFloat(e[4]).formatMoney();
                e[5] = parseFloat(e[5]).formatMoney();
                e[5] = vt[e[2]] + ' ' + e[5];
                e[6] = parseFloat(e[6]).formatMoney();
                e[2] = vg[e[2]];
                vldate.push(e);
            });
            let value = Form.table('datosGastos', 'datosReportesC cfm','striped',['Mov', 'Fecha', 'Tipo', 'Responsable', 'Anterior', 'Monto', 'Actual'], vldate);
            $('.cTableBanco').html(value);
            $('.datosReportesC > td:nth-child(5)').addClass('right-align');
            $('.datosReportesC > td:nth-child(6)').addClass('right-align');
            $('.datosReportesC > td:nth-child(7)').addClass('right-align');
            $('.datosReportesC > td:nth-child(1)').hide();
            $('.datosReportesC').parent().parent().children('thead').children('tr').children('th:nth-child(1)').hide();
        });
    }
    nuovo(type, v){
        let j = B.sige.und;
        let sm = {1 : 'la venta', 2 : 'los otros ingresos'};
        $('.pos-actual').html('<a class="breadcrumb">Banco socios</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Detalle de')+' '+sm[type]+'</a>');
        let btn1 = B.sige.creator.atrs('getBancos');
        let vb = {1 : 'b-saveNuevoVenta', 2 : 'b-saveNuevoOtros'};
        let btn2 = (v === j) ? Form.btn(false, 'green', vb[type], 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto', 'number', 'Monto de '+sm[type], '', '', j, 12, v[2]);
        let title = Form.input('title', 'text', 'Nombre de '+sm[type], '', '', j, 150, v[2]);
        let formaPago = Access.formaPago();
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('be_descr','Observaciones adicionales.', 200, j, v[5]);
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s3 input-field">${ref}</div>
            <div class="col s6 input-field">${title}</div>
            <div class="col s3 input-field">${monto}</div>
            <div class="col s12 m4 input-field">${formaPago}</div>
            <div class="col s12 m8 input-field">${cuenta}</div>
            <div class="col s12 input-field">${descr}</div>
            <div class="col s12 offset-m9 m3 smnb">${btn2}</div>
        `);
        $('.formaPago').formSelect();
        B.sige.uf();
    }
    nuevoTraspaso(v){
        let j = B.sige.und;
        let sm = {1 : 'la venta', 2 : 'los otros ingresos'};
        $('.pos-actual').html('<a class="breadcrumb">Banco socios</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Detalle de')+' traspaso</a>');
        let btn1 = B.sige.creator.atrs('getBancos');
        let vb = {1 : 'b-saveNuevoVenta', 2 : 'b-saveNuevoOtros'};
        let btn2 = (v === j) ? Form.btn(false, 'green', 'b-saveTrapaso disabled', 'Guardar') : '';
        v = (v === j) ? [B.ref(),'','','','','','','','','','','','',''] : v;
        $('.metainfo').hide();
        let ref = Form.input('ref', 'text', 'Referencia', '', '', j, j, v[0],'disabled="disabled"');
        let monto = Form.input('monto mtotraspaso', 'number', 'Monto del traspaso', '', '', j, 12, v[2]);
        let title = Form.input('title', 'text', 'Nombre del traspaso', '', '', j, 150, v[2]);
        let formaPago = Access.formaPago();
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, 150, v[4]);
        let descr = Form.area('be_descr','Observaciones adicionales.', 200, j, v[5]);
        let SelectedEmpleadoE = Form.input('SelectedEmpleadoQ', 'text', 'Busque un empleado y presione enter', '', '');
        $('.cPart').html(`
            <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
            <div class="col s12 m9 input-field">${SelectedEmpleadoE}</div>
            <div class="col s12 m3 smvalue smnb">${Form.btn(false, 'green', 'b-getEmployee', 'Buscar')}</div>
            <div class="jmnhide">
                <div class="col s6 saldoPdte bancosocio">$0.00</div>
                <div class="col s6 saldoPdte empleado">$0.00</div>
                <div class="col s6 grey-text">Saldo del banco</div>
                <div class="col s6 grey-text">Saldo del empleado</div>
                <div class="col s3 input-field">${ref}</div>
                <div class="col s6 input-field">${title}</div>
                <div class="col s3 input-field">${monto}</div>
                <div class="col s12 m4 input-field">${formaPago}</div>
                <div class="col s12 m8 input-field">${cuenta}</div>
                <div class="col s12 input-field">${descr}</div>
                <div class="col s12 offset-m9 m3 smnb">${btn2}</div>
            </div>
        `);
        $('.formaPago').formSelect();
        Form.autocomplete('.SelectedEmpleadoQ', B.sige.validator.getForSelect('trabajadores',0, 2));
        B.sige.uf();
    }
    clicks(){
        u.click('.b-nuevoReportes', () => {
            $('.pos-actual').html('<a class="breadcrumb">Banco socios</a><a class="breadcrumb">Reportes</a>');
            let btn1 = B.sige.creator.atrs('getBancos');
            let btn2 = Form.btn(false, 'blue', 'b-edoCuentaEx', 'Estado de cuenta');
            let btn3 = Form.btn(false, 'blue', 'b-listaIngresos', 'Lista ingresos');
            let btn4 = Form.btn(false, 'blue', 'b-listaEgresos', 'Lista egresos');
            let btn5 = Form.btn(false, 'blue', 'b-listaTraspasos', 'Lista traspasos');
            let btn6 = Form.btn(false, 'blue', 'b-employeeMovements', 'Comprobar pagos');
            let btn7 = Form.btn(false, 'blue', 'b-employeeBalance', 'Saldos de empleados');
            $('.metainfo').hide();
            $('.cPart').html(`
                <div class="col s12 offset-m9 m3 smnb">${btn1}</div>
                <div class="col s12 m3 smnb">${btn2}</div>
                <div class="col s12 m3 smnb">${btn3}</div>
                <div class="col s12 m3 smnb">${btn4}</div>
                <div class="col s12 m3 smnb">${btn5}</div>
                <div class="col s12 m6 smnb">${btn6}</div>
                <div class="col s12 m6 smnb">${btn7}</div>
                <div class="col s12 FilterData2"></div>
                <div class="col s12 contenidoReporte"></div>
            `);
        });
        u.click('.b-employeeMovements', () => {
            $('.FilterData2').html();
            B.sige.asyn.post({class:'bancos', method: 'getSPayments'}, r => {
                r = Core.json(false, r, false);
                let vldate = [];
				$.each(r, (i, e) => {
                    e[4] = parseFloat(e[4]).formatMoney();
                    e[5] = parseFloat(e[5]).formatMoney();
					e[1] = B.sige.fecha.date(e[1],'-',':');
					vldate.push([e[0], e[1], e[4], e[5], e[3], e[2]]);
                });
				let value = Form.table('datosReportesC xxx mpagox', 'datosReportesC cfm cpc','striped',['Id', 'Fecha', 'Monto', 'Comprobado', 'Concepto', 'Responsable'], vldate);
				$('.contenidoReporte').html(value);
			});
        });
        u.click('.b-employeeBalance', () => {
            $('.FilterData2').html();
            B.sige.asyn.post({class:'bancos', method: 'getSaldos'}, r => {
                r = Core.json(false, r, false);
                let vldate = [];
                $.each(r, (i, e) => {
                    e[2] = parseFloat(e[2]).formatMoney();
					vldate.push(e);
                });
                let value = Form.table('datosReportesDex xxx', 'datosReportesDex cfm cpc','striped',['Id', 'Empleado', 'Saldo'], vldate);
				$('.contenidoReporte').html(value);
            });
        });
        u.click('tr.datosReportesDex', (v) => {
            let id = v.attr('id');
            B.sige.asyn.post({class:'bancos', method: 'getAllMovementsById', param: id}, r => {
                r = Core.json(false, r, false);
                $('.contenidoReporte').html(`
                    <div class="row">
                        <div class="col s12 offset-m8 m4 smnb">${Form.btn(false, 'blue-grey', 'b-employeeBalance', 'Regresar a la lista')}</div>
                        <div class="col s12 center-align"><h5>Empleado: ${r[2]}</h5></div>
                        <div class="col s6 m3 center-align"><h6>Ingreso banco</h6></div>
                        <div class="col s6 m3 center-align"><h6>Ingreso venta</h6></div>
                        <div class="col s6 m3 center-align"><h6>Ingreso traspaso</h6></div>
                        <div class="col s6 m3 center-align"><h6>Total ingreso</h6></div>
                    </div>
                    <div class="row">
                        <div class="col s6 m3 center-align bolder">${r[3][0].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${r[3][1].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${r[3][2].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${r[3][3].formatMoney()}</div>
                    </div>
                    <div class="row">
                        <div class="col s6 m3 center-align"><h6>Egreso pago</h6></div>
                        <div class="col s6 m3 center-align"><h6>Invalidado</h6></div>
                        <div class="col s6 m3 center-align"><h6>Egreso traspaso</h6></div>
                        <div class="col s6 m3 center-align"><h6>SALDO ACTUAL</h6></div>
                    </div>
                    <div class="row">
                        <div class="col s6 m3 center-align bolder">${r[3][4].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${r[3][5].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${r[3][7].formatMoney()}</div>
                        <div class="col s6 m3 center-align bolder">${parseFloat(r[0][0]).formatMoney()}</div>
                        <div class="col s12 center-align"><h5>Historial de movimientos</h5></div>
                    </div>
                    <div class="row datoscacahuate"></div>
                `);
                let vldate = [];
                let vg = {3:'Trapaso de banco', 4: 'Ingreso por venta', 5: 'Traspaso para', 6: 'Traspaso de', 7: 'Gasto'};
                let vt = {3:'+', 4: '+', 5: '-', 6: '+', 7: '-'};
                $.each(r[1], (i, e) => {
                    e[4] = parseFloat(e[4]).formatMoney();
                    e[5] = parseFloat(e[5]).formatMoney();
                    e[5] = vt[e[2]] + ' ' + e[5];
                    e[6] = parseFloat(e[6]).formatMoney();
                    e[2] = vg[e[2]];
                    vldate.push(e);
                });
                let value = Form.table('datosGastos', 'datosReportesC cfm','striped',['Mov', 'Fecha', 'Tipo', 'Responsable', 'Anterior', 'Monto', 'Actual'], vldate);
                $('.datoscacahuate').html(value);
                $('.datosReportesC > td:nth-child(5)').addClass('right-align');
                $('.datosReportesC > td:nth-child(6)').addClass('right-align');
                $('.datosReportesC > td:nth-child(7)').addClass('right-align');
                $('.datosReportesC > td:nth-child(1)').hide();
                $('.datosReportesC').parent().parent().children('thead').children('tr').children('th:nth-child(1)').hide();
            });
            
        });
        u.click('.b-listaTraspasos', () => {
            $('.FilterData2').html();
            let btn2 = Form.btn(false, 'blue', 'b-detalleReporteExcel', 'Exportar a excel');
			let ca = Access.reportes('getTraspasosPara', true, false, false, false, true, true, ['<div class="col s12 m4 smnb">'+btn2+'</div>', function(){}], true);
			$('.FilterData2').html(ca[0]);
			ca[1]();
        });
        u.click('.b-getTraspasosPara', () => {
            let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sEmpleado').val()
			];
			if (v[2] == null)
                v[2] = 0;
            let title = (v[2] == 0) ? 'Banco socios': $('.rep_sEmpleado').parent().children('input').val();
			B.sige.asyn.post({class:'bancos', method: 'getTraspasosPara', param: v}, r => {
                r = Core.json(false, r, false);
                let total = 0;
                let vldate = [];
				$.each(r, (i, e) => {
                    switch(e[2]){
                        case '3':
                            e[2] = 'Traspaso bancos';
                        break;
                        case '5':
                            e[2] = '<span class="red-text">Salida</span> traspaso para';
                        break;
                        case '6':
                            e[2] = '<span class="green-text">Entrada</span> traspaso de '+e[5]+' para';
                        break;
                    }
                    e[4] = parseFloat(e[4]);
                    total += e[4]
                    e[4] = e[4].formatMoney();
					e[1] = B.sige.fecha.date(e[1],'-',':');
					vldate.push([e[0], e[1], e[2], e[3], e[4]]);
                });
				let value = Form.table('datosReportesC xxx', 'datosReportesC cfm cpc','striped',['Id', 'Fecha', 'Tipo', 'Empleado', 'Monto'], vldate);
				$('.contenidoReporte').html(value);
				$('.datosReportesC > thead').prepend('<tr><th colspan="4" class="encabezadoReporte"></th></tr>');
                $('.encabezadoReporte').html(Access.headTable('Lista de traspasos '+title, '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
                $('th > table.datosReportesC').children('tbody').children('tr:nth-child(3)').remove();
                $('tr.datosReportesC > td:nth-child(1)').hide();
                $('table.datosReportesC').children('thead').children('tr:nth-child(2)').children('th:nth-child(1)').hide();
                $('tbody.datosReportesC').append('<tr><td>&nbsp;</td><td colspan="2"><b>TOTALES</b></td><td>'+total.formatMoney()+'</td></tr>');
			});
        });
        u.click('.b-listaEgresos', () => {
            $('.FilterData2').html();
            let btn2 = Form.btn(false, 'blue', 'b-detalleReporteExcel', 'Exportar a excel');
			let ca = Access.reportes('getEgresosPara', true, false, false, false, true, true, ['<div class="col s12 m4 smnb">'+btn2+'</div>', function(){}], true);
			$('.FilterData2').html(ca[0]);
			ca[1]();
        });
        u.click('.b-getEgresosPara', () => {
            let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sEmpleado').val()
			];
			if (v[2] == null)
                v[2] = 0;
            let title = (v[2] == 0) ? 'Banco socios': $('.rep_sEmpleado').parent().children('input').val();
			B.sige.asyn.post({class:'bancos', method: 'getEgresosPara', param: v}, r => {
                r = Core.json(false, r, false);
                let total = 0;
                let vldate = [];
				$.each(r, (i, e) => {
                    switch(e[2]){
                        case '3':
                            e[2] = 'Egreso por traspaso a';
                        break;
                        case '5':
                            e[2] = 'Egreso por traspaso a';
                        break;
                        case '7':
                            e[2] = 'Egreso por pago a';
                        break;
                    }
                    e[4] = parseFloat(e[4]);
                    total += e[4]
                    e[4] = e[4].formatMoney();
					e[1] = B.sige.fecha.date(e[1],'-',':');
					vldate.push(e);
                });
				let value = Form.table('datosReportesC xxx', 'datosReportesC cfm cpc','striped',['Id', 'Fecha', 'Tipo', 'Concepto', 'Monto'], vldate);
				$('.contenidoReporte').html(value);
				$('.datosReportesC > thead').prepend('<tr><th colspan="4" class="encabezadoReporte"></th></tr>');
                $('.encabezadoReporte').html(Access.headTable('Lista de egresos '+title, '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
                $('th > table.datosReportesC').children('tbody').children('tr:nth-child(3)').remove();
                $('tr.datosReportesC > td:nth-child(1)').hide();
                $('table.datosReportesC').children('thead').children('tr:nth-child(2)').children('th:nth-child(1)').hide();
                $('tbody.datosReportesC').append('<tr><td>&nbsp;</td><td colspan="2"><b>TOTALES</b></td><td>'+total.formatMoney()+'</td></tr>');
			});
        });
        u.click('.b-listaIngresos', () => {
            $('.FilterData2').html();
            let btn2 = Form.btn(false, 'blue', 'b-detalleReporteExcel', 'Exportar a excel');
			let ca = Access.reportes('getIngresosPara', true, false, false, false, true, true, ['<div class="col s12 m4 smnb">'+btn2+'</div>', function(){}], true);
			$('.FilterData2').html(ca[0]);
			ca[1]();
        });
        u.click('.b-getIngresosPara', () => {
            let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sEmpleado').val()
			];
			if (v[2] == null)
                v[2] = 0;
            let title = (v[2] == 0) ? 'Banco socios': $('.rep_sEmpleado').parent().children('input').val();
			B.sige.asyn.post({class:'bancos', method: 'getIngresosPara', param: v}, r => {
                r = Core.json(false, r, false);
                let total = 0;
                let vldate = [];
				$.each(r, (i, e) => {
                    switch(e[2]){
                        case '1':
                        case '4':
                            e[2] = 'Ingreso por venta';
                        break;
                        case '2':
                            e[2] = 'Ingreso por otros';
                        break;
                        case '3':
                            e[2] = 'Ingreso por traspaso banco socios';
                        break;
                        case '6':
                            e[2] = 'Traspaso del empleado';
                        break;
                    }
                    e[4] = parseFloat(e[4]);
                    total += e[4]
                    e[4] = e[4].formatMoney();
					e[1] = B.sige.fecha.date(e[1],'-',':');
					vldate.push(e);
                });
				let value = Form.table('datosReportesC xxx', 'datosReportesC cfm cpc','striped',['Id', 'Fecha', 'Tipo', 'Concepto', 'Monto'], vldate);
				$('.contenidoReporte').html(value);
				$('.datosReportesC > thead').prepend('<tr><th colspan="4" class="encabezadoReporte"></th></tr>');
                $('.encabezadoReporte').html(Access.headTable('Lista de ingresos '+title, '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
                $('th > table.datosReportesC').children('tbody').children('tr:nth-child(3)').remove();
                $('tr.datosReportesC > td:nth-child(1)').hide();
                $('table.datosReportesC').children('thead').children('tr:nth-child(2)').children('th:nth-child(1)').hide();
                $('tbody.datosReportesC').append('<tr><td>&nbsp;</td><td colspan="2"><b>TOTALES</b></td><td>'+total.formatMoney()+'</td></tr>');
			});
        });
        u.click('.b-detalleReporteExcel', (v) => {
            let data = $('.datosReportesC').html();
            if (data == undefined)
                return false;
            data = $.blurhouse.encode(data);
            B.sige.asyn.post({class:'bancos', method: 'exportData', param: data}, r => {
                r = Core.json(false, r, false);
                if (r[0] == 'true')
                    window.open('concentrado.php', '_blank');
            });
        });
        u.click('.b-edoCuentaEx', () => {
            $('.FilterData2').html();
            let btn2 = Form.btn(false, 'blue', 'b-detalleReporteExcel', 'Exportar a excel');
			let ca = Access.reportes('getEdoCuenta', true, false, false, false, true, true, ['<div class="col s12 m4 smnb">'+btn2+'</div>', function(){}], true);
			$('.FilterData2').html(ca[0]);
			ca[1]();
        });
        u.click('.b-cancelarPagoComp', (v) => {
            let id = v.attr('target');
            let data = v.attr('data');
            let obs = prompt('Motivo de la cancelación');
            B.sige.asyn.post({class:'bancos', method: 'cancelPaymentData', id: id, obs: obs}, r => {
                r = Core.json(false, r, false);
                if (r[0] == 'true'){
                    $('tr#'+data).click();
                }
            });
        });
        u.click('.b-getEdoCuenta', () => {
            let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sEmpleado').val()
			];
			if (v[2] == null)
                v[2] = 0;
            let title = (v[2] == 0) ? 'Banco socios': $('.rep_sEmpleado').parent().children('input').val();
			B.sige.asyn.post({class:'bancos', method: 'getEdoCuenta', param: v}, r => {
                r = Core.json(false, r, false);
                let saldoinicio = 0, saldofin = 0;
                let vldate = [];
				$.each(r, (i, e) => {
                    if (i == 0)
                        saldofin = e[6];
                    saldoinicio = e[4];
                    switch(e[2]){
                        case '1':
                        case '4':
                            e[2] = 'Ingreso por venta';
                        break;
                        case '2':
                            e[2] = 'Ingreso por otros';
                        break;
                        case '3':
                            e[2] = 'Traspaso';
                        break;
                        case '5':
                            e[2] = 'Traspaso a';
                        break;
                        case '6':
                            e[2] = 'Traspaso de';
                        break;
                        case '7':
                            e[2] = 'Gasto';
                        break;
                    }
                    e[4] = parseFloat(e[4]).formatMoney();
                    e[5] = parseFloat(e[5]).formatMoney();
                    e[6] = parseFloat(e[6]).formatMoney();
					e[1] = B.sige.fecha.date(e[1],'-',':');
					vldate.push(e);
                });
                saldoinicio = parseFloat(saldoinicio).formatMoney();
                saldofin = parseFloat(saldofin).formatMoney();
				let value = Form.table('datosReportesC xxx', 'datosReportesC cfm cpc','striped',['Id', 'Fecha', 'Tipo', 'Responsable/concepto', 'Anterior', 'Monto', 'Actual', 'Estado'], vldate);
				$('.contenidoReporte').html(value);
				$('.datosReportesC > thead').prepend('<tr><th colspan="7" class="encabezadoReporte"></th></tr>');
                $('.encabezadoReporte').html(Access.headTable('Estado de cuenta de '+title, '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr><tr><th colspan="3">Saldo al inicio: '+saldoinicio+'</th><th colspan="3">Saldo al corte: '+saldofin+'</th></tr>'));
                $('th > table.reporteX').children('tbody').children('tr:nth-child(3)').remove();
                $('tr.datosReportesC > td:nth-child(1)').hide();
                $('table.datosReportesC').children('thead').children('tr:nth-child(2)').children('th:nth-child(1)').hide();
				// $('tbody.reporteX').append('<tr><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+montoOriginal.formatMoney()+'</b></td><td><b>&nbsp;</b></td><td><b>'+saldo.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
			});
        });
        u.keyup('.mtotraspaso', () => {
            let banco = parseFloat($('.saldoPdte.bancosocio').attr('data-amount'));
            let empleado = parseFloat($('.saldoPdte.empleado').attr('data-amount'));
            let value = $('.mtotraspaso').val();
            if (value == '')
                value = '0';
            value = parseFloat(value);
            if (banco - value < 0 ){
                Core.toast('Fondos insuficientes','red');
                $('.mtotraspaso').val('').keyup();
                return false;
            }
            let nuevobanco = parseFloat((banco - value).toFixed(2));
            let nuevoempleado = parseFloat((empleado + value).toFixed(2));
            $('.saldoPdte.bancosocio').html(nuevobanco.formatMoney());
            $('.saldoPdte.empleado').html(nuevoempleado.formatMoney());
        });
        u.click('tr.datosReportesC', (v) => {
            let id = v.attr('id');
            B.sige.asyn.post({class:'bancos', method: 'getMovementById', id: id}, r => {
                r = Core.json(false, r, false);
                let title = '', res = 'Cargado por', estado = 'Realizado', after = ``;
                r[6] = parseFloat(r[6]).formatMoney();
                switch(r[3]){
                    case '1':
                        title = 'Ingreso por venta';
                        res = '';
                    break;
                    case '2':
                        title = 'Ingreso por otros ingresos';
                    break;
                    case '3':
                        title = 'Traspaso';
                        res = 'Beneficiario';
                    break;
                    case '4':
                        title = 'Ingreso por venta empleado';
                    break;
                    case '5':
                        title = 'Traspaso para';
                        res = 'Beneficiario';
                    break;
                    case '6':
                        title = 'Traspaso de';
                        res = 'Emisor';
                    break;
                    case '7':
                        title = 'Gasto';
                        res = 'Pago realizado a';
                        let ms = ['Pendiente', 'Autorizado', 'Invalidado', 'Devuelto']
                        estado = ms[r[13]];
                        if ($('.mpagox').length == 0){
                            after = `
                                <tr>
                                    <th colspan="2" class="smnb">${Form.btn(false, 'green', 'b-ComprobarPagoMov', 'Comprobar pago', '', 'target="'+r[0]+'"')}</th>
                                </tr>
                            `;
                        } else {
                            after = `
                                <tr>
                                    <th colspan="2" class="smnb">${Form.btn(false, 'green', 'b-AutorizarPagoMov', 'Autorizar gasto', '', 'target="'+r[0]+'"')}</th>
                                </tr>
                                </tr>
                                    <th class="smnb">${Form.btn(false, 'red', 'b-RechazarPagoMov', 'Invalidar gasto', '', 'target="'+r[0]+'"')}</th>
                                    <th class="smnb">${Form.btn(false, 'indigo', 'b-CubiertoPagoMov', 'Gasto cubierto por el trabajador', '', 'target="'+r[0]+'"')}</th>
                                </tr>
                            `;
                        }
                        if (r[16].length > 0){
                            after += `
                                <tr>
                                    <th colspan="2" class="center-align">Comprobaciones anteriores</th>
                                </tr>
                            `
                        }
                        $.each(r[16], (i, e) => {
                            let btn = ($('.mpagox').length > 0) ? Form.btn(false, 'red', 'b-cancelarPagoComp', 'Invalidar comprobante', '', 'target="'+e[0]+'" data="'+id+'"') : '';
                            after += `
                            <tr>
                                <th colspan="2" class="center-align">Comprobación del ${e[9]}</th>
                            </tr>
                            <tr>
                                <th>Monto</th>
                                <td>${parseFloat(e[4]).formatMoney()}</td>
                            </tr>
                            <tr>
                                <th>Forma de pago</th>
                                <td>${e[5]}</td>
                            </tr>
                            <tr>
                                <th>No de cheque, cuenta, </th>
                                <td>${e[6]}</td>
                            </tr>
                            <tr>
                                <th>Comprobado por</th>
                                <td>${e[7]}</td>
                            </tr>
                            <tr>
                                <th>Observaciones</th>
                                <td>${e[8]}</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td class="smnb">${btn}</td>
                            </tr>
                        `
                        });
                    break;
                }
                let extra = (r[17] == 'null' && r[17] == 'nophoto') ? `<img width="80%" class="reponsive" src="data/${r[0]}.${r[17]}">` : 'Sin foto';
                ///
                B.sige.ui.question('Detalles del movimiento',`
                    <table>
                        <tr>
                            <th>Tipo de movimiento</th>
                            <td>${title}</td>
                        </tr>
                        <tr>
                            <th>Referencia</th>
                            <td>${r[0]}</td>
                        </tr>
                        <tr>
                            <th>Cargado a sistema por</th>
                            <td>${r[1]}</td>
                        </tr>
                        <tr>
                            <th>${res}</th>
                            <td>${r[2]}</td>
                        </tr>
                        <tr>
                            <th>Título</th>
                            <td>${r[4]}</td>
                        </tr>
                        <tr>
                            <th>Descripción</th>
                            <td>${r[5]}</td>
                        </tr>
                        <tr>
                            <th>Forma de pago</th>
                            <td>${r[7]}</td>
                        </tr>
                        <tr>
                            <th>Cuenta</th>
                            <td>${r[8]}</td>
                        </tr>
                        <tr>
                            <th>Monto</th>
                            <td>${r[6]}</td>
                        </tr>
                        <tr>
                            <th>Fecha de realización</th>
                            <td>${r[15]}</td>
                        </tr>
                        <tr>
                            <th>Estado de comprobación</th>
                            <td>${estado}</td>
                        </tr>
                        <tr>
                            <th>Foto (si aplica)</th>
                            <td>${extra}</td>
                        </tr>
                        ${after}
                    </table>
                `,'Cerrar',() => {
                    
                }, () => {
                    
                });
            });
        });
        u.click('.b-getBancos', () => {
            B.sige.creator.getInstance('BancoSocios').get();
        });
        u.click('.b-nuevoTraspasos', () => {
            B.sige.creator.getInstance('BancoSocios').nuevoTraspaso();
        });
        u.click('.b-nuevoComprobacionesGcm', () => {
            $('.b-nuevoReportes').click();
            $('.b-employeeMovements').click();
        });
        u.click('.b-nuevaVenta', () => {
            B.sige.creator.getInstance('BancoSocios').nuovo(1);
        });
        u.click('.b-nuevoIngreso', () => {
            B.sige.creator.getInstance('BancoSocios').nuovo(2);
        });
        u.click('.b-getEmployee', () => {
            if ($('.SelectedEmpleadoQ').data('id') == undefined){
                Core.toast('Seleccione un empleado para continuar.','red');
                return false;
            }
            B.sige.asyn.post({class:'bancos', method: 'getFinantialData', id: $('.SelectedEmpleadoQ').data('id')}, r => {
                r = Core.json(false, r, false);
                $('.saldoPdte.empleado').html(parseFloat(r[0]).formatMoney()).attr('data-amount', r[0]);
                $('.saldoPdte.bancosocio').html(parseFloat(r[1]).formatMoney()).attr('data-amount', r[1]);
                $('.b-saveTrapaso').removeClass('disabled');
                $('.jmnhide').slideDown(200);
            });
        });
        u.click('.b-saveTrapaso', () => {
            let v = [
                $('.ref').val(),
                $('.title').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.be_descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            if ($('.SelectedEmpleadoQ').data('id') == undefined){
                Core.toast('Seleccione un empleado para continuar.','red');
                return false;
            }
            v.unshift($('.SelectedEmpleadoQ').data('id'));
            B.sige.asyn.post({class:'bancos', method: 'saveTraspaso', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('BancoSocios').get();
                    break;
                }
            });
        });
        u.click('.b-saveNuevoVenta, .b-saveNuevoOtros', (r) => {
            let action = '';
            if (r.hasClass('b-saveNuevoVenta'))
                action = 'nuevaVenta';
            if (r.hasClass('b-saveNuevoOtros'))
                action = 'nuevaOtros';
            let v = [
                $('.ref').val(),
                $('.title').val(),
                $('.monto').val(),
                $('select.formaPago').val(),
                $('.cuenta').val(),
                $('.be_descr').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'bancos', method: action, param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('BancoSocios').get();
                    break;
                }
            });
        });
    }
}
class Precios{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'monetization_on';
        this.color = 'blue-grey';
        this.descr = 'agregar precios, modificar precios, eliminar precios, crear precios';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoPrecios', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestPrecios', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delPrecios', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en Precios', 'datosPreciosC'));
        this.get();
    }
    get(){
        $('.b-gestPrecios, .b-delPrecios').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Precios</a>');
        B.sige.asyn.post({class:'precios', method: 'getAllPrecios'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[3] = parseFloat(e[3]).formatMoney();
                e[4] = parseFloat(e[4]).formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosPrecios', 'datosPreciosC cfm','striped',['id', 'Nombre clave', 'Artículo', 'Mayoreo', 'Lo compro en', 'Proveedor'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        B.sige.asyn.post({class:'precios', method: 'getMetaPrecio'}, r => {
            r = Core.json(false, r, false); //r[0] = artículos | r[1] = proveedores
            let j = B.sige.und;
            $('.pos-actual').html('<a class="breadcrumb">Precios</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' precio</a>');
            let btn1 = B.sige.creator.atrs('getPrecios');
            let btn2 = Form.btn(false, 'green', 'b-savePrecio', 'Guardar');
            v = (v === j) ? ['','','','','','','','','',''] : v;
            $('.metainfo').hide();
            let articulo = Form.input('articulo', 'text', 'Busque un artículo', '', '', j, j);
            let SelectedEmpresaC = Form.select('SelectedEmpresaC','¿A qué empresa aplica?', B.sige.validator.getForSelect('empresas',0, 1));
            let SelectedSucursalC = Form.select('SelectedSucursalC','¿A qué sucursal aplica?', []); //v[2]
            let nombre = Form.input('nombre', 'text', 'Nombre clave', '', '', j, 100, v[3]);
			let proveedor = Form.select('SelectedProveedor','Seleccione un proveedor', r[1]); //v[7]
            let mayoreo = Form.input('mayoreo', 'number', 'Precio mayoreo', '', '', j, 12, v[4]);
            let normal = Form.input('normal', 'number', 'Precio menudeo o normal', '', '', j, 12, v[5]);
            let comprac = Form.input('comprac', 'number', 'Precio de compra', '', '', j, 12, v[10]);
            let descr = Form.area('descr','Observaciones', 200, j, v[6]);
            $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+articulo+'</div><div class="col s12 m6 input-field">'+nombre+'</div><div class="col s12 m6 input-field">'+proveedor+'</div><div class="col s12 m6 input-field">'+SelectedEmpresaC+'</div><div class="col s12 m6 input-field">'+SelectedSucursalC+'</div><div class="col s12 m4 input-field">'+mayoreo+'</div><div class="col s12 m4 input-field">'+normal+'</div><div class="col s12 m4 input-field">'+comprac+'</div><div class="col s12 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
            Form.autocomplete('.articulo', r[0], function(e){
                if (e.id === v[1])
                    $('.articulo').val(e.text.substring(0, e.text.length-1)).keyup().enter();
            });
			let sdt = (v[2] !== '') ? v[2] : $.cookie('sucursal');
			let empresa = B.sige.validator.searchIn('sucursales', sdt, 0);
            if (v[9] !== ''){
				B.prop('SelectedProveedor', v[9]);
            }
			$('.SelectedProveedor').formSelect();
			B.prop('SelectedEmpresaC', empresa[0][1]);
			$('.SelectedEmpresaC').formSelect();
			$('select.SelectedEmpresaC').change();
			B.prop('SelectedSucursalC', sdt);
			$('.SelectedSucursalC').formSelect();
            B.sige.uf();
        });

    }
    clicks(){
        u.change('select.SelectedEmpresaC', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalC').html(Form.initSelect(filter));
            $('.SelectedSucursalC').formSelect();
        });
        u.click('.b-nuevoPrecios', () => {
            B.sige.creator.getInstance('Precios').nuovo();
        });
        u.click('.b-getPrecios', () => {
            B.sige.creator.getInstance('Precios').get();
        });
        u.click('.datosPreciosC', v => {
            B.sige.validator.vTable(v, 'datosPreciosC', '.b-gestPrecios, .b-delPrecios');
        });
        u.click('.b-gestPrecios', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'precios', method: 'getPreciosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Precios').nuovo(r);
            });
        });
        u.click('.b-delPrecios', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este precio? Esta acción es irreversible','Aceptar',() => {
                B.sige.asyn.post({class:'precios', method: 'delPrecios', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Precios').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-savePrecio', () => {
            let v = [$('.articulo').data('id'), $('select.SelectedSucursalC').val(), $('.nombre').val(), $('.mayoreo').val(), $('.normal').val(), $('.descr').val(), $('select.SelectedProveedor').val(), $('.comprac').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'precios', method: 'savePrecio', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'equals':
                        Core.toast('Error, ya existe una configuración que incluye el mismo producto para la misma sucursal.','red rounded');
                    break;
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Precios').get();
                    break;
                }
            });
        });
    }
}
class OrdenDeCompra{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Compras';
        this.icon = 'playlist_add_check';
        this.color = 'purple';
        this.descr = 'agregar orden de compra, modificar orden de compra, eliminar orden de compra, crear orden de compra';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoOrdenCompra', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestOrdenCompra', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delOrdenCompra', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en órdenes de compra', 'datosOrdenCompraC'));
        this.get();
    }
    get(){
        $('.b-gestOrdenCompra, .b-delOrdenCompra').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Órdenes de compra</a>');
        B.sige.asyn.post({class:'ordendecompra', method: 'getAllOrden'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[3] = B.sige.fecha.date(e[3],'-');
                e[4] = B.sige.fecha.date(e[4],'-');
                e[5] = parseFloat(e[5]).formatMoney();
                e[6] = (e[6] === '0') ? 'Contado' : 'Crédito';
                vldate.push(e);
            });
            let value = Form.table('datosOrdenCompra', 'datosOrdenCompraC cfm','striped',['id', 'Departamento', 'Proveedor', 'Fecha de emisión', 'Fecha de llegada', 'Monto', 'Tipo'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        B.sige.asyn.post({class:'ordendecompra', method: 'getMetaOrdenDeCompra'}, r => {
            r = Core.json(false, r, false);
            let j = B.sige.und;
            $('.pos-actual').html('<a class="breadcrumb">Órdenes de compra</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' orden</a>');
            let btn1 = B.sige.creator.atrs('getOrdenCompra');
            let btn3 = Form.btn(false,'green','b-addArticulo','Agregar');
            let btn4 = Form.btn(false,'green','b-addPago','Añadir Nuevo Pago');
            let btn5 = Form.btn(true,'green','b-genOrden','Generar orden de compra');
            let btn6 = Form.btn(false,'green','b-imprimirOrdenC','Imprimir');
            let btn7 = Form.btn(false,'green','b-editarOrdenC','Editar');
            let tipoPago = Access.tipoPago();
            v = (v === j) ? ["","","","","","","","","","","","","","",[],[]] : v;
            $('.metainfo').hide();
            let select1 = Form.select('SelectedEmpresaF','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
            let select2 = Form.select('SelectedSucursalF','Seleccione una sucursal', []);
            let select3 = Form.select('SelectedDepartamentoF','Seleccione un departamento', []); //v[2]
            let select4 = Form.select('SelectedEmpleadoF','Seleccione un responsable', []); //v[3]
            let select5 = Form.select('SelectedProveedor','Seleccione un proveedor', r[0]); //v[4]
            let femision = Form.input('femision', 'text', 'Fecha de emisión', '', '', j, j, v[5]);
            let fllegada = Form.input('fllegada', 'text', 'Fecha de llegada', '', '', j, j, v[6]);
            let observac = Form.area('observac','Observaciones generales', 200, j, v[11]);
            let familia = Form.select('familiaF','Familia a la que pertenece', B.sige.validator.getForSelect('familias',0, 1));
            let articuloF = Form.select('articuloF','Seleccione Artículo', []);
            $('.cPart').html('<div class="col s6 m3 smnb">'+btn6+'</div><div class="col s6 m3 smnb">'+btn7+'</div><div class="col s12 offset-m3 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+select5+'</div><div class="col s12 m6 input-field">'+select1+'</div><div class="col s12 m6 input-field">'+select2+'</div><div class="col s12 m6 input-field">'+select3+'</div><div class="col s12 m6 input-field">'+select4+'</div><div class="col s12 m6 input-field">'+femision+'</div><div class="col s12 m6 input-field">'+fllegada+'</div><div class="col s12 input-field">'+observac+'</div><div class="col s12"><div class="col s9 m11 tg-title">Seleccione artículos disponibles</div><div class="col s3 m1 tg-title">'+Form.btn(false,'blue','b-ayuda','','help')+'</div></div><div class="col s12 m3 input-field">'+familia+'</div><div class="col s12 m4 input-field">'+articuloF+'</div><div class="col s12 offset-m2 m3 smnb addxm">'+btn3+'</div><div class="col s12 tg-title">Lista de artículos seleccionados</div><div class="col s12 margintop20"><table class="striped responsive-table"><tbody class="contentarticle"></tbody><tr><td colspan="6" class="right-align">SUBTOTAL</td><td class="grandSubtotal">0.0</td></tr><tr><td colspan="6" class="right-align">I.V.A. 16%</td><td class="grandIva">0.0</td></tr><tr><td colspan="6" class="right-align">TOTAL</td><td class="grandTotal">0.0</td></tr></table></div><div class="col s6 m4">'+tipoPago+'</div><div class="col s6 m5 restCover">Resta por cubrir: <span class="grandTotal"></span></div><div class="col s12 m3 addPagoMixto smnb">'+btn4+'</div><div class="col s12 contnigoPagos"></div><div class="col s12 smnb">'+btn5+'</div>');
            if (v[1] !== ''){
                B.sige.creator.getInstance('Trabajadores').editing = true;
                let sucursal = B.sige.validator.searchIn('departamentos', v[2], 0);
                let empresa = B.sige.validator.searchIn('sucursales', sucursal[0][1], 0);
                B.prop('SelectedEmpresaF', empresa[0][1]);
                $('.SelectedEmpresaF').formSelect();
                $('select.SelectedEmpresaF').change();
                B.prop('SelectedSucursalF', sucursal[0][1]);
                $('select.SelectedSucursalF').change();
                B.prop('SelectedDepartamentoF', v[2]);
                $('.SelectedDepartamentoF').formSelect();
                Access.getEmpleadosByDepId(v[2], () => {
                    B.prop('SelectedEmpleadoF',v[3]);
                    $('.SelectedEmpleadoF').formSelect();
                },'f');
            } else{
				let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
				B.prop('SelectedEmpresaF', empresa[0][1]);
                $('.SelectedEmpresaF').formSelect();
				$('select.SelectedEmpresaF').change();
                B.prop('SelectedSucursalF', $.cookie('sucursal'));
                $('select.SelectedSucursalF').change();
				
                $('.SelectedDepartamentoF, .SelectedEmpleadoF, .familiaF, .articuloF').formSelect();
            }
            B.sige.fecha.Dtpicker('.femision');
            B.sige.fecha.Dtpicker('.fllegada');
            if (v[4] !== '')
                B.prop('SelectedProveedor', v[4]);
            if (v[8] !== '')
                B.prop('tipoPago', v[8]);
            $('.SelectedProveedor, .tipoPago').formSelect();
            if (v[14].length > 0){
                $.each(v[14], (i, e) => {
                    B.sige.creator.getInstance('OrdenDeCompra').addArticle(e[3], e);
                });
            }
            if (v[15].length > 0){
                $.each(v[15], (i, e) => {
                    B.sige.creator.getInstance('OrdenDeCompra').addPago(e);
                });
            }
            if (v[0] !== ''){
                setTimeout(() => {
                    $('.cantidad').keyup();
                    $('.montoKeyUp').eq(0).keyup();
                    B.sige.creator.getInstance('OrdenDeCompra').enadis(true);
                    if (v[12] === '1'){
                        $('.b-editarOrdenC').attr('disabled','disabled');
                        Core.toast('Esta órden de compra es solo lectura dado que ya fue formalizada.','blue rounded');
                    }
                }, 120);
            } else 
                B.sige.creator.getInstance('OrdenDeCompra').enadis(false);
            B.sige.uf();
        });
    }
    enadis(v){
        let selects = $('select.SelectedEmpresaF, select.SelectedSucursalF, select.SelectedDepartamentoF, select.SelectedEmpleadoF, select.familiaF, select.articuloF, select.tipoPago, select.impuestoS, select.formaPago, select.SelectedProveedor');
        let buttons = $('.b-addArticulo, .b-remArticulo, .b-remPago, .b-genOrden, .b-ayuda');
        let actions = $('.b-imprimirOrdenC, .b-editarOrdenC');
        let inputs = $('.femision, .fllegada, .observac, .cantidad, .precioun, .monto, .cuenta, .observ');
        if (v){
            selects.attr('disabled', 'disabled');
            buttons.attr('disabled', 'disabled');
            actions.removeAttr('disabled');
            inputs.attr('disabled', 'disabled').addClass('bolder');
            selects.formSelect();
        } else {
            selects.removeAttr('disabled', 'disabled');
            buttons.removeAttr('disabled', 'disabled');
            actions.attr('disabled', 'disabled');
            inputs.removeAttr('disabled', 'disabled').removeClass('bolder');
            selects.formSelect();
        }
    }
    addArticle(id, param){
        B.sige.asyn.post({class:'precios', method: 'getArticulosByPrecioId', param: id}, r => {
            let j = B.sige.und;
            r = Core.json(false, r, false);
            param = (param === j) ? ['','','','','','','','','',''] : param;
            let clsid = z.clsid();
            let btn1 = Form.btn(false,'red','b-remArticulo','Eliminar','','target="'+clsid+'"');
            let codeInv = Form.input('codeInv', 'text', 'Cod. de inventario', '', '', j, j, r[1]);
            let codeArt = Form.input('codeArt', 'text', 'Cod. del artículo', '', '', j, j, r[2]);
            let nombArt = Form.input('nombArt', 'text', 'Nombre', '', '', j, j, r[3]);
            let familia = Form.input('familia', 'text', 'Familia', '', '', j, j, r[4]);
            
            let cantidad = Form.input('cantidad', 'number', 'Cant.', '', '', j, j, param[4], 'clsid="'+clsid+'"');
            let unidades = Form.input('unidades', 'text', 'Unidad', '', '', j, j, r[5]);
            let precioun = Form.input('precioun', 'number', 'Precio unitario', '', '', j, j, param[5], 'clsid="'+clsid+'"');
            let subtotal = Form.input('subtotal', 'text', 'Subtotal', '', '', j, j, '', 'clsid="'+clsid+'"');
            let impuesto = Access.impuesto(clsid);
            let subimpue = Form.input('subimpue', 'text', 'Más impuesto', '', '', j, j, '', 'clsid="'+clsid+'"');
            let total = Form.input('total', 'text', 'Total', '', '', j, j, '', 'clsid="'+clsid+'"');
            let kl = 'class="input-field"';
            $('.contentarticle').append('<tr class="headm '+clsid+'" precio="'+r[0]+'"><td '+kl+'>'+codeInv+'</td><td '+kl+'>'+codeArt+'</td><td '+kl+'>'+familia+'</td><td colspan="3" '+kl+'>'+nombArt+'</td><td class="smnb">'+btn1+'</td></tr><tr class="childUnit '+clsid+'" precio="'+r[0]+'" clsid="'+clsid+'"><td class="input-field cantidad">'+cantidad+'</td><td '+kl+'>'+unidades+'</td><td class="input-field preciou">'+precioun+'</td><td class="input-field subtotal">'+subtotal+'</td><td class="input-field impuesto">'+impuesto+'</td><td class="input-field iva">'+subimpue+'</td><td class="input-field total">'+total+'</td></tr>');
            if (param[7] !== ''){
                B.prop('impuestoS[clsid="'+clsid+'"]', param[7]);
            } else
                $('.cantidad[clsid="'+clsid+'"], .precioun[clsid="'+clsid+'"]').val('0').keyup();
            $('.codeInv, .codeArt, .nombArt, .familia, .unidades, .subtotal, .subimpue, .total').attr('disabled','disabled').addClass('bolder');
            $('.impuestoS[clsid="'+clsid+'"]').formSelect();
            B.sige.uf();
        });
    }
    calcLine(clsid){
        let cant = $('.cantidad[clsid="'+clsid+'"]').val();
        cant = (cant === '') ? 0 : cant;
        let prun = $('.precioun[clsid="'+clsid+'"]').val();
        prun = (prun === '') ? 0 : prun;
        let subtotal = parseFloat(cant * prun);
        $('.subtotal[clsid="'+clsid+'"]').val(subtotal.formatMoney());
        let impuesto = $('select.impuestoS[clsid="'+clsid+'"]').val();
        impuesto = (impuesto == null || impuesto === undefined || impuesto === '') ? 0 : parseInt(impuesto);
        switch(impuesto){
            case 0:
                impuesto = 0;
            break;
            case 1:
                impuesto = 0.16;
            break;
        }
        impuesto = subtotal * impuesto;
        $('.subimpue[clsid="'+clsid+'"]').val(impuesto.formatMoney());
        let total = subtotal + impuesto;
        $('.total[clsid="'+clsid+'"]').val(total.formatMoney());
        B.sige.uf();
        this.calcTotales();
    }
    calcTotales(){
        let subtotal = 0;
        $('input.subtotal').each(function(){
            let pre = $(this).val().toSimpleNumber();
            subtotal += parseFloat(pre);
        });
        $('.grandSubtotal').html(subtotal.formatMoney());
        let subimpue = 0;
        $('input.subimpue').each(function(){
            let pre = $(this).val().toSimpleNumber();
            subimpue += parseFloat(pre);
        });
        $('.grandIva').html(subimpue.formatMoney());
        let total = 0;
        $('input.total').each(function(){
            let pre = $(this).val().toSimpleNumber();
            total += parseFloat(pre);
        });
        $('.grandTotal').html(total.formatMoney()).attr('target', total);
    }
    addPago(v){
        let a = Access.miniComprobacion(v);
        let clsid = z.clsid();
        let btn1 = Form.btn(false,'red','b-remPago','Eliminar','','target="'+clsid+'"');
        $('.contnigoPagos').append('<div class="methodPago row col s12 '+clsid+'" clsid="'+clsid+'"><div class="col s12 offset-m10 m2 smnb">'+btn1+'</div><div class="col s12 a002">'+a[0]+'</div></div>');
        a[1]();
        $('.monto').addClass('montoKeyUp');
    }
    clicks(){
        u.blur('.cantidad, .precioun', () => {
            $('.montoKeyUp').keyup();
        });
        u.click('.b-genOrden', () => {
            let v = [
                $('select.SelectedDepartamentoF').val(),
                $('select.SelectedEmpleadoF').val(),
                $('select.SelectedProveedor').val(),
                $('.femision').val(),
                $('.fllegada').val(),
                $('.observac').val(),
                $('select.tipoPago').val(),
                $('td.grandTotal').html().toSimpleNumber(),
                [], //Productos de la lista
                [] //Métodos de pago
            ], flag = true;
            $('.childUnit').each(function(){
                let clsid = $(this).attr('clsid');
                let precio = $(this).attr('precio');
                let vld = $('select.impuestoS[clsid="'+clsid+'"]').val();
                if (vld == null)
                    return;
                let j = [
                    precio,
                    $('.cantidad[clsid="'+clsid+'"]').val().toSimpleNumber(),
                    $('.precioun[clsid="'+clsid+'"]').val().toSimpleNumber(),
                    $('.subtotal[clsid="'+clsid+'"]').val().toSimpleNumber(),
                    vld.toSimpleNumber(),
                    $('.subimpue[clsid="'+clsid+'"]').val().toSimpleNumber(),
                    $('.total[clsid="'+clsid+'"]').val().toSimpleNumber()
                ];
                let flag2 = B.sige.validator.vForm(j);
                if (j[6] === 0)
                    flag2 = false;
                if (flag2)
                    v[8].push(j);
            });
            /*
            if (v[6] == '0'){
                let suma = 0;
                $('.methodPago').each(function(){
                    let m = $(this).children('.a002').children('.command').children('.cDir-content');
                    let j = [
                        m.children('div:nth-child(1)').children('input').val(),
                        m.children('div:nth-child(2)').children('.select-wrapper').children('select').val(),
                        m.children('div:nth-child(3)').children('input').val(),
                        m.children('div:nth-child(4)').children('input').val()
                    ];
                    let flag2 = B.sige.validator.vForm(j);
                    j[0] = j[0].toSimpleNumber();
                    suma += j[0];
                    if (flag2)
                        v[9].push(j);
                });
                if (suma < v[7]){
                    Core.toast('En Ordenes al contado se debe cubrir el monto total. Falta por completar '+(v[7] - suma).formatMoney()+'.','blue rounded');
                    flag = false;
                }
            }
            */
            flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'ordendecompra', method: 'saveOrden', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('OrdenDeCompra').get();
                    break;
                }
            });
        });
        u.keyup('.montoKeyUp', v => {
            let t = $('span.grandTotal').attr('target').toSimpleNumber();
            let acumulado = 0;
            $('.montoKeyUp').each(function(){
                let monto = $(this).val();
                monto = (monto === '') ? 0 : parseFloat(monto);
                acumulado += monto;
            });
            let r = (t - acumulado);
            if (r < 0){
                Core.toast('No puede abonar más dinero ('+(acumulado).formatMoney()+') del que resta por cubrir ('+t.formatMoney()+')','blue rounded');
                v.val(0);
                $('.montoKeyUp').keyup();
                return false;
            }
            r = (r < 0) ? 0 : r;
            $('span.grandTotal').html(r.formatMoney());
        });
        u.click('.b-editarOrdenC', () => {
            B.sige.creator.getInstance('OrdenDeCompra').enadis(false);
        });
        u.click('.b-addPago', () => {
            B.sige.creator.getInstance('OrdenDeCompra').addPago(undefined);
        });
        u.change('select.tipoPago', () => {
            /*
            if (v.val() == '0'){
                $('.addPagoMixto').show();
            } else {
                $('.addPagoMixto').hide();
                $('.contnigoPagos').html('');
            }
            */
        });
        u.change('select.impuestoS', v => {
            let clsid = v.attr('clsid');
            B.sige.creator.getInstance('OrdenDeCompra').calcLine(clsid);
            $('.montoKeyUp').keyup();
        });
        u.keyup('.cantidad, .precioun', v => {
            let clsid = v.attr('clsid');
            B.sige.creator.getInstance('OrdenDeCompra').calcLine(clsid);
        });
        u.click('.b-remPago', v => {
            let id = v.attr('target');
            $('.methodPago.'+id).remove();
            $('.montoKeyUp').keyup();
        });
        u.click('.b-remArticulo', v => {
            let id = v.attr('target');
            $('tr.'+id).remove();
        });
        u.click('.b-addArticulo', () => {
            let art = $('select.articuloF').val();
            if ($('.headm[precio="'+art+'"]').length > 0){
                Core.toast('Ya ha añadido este producto a su lista','yellow black-text rounded');
                return false;
            }
            B.sige.creator.getInstance('OrdenDeCompra').addArticle(art);
        });
        u.change('select.SelectedEmpresaF', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalF').html(Form.initSelect(filter));
            $('.SelectedSucursalF').formSelect();
            $('select.SelectedSucursalF').change();
        });
        u.change('select.SelectedSucursalF', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('departamentos', v.val(), 1), 0, 4);
            $('select.SelectedDepartamentoF').html(Form.initSelect(filter));
            $('.SelectedDepartamentoF').formSelect();
            $('select.SelectedDepartamentoF').change();
        });
        u.change('select.SelectedDepartamentoF', v => {
            Access.getEmpleadosByDepId(v.val(), () => {
                $('select.SelectedEmpleadoF').change();
                $('.SelectedEmpleadoF').formSelect();
            },'F');
        });
        u.change('select.familiaF', v => {
            let s = $('select.SelectedSucursalF').val();
            if (s == null || s === undefined || s === ''){
                Core.toast('Seleccione una sucursal primero','yellow black-text rounded');
                return false;
            }
            Access.getArticulosByPrecioId([s, v.val()], () => {
                $('select.articuloF').change();
                $('.articuloF').formSelect();
            },'F');
        });
        u.click('.b-ayuda', () => {
            alert('Ayuda');
        });
        u.click('.b-nuevoOrdenCompra', () => {
            B.sige.creator.getInstance('OrdenDeCompra').nuovo();
        });
        u.click('.b-getOrdenCompra', () => {
            B.sige.creator.getInstance('OrdenDeCompra').get();
        });
        u.click('.datosOrdenCompraC', v => {
            B.sige.validator.vTable(v, 'datosOrdenCompraC', '.b-gestOrdenCompra, .b-delOrdenCompra, .b-formalizarOrden, .b-pagarCuentaPorPagar');
        });
        u.click('.b-gestOrdenCompra', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'ordendecompra', method: 'getOrdenesById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('OrdenDeCompra').nuovo(r);
            });
        });
        u.click('.b-delOrdenCompra', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta orden de compra? Esta acción es irreversible','Aceptar',() => {
                B.sige.asyn.post({class:'ordendecompra', method: 'delOrden', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                        case 'formalized':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('OrdenDeCompra').get();
                        break;
                    }
                });
            });
        });
    }
}
class FormalizarOrdenCompra{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Compras';
        this.icon = 'description';
        this.color = 'light-blue';
        this.descr = 'formalizar orden de compra, formalizar ordenes de compra';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s8 m3','gestOrdenCompra', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s12 offset-m6 m3','formalizarOrden', 'Formalizar'));
        mc.append(B.sige.creator.simpliest('Buscar en órdenes de compra'));
        this.get();
    }
    get(){
        $('.b-gestOrdenCompra, .b-formalizarOrden').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Órdenes de compra por formalizar</a>');
        B.sige.asyn.post({class:'ordendecompra', method: 'getAllOrdenFormalizada'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[3] = B.sige.fecha.date(e[3],'-');
                e[4] = B.sige.fecha.date(e[4],'-');
                e[5] = parseFloat(e[5]).formatMoney();
                e[6] = (e[6] === '0') ? 'Contado' : 'Crédito';
                vldate.push(e);
            });
            let value = Form.table('datosOrdenCompra', 'datosOrdenCompraC cfm','striped',['id', 'Departamento', 'Proveedor', 'Fecha de emisión', 'Fecha de llegada', 'Monto', 'Tipo'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-formalizarOrden', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'ordendecompra', method: 'getOrdenesArticulosById', param: id}, r => {
                $('.metainfo').hide();
                r = Core.json(false, r, false);
                let j = B.sige.und;
                let btn1 = Form.btn(true,'green','b-formalizarArticulos','Formalizar e inventariar');
                let btn2 = Form.btn(false,'blue-grey','b-getFormalizarOC','Regresar');
                
                $('.cPart').html('<div class="col s9 tg-title">Formalización</div><div class="col s3 smnb">'+btn2+'</div><div class="col s12 flow-text jutify-align">Verifique y confirme si el número de artículos para cada uno son iguales o si presentan una variación. Esto es importante dado que al formalizar, los números que estén establecidos se agregarán al inventario.</div><div class="col s12 articulosS"></div><div class="col s12 final smnb">'+btn1+'</div>');
                let vldate = [];
                $.each(r, (i, e) => {
                    let clsid = z.clsid();
                    let nuevaU = Form.input('nuevaU', 'number', 'Nueva', '', '', j, j, e[4], 'clsid="'+clsid+'" actual="'+e[4]+'" ordid="'+e[0]+'"');
					console.log(nuevaU);
                    vldate.push([e[0], e[3][1], e[3][2], e[3][4], e[3][3], e[4] +' '+e[3][5], '<div class="input-field">'+nuevaU+'</div>', '<span class="dif '+clsid+'">0</span>']);
                });
                let value = Form.table('datosFormalizar', 'datosFormalizarC','striped',['id', 'Cod. Inventario.', 'Cod. Art.', 'Familia', 'Artículo', 'Unidades', 'Pieza', 'Diferencias'], vldate);
                $('.articulosS').html(value);
                B.sige.uf();
            });
        });
        u.click('.b-getFormalizarOC', () => {
            B.sige.creator.getInstance('FormalizarOrdenCompra').get();
        });
        u.click('.b-formalizarArticulos', () => {
            let arr = [];
            $('.nuevaU').each(function(){
                arr.push([$(this).attr('ordid'), $(this).val()]);
            });
            B.sige.ui.question('Confirmar acción','Se registrarán las unidades marcadas en el inventario. ¿Desea continuar?','Aceptar',() => {
                B.sige.asyn.post({class:'ordendecompra', method: 'formalizeOrden', param: arr}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('FormalizarOrdenCompra').get();
                        break;
                    }
                });
            });
        });
        u.keyup('.nuevaU', v => {
            let clsid = v.attr('clsid');
            let actual = v.attr('actual').toSimpleNumber();
            let nuevo = v.val().toSimpleNumber();
            $('.dif.'+clsid).html((nuevo - actual).toFixed(4));
        });
    }
}
class CuentasPorPagar{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Compras';
        this.icon = 'account_balance_wallet';
        this.color = 'green';
        this.descr = 'cuentas por pagar';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s8 m3','gestOrdenCompra', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s12 offset-m6 m3','pagarCuentaPorPagar disabled', 'Abonar o pagar'));
        mc.append(B.sige.creator.simpliest('Buscar en órdenes de compra a crédito', 'datosOrdenCompraC'));
        this.get();
    }
    get(){
        $('.b-gestOrdenCompra, .b-formalizarOrden').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Órdenes de compra por formalizar</a>');
        B.sige.asyn.post({class:'ordendecompra', method: 'getAllOrdenCredito'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[4] = (e[2]-e[3]).formatMoney();
                e[2] = parseFloat(e[2]).formatMoney();
                e[3] = parseFloat(e[3]).formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosOrdenCompra', 'datosOrdenCompraC cfm','striped',['id', 'Proveedor', 'Monto', 'Cubierto', 'Resta'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-pagarCuentaPorPagar', v => {
            let id = v.attr('target');
            let ac = Access.comprobacion();
            B.sige.asyn.post({class:'ordendecompra', method: 'setIdComprobacion', param: id}, r => {
                r = Core.json(false, r, false);
                r[1][0] = parseFloat(r[1][0]);
                r[1][1] = parseFloat(r[1][1]);
                r[1][2] = r[1][0] - r[1][1];
                if (r[1][0] === r[1][1]){
                    Core.toast('Este gasto se ha comprobado en su totalidad. Si desea modificar un registro, de clic en detalles','blue rounded');
                    return false;
                }
                B.sige.ui.question('Comprobar gastos','<div class="row"><div class="col s12 m8">Monto pendiente por comprobar</div><div class="col s12 m4 totalPorCubrir" target="'+r[1][2]+'">'+r[1][2].formatMoney()+'</div><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Guardar',() => {
                    let v = [
                        $('.monto').val(),
                        $('select.formaPago').val(),
                        $('.cuenta').val(),
                        $('.SelectedEmpleadoD').val(),
                        $('.observ').val(),
                    ];
                    let flag = B.sige.validator.vForm(v);
                    if (!flag)
                        return false;
                    B.sige.asyn.post({class:'ordendecompra', method: 'comprobarOrdenCompra', param: v}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'mayor':
                                Core.toast('El monto que está comprobando es mayor al monto que resta para este pago','red rounded');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                B.sige.creator.getInstance('CuentasPorPagar').get();
                            break;
                        }
                    });
                }, () => {
                    ac[1]();
                    $('.monto').addClass('gastoKeyUp');
                });
            });
        });
        u.click('.b-getFormalizarOC', () => {
            B.sige.creator.getInstance('FormalizarOrdenCompra').get();
        });
        u.click('.b-formalizarArticulos', () => {
            let arr = [];
            $('.nuevaU').each(function(){
                arr.push([$(this).attr('ordid'), $(this).val()]);
            });
            B.sige.ui.question('Confirmar acción','Se registrarán las unidades marcadas en el inventario. ¿Desea continuar?','Aceptar',() => {
                B.sige.asyn.post({class:'ordendecompra', method: 'formalizeOrden', param: arr}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('FormalizarOrdenCompra').get();
                        break;
                    }
                });
            });
        });
        u.keyup('.nuevaU', v => {
            let clsid = v.attr('clsid');
            let actual = v.attr('actual').toSimpleNumber();
            let nuevo = v.val().toSimpleNumber();
            $('.dif.'+clsid).html((nuevo - actual).toFixed(4));
        });
    }
}
class CuentasPorCobrar{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Ventas';
        this.icon = 'account_balance_wallet';
        this.color = 'lime';
        this.descr = 'cuentas por cobrar';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s8 m3','edoctn', 'Estado de cuenta', 'blue'));
        mc.append(B.sige.creator.simbtn('s8 m3','detallePagos', 'Pagos'));
        mc.append(B.sige.creator.simbtn('s8 m3','initReporte', 'Reportes','blue'));
        mc.append(B.sige.creator.simbtn('s12 m3','pagarCuentaPorCobrar disabled', 'Abonar o pagar'));
        mc.append(B.sige.creator.simpliest('Buscar en órdenes de compra a crédito', 'datosCuentasPorCobrarC'));
        this.get();
    }
    get(){
        $('.b-formalizarOrden, .b-detallePagos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Cuentas por cobrar</a>');
        B.sige.asyn.post({class:'PuntoVenta', method: 'getAllVentasPagar'}, r => {
            r = Core.json(false, r, false);
            let vldate = [], totalm = 0, debe = 0;
            $.each(r, (i, e) => {
                e[2] = e[2].toSimpleNumber();
				totalm += e[2];
				e[2] = e[2].formatMoney();
				debe += e[7];
				e[7] = e[7].formatMoney();
                e[1] = B.sige.fecha.date(e[1],'-');
                vldate.push([e[0], e[1], e[6], e[2], e[7], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosCuentasPorCobrar', 'datosCuentasPorCobrarC cfm','striped',['id', 'Fecha', 'Cliente', 'Monto', 'Saldo', 'Forma de Pago', 'No. de cheque', 'Observaciones'], vldate);
            $('.cPart').html(value);
			$('tbody.datosCuentasPorCobrar').append('<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>TOTAL</b></td><td><b>'+totalm.formatMoney()+'</b></td><td><b>'+debe.formatMoney()+'</b></td><td><b>&nbsp;</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');
        });
    }
    clicks(){
		u.click('.b-detalleReporte, .b-detalleReporte2', v => {
			let target = v.attr('target');
			B.sige.ui.question('Detalles','<div class="ctentm"></div>','Cerrar', () => {
				
			}, () => {
				B.sige.creator.getInstance('PuntoVenta').getComplexTicket(target, '.ctentm');
			});
		});
		u.click('.b-edoctn', () => {
			$('.b-initReporte').click();
			$('.b-CPCresumido').click();
			$('.cPart').children('.col.s12.m4.smnb').hide();
		});
        u.click('.b-detallePagos', v => {
            $('.pos-actual').html('<a class="breadcrumb">Cuentas por cobrar</a><a class="breadcrumb">Pagos realizados</a>');
            $('.metainfo').hide();
            let id = v.attr('target');
            B.sige.asyn.post({class:'PuntoVenta', method: 'getComprobacionesById', param: id}, r => {
                r = Core.json(false, r, false);
                let btn1 = B.sige.creator.atrs('getCuentasPorCobrar');
                let bt1 = Form.btn(false,'green','disabled b-detalleComprobante','Detalles');
                let bt2 = Form.btn(false,'red','disabled b-borrarComprobanteP','Borrar');
                
                $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="tg-title col offset-m6 s6 m3 smnb">'+bt1+'</div><div class="tg-title col s6 m3 smnb">'+bt2+'</div><div class="col s12 tableGatos"></div>');
                let vldate = [];
                $.each(r, (i, e) => {
                    e[2] = B.sige.fecha.date(e[2],'-',':');
                    e[3] = parseFloat(e[3]).formatMoney();
                    e[4] = B.sige.access.formas[e[4]][1];
                    vldate.push([e[0], e[1], e[2], e[3], e[4], e[5]]);
                });
                let value = Form.table('datosComprobantes', 'datosComprobantesC cfm','striped',['id', 'Referencia', 'Fecha', 'Monto', 'Forma de pago', 'Responsable'], vldate);
                $('.tableGatos').html(value);
            });
        });
        u.click('.datosCuentasPorCobrarC', v => {
            B.sige.validator.vTable(v, 'datosCuentasPorCobrarC', '.b-pagarCuentaPorCobrar, .b-detallePagos');
        });
        u.click('.b-getCuentasPorCobrar', () => {
            B.sige.creator.getInstance('CuentasPorCobrar').get();
			$('.b-getCuentasPorCobrar').removeClass('b-getCuentasPorCobrar').addClass('b-return');
			$('.m-content').slideDown(150);
        });
        u.click('.b-pagarCuentaPorCobrar', v => {
            let id = v.attr('target');
            let ac = Access.comprobacion();
            B.sige.asyn.post({class:'PuntoVenta', method: 'setIdComprobacion', param: id}, r => {
                r = Core.json(false, r, false);
                r[1][0] = parseFloat(r[1][0]);
                r[1][1] = parseFloat(r[1][1]);
                r[1][2] = r[1][0] - r[1][1];
                if (r[1][1] >= r[1][0]){
                    Core.toast('Esta venta se ha comprobado en su totalidad.','blue rounded');
                    return false;
                }
                B.sige.ui.question('Comprobar cuenta por cobrar','<div class="row"><div class="col s12 m8">Monto pendiente por comprobar</div><div class="col s12 m4 totalPorCubrir" target="'+r[1][2]+'">'+r[1][2].formatMoney()+'</div><div class="col s12">'+ac[0]+'</div><div class="height400"></div></div>','Guardar',() => {
                    let v = [
                        $('.monto').val(),
                        $('select.formaPago').val(),
                        $('.cuenta').val(),
                        $('.SelectedEmpleadoD').val(),
                        $('.observ').val(),
                    ];
                    let flag = B.sige.validator.vForm(v);
                    if (!flag)
                        return false;
                    B.sige.asyn.post({class:'PuntoVenta', method: 'comprobarVenta', param: v}, r => {
                        r = Core.json(false, r, false);
                        switch(r[0]){
                            case 'error':
                            case 'denied':
                                B.msn(r[0]);
                            break;
                            case 'mayor':
                                Core.toast('El monto que está comprobando es mayor al monto que resta para este pago','red rounded');
                            break;
                            case 'true':
                                B.msn(r[0]);
                                B.sige.creator.getInstance('CuentasPorCobrar').get();
                            break;
                        }
                    });
                }, () => {
                    ac[1]();
					let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
					B.prop('SelectedEmpresaD', empresa[0][1]);
					$('select.SelectedEmpresaD').change();
					B.prop('SelectedSucursalD', $.cookie('sucursal'));
					$('select.SelectedSucursalD').change();
					$('select.SelectedEmpresaD, select.SelectedSucursalD').attr('disabled', 'disabled');
					$('.SelectedEmpresaD').formSelect();
					$('.SelectedSucursalD').formSelect();
                    $('.monto').addClass('gastoKeyUp');
                });
            });
        });
        u.keyup('.nuevaU', v => {
            let clsid = v.attr('clsid');
            let actual = v.attr('actual').toSimpleNumber();
            let nuevo = v.val().toSimpleNumber();
            $('.dif.'+clsid).html((nuevo - actual).toFixed(4));
        });
        u.click('.b-borrarComprobanteP', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este comprobante? Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'PuntoVenta', method: 'delComprobante', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                        case 'comprobado':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('CuentasPorCobrar').get();
                        break;
                    }
                });
            });
        });
		/*ACCIONES DE REPORTES*/
		u.click('.b-CPCresumido', () => {
			$('.FilterData').html();
			B.sige.asyn.post({class:'PuntoVenta', method: 'getCPCresumido'}, r => {
				r = Core.json(false, r, false);
				let vldate = [], deudatotal = 0, abonosrealizados = 0, deudaporcubrir = 0;
				$.each(r, (i, e) => {
					e[2] = e[2].toSimpleNumber();
					deudatotal += e[2];
					e[2] = '<div class="detalleDeudaLista" target="'+e[5]+'">'+e[2].formatMoney()+'</div>';
					
					e[3] = e[3].toSimpleNumber();
					abonosrealizados += e[3];
					e[3] = '<div class="detalleAbonoLista" target="'+e[5]+'">'+e[3].formatMoney()+'</div>';
					
					deudaporcubrir += e[4];
					e[4] = '<div class="detalleDcubrLista" target="'+e[5]+'">'+e[4].formatMoney()+'</div>';
					vldate.push([e[0], e[1], e[2], e[3], e[4]]);
				});
				//let btn2 = Form.btn(false, 'green', 'b-detalleReporte cpc disabled', 'Detalles');
				let ca = Access.reportes('', false, false, false, false, false, true, [
					'',function(){} //<div class="col s12 m4 smnb">'+btn2+'</div>
				]);
				$('.FilterData').html(ca[0]);
				ca[1]();
				let value = Form.table('reporteX xxx', 'reporteX cfm cpc','striped',['Id','Cliente', 'Deuda total', 'Abonos realizados', 'Deuda por cubrir'], vldate);
				$('.contenidoReporte').html(value);
				$('tbody.reporteX').append('<tr><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+deudatotal.formatMoney()+'</b></td><td><b>'+abonosrealizados.formatMoney()+'</b></td><td><b>'+deudaporcubrir.formatMoney()+'</b></td></tr>');
			});
		});
		u.click('.detalleDeudaLista', v => {
			let target = v.attr('target');
			B.sige.asyn.post({class:'PuntoVenta', method: 'detalleDeudaLista', param: target}, r => {
				r = Core.json(false, r, false);
				let vldate = [], deudatotal = 0, cubierto = 0, saldo = 0;
				$.each(r, (i, e) => {
					e[1] = B.sige.fecha.date(e[1],'-', ':');
					e[2] = e[2].toSimpleNumber();
					deudatotal += e[2];
					e[2] = e[2].formatMoney();
					e[3] = e[3].toSimpleNumber();
					cubierto += e[3];
					e[3] = e[3].formatMoney();
					saldo += e[4];
					e[4] = e[4].formatMoney();
					vldate.push(e);
				});
				let value = Form.table('reporteXX xxx', 'reporteXX cfm cpc','striped',['Id', 'Fecha', 'Monto', 'Cubierto', 'Saldo', 'Observaciones'], vldate);
				B.sige.ui.question('Notas con deuda','<div class="col s12 smnb">'+Form.btn(false, 'green', 'b-detalleReporte2 cpc disabled', 'Detalles')+'</div><div class="col s12">'+value+'</div>','Cerrar',() => {}, () => {
					$('tbody.reporteXX').append('<tr><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+deudatotal.formatMoney()+'</b></td><td><b>'+cubierto.formatMoney()+'</b></td><td><b>'+saldo.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
				});
			});
		});
		u.click('.detalleAbonoLista', v => {
			let target = v.attr('target');
			B.sige.asyn.post({class:'PuntoVenta', method: 'detalleAbonoLista', param: target}, r => {
				r = Core.json(false, r, false);
				let vldate = [], cubierto = 0;
				$.each(r, (i, e) => {
					e[2] = B.sige.fecha.date(e[2],'-', ':');
					e[3] = e[3].toSimpleNumber();
					cubierto += e[3];
					e[3] = e[3].formatMoney();
					vldate.push([e[1], e[0], e[2], e[3], e[4]]);
				});
				let value = Form.table('reporteXX xxx', 'reporteXX cfm cpc','striped',['Pertenece a', 'Id pago', 'Fecha', 'Monto', 'Forma de pago'], vldate);
				B.sige.ui.question('Abonos realizados','<div class="col s12 smnb">'+Form.btn(false, 'green', 'b-detalleReporte2 cpc disabled', 'Detalles')+'</div><div class="col s12">'+value+'</div>','Cerrar',() => {}, () => {
					$('tbody.reporteXX').append('<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+cubierto.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
				});
			});
		});
		u.click('.detalleDcubrLista', v => {
			let target = v.attr('target');
			B.sige.asyn.post({class:'PuntoVenta', method: 'detalleDcubrLista', param: target}, r => {
				r = Core.json(false, r, false);
				let vldate = [], deudatotal = 0, cubierto = 0, saldo = 0;
				$.each(r, (i, e) => {
					e[1] = B.sige.fecha.date(e[1],'-', ':');
					e[2] = e[2].toSimpleNumber();
					deudatotal += e[2];
					e[2] = e[2].formatMoney();
					e[3] = e[3].toSimpleNumber();
					cubierto += e[3];
					e[3] = e[3].formatMoney();
					saldo += e[4];
					e[4] = e[4].formatMoney();
					vldate.push(e);
				});
				let value = Form.table('reporteXX xxx', 'reporteXX cfm cpc','striped',['Id', 'Fecha', 'Monto', 'Cubierto', 'Saldo', 'Observaciones'], vldate);
				B.sige.ui.question('Deudas por cubrir','<div class="col s12 smnb">'+Form.btn(false, 'green', 'b-detalleReporte2 cpc disabled', 'Detalles')+'</div><div class="col s12">'+value+'</div>','Cerrar',() => {}, () => {
					$('tbody.reporteXX').append('<tr><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+deudatotal.formatMoney()+'</b></td><td><b>'+cubierto.formatMoney()+'</b></td><td><b>'+saldo.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
				});
			});
		});
		u.click('.reporteX.cpc', v => {
			$('.b-detalleReporte.cpc').attr('target', v.attr('id')).removeClass('disabled');
		});
		u.click('.reporteXX.cpc', v => {
			$('.b-detalleReporte2.cpc').attr('target', v.attr('id')).removeClass('disabled');
		});
		u.click('.b-CPCliente', () => {
			$('.FilterData').html();
			let existe = Form.select('CPCSaldo','Tipo de salida', [[0,'Mostrar todos'], [1, 'Mostrar sólo los que tienen saldo']]);
			let btn2 = Form.btn(false, 'green', 'b-detalleReporte cpc disabled', 'Detalles');
			let ca = Access.reportes('CPClientex', true, true, false, false, true, true, ['<div class="col s12 m6 input-field">'+existe+'</div><div class="col s12 m4 smnb">'+btn2+'</div>', function(){
				$('.CPCSaldo').formSelect();
			}]);
			$('.FilterData').html(ca[0]);
			ca[1]();

		});
		u.click('.b-CPClientex', function(){
			let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sCliente').val(),
				$('select.CPCSaldo').val()
			];
			if (v[3] == null)
				v[3] = 0;
			B.sige.asyn.post({class:'PuntoVenta', method: 'getCPCliente', param: v}, r => {
				r = Core.json(false, r, false);
				let vldate = [], montoOriginal = 0, saldo = 0;
				$.each(r, (i, e) => {
					e[1] = B.sige.fecha.date(e[1],'-',':');
					e[2] = e[2].toSimpleNumber();
					montoOriginal += e[2];
					e[2] = e[2].formatMoney();
					saldo += e[4];
					e[4] = e[4].formatMoney();
					vldate.push(e);
				});
				let value = Form.table('reporteX xxx', 'reporteX cfm cpc','striped',['Nota', 'Fecha registro', 'Monto original', 'Forma de pago', 'Saldo', 'Cliente'], vldate);
				$('.contenidoReporte').html(value);
				$('.reporteX > thead').prepend('<tr><th colspan="6" class="encabezadoReporte"></th></tr>');
				$('.encabezadoReporte').html(Access.headTable('Reporte de cuentas por cobrar', '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
				$('tbody.reporteX').append('<tr><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+montoOriginal.formatMoney()+'</b></td><td><b>&nbsp;</b></td><td><b>'+saldo.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
			});
		});
		u.click('.b-CPCpagos', () => {
			$('.FilterData').html();
			let btn2 = Form.btn(false, 'green', 'b-detalleReporte cpc disabled', 'Detalles');
			let ca = Access.reportes('CPCpagosx', true, true, false, false, true, true, ['<div class="col s12 offset-m8 m4 smnb">'+btn2+'</div>', function(){}]);
			$('.FilterData').html(ca[0]);
			ca[1]();
		});
		u.click('.b-CPCpagosx', function(){
			let v = [
				$('.rep_finicio').val(),
				$('.rep_ffin').val(),
				$('select.rep_sCliente').val(),
			];
			B.sige.asyn.post({class:'PuntoVenta', method: 'CPCpagos', param: v}, r => {
				r = Core.json(false, r, false);
				let vldate = [], importe = 0;
				$.each(r, (i, e) => {
					e[1] = B.sige.fecha.date(e[1],'-',':');
					e[3] = e[3].toSimpleNumber();
					importe += e[3];
					e[3] = e[3].formatMoney();
					vldate.push(e);
				});
				let value = Form.table('reporteX xxx', 'reporteX cfm cpc','striped',['Nota', 'Fecha de pago', 'Cliente', 'Importe', 'Forma de pago'], vldate);
				$('.contenidoReporte').html(value);
				$('.reporteX > thead').prepend('<tr><th colspan="5" class="encabezadoReporte"></th></tr>');
				$('.encabezadoReporte').html(Access.headTable('Reporte de pagos realizados', '<tr><th colspan="3">Fecha de inicio: '+v[0]+'</th><th colspan="3">Fecha de fin: '+v[1]+'</th></tr>'));
				$('tbody.reporteX').append('<tr><td>&nbsp;</td><td>&nbsp;</td><td><b>TOTALES</b></td><td><b>'+importe.formatMoney()+'</b></td><td>&nbsp;</td></tr>');
			});
		});
    }
	reportes(){
		let btn_1 = Form.btn(false, 'green', 'b-CPCresumido', 'Reporte Resumido');
		let btn_2 = Form.btn(false, 'green', 'b-CPCliente', 'Reporte cliente');
		let btn_3 = Form.btn(false, 'green', 'b-CPCpagos', 'Reporte de pagos');
		$('.cPart').append('<div class="col s12 m4 smnb">'+btn_1+'</div>');
		$('.cPart').append('<div class="col s12 m4 smnb">'+btn_2+'</div>');
		$('.cPart').append('<div class="col s12 m4 smnb">'+btn_3+'</div>');
		$('.cPart').append('<div class="col s12 FilterData"></div>');
		$('.cPart').append('<div class="col s12 contenidoReporte"></div>');
		$('.b-return').addClass('b-getCuentasPorCobrar').removeClass('b-return');
	}
}
class Inventarios{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'storage';
        this.color = 'green';
        this.descr = 'agregar inventarios, modificar inventarios, eliminar inventarios, crear inventarios';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 offset-m9 m3','gestInventarios', 'Detalles'));
        mc.append(B.sige.creator.simpliest('Buscar en inventarios', 'datosInventariosC'));
        this.get();
    }
    get(){
        $('.b-gestInventarios').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Inventarios</a>');
        B.sige.asyn.post({class:'inventarios', method: 'getAllInventarios'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[5] = B.sige.fecha.date(e[5],'-',':');
                vldate.push(e);
            });
            let value = Form.table('datosInventarios', 'datosInventariosC cfm','striped',['id', 'Clave', 'Artículo', 'Familia', 'Existencia', 'Último mvto.'], vldate);
            $('.cPart').html(value);
        });
    }
    getMovements(){
        B.sige.asyn.post({class:'inventarios', method: 'getAllMovementsFromId'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $('.invact').val(r[1]);
            $.each(r[0], (i, e) => {
                e[5] = B.sige.fecha.date(e[5],'-',':');
                e[2] = (e[2] === '0') ? 'Entrada' : 'Salida';
                vldate.push([e[0], e[1], e[2], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosMovimientos', 'datosMovimientosC cfm','striped',['id', 'Responsable', 'Tipo', 'Unidades', 'observaciones', 'Fecha'], vldate);
            $('.movement').html(value);
        });
    }
    clicks(){
        u.click('.datosInventariosC', v => {
            B.sige.validator.vTable(v, 'datosInventariosC', '.b-gestInventarios');
        });
        u.click('.b-getInventarios', () => {
            B.sige.creator.getInstance('Inventarios').get();
        });
        u.click('.b-addRemove', () => {
            let tipo = Access.tipo();
            let codeinv = Form.input('unidadesTo', 'number', 'Unidades ('+$('.unidad').val()+')', '', '');
            let descr = Form.area('businessDescr','Observaciones de este movimiento');
            
            B.sige.ui.question('Añadir o quitar '+$('.codeinv').val()+' '+$('.nombre').val()+' ('+$('.code').val()+') ','<div class="row"><div class="col s12 m4 input-field">'+tipo+'</div><div class="col s12 m8 input-field">'+codeinv+'</div><div class="col s12 input-field">'+descr+'</div><div class="col s12">Los registros del inventario son permanentes y no pueden modificarse.</div></div>','Guardar',() => {
                let v = [$('select.tipoIngreso').val(), $('.unidadesTo').val(), $('.businessDescr').val()];
                let flag = true;
                $.each(v,(i, e) => {
                    if (e === '' || e == null || e === undefined)
                        flag = false;
                });
                if (!flag){
                    B.msn('empty');
                    return false;
                }
                B.sige.asyn.post({class:'inventarios', method: 'addManuallyToMovements', param: v}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                        case 'insuficient':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Inventarios').getMovements();
                        break;
                    }
                });
            }, () => {
                $('.tipoIngreso').formSelect();
            });
        });
        u.click('.b-gestInventarios', v => {
            let id = v.attr('target');
            //Detalles
            B.sige.asyn.post({class:'inventarios', method: 'getInventariosById', param: id}, r => {
                $('.metainfo').hide();
                let j = B.sige.und;
                r = Core.json(false, r, false);
                let btn1 = B.sige.creator.atrs('getInventarios');
                let btn2 = Form.btn(false,'green','b-addRemove','Agregar o quitar');
                let codeinv = Form.input('codeinv', 'text', 'Código de inventario', '', '', j, j, r[1]);
                let code = Form.input('code', 'text', 'Código del artículo', '', '', j, j, r[2]);
                let nombre = Form.input('nombre', 'text', 'Nombre del artículo', '', '', j, j, r[3]);
                let familia = Form.input('familia', 'text', 'Familia', '', '', j, j, r[4]);
                let unidad = Form.input('unidad', 'text', 'unidad', '', '', j, j, r[5]);
                let empresa = Form.input('empresa', 'text', 'Empresa', '', '', j, j, r[6]);
                let sucursal = Form.input('sucursal', 'text', 'Sucursal', '', '', j, j, r[7]);
                let mayoreo = Form.input('mayoreo', 'number', 'Mayoreo', '', '', j, j, r[8]);
                let menudeo = Form.input('menudeo', 'number', 'Menudeo', '', '', j, j, r[9]);
                let invact = Form.input('invact', 'number', 'Existencia', '', '', j, j, r[10]);
                $('.cPart').html('<div class="row"><div class="col s12 m9 smnb tg-title">Detalles del producto</div><div class="col s12 m3 smnb">'+btn1+'</div><div class="col s12 m3 input-field">'+codeinv+'</div><div class="col s12 m3 input-field">'+code+'</div><div class="col s12 m6 input-field">'+nombre+'</div><div class="col s12 m3 input-field">'+familia+'</div><div class="col s12 m3 input-field">'+unidad+'</div><div class="col s12 m6 input-field">'+empresa+'</div><div class="col s12 m6 input-field">'+sucursal+'</div><div class="col s12 m2 input-field">'+mayoreo+'</div><div class="col s12 m2 input-field">'+menudeo+'</div><div class="col s12 m2 input-field">'+invact+'</div></div><div class="row"><div class="col s9 tg-title">Historial de movimientos</div><div class="col s3 smnb">'+btn2+'</div><div class="col s12">Filtros</div><div class="col s12 movement"></div></div>');
                $('.codeinv, .code, .nombre, .familia, .unidad, .empresa, .sucursal, .mayoreo, .menudeo, .invact').attr('disabled','disabled').addClass('bolder');
                B.sige.uf();
                B.sige.creator.getInstance('Inventarios').getMovements();
            });
        });
    }
}
class Traspasos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'swap_horiz';
        this.color = 'teal';
        this.descr = 'agregar traspasos, modificar traspasos, eliminar traspasos, crear traspasos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoTraspaso', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s4 m3','detallesTraspaso disabled', 'Detalles'));
        mc.append(B.sige.creator.simbtn('s4 offset-m3 m3','borrarTraspaso disabled', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en traspasos', 'datosTraspasosC'));
        this.get();
    }
    get(){
        $('.b-detallesTraspaso, .b-borrarTraspaso').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Traspasos</a>');
        B.sige.asyn.post({class:'traspasos', method: 'getAllTraspasos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1], e[6]+' '+e[2], e[3], e[4], e[5]]);
            });
            let value = Form.table('datosTraspasos', 'datosTraspasosC cfm','striped',['id', 'Empresa', 'Sucursal', 'Producto', 'Unidades', 'Responsable.'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Traspasos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' traspaso</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveTraspaso', 'Guardar');
        v = (v === j) ? ['', '', '', '', '', '', '', '', ''] : v;
        $('.metainfo').hide();
        let select1 = Form.select('SelectedEmpresaG','Seleccione una empresa origen', B.sige.validator.getForSelect('empresas',0, 1));
        let select2 = Form.select('SelectedSucursalG','Seleccione una sucursal origen', []);
        let familia = Form.select('familiaG','Familia a la que pertenece', B.sige.validator.getForSelect('familias',0, 1));
        let articuloF = Form.select('articuloG','Seleccione Artículo', []);
        let select3 = Form.select('SelectedEmpresaH','Seleccione una empresa destino', B.sige.validator.getForSelect('empresas',0, 1));
        let unidades = Form.input('unidadez', 'number', 'Unidades', '', '', j, 12, v[5]);
        let select4 = Form.select('SelectedSucursalH','Seleccione una sucursal destino', []);
        let observac = Form.area('observac','Observaciones generales', 200, j, v[6]);
        let btn1 = B.sige.creator.atrs('getTraspasos');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 tg-title">Traspaso de origen</div><div class="col s12 m6 input-field">'+select1+'</div><div class="col s12 m6 input-field">'+select2+'</div><div class="col s12 m4 input-field">'+familia+'</div><div class="col s12 m6 input-field">'+articuloF+'</div><div class="col s12 m2 input-field">'+unidades+'</div><div class="col s12 tg-title">Traspaso al destino</div><div class="col s12 m6 input-field">'+select3+'</div><div class="col s12 m6 input-field">'+select4+'</div><div class="col s12 input-field">'+observac+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        $('.unidadez').attr('disabled','disabled');
        B.sige.uf();
		let st = (v[3] !== '') ? v[3] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('SelectedEmpresaG', empresa[0][1]);
		$('select.SelectedEmpresaG').change()
		$('.SelectedEmpresaG').formSelect();
		B.prop('SelectedSucursalG', st);
		$('.SelectedSucursalG').formSelect();
		if (v[3] !== ''){
			$('select.SelectedEmpresaG').attr('disabled', 'disabled');
			$('select.SelectedSucursalG').attr('disabled', 'disabled');
			$('.SelectedEmpresaG').formSelect();
			$('.SelectedSucursalG').formSelect();
		}
        if (v[4] !== ''){
            let empresa = B.sige.validator.searchIn('sucursales', v[4], 0);
            B.prop('SelectedEmpresaH', empresa[0][1]);
            $('select.SelectedEmpresaH').change().attr('disabled', 'disabled');
            $('.SelectedEmpresaH').formSelect();
            B.prop('SelectedSucursalH', v[4]);
            $('select.SelectedSucursalH').change().attr('disabled', 'disabled');
            $('.SelectedSucursalH').formSelect();
        } else
            $('.SelectedEmpresaH, .SelectedSucursalH').formSelect();
        if (v[8] !== ''){
            B.prop('familiaG', v[8]);
            $('select.familiaG').change();
        }
		z.observer('.articuloG option[value="'+v[2]+'"]',function(){
			if (v[2] !== ''){
				B.prop('articuloG', v[2]);
				$('select.articuloG').change();
				$('.articuloG').formSelect();
				$('.unidadez').attr('disabled', 'disabled');
			}
		});
        if (v[1] !== ''){
            $('select.familiaG, select.articuloG').attr('disabled', 'disabled');
            $('.observac').attr('disabled', 'disabled');
            $('.b-saveTraspaso').attr('disabled', 'disabled').html('Solo lectura');
        }
        $('.familiaG').formSelect();
    }
    clicks(){
        u.change('select.familiaG', v => {
            let s = $('select.SelectedSucursalG').val();
            if (s == null || s === undefined || s === ''){
                Core.toast('Seleccione una sucursal primero','yellow black-text rounded');
                return false;
            }
            Access.getArticulosByPrecioId([s, v.val()], () => {
                $('select.articuloG').change();
                $('.articuloG').formSelect();
                $('.unidadez').removeAttr('disabled');
            },'G');
        });
        u.keyup('.unidadez', v => {
            let value = v.val();
            if (value === '')
                return false;
            value = value.toSimpleNumber();
            let disponible = $('.articuloG').parent().children('input').val().split('-')[1].split(' ')[1].toSimpleNumber();
            if (value > disponible){
                Core.toast('No hay suficientes unidades para realizar un traspaso.','red rounded');
                v.val(disponible);
            }
        });
        u.change('select.SelectedEmpresaG', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalG').html(Form.initSelect(filter));
            $('.SelectedSucursalG').formSelect();
            $('select.SelectedSucursalG').change();
        });
        u.change('select.SelectedEmpresaH', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalH').html(Form.initSelect(filter));
            $('.SelectedSucursalH').formSelect();
            $('select.SelectedSucursalH').change();
        });
        u.click('.b-detallesTraspaso', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'traspasos', method: 'getTraspasosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Traspasos').nuovo(r);
            });
        });
        u.change('select.SelectedSucursalG', () => {
            $('select.articuloG').html(Form.initSelect([]));
            $('.articuloG').formSelect();
        });
        u.click('.datosTraspasosC', v => {
            B.sige.validator.vTable(v, 'datosTraspasosC', '.b-detallesTraspaso, .b-borrarTraspaso');
        });
        u.click('.b-getTraspasos', () => {
            B.sige.creator.getInstance('Traspasos').get();
        });
        u.click('.b-nuevoTraspaso', () => {
            B.sige.creator.getInstance('Traspasos').nuovo();
        });
        u.click('.b-saveTraspaso', () => {
            let v = [
                $('select.SelectedSucursalG').val(),
                $('select.articuloG').val(),
                $('.unidadez').val(),
                $('select.SelectedSucursalH').val(),
                $('.observac').val(),
            ];
            if (v[0] === v[3]){
                Core.toast('Elija una sucursal destino diferente al del origen.','red rounded');
                return false;
            }
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'traspasos', method: 'saveTraspaso', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'insufficient':
                        Core.toast('No hay suficiente inventario para completar el traspaso','red rounded');
                    break;
                    case 'noproduct':
                        Core.toast('El artículo no está registrado en la sucursal destino. Primero registre el nuevo artículo en la módulo inventarios.','red rounded');
                    break;
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Traspasos').get();
                    break;
                }
            });
        });
    }
}
class EntradaAlmacen{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'add_circle';
        this.color = 'green';
        this.descr = 'agregar al almacén, modificar al almacén, eliminar al almacén, crear entrada almacén';
        this.idPermiso = permiso;
        this.clicks();
		this.editado = false;
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoEAlmacen', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s4 m3','detallesEAlmacen disabled', 'Detalles'));
		if (B.sige.validator.hasAccessTo('Cancelaciones'))
			mc.append(B.sige.creator.simbtn('s4 offset-m3 m3','borrarEAlmacen disabled', 'Cancelar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en entrada a almacén', 'datosEAlmacensC'));
        this.get();
    }
    get(){
        $('.b-detallesEAlmacen, .b-borrarEAlmacen').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Entrada Almacén</a>');
        B.sige.asyn.post({class:'ealmacen', method: 'getAllEAlmacen'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
				e[4] = e[4].toSimpleNumber().formatMoney();
				e[7] = B.sige.fecha.date(e[7],'-',':');
                vldate.push([e[0], e[1], e[2], e[3], e[8], e[4], e[5], e[6], e[7]]);
            });
            let value = Form.table('datosEAlmacens', 'datosEAlmacensC cfm','striped',['id', 'Proveedor', 'Producto', 'Unidades', 'Lote/Bulto', 'Precio', 'Nota','Responsable', 'Fecha'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Entrada Almacén</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' entrada</a>');
        v = (v === j) ? ['', '', '', '', '', '', '', '', '', '', '', '', ''] : v;
		/*
		0 = id de la entrada almacén
		1 = usuario
		2 = artículo
		3 = sucursal
		4 = unidades
		5 = No. de notas
		6 = Observaciones generales
		7 = fecha
		8 = Familia
		12 = lote o bulto
		**/
        $('.metainfo').hide();
		let proveedor = Form.select('proveedorM','Seleccione un proveedor', B.sige.validator.getForSelect('proveedores',0, 1)); //v[9]
        let select1 = Form.select('SelectedEmpresaM','Seleccione una empresa', B.sige.validator.getForSelect('empresas', 0, 1));
        let select2 = Form.select('SelectedSucursalM','Seleccione una sucursal', []);
        let familia = Form.select('familiaM','Familia a la que pertenece', B.sige.validator.getForSelect('familias', 0, 1));
        let articuloF = Form.select('articuloM','Seleccione Artículo', []);
		let unidades = Form.input('unidadezA', 'number', 'Unidades', '', '', j, 12, v[4]);
		let loteBulto = Form.input('loteBulto', 'text', 'Lote o bulto', '', '', j, 25, v[12]);
		let notaCapturar = Form.input('notaCapturar', 'text', 'No. de nota', '', '', j, 12, v[5]);
		let precioM = Form.input('precioM', 'number', 'Precio', '', '', j, 12, v[9]);
		let formaPagoM = Form.select('formaPagoM ','Forma de pago', [[0, 'Pago en efectivo'], [1, 'Pago con cheque'], [2, 'Pago por transferencia'], [3, 'Pago a tarjeta de crédito'], [4, 'Pago a tarjeta de débito'], [5, 'Pago a través de internet'], [6, 'Depósito a cuenta'], [7, 'Otro']]); //v[10];
		
		let observac = Form.area('observac','Observaciones generales', 200, j, v[6]);
        let btn1 = B.sige.creator.atrs('getEAlmacen');
		let btn2 = Form.btn(false, 'green', 'b-saveEAlmacen', 'Guardar');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+proveedor+'</div><div class="col s12 m6 input-field">'+select1+'</div><div class="col s12 m6 input-field">'+select2+'</div><div class="col s12 m4 input-field">'+familia+'</div><div class="col s12 m6 input-field">'+articuloF+'</div><div class="col s12 m2 input-field">'+unidades+'</div><div class="col s12 m4 input-field">'+loteBulto+'</div><div class="col s12 m4 input-field">'+notaCapturar+'</div><div class="col s12 m4 input-field">'+precioM+'</div><div class="col s12 m4 input-field">'+formaPagoM+'</div><div class="col s8 input-field">'+observac+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        $('.unidadezA').attr('disabled','disabled');
        B.sige.uf();
		let st = (v[3] !== '') ? v[3] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('SelectedEmpresaM', empresa[0][1]);
		$('select.SelectedEmpresaM').change();
		B.prop('SelectedSucursalM', st);
		$('select.SelectedSucursalM').change();
        if (v[3] !== '')
            $('select.SelectedEmpresaM, select.SelectedSucursalM').attr('disabled', 'disabled');
		$('.SelectedEmpresaM, .SelectedSucursalM').formSelect();
		if (v[8] !== '' && v[8] !== '0'){
            B.prop('proveedorM', v[8]);
            $('select.proveedorM').change();
        }
		if (v[10] !== ''){
            B.prop('formaPagoM', v[10]);
            $('select.formaPagoM').change();
        }
        if (v[11] !== ''){
            B.prop('familiaM', v[11]);
            $('select.familiaM').change();
        }
		if (v[2] !== ''){
			z.observer('.articuloM > option[value="'+v[2]+'"]', function(){
				B.prop('articuloM', v[2]);
				$('select.articuloM').change();
				$('.articuloM').formSelect();
			});
		}
        if (v[0] !== ''){
            $('select.familiaM, select.articuloM, select.proveedorM, select.formaPagoM').attr('disabled', 'disabled');
            $('.observac, .unidadezA, .notaCapturar, .precioM').attr('disabled', 'disabled');
            $('.b-saveEAlmacen').attr('disabled', 'disabled').html('Solo lectura');
			B.sige.creator.getInstance('EntradaAlmacen').editado = true;
        }
        $('.familiaM, .articuloM, .proveedorM, .formaPagoM').formSelect();
    }
    clicks(){
        u.change('select.familiaM', v => {
            let s = $('select.SelectedSucursalM').val();
            if (s == null || s === undefined || s === ''){
                Core.toast('Seleccione una sucursal primero','yellow black-text rounded');
                return false;
            }
            Access.getArticulosByPrecioId([s, v.val()], () => {
                $('select.articuloM').change();
                $('.articuloM').formSelect();
				if (!B.sige.creator.getInstance('EntradaAlmacen').editado)
                	$('.unidadezA').removeAttr('disabled');
            },'M');
        });
        u.change('select.SelectedEmpresaM', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalM').html(Form.initSelect(filter));
            $('.SelectedSucursalM').formSelect();
            $('select.SelectedSucursalM').change();
        });
        u.click('.b-detallesEAlmacen', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'ealmacen', method: 'getEAlmacenById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('EntradaAlmacen').nuovo(r);
            });
        });
        u.change('select.SelectedSucursalM', () => {
            $('select.articuloM').html(Form.initSelect([]));
            $('.articuloM').formSelect();
        });
        u.click('.datosEAlmacensC', v => {
            B.sige.validator.vTable(v, 'datosEAlmacensC', '.b-detallesEAlmacen, .b-borrarEAlmacen');
        });
        u.click('.b-getEAlmacen', () => {
            B.sige.creator.getInstance('EntradaAlmacen').get();
        });
        u.click('.b-nuevoEAlmacen', () => {
            B.sige.creator.getInstance('EntradaAlmacen').nuovo();
        });
        u.click('.b-saveEAlmacen', () => {
            let v = [
                $('select.SelectedSucursalM').val(),
                $('select.articuloM').val(),
                $('.unidadezA').val(),
                $('.notaCapturar').val(),
                $('.observac').val(),
				$('select.proveedorM').val(),
				$('.precioM').val(),
				$('select.formaPagoM').val(),
				$('.loteBulto').val()
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'ealmacen', method: 'saveEAlmacen', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('EntradaAlmacen').get();
                    break;
                }
            });
        });
		u.click('.b-borrarEAlmacen', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta entrada? Los productos se descontarán de inventario.','Aceptar',() => {
                B.sige.asyn.post({class:'ealmacen', method: 'delEntrada', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('EntradaAlmacen').get();
                        break;
                    }
                });
            });
        });
    }
}
class SalidaAlmacen{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'do_not_disturb_on';
        this.color = 'red';
        this.descr = 'agregar al almacén, modificar al almacén, eliminar al almacén, crear salida almacén';
        this.idPermiso = permiso;
        this.clicks();
		this.editado = false;
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoSAlmacen', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s4 m3','detallesSAlmacen disabled', 'Detalles'));
        //mc.append(B.sige.creator.simbtn('s4 offset-m3 m3','borrarEAlmacen disabled', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en salida a almacén', 'datosSAlmacensC'));
        this.get();
    }
    get(){
        $('.b-detallesSAlmacen, .b-borrarSAlmacen').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Salida Almacén</a>');
        B.sige.asyn.post({class:'salmacen', method: 'getAllSAlmacen'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
				e[4] = e[4].toSimpleNumber().formatMoney();
				e[7] = B.sige.fecha.date(e[7],'-',':');
                vldate.push([e[0], e[1], e[2], e[3], e[8], e[4], e[5], e[6], e[7]]);
            });
            let value = Form.table('datosSAlmacens', 'datosSAlmacensC cfm','striped',['id', 'Proveedor', 'Producto', 'Unidades', 'Lote/Bulto', 'Precio', 'Nota','Responsable', 'Fecha'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Salida Almacén</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' salida</a>');
        v = (v === j) ? ['', '', '', '', '', '', '', '', ''] : v;
        $('.metainfo').hide();
		let proveedor = Form.select('proveedorN','Seleccione un proveedor', B.sige.validator.getForSelect('proveedores',0, 1)); //v[9]
        let select1 = Form.select('SelectedEmpresaN','Seleccione una empresa', B.sige.validator.getForSelect('empresas', 0, 1));
        let select2 = Form.select('SelectedSucursalN','Seleccione una sucursal', []);
        let familia = Form.select('familiaN','Familia a la que pertenece', B.sige.validator.getForSelect('familias', 0, 1));
        let articuloF = Form.select('articuloN','Seleccione Artículo', []);
		let unidades = Form.input('unidadezB', 'number', 'Unidades', '', '', j, 12, v[4]);
		let notaCapturar = Form.input('notaCapturar', 'text', 'No. de nota', '', '', j, 12, v[5]);
		let observac = Form.area('observac','Observaciones generales', 200, j, v[6]);
		let loteBulto = Form.input('loteBulto', 'text', 'Lote o bulto', '', '', j, 25, v[11]);
		let precioM = Form.input('precioM', 'number', 'Precio', '', '', j, 12, v[9]);
		let formaPagoM = Form.select('formaPagoM ','Forma de pago', [[0, 'Pago en efectivo'], [1, 'Pago con cheque'], [2, 'Pago por transferencia'], [3, 'Pago a tarjeta de crédito'], [4, 'Pago a tarjeta de débito'], [5, 'Pago a través de internet'], [6, 'Depósito a cuenta'], [7, 'Otro']]); //v[10];
		
        let btn1 = B.sige.creator.atrs('getSAlmacen');
		let btn2 = Form.btn(false, 'green', 'b-saveSAlmacen', 'Guardar');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+proveedor+'</div><div class="col s12 m6 input-field">'+select1+'</div><div class="col s12 m6 input-field">'+select2+'</div><div class="col s12 m4 input-field">'+familia+'</div><div class="col s12 m6 input-field">'+articuloF+'</div><div class="col s12 m2 input-field">'+unidades+'</div><div class="col s12 m4 input-field">'+loteBulto+'</div><div class="col s12 m4 input-field">'+notaCapturar+'</div><div class="col s12 m4 input-field">'+precioM+'</div><div class="col s12 m4 input-field">'+formaPagoM+'</div><div class="col s12 m8 input-field">'+observac+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        $('.unidadezA').attr('disabled','disabled');
        B.sige.uf();
        let st = (v[3] !== '') ? v[3] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('SelectedEmpresaN', empresa[0][1]);
		$('select.SelectedEmpresaN').change();
		B.prop('SelectedSucursalN', st);
		$('select.SelectedSucursalN').change();
        if (v[3] !== '')
            $('select.SelectedEmpresaN, select.SelectedSucursalN').attr('disabled', 'disabled');
		$('.SelectedEmpresaN, .SelectedSucursalN, .formaPagoM').formSelect();
		if (v[8] !== '' && v[8] !== '0'){
            B.prop('proveedorN', v[8]);
            $('select.proveedorN').change();
        }
        if (v[12] !== ''){
            B.prop('familiaN', v[12]);
            $('select.familiaN').change();
        }
		if (v[10] !== ''){
            B.prop('formaPagoM', v[10]);
            $('select.formaPagoM').change();
        }
        if (v[2] !== ''){
			z.observer('.articuloN > option[value="'+v[2]+'"]', function(){
				B.prop('articuloN', v[2]);
				$('select.articuloN').change();
				$('.articuloN').formSelect();
			});
		}
        if (v[0] !== ''){
            $('select.familiaN, select.articuloN, select.proveedorN, select.formaPagoM').attr('disabled', 'disabled');
            $('.observac, .unidadezB, .notaCapturar, .loteBulto, .precioM').attr('disabled', 'disabled');
            $('.b-saveSAlmacen').attr('disabled', 'disabled').html('Solo lectura');
			B.sige.creator.getInstance('SalidaAlmacen').editado = true;
        }
        $('.familiaN, .articuloN, .proveedorN, .formaPagoM').formSelect();
    }
    clicks(){
        u.change('select.familiaN', v => {
            let s = $('select.SelectedSucursalN').val();
            if (s == null || s === undefined || s === ''){
                Core.toast('Seleccione una sucursal primero','yellow black-text rounded');
                return false;
            }
            Access.getArticulosByPrecioId([s, v.val()], () => {
                $('select.articuloN').change();
                $('.articuloN').formSelect();
				if (!B.sige.creator.getInstance('SalidaAlmacen').editado)
                	$('.unidadezB').removeAttr('disabled');
            },'N');
        });
        u.keyup('.unidadezB', v => {
			let value = v.val();
            if (value === '')
                return false;
            value = value.toSimpleNumber();
            let disponible = $('.articuloN').parent().children('input').val().split('-');
			disponible = disponible[disponible.length - 1].split(' ')[1].toSimpleNumber();
            if (value > disponible){
                Core.toast('No hay suficientes unidades para realizar una salida.','red rounded');
                v.val(disponible);
            }
        });
        u.change('select.SelectedEmpresaN', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalN').html(Form.initSelect(filter));
            $('.SelectedSucursalN').formSelect();
            $('select.SelectedSucursalN').change();
        });
        u.click('.b-detallesSAlmacen', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'salmacen', method: 'getSAlmacenById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('SalidaAlmacen').nuovo(r);
            });
        });
        u.change('select.SelectedSucursalN', () => {
            $('select.articuloN').html(Form.initSelect([]));
            $('.articuloN').formSelect();
        });
        u.click('.datosSAlmacensC', v => {
            B.sige.validator.vTable(v, 'datosSAlmacensC', '.b-detallesSAlmacen, .b-borrarSAlmacen');
        });
        u.click('.b-getSAlmacen', () => {
            B.sige.creator.getInstance('SalidaAlmacen').get();
        });
        u.click('.b-nuevoSAlmacen', () => {
            B.sige.creator.getInstance('SalidaAlmacen').nuovo();
        });
        u.click('.b-saveSAlmacen', () => {
            let v = [
                $('select.SelectedSucursalN').val(),
                $('select.articuloN').val(),
                $('.unidadezB').val(),
                $('.notaCapturar').val(),
                $('.observac').val(),
                $('select.proveedorN').val(),
                $('.precioM').val(),
                $('select.formaPagoM').val(),
				$('.loteBulto').val(),
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'salmacen', method: 'saveSAlmacen', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'insufficient':
                        Core.toast('No hay suficiente inventario para completar la salida','red rounded');
                    break;
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('SalidaAlmacen').get();
                    break;
                }
            });
        });
    }
}
class ActivoFijo{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'account_balance';
        this.color = 'amber';
        this.descr = 'agregar activo, modificar activo, eliminar activo, crear activo';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoActivos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestActivos', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delActivos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en activo fijo', 'datosActivosC'));
        this.get();
    }
    get(){
        $('.b-gestActivos, .b-delActivos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Activos</a>');
        B.sige.asyn.post({class:'activoFijo', method: 'getAllActivos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = e[2].toSimpleNumber().formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosActivos', 'datosActivosC cfm','striped',['id','Nombre', 'Monto','Fecha de compra'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Activos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' activo</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveActivo', 'Guardar');
        v = (v === j) ? ['','','','','',''] : v;
        $('.metainfo').hide();
        let sEmpresaK = Form.select('sEmpresaK','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let sSucursalK = Form.select('sSucursalK','Seleccione una sucursal', []);
        
        let name = Form.input('name', 'text', 'Nombre del activo', '', '', j, 100, v[1]);
        let descr = Form.input('descr', 'text', 'Descripción', '', '', j, 200, v[2]);
        let noserie = Form.input('noserie', 'text', 'No. de serie', '', '', j, 100, v[6]);
        let sueldo = Form.input('sueldo', 'number', 'Precio de compra (únicamente números con o sin decimales)', '', '', j, 12, v[3]);
        let fechaC = Form.input('fechaCompra', 'text', 'Fecha de compra', '', '', j, j, v[4]);
        let btn1 = B.sige.creator.atrs('getActivos');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s8 input-field">'+name+'</div><div class="col s4 input-field">'+noserie+'</div><div class="col s12 input-field">'+descr+'</div><div class="col s12 m6 input-field">'+sueldo+'</div><div class="col s12 m6 input-field">'+fechaC+'</div><div class="col s12 m6 input-field">'+sEmpresaK+'</div><div class="col s12 m6 input-field">'+sSucursalK+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        let st = (v[5] !== '') ? v[5] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('sEmpresaK', empresa[0][1]);
		$('.sEmpresaK').formSelect();
		$('select.sEmpresaK').change();
		B.prop('sSucursalK', st);
		$('.sSucursalK').formSelect();
        B.sige.uf();
        B.sige.fecha.Dtpicker('.fechaCompra');
    }
    clicks(){
        u.change('select.sEmpresaK', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.sSucursalK').html(Form.initSelect(filter));
            $('.sSucursalK').formSelect();
            $('select.sSucursalK').change();
        });
        u.click('.b-nuevoActivos', () => {
            B.sige.creator.getInstance('ActivoFijo').nuovo();
        });
        u.click('.b-getActivos', () => {
            B.sige.creator.getInstance('ActivoFijo').get();
        });
        u.click('.datosActivosC', v => {
            B.sige.validator.vTable(v, 'datosActivosC', '.b-gestActivos, .b-delActivos');
        });
        u.click('.b-gestActivos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'activoFijo', method: 'getActivosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('ActivoFijo').nuovo(r);
            });
        });
        u.click('.b-delActivos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este activo fijo?','Aceptar',() => {
                B.sige.asyn.post({class:'activoFijo', method: 'delActivos', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('ActivoFijo').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveActivo', () => {
            let v = [$('.name').val(),$('.descr').val(), $('.sueldo').val(), $('.fechaCompra').val(), $('select.sSucursalK').val(), $('.noserie').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'activoFijo', method: 'saveActivo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('ActivoFijo').get();
                    break;
                }
            });
        });
    }
}
class Sueldos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'attach_money';
        this.color = 'light-green';
        this.descr = 'agregar cliente, modificar cliente, eliminar cliente, crear cliente';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoSueldos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestSueldos', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delSueldos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en sueldos', 'datosSueldosC'));
        this.get();
    }
    get(){
        $('.b-gestSueldos, .b-delSueldos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Sueldos</a>');
        B.sige.asyn.post({class:'sueldos', method: 'getAllSueldos'}, r => {
            r = Core.json(false, r, false);
            B.data.sueldos = r;
            let vldate = [];
            $.each(r, (i, e) => {
                e[4] = B.sige.fecha.date(e[4],'-',':');
                e[3] = parseFloat(e[3]).formatMoney();
                vldate.push([e[0], e[1], e[2], e[3], e[4]]);
            });
            let value = Form.table('datosSueldos', 'datosSueldosC cfm','striped',['id','Nombre', 'Descripción','Monto','última modificación'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Sueldos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' sueldo</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveSueldo', 'Guardar');
        v = (v === j) ? ['','',''] : v;
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre', '', '', j, 50, v[1]);
        let descr = Form.input('descr', 'text', 'Descripción', '', '', j, 100, v[2]);
        let sueldo = Form.input('sueldo', 'number', 'Monto (únicamente números con o sin decimales)', '', '', j, 15, v[3]);
        let btn1 = B.sige.creator.atrs('getSueldos');
        let ap = Access.personal([v[1][1], v[1][2], v[1][3], v[1][4], v[1][6], v[1][7], v[1][8], v[1][9], v[1][10], v[1][11]]);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s4 input-field">'+name+'</div><div class="col s8 input-field">'+descr+'</div><div class="col s12 input-field">'+sueldo+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        ap[1]();
        B.sige.uf();
    }
    clicks(){
        u.click('.b-nuevoSueldos', () => {
            B.sige.creator.getInstance('Sueldos').nuovo();
        });
        u.click('.b-getSueldos', () => {
            B.sige.creator.getInstance('Sueldos').get();
        });
        u.click('.datosSueldosC', v => {
            B.sige.validator.vTable(v, 'datosSueldosC', '.b-gestSueldos, .b-delSueldos');
        });
        u.click('.b-gestSueldos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'sueldos', method: 'getSueldosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Sueldos').nuovo(r);
            });
        });
        u.click('.b-delSueldos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este sueldo? Los trabajadores que tengan asociado este sueldo serán inhabilitados','Aceptar',() => {
                B.sige.asyn.post({class:'sueldos', method: 'delSueldos', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Sueldos').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveSueldo', () => {
            let v = [$('.name').val(),$('.descr').val(),$('.sueldo').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'sueldos', method: 'saveSueldo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Sueldos').get();
                    break;
                }
            });
        });
    }
}
class Clientes{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'local_library';
        this.color = 'red';
        this.descr = 'agregar cliente, modificar cliente, eliminar cliente, crear cliente';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoClientes', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestClientes', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delClientes', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en clientes', 'datosClientesC'));
        this.get();
    }
    get(){
        $('.b-gestClientes, .b-delClientes').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Clientes</a>');
        B.sige.asyn.post({class:'clientes', method: 'getAllClientes'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1], e[2], e[3], e[4]]);
            });
            let value = Form.table('datosClientes', 'datosClientesC cfm','striped',['id','Nombre', 'correo','telefono','celular'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Clientes</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' cliente</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveCliente', 'Guardar');
        v = (v === j) ? ['',['','','','','',['','','','','','','',''],'','','','','',''],'','','',''] : v;
        $('.metainfo').hide();
        let obv = Form.input('observ', 'text', 'Observaciones', '', '', j, 150, v[2]);
        let btn1 = B.sige.creator.atrs('getClientes');
        let ap = Access.personal([v[1][1], v[1][2], v[1][3], v[1][4], v[1][6], v[1][7], v[1][8], v[1][9], v[1][10], v[1][11]]);
		
		let SelectedEmpresaClientes = Form.select('SelectedEmpresaClientes','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let SelectedSucursalClientes = Form.select('SelectedSucursalClientes','Seleccione primero una empresa', []);
		
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12">'+ap[0]+'</div><div class="col s12">'+Access.direccion([v[1][5][1], v[1][5][2], v[1][5][3], v[1][5][4], v[1][5][5], v[1][5][6], v[1][5][7]])+'</div><div class="col s12 m6 input-field">'+SelectedEmpresaClientes+'</div><div class="col s12 m6 input-field">'+SelectedSucursalClientes+'</div><div class="col s12 input-field">'+obv+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        ap[1]();
		
        if (v[1][2] === '2')
            $('.genero').change();
		let st = (v[5] != '' && v[5] != '0') ? v[5] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('SelectedEmpresaClientes', empresa[0][1]);
		$('.SelectedEmpresaClientes').formSelect();
		$('select.SelectedEmpresaClientes').change();
		B.prop('SelectedSucursalClientes', st);
		$('.SelectedSucursalClientes').formSelect();
        B.sige.uf();
    }
    clicks(){
		u.change('select.SelectedEmpresaClientes', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalClientes').html(Form.initSelect(filter));
            $('.SelectedSucursalClientes').formSelect();
        });
        u.click('.b-nuevoClientes', () => {
            B.sige.creator.getInstance('Clientes').nuovo();
        });
        u.click('.b-getClientes', () => {
            B.sige.creator.getInstance('Clientes').get();
        });
        u.click('.datosClientesC', v => {
            B.sige.validator.vTable(v, 'datosClientesC', '.b-gestClientes, .b-delClientes');
        });
        u.click('.b-gestClientes', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'clientes', method: 'getClientesById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Clientes').nuovo(r);
            });
        });
        u.click('.b-delClientes', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este cliente? Se eliminarán permanentemente todos los registros asociados','Aceptar',() => {
                B.sige.asyn.post({class:'clientes', method: 'delClientes', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Clientes').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveCliente', () => {
            let v = [
                $('.observ').val(),
                $('.nrazon').val(),
                $('select.genero').val(),
                $('.curp').val(),
                $('.rfc').val(),
                $('select.civil').val(),
                $('.correo').val(),
                $('.fechaNac').val(),
                $('.imss').val(),
                $('.licencia').val(),
                $('.direccion').val(),
                $('.ciudad').val(),
                $('.estado').val(),
                $('.cp').val(),
                $('.pais').val(),
                $('.tcasa').val(),
                $('.tcelular').val(),
				$('select.SelectedSucursalClientes').val()
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'clientes', method: 'saveCliente', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Clientes').get();
                    break;
                }
            });
        });
    }
}
class Proveedores{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'assignment_ind';
        this.color = 'blue';
        this.descr = 'agregar proveedores, modificar proveedores, eliminar proveedores, crear proveedores';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoProveedores', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestProveedores', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delProveedores', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en proveedores', 'datosProveedoresC'));
        this.get();
    }
    get(){
        $('.b-gestProveedores, .b-delProveedores').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Proveedores</a>');
        B.sige.asyn.post({class:'proveedores', method: 'getAllProveedores'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1], e[2], e[3], e[4]]);
            });
            let value = Form.table('datosProveedores', 'datosProveedoresC cfm','striped',['id','Nombre', 'correo','telefono','celular'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Proveedores</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' proveedor</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveProveedor', 'Guardar');
        v = (v === j) ? ['',['','','','','',['','','','','','','',''],'','','','','',''],'','','',''] : v;
        $('.metainfo').hide();
        let obv = Form.input('observ', 'text', 'Observaciones', '', '', j, 200, v[2]);
        let btn1 = B.sige.creator.atrs('getProveedores');
        let ap = Access.personal([v[1][1], v[1][2], v[1][3], v[1][4], v[1][6], v[1][7], v[1][8], v[1][9], v[1][10], v[1][11]]);
		let SelectedEmpresaProveedores = Form.select('SelectedEmpresaProveedores','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let SelectedSucursalProveedores = Form.select('SelectedSucursalProveedores','Seleccione primero una empresa', []);
		
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12">'+ap[0]+'</div><div class="col s12">'+Access.direccion([v[1][5][1], v[1][5][2], v[1][5][3], v[1][5][4], v[1][5][5], v[1][5][6], v[1][5][7]])+'</div><div class="col s12 m6 input-field">'+SelectedEmpresaProveedores+'</div><div class="col s12 m6 input-field">'+SelectedSucursalProveedores+'</div><div class="col s12 input-field">'+obv+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        ap[1]();
		if (v[1][2] === '2')
            $('.genero').change();
		
		let st = (v[5] != '' && v[5] != '0') ? v[5] : $.cookie('sucursal');
		let empresa = B.sige.validator.searchIn('sucursales', st, 0);
		B.prop('SelectedEmpresaProveedores', empresa[0][1]);
		$('.SelectedEmpresaProveedores').formSelect();
		$('select.SelectedEmpresaProveedores').change();
		B.prop('SelectedSucursalProveedores', st);
		$('.SelectedSucursalProveedores').formSelect();
		
		
        B.sige.uf();
    }
    clicks(){
		u.change('select.SelectedEmpresaProveedores', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalProveedores').html(Form.initSelect(filter));
            $('.SelectedSucursalProveedores').formSelect();
        });
        u.click('.b-nuevoProveedores', () => {
            B.sige.creator.getInstance('Proveedores').nuovo();
        });
        u.click('.b-getProveedores', () => {
            B.sige.creator.getInstance('Proveedores').get();
        });
        u.click('.datosProveedoresC', v => {
            B.sige.validator.vTable(v, 'datosProveedoresC', '.b-gestProveedores, .b-delProveedores');
        });
        u.click('.b-gestProveedores', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'proveedores', method: 'getProveedoresById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Proveedores').nuovo(r);
            });
        });
        u.click('.b-delProveedores', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este proveedor? Se eliminarán permanentemente todos los registros asociados','Aceptar',() => {
                B.sige.asyn.post({class:'proveedores', method: 'delProveedores', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Proveedores').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveProveedor', () => {
            let v = [
                $('.observ').val(), $('.nrazon').val(), $('select.genero').val(), $('.curp').val(),
                $('.rfc').val(), $('select.civil').val(), $('.correo').val(), $('.fechaNac').val(),
                $('.imss').val(), $('.licencia').val(), $('.direccion').val(), $('.ciudad').val(),
                $('.estado').val(), $('.cp').val(), $('.pais').val(), $('.tcasa').val(), 
                $('.tcelular').val(), $('select.SelectedSucursalProveedores').val()
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'proveedores', method: 'saveProveedor', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Proveedores').get();
                    break;
                }
            });
        });
    }
}
class Articulos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'card_giftcard';
        this.color = 'lime';
        this.descr = 'agregar articulos, modificar articulos, eliminar articulos, crear articulos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoArticulos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestArticulos', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delArticulos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en artículos', 'datosArticulosC'));
        this.get();
    }
    get(){
        $('.b-gestArticulos, .b-delArticulos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Artículos</a>');
        B.sige.asyn.post({class:'articulos', method: 'getAllArticulos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                e[2] = (e[2] === '') ? '<B class="red-text">ASIGNE UNA UNIDAD</B>' : e[2];
                e[3] = (e[3] === '') ? '<B class="red-text">ASIGNE UNA FAMILIA</B>' : e[3];
                vldate.push([e[0], e[4], e[1], e[2], e[3]]);
            });
            let value = Form.table('datosArticulos', 'datosArticulosC cfm','striped',['id', 'Código', 'Nombre', 'Unidad','Familia'], vldate);
            $('.cPart').html(value);
        });
    }
    nuovo(v = ['','','','','','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Artículos</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' artículo</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveArticulo', 'Guardar');
        let btn1 = B.sige.creator.atrs('getArticulos');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre del artículo', '', '', j, 75, v[1]);
        let code = Form.input('code', 'text', 'Código del artículo', '', '', j, 25, v[7]);
        let descr = Form.area('descr','Descripción del artículo', j, 200, v[2]);
        let unidad = Form.select('unidad','Seleccione una unidad', B.sige.validator.getForSelect('unidades',0, 1)); // v[3]
        let familia = Form.select('familia','Familia a la que pertenece', B.sige.validator.getForSelect('familias',0, 1)); //v[4]
        let codigoSAT = Form.input('codigoSAT', 'text', 'Código del SAT', '', '', j, 10, v[5]);
        let unidadSAT = Form.input('unidadSAT', 'text', 'Unidad del SAT', '', '', j, 10, v[6]);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m8 input-field">'+name+'</div><div class="col s12 m4 input-field">'+code+'</div><div class="col s12 m12 input-field">'+descr+'</div><div class="col s12"></div><div class="col s12 m3 input-field">'+unidad+'</div><div class="col s12 m3 input-field">'+familia+'</div><div class="col s12 m3 input-field">'+codigoSAT+'</div><div class="col s12 m3 input-field">'+unidadSAT+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        if (v[3] !== '')
            B.prop('unidad', v[3]);
        if (v[4] !== '')
            B.prop('familia', v[4]);
        $('.unidad, .familia').formSelect();
        B.sige.uf();
    }
    clicks(){
        u.click('.b-nuevoArticulos', () => {
            B.sige.creator.getInstance('Articulos').nuovo();
        });
        u.click('.b-getArticulos', () => {
            B.sige.creator.getInstance('Articulos').get();
        });
        u.click('.datosArticulosC', v => {
            B.sige.validator.vTable(v, 'datosArticulosC', '.b-gestArticulos, .b-delArticulos');
        });
        u.click('.b-gestArticulos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'articulos', method: 'getArticulosById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Articulos').nuovo(r);
            });
        });
        u.click('.b-delArticulos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este artículo? Se eliminarán permanentemente todos los registros asociados','Aceptar',() => {
                B.sige.asyn.post({class:'articulos', method: 'delArticulos', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Articulos').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveArticulo', () => {
            let v = [$('.name').val(),$('.descr').val(),$('select.unidad').val(),$('select.familia').val(),$('.codigoSAT').val(),$('.unidadSAT').val(),$('.code').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'articulos', method: 'saveArticulo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':{
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Articulos').get();
						$('.b-return').click();
						$('.openCard[target="Precios"]').click();
						$('.b-nuevoPrecios').click();
						let ss = setInterval(function(){
							if ($('.articulo').length == 1){
								$('.articulo').val(v[6]);
								$('.articulo').keyup();
								$('.autocomplete-content > li:first-child').click();
								B.sige.uf();
								clearInterval(ss);
							}
						}, 200);
						break;
					}
                }
            });
        });
    }
}
class Familias{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'face';
        this.color = 'cyan';
        this.descr = 'agregar familia, modificar familia, eliminar familia, crear familia';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoFamilias', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestFamilias', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delFamilias', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en familias', 'datosFamiliasC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Familias</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' familia</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveFamilia', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre de la familia', '', '', j, 50, v[0]);
        let descr = Form.area('businessDescr','Descripción de la familia', j, 200, v[1]);
        let btn1 = B.sige.creator.atrs('getFamilias');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');

        B.sige.uf();
    }
    get(){
        $('.b-gestFamilias, .b-delFamilias').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Familias</a>');
        B.sige.asyn.post({class:'familias', method: 'getAllFamilias'}, r => {
            r = Core.json(false, r, false);
            B.data.familias = r;
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1]]);
            });
            let value = Form.table('datosFamilias', 'datosFamiliasC cfm','striped',['id','Nombre'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
		u.change('select.SelectedEmpresaFamilias', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalFamilias').html(Form.initSelect(filter));
            $('.SelectedSucursalFamilias').formSelect();
        });
        u.click('.b-getFamilias', () => {
            B.sige.creator.getInstance('Familias').get();
        });
        u.click('.datosFamiliasC', v => {
            B.sige.validator.vTable(v, 'datosFamiliasC', '.b-gestFamilias, .b-delFamilias');
        });
        u.click('.b-nuevoFamilias', () => {
            B.sige.creator.getInstance('Familias').nuovo();
        });
        u.click('.b-gestFamilias', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'familias', method: 'getFamiliaById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Familias').nuovo([r[1], r[2], r[5]]);
            });
        });
        u.click('.b-saveFamilia', () => {
            let v = [$('.name').val(), $('.businessDescr').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'familias', method: 'saveFamilia', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Familias').get();
                    break;
                }
            });
        });
        u.click('.b-delFamilias', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta familia? Los productos asociados a ella se quedarán sin este dato','Aceptar',() => {
                B.sige.asyn.post({class:'familias', method: 'delFamilias', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Familias').get();
                        break;
                    }
                });
            });
        });
    }
}
class Unidades{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Productos';
        this.icon = 'linear_scale';
        this.color = 'deep-purple';
        this.descr = 'agregar unidades, modificar unidades, eliminar unidades, crear unidades';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoUnidades', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestUnidades', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delUnidades', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en unidades', 'datosUnidadesC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Unidades</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' unidad</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveUnidad', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre de la unidad', '', '', j, 50, v[0]);
        let descr = Form.area('businessDescr','Descripción de la unidad', j, 150, v[1]);
        let btn1 = B.sige.creator.atrs('getUnidades');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        B.sige.uf();
    }
    get(){
        $('.b-gestUnidades, .b-delUnidades').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Unidades</a>');
        B.sige.asyn.post({class:'unidades', method: 'getAllUnidades'}, r => {
            r = Core.json(false, r, false);
            B.data.unidades = r;
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1]]);
            });
            let value = Form.table('datosUnidades', 'datosUnidadesC cfm','striped',['id','Nombre'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getUnidades', () => {
            B.sige.creator.getInstance('Unidades').get();
        });
        u.click('.datosUnidadesC', v => {
            B.sige.validator.vTable(v, 'datosUnidadesC', '.b-gestUnidades, .b-delUnidades');
        });
        u.click('.b-nuevoUnidades', () => {
            B.sige.creator.getInstance('Unidades').nuovo();
        });
        u.click('.b-gestUnidades', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'unidades', method: 'getUnidadById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Unidades').nuovo([r[1], r[2]]);
            });
        });
        u.click('.b-saveUnidad', () => {
            let v = [$('.name').val(), $('.businessDescr').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'unidades', method: 'saveUnidad', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Unidades').get();
                    break;
                }
            });
        });
        u.click('.b-delUnidades', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta unidad? Los productos asociados a ella se quedarán sin este dato','Aceptar',() => {
                B.sige.asyn.post({class:'unidades', method: 'delUnidades', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Unidades').get();
                        break;
                    }
                });
            });
        });
    }
}
class Catalogos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'collections_bookmark';
        this.color = 'teal';
        this.descr = 'agregar catálogos, modificar catálogos, eliminar catálogos, crear catálogos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoCatalogos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestCatalogos', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delCatalogos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en catalogos', 'datosCatalogosC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Catálogos</a><a class="breadcrumb">'+(v[0] == '' ? 'Nuevo' : 'Editar')+' catálogo</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveCatalogo', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre del catálogo', '', '', j, 50, v[0]);
        let descr = Form.area('businessDescr','Descripción del catálogo', j, 150, v[1]);
        let btn1 = B.sige.creator.atrs('getCatalogos');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        B.sige.uf();
    }
    get(){
        $('.b-gestCatalogos, .b-delCatalogos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Catálogos</a>');
        B.sige.asyn.post({class:'catalogos', method: 'getAllCatalogos'}, r => {
            r = Core.json(false, r, false);
            B.data.catalogos = r;
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1]]);
            });
            let value = Form.table('datosCatalogos', 'datosCatalogosC cfm','striped',['id','Nombre'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getCatalogos', () => {
            B.sige.creator.getInstance('Catalogos').get();
        });
        u.click('.datosCatalogosC', v => {
            B.sige.validator.vTable(v, 'datosCatalogosC', '.b-gestCatalogos, .b-delCatalogos');
        });
        u.click('.b-nuevoCatalogos', () => {
            B.sige.creator.getInstance('Catalogos').nuovo();
        });
        u.click('.b-gestCatalogos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'catalogos', method: 'getUnidadById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Catalogos').nuovo([r[1], r[2]]);
            });
        });
        u.click('.b-saveCatalogo', () => {
            let v = [$('.name').val(), $('.businessDescr').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'catalogos', method: 'saveCatalogo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Catalogos').get();
                    break;
                }
            });
        });
        u.click('.b-delCatalogos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta unidad? Los productos asociados a ella se quedarán sin este dato','Aceptar',() => {
                B.sige.asyn.post({class:'catalogos', method: 'delCatalogos', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Catalogos').get();
                        break;
                    }
                });
            });
        });
    }
}
class Percepciones{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Nómina';
        this.icon = 'keyboard_arrow_right';
        this.color = 'green';
        this.descr = 'agregar percepciones, modificar percepciones, eliminar percepciones, crear percepciones';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoPercepciones', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestPercepciones', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delPercepciones', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en percepciones', 'datosPercepcionesC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Percepciones</a><a class="breadcrumb">'+(v[0] == '' ? 'Nuevo' : 'Editar')+' percepción</a>');
        let btn2 = Form.btn(false, 'green', 'b-savePercepcion', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre de la percepción', '', '', j, 50, v[0]);
        let descr = Form.input('businessDescr', 'number', 'Monto de la percepción', '', '', j, 50, v[1]);
        let btn1 = B.sige.creator.atrs('getPercepciones');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        B.sige.uf();
    }
    get(){
        $('.b-gestPercepciones, .b-delPercepciones').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Percepciones</a>');
        B.sige.asyn.post({class:'percepciones', method: 'getAllPercepciones'}, r => {
            r = Core.json(false, r, false);
            B.data.percepciones = r;
            let vldate = [];
            $.each(r, (i, e) => {
				e[2] = e[2].toSimpleNumber().formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosPercepciones', 'datosPercepcionesC cfm','striped',['id','Nombre', 'Monto de la percepción'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getPercepciones', () => {
            B.sige.creator.getInstance('Percepciones').get();
        });
        u.click('.datosPercepcionesC', v => {
            B.sige.validator.vTable(v, 'datosPercepcionesC', '.b-gestPercepciones, .b-delPercepciones');
        });
        u.click('.b-nuevoPercepciones', () => {
            B.sige.creator.getInstance('Percepciones').nuovo();
        });
        u.click('.b-gestPercepciones', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'percepciones', method: 'getPercepcionById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Percepciones').nuovo([r[1], r[2]]);
            });
        });
        u.click('.b-savePercepcion', () => {
            let v = [$('.name').val(), $('.businessDescr').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'percepciones', method: 'savePercepcion', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Percepciones').get();
                    break;
                }
            });
        });
        u.click('.b-delPercepciones', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta percepción?','Aceptar',() => {
                B.sige.asyn.post({class:'percepciones', method: 'delPercepciones', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Percepciones').get();
                        break;
                    }
                });
            });
        });
    }
}
class Deducciones{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Nómina';
        this.icon = 'keyboard_arrow_left';
        this.color = 'red';
        this.descr = 'agregar deducciones, modificar deducciones, eliminar deducciones, crear deducciones';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoDeducciones', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestDeducciones', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delDeducciones', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en deducciones', 'datosDeduccionesC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Deducciones</a><a class="breadcrumb">'+(v[0] == '' ? 'Nuevo' : 'Editar')+' deducción</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveDeduccion', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre de la deducción', '', '', j, 50, v[0]);
        let descr = Form.input('businessDescr', 'number', 'Monto de la deducción', '', '', j, 50, v[1]);
        let btn1 = B.sige.creator.atrs('getDeducciones');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        B.sige.uf();
    }
    get(){
        $('.b-gestDeducciones, .b-delDeducciones').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Deducciones</a>');
        B.sige.asyn.post({class:'deducciones', method: 'getAllDeducciones'}, r => {
            r = Core.json(false, r, false);
            B.data.deducciones = r;
            let vldate = [];
            $.each(r, (i, e) => {
				e[2] = e[2].toSimpleNumber().formatMoney();
                vldate.push(e);
            });
            let value = Form.table('datosDeducciones', 'datosDeduccionesC cfm','striped',['id','Nombre', 'Monto de la deducción'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getDeducciones', () => {
            B.sige.creator.getInstance('Deducciones').get();
        });
        u.click('.datosDeduccionesC', v => {
            B.sige.validator.vTable(v, 'datosDeduccionesC', '.b-gestDeducciones, .b-delDeducciones');
        });
        u.click('.b-nuevoDeducciones', () => {
            B.sige.creator.getInstance('Deducciones').nuovo();
        });
        u.click('.b-gestDeducciones', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'deducciones', method: 'getDeduccionById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Deducciones').nuovo([r[1], r[2]]);
            });
        });
        u.click('.b-saveDeduccion', () => {
            let v = [$('.name').val(), $('.businessDescr').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'deducciones', method: 'saveDeduccion', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Deducciones').get();
                    break;
                }
            });
        });
        u.click('.b-delDeducciones', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar esta deducción?','Aceptar',() => {
                B.sige.asyn.post({class:'deducciones', method: 'delDeducciones', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Deducciones').get();
                        break;
                    }
                });
            });
        });
    }
}
class Estajos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'layers';
        this.color = 'blue';
        this.descr = 'agregar estajos, modificar estajos, eliminar estajos, crear estajos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s6 m3','nuevoEstajos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s6 offset-m6 m3','delEstajos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en estajos', 'datosEstajosC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Estajos</a><a class="breadcrumb">'+(v[0] == '' ? 'Nuevo' : 'Editar')+' estajo</a>');
        $('.metainfo').hide();
		B.sige.asyn.post({class:'trabajadores', method: 'getTrabajadorByEstajos'}, r => {
			let btn2 = Form.btn(false, 'green', 'b-saveEstajo', 'Guardar');
			let btn3 = Form.btn(false, 'green', 'b-byTodos', 'Todos');
			r = Core.json(false, r, false);
			let fechaEstajo = Form.input('fechaEstajo', 'text', 'Fecha del estajo', '', '', j, 50, v[1]);
			let unitEstajo = Form.input('unitEstajo', 'number', 'No. de unidades', '', '', j, 50, v[2]);
			let SelectedEmpleadoE = Form.input('SelectedEmpleadoE', 'text', 'Busque un empleado de estajo y presione enter', '', ''); //v[3]
			let observ = Form.area('observ','Observaciones finales.', 150, '', v[4]); //v[4]
			let btn1 = B.sige.creator.atrs('getEstajos');
			$('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m6 input-field">'+fechaEstajo+'</div><div class="col s12 m6 input-field">'+unitEstajo+'</div><div class="col s12 m2 smnb">'+btn3+'</div><div class="col s12 m10 input-field">'+SelectedEmpleadoE+'</div><div class="col s12 input-field">'+observ+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
			B.sige.fecha.Dtpicker('.fechaEstajo');
			Form.autocomplete('.SelectedEmpleadoE', r);
			if (v[0] !== ''){
				let trabajo = B.sige.validator.searchSensitive('', v[4], 0, r);
				$('.SelectedEmpleadoE').val(trabajo[0][1]);
			}
			B.sige.uf();
		});
    }
    get(){
        $('.b-gestEstajos, .b-delEstajos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Estajos</a>');
        B.sige.asyn.post({class:'estajos', method: 'getAllEstajos'}, r => {
            r = Core.json(false, r, false);
            B.data.estajos = r;
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push(e);
            });
            let value = Form.table('datosEstajos', 'datosEstajosC cfm','striped',['id', 'Fecha', 'Empleado', 'Unidades de estajo'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getEstajos', () => {
            B.sige.creator.getInstance('Estajos').get();
        });
        u.click('.datosEstajosC', v => {
            B.sige.validator.vTable(v, 'datosEstajosC', '.b-gestEstajos, .b-delEstajos');
        });
        u.click('.b-nuevoEstajos', () => {
            B.sige.creator.getInstance('Estajos').nuovo();
        });
        u.click('.b-gestEstajos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'estajos', method: 'getEstajoById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Estajos').nuovo([r[1], r[2]]);
            });
        });
        u.click('.b-saveEstajo', () => {
            let v = [$('.fechaEstajo').val(), $('.unitEstajo').val(), $('.SelectedEmpleadoE').data('id'), $('.observ').val()];
			if (v[2] == undefined)
				v[2] = $('.SelectedEmpleadoE').val();
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'estajos', method: 'saveEstajo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Estajos').get();
                    break;
                }
            });
        });
        u.click('.b-delEstajos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este estajo?','Aceptar',() => {
                B.sige.asyn.post({class:'estajos', method: 'delEstajos', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Estajos').get();
                        break;
                    }
                });
            });
        });
		    u.click('.b-byTodos', () => {
            B.sige.ui.question('Confirmar acción','¿Está seguro de marcar el mismo estajo a todos sus trabajadores?<br><br>Con ello, <b>todos los trabajadores de estajo que pertenezcan a esta sucursal</b> recibirán la misma cantidad de unidades de estajo para la misma fecha y con las mismas observaciones. Sin embargo, podrá eliminar estajos individualmente en la tabla inicial de estajos.','Confirmar',() => {
                $('.SelectedEmpleadoE').val('TODOS_ESTAJO');
				$('.SelectedEmpleadoE').removeData();
				B.sige.uf();
            });
        });
    }
}
class SubCatalogo{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Económico';
        this.icon = 'collections_bookmark';
        this.color = 'amber';
        this.descr = 'agregar Subcatálogos, modificar Subcatálogos, eliminar Subcatálogos, crear Subcatálogos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoSubCatalogo', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestSubCatalogo', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delSubCatalogo', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en subcatalogos', 'datosSubCatalogosC'));
        this.get();
    }
    nuovo(v = ['','','','', '', '', '', '', '']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Subcatálogos</a><a class="breadcrumb">'+(v[0] == '' ? 'Nuevo' : 'Editar')+' subcatálogo</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveSubcatalogo', 'Guardar');
        $('.metainfo').hide();
        let name = Form.input('name', 'text', 'Nombre del subcatálogo', '', '', j, 200, v[0]);
		let descr = Form.area('businessDescr','Descripción del subcatálogo', 200, j, v[1]);
        let btn1 = B.sige.creator.atrs('getSubCatalogo');
        let select = Form.input('SelectedCatalogos', 'text', 'Busque un catálogo', '', '', j, j);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m4 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 input-field">'+select+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        Form.autocomplete('.SelectedCatalogos', B.sige.validator.getForSelect('catalogos', 0, 1), function(e){
            if (e.id === v[2])
                $('.SelectedCatalogos').val(e.text.substring(0, e.text.length-1)).keyup().enter();
        });
        B.sige.uf();
    }
    get(){
        $('.b-gestSubCatalogo, .b-delSubCatalogo').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Subcatálogos</a>');
        B.sige.asyn.post({class:'subcatalogos', method: 'getAllSubcatalogos'}, r => {
            r = Core.json(false, r, false);
            B.data.subcatalogos = r;
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[2], e[3]]);
            });
            let value = Form.table('datosSucursal', 'datosSubCatalogosC cfm','striped',['id','Catálogo', 'Subcatálogo'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getSubCatalogo', () => {
            B.sige.creator.getInstance('SubCatalogo').get();
        });
        u.click('.b-nuevoSubCatalogo', () => {
            B.sige.creator.getInstance('SubCatalogo').nuovo();
        });
        u.click('.datosSubCatalogosC', v => {
            B.sige.validator.vTable(v, 'datosSubCatalogosC', '.b-gestSubCatalogo, .b-delSubCatalogo');
        });
        u.click('.b-delSubCatalogo', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar a esta sucursal? Se eliminarán los departamentos y demás elementos asociados. Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'subcatalogos', method: 'delSucursal', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('SubCatalogo').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-gestSubCatalogo', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'subcatalogos', method: 'getCatalogoById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('SubCatalogo').nuovo([r[2], r[3], r[1][0]]);
            });
        });
        u.click('.b-saveSubcatalogo', () => {
            let v = [$('.name').val(), $('.businessDescr').val(), $('.SelectedCatalogos').data('id')];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'subcatalogos', method: 'saveSubcatalogo', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('SubCatalogo').get();
                    break;
                }
            });
        });
    }
}
class Empresas{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'business';
        this.color = 'red';
        this.descr = 'agregar empresas, modificar empresas, eliminar empresas, crear empresas';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoEmpresa', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestEmpresa', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delEmpresa', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en empresas', 'datosEmpresaC'));
        this.get();
    }
    clicks(){
        u.click('.b-nuevoEmpresa', () => {
            B.sige.creator.getInstance('Empresas').nuovo();
        });
        u.click('.b-getEmpresas', () => {
            B.sige.creator.getInstance('Empresas').get();
        });
        u.click('.datosEmpresaC', v => {
            B.sige.validator.vTable(v, 'datosEmpresaC', '.b-gestEmpresa, .b-delEmpresa');
        });
        u.click('.b-delEmpresa', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar a esta empresa? Se eliminarán las sucursales, departamentos y demás elementos asociados. Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'empresas', method: 'deleteEmpresa', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
							Core.toast('Empresa eliminada con exito','green');
							Core.toast('Su sesión ha caducado','blue');
                            B.sige.login.logout();
                        break;
                    }
                });
            });
        });
        u.click('.b-gestEmpresa', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'empresas', method: 'getEmpresaById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Empresas').nuovo([r[1], r[2], r[4], r[3][1], r[3][2], r[3][3], r[3][4], r[3][5], r[3][6], r[3][7]]);
            });
        });
        u.click('.b-saveEmpresa', () => {
            let v = [
                $('.name').val(), $('.businessDescr').val(), $('.mail').val(), $('.direccion').val(),$('.ciudad').val(),
                $('.estado').val(),$('.cp').val(),$('.pais').val(),$('.tcasa').val(),$('.tcelular').val()
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'empresas', method: 'saveEmpresa', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        location.reload();
                    break;
                }
            });
        });
    }
    nuovo(v = ['','','','', '', '', '', '', '', '']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Empresas</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' empresa</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveEmpresa', 'Guardar');
        $('.metainfo').hide();
        let descr = Form.area('businessDescr','Descripción de la empresa', 200, j, v[1]);
        let name = Form.input('name', 'text', 'Nombre de la empresa', '', '', j, 200, v[0]);
        let mail = Form.input('mail', 'email', 'Correo de la empresa', '', '', j, 200, v[2]);
        let btn1 = B.sige.creator.atrs('getEmpresas');
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 m8 input-field">'+name+'</div><div class="col s12 m4 input-field">'+mail+'</div><div class="col s12 m12 input-field">'+descr+'</div><div class="col s12 m12">'+Access.direccion([v[3], v[4], v[5], v[6], v[7], v[8], v[9]])+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        B.sige.uf();
    }
    get(fn){
        $('.b-gestEmpresa, .b-delEmpresa').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Empresas</a>');
        B.sige.asyn.post({class:'empresas', method: 'getAllEmpresas'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[1], e[4]]);
            });
            let value = Form.table('datosEmpresa', 'datosEmpresaC cfm','striped',['id','Nombre', 'correo'], vldate);
            $('.cPart').html(value);
            if (typeof fn === "function")
                fn();
        });
    }
}
class Sucursales{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'location_city';
        this.color = 'amber';
        this.descr = 'agregar sucursales, modificar sucursales, eliminar sucursales, crear sucursales';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoSucursales', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestSucursales', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delSucursales', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en sucursales', 'datosSucursalC'));
        this.get();
    }
    nuovo(v = ['','','','', '', '', '', '', '']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Sucurales</a><a class="breadcrumb">'+(v === j ? 'Nueva' : 'Editar')+' sucursal</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveSucursal', 'Guardar');
        $('.metainfo').hide();
        let descr = Form.area('businessDescr','Descripción de la sucursal', 200, j, v[0]);
        let name = Form.input('name', 'text', 'Nombre de la sucursal', '', '', j, 200, v[1]);
        let btn1 = B.sige.creator.atrs('getSucursales');
        let select = Form.input('SelectedEmpresa', 'text', 'Busque una empresa', '', '', j, j);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+name+'</div><div class="col s12 m8 input-field">'+descr+'</div><div class="col s12 m4 input-field">'+select+'</div><div class="col s12 m12">'+Access.direccion([v[3], v[4], v[5], v[6], v[7], v[8], v[9]])+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        Form.autocomplete('.SelectedEmpresa', B.sige.validator.getForSelect('empresas', 0, 1), function(e){
            if (e.id === v[2])
                $('.SelectedEmpresa').val(e.text.substring(0, e.text.length-1)).keyup().enter();
        });
        B.sige.uf();
    }
    get(){
        $('.b-gestSucursales, .b-delSucursales').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Sucursales</a>');
        B.sige.asyn.post({class:'sucursales', method: 'getAllSucursales'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push([e[0], e[2]]);
            });
            let value = Form.table('datosSucursal', 'datosSucursalC cfm','striped',['id', 'Nombre de la sucursal'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.click('.b-getSucursales', () => {
            B.sige.creator.getInstance('Sucursales').get();
        });
        u.click('.b-nuevoSucursales', () => {
            B.sige.creator.getInstance('Sucursales').nuovo();
        });
        u.click('.datosSucursalC', v => {
            B.sige.validator.vTable(v, 'datosSucursalC', '.b-gestSucursales, .b-delSucursales');
        });
        u.click('.b-delSucursales', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar a esta sucursal? Se eliminarán los departamentos y demás elementos asociados. Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'sucursales', method: 'delSucursal', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Sucursales').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-gestSucursales', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'sucursales', method: 'getSucursalById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Sucursales').nuovo([r[4], r[3], r[1][0], r[2][1], r[2][2], r[2][3], r[2][4], r[2][5], r[2][6], r[2][7]]);
            });
        });
        u.click('.b-saveSucursal', () => {
            let v = [
                $('.name').val(), $('.businessDescr').val(), $('.SelectedEmpresa').data('id'), $('.direccion').val(),$('.ciudad').val(),
                $('.estado').val(),$('.cp').val(),$('.pais').val(),$('.tcasa').val(),$('.tcelular').val()
            ];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'sucursales', method: 'saveSucursal', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        location.reload();
                    break;
                }
            });
        });
    }
}
class Departamentos{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Empresa';
        this.icon = 'store';
        this.color = 'indigo';
        this.descr = 'agregar departamentos, modificar departamentos, eliminar departamentos, crear departamentos';
        this.idPermiso = permiso;
        this.clicks();
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s4 m3','nuevoDepartamentos', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s8 m3','gestDepartamentos', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s12 offset-m3 m3','delDepartamentos', 'Borrar', 'red'));
        mc.append(B.sige.creator.simpliest('Buscar en departamentos', 'datosDepartamentoC'));
        this.get();
    }
    nuovo(v = ['','','']){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Departamentos</a><a class="breadcrumb">'+(v[0] === '' ? 'Nuevo' : 'Editar')+' departamento</a>');
        let btn2 = Form.btn(false, 'green', 'b-saveDepartamento', 'Guardar');
        $('.metainfo').hide();
        let descr = Form.area('businessDescr','Descripción del departamento', 200, j, v[1]);
        let name = Form.input('name', 'text', 'Nombre del departamento', '', '', j, 200, v[0]);
        let btn1 = B.sige.creator.atrs('getDepartamentos');
        let select1 = Form.select('SelectedEmpresa','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let select2 = Form.select('SelectedSucursal','Seleccione una sucursal', []);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12 input-field">'+name+'</div><div class="col s12 m4 input-field">'+descr+'</div><div class="col s12 m4 input-field">'+select1+'</div><div class="col s12 m4 input-field">'+select2+'</div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        /*
		Form.autocomplete('.SelectedEmpresa', B.sige.validator.getForSelect('empresas', 0, 1), function(e){
            if (e.id === v[2])
                $('.SelectedEmpresa').val(e.text.substring(0, e.text.length-1)).keyup().enter();
        });
        */
        
        if (v[2] !== ''){
            let empresa = B.sige.validator.searchIn('sucursales', v[2], 0);
            B.prop('SelectedEmpresa', empresa[0][1]);
            $('.SelectedEmpresa').formSelect();
            $('select.SelectedEmpresa').change();
            B.prop('SelectedSucursal', v[2]);
            $('.SelectedSucursal').formSelect();
        } else{
            let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
            B.prop('SelectedEmpresa', empresa[0][1]);
            $('.SelectedEmpresa').formSelect();
            $('select.SelectedEmpresa').change();
            B.prop('SelectedSucursal', $.cookie('sucursal'));
            $('.SelectedSucursal').formSelect();
		}
        B.sige.uf();
    }
    get(){
        $('.b-gestDepartamentos, .b-delDepartamentos').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Departamentos</a>');
        B.sige.asyn.post({class:'departamentos', method: 'getAllDepartamentos'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push(e);
            });
            let value = Form.table('datosDepartamento', 'datosDepartamentoC cfm','striped',['id','Nombre del departamento'], vldate);
            $('.cPart').html(value);
        });
    }
    clicks(){
        u.change('select.SelectedEmpresa', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursal').html(Form.initSelect(filter));
            $('.SelectedSucursal').formSelect();
        });
        u.click('.b-getDepartamentos', () => {
            B.sige.creator.getInstance('Departamentos').get();
        });
        u.click('.datosDepartamentoC', v => {
            B.sige.validator.vTable(v, 'datosDepartamentoC', '.b-gestDepartamentos, .b-delDepartamentos');
        });
        u.click('.b-nuevoDepartamentos', () => {
            B.sige.creator.getInstance('Departamentos').nuovo();
        });
        u.click('.b-gestDepartamentos', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'departamentos', method: 'getDepartamentoById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Departamentos').nuovo([r[2], r[3], r[1]]);
            });
        });
        u.click('.b-delDepartamentos', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar a este departamento ? Se eliminarán los elementos asociados. Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'departamentos', method: 'delDepartamento', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Departamentos').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveDepartamento', () => {
            let v = [$('.name').val(), $('.businessDescr').val(), $('select.SelectedSucursal').val()];
            let flag = B.sige.validator.vForm(v);
            if (!flag)
                return false;
            B.sige.asyn.post({class:'departamentos', method: 'saveDepartamento', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        location.reload();
                    break;
                }
            });
        });
    }
}
class Trabajadores{
    constructor(permiso, title){
        this.title = title;
        this.seccion = 'Nómina';
        this.icon = 'people';
        this.color = 'pink';
        this.descr = 'agregar trabajadores, modificar trabajadores, eliminar trabajadores, crear trabajadores';
        this.idPermiso = permiso;
        this.clicks();
        this.editing = '';
    }
    build(){
        B.sige.creator.breturn();
		let mc = $('.m-content');
        mc.append(B.sige.creator.simbtn('s6 m3','nuevoTrabajadores', 'Nuevo'));
        mc.append(B.sige.creator.simbtn('s6 m3','gestTrabajadores', 'Gestionar'));
        mc.append(B.sige.creator.simbtn('s6 m3','gestPermission', 'Acceso a empresas'));
        mc.append(B.sige.creator.simbtn('s6 m3','delTrabajadores', 'Borrar', 'red'));
        /*mc.append(B.sige.creator.simbtn('s6 m3','horarioTrabajadores', 'horarios'));*/
        mc.append(B.sige.creator.simpliest('Buscar en trabajadores', 'datosTrabajadoresC'));
        this.get();
    }
    nuovo(v){
        let j = B.sige.und;
        $('.pos-actual').html('<a class="breadcrumb">Trabajadores</a><a class="breadcrumb">'+(v === j ? 'Nuevo' : 'Editar')+' trabajador</a>');
        let btn1 = B.sige.creator.atrs('getTrabajadores');
        let btn2 = Form.btn(false, 'green', 'b-saveTrabajador', 'Guardar');
        v = (v === j) ? ["",["","","","","",["","","","","","","",""],"","","","","",""],"","","","","","","","","","",[],"",""] : v;
        $('.metainfo').hide();
        let select1 = Form.select('SelectedEmpresaB','Seleccione una empresa', B.sige.validator.getForSelect('empresas',0, 1));
        let select2 = Form.select('SelectedSucursalB','Seleccione una sucursal', []);
        let select3 = Form.select('SelectedDepartamentoB','Seleccione un departamento', []); //v[2]
        let puesto = Form.input('puesto', 'text', 'Puesto de trabajo', '', '', j, j, v[3]);
        let select4 = Form.select('jornada','Seleccione una jornada', [[0, 'Estajo (semanal)'], [1, 'Semanal'], [2, 'Quincenal'], [3, 'No aplica jornada']]); //v[4]
        let select5 = Form.select('auth','Autenticación de dos pasos', [['no', 'Si (próximamente)'], ['no', 'No']]); //v['5']
        let select6 = Form.select('sueldo','Sueldo', B.sige.validator.getForSelect('sueldos',0, [1, 3]));
        let pass1 = Form.input('password1', 'password', 'Contraseña de acceso', '', '', j, j, '');
        let pass2 = Form.input('password2', 'password', 'Repetir contraseña', '', '', j, j, '');
        let lcredito = Form.input('lcredito', 'text', 'Límite de crédito', '', '', j, j, v[14]);
        let admin = Form.select('admin','¿Permitir cambio de empresa?', [[0, 'No'], [1, 'Si']]);
        let ap = Access.personal([v[1][1], v[1][2], v[1][3], v[1][4], v[1][6], v[1][7], v[1][8], v[1][9], v[1][10], v[1][11]]);
        $('.cPart').html('<div class="col s12 offset-m9 m3 smnb">'+btn1+'</div><div class="col s12">'+ap[0]+'</div><div class="col s12">'+Access.direccion([v[1][5][1], v[1][5][2], v[1][5][3], v[1][5][4], v[1][5][5], v[1][5][6], v[1][5][7]])+'</div><div class="col s12"><div class="tg-title">Datos de acceso y privilegios</div></div><div class="col s12 m6 input-field">'+select1+'</div><div class="col s12 m6 input-field">'+select2+'</div><div class="col s12 m6 input-field">'+select3+'</div><div class="col s12 m6 input-field">'+puesto+'</div><div class="col s12 m6 input-field">'+select4+'</div><div class="col s12 m6 input-field">'+select5+'</div><div class="col s12 m6 input-field">'+select6+'</div><div class="col s12 m6 input-field">'+lcredito+'</div><div class="col s12 m6 input-field">'+pass1+'</div><div class="col s12 m6 input-field">'+pass2+'</div><div class="col s12 m12 input-field preciomTb"></div><div class="col s12 m12 input-field adminTb">'+admin+'</div><div class="col s12 m12 privilegiosTb"></div><div class="col s12 offset-m9 m3 smnb">'+btn2+'</div>');
        $.each(B.data.modulos,(i, e) => {
            let perm = Form.select('permiso '+e,'Seleccione el permiso', [[0, 'No'], [1, 'Si']]);
            $('.privilegiosTb').append('<div class="col s12 lineMnTeal"><div class="col s12 m8">Permiso a: '+e+'</div><div class="col s12 m4">'+perm+'</div></div>');
			if (Ls.get('Usuarios') == '0'){
				B.prop('permiso.'+e, 0);
				$('select.permiso.'+e).attr('disabled', 'disabled');
			}
        });
        ap[1]();
        if (v[2] !== ''){
			if (v[2] != '0'){
				B.sige.creator.getInstance('Trabajadores').editing = true;
				let sucursal = B.sige.validator.searchIn('departamentos', v[2], 0);
				let empresa = B.sige.validator.searchIn('sucursales', sucursal[0][1], 0);
				B.prop('SelectedEmpresaB', empresa[0][1]);
				$('.SelectedEmpresaB').formSelect();
				$('select.SelectedEmpresaB').change();
				B.prop('SelectedSucursalB', sucursal[0][1]);
				$('select.SelectedSucursalB').change();
				$('.SelectedSucursalB').formSelect();
				B.prop('SelectedDepartamentoB', v[2]);
				$('.SelectedDepartamentoB').formSelect();
				$('.correo').attr('disabled','disabled');
			} else {
				$('.SelectedEmpresaB, .SelectedSucursalB, .SelectedDepartamentoB').formSelect();
			}
        } else{
            B.sige.creator.getInstance('Trabajadores').editing = false;
            $('.SelectedEmpresaB, .SelectedSucursalB, .SelectedDepartamentoB').formSelect();
        }
        if (v[4] !== '')
            B.prop('jornada', v[4]);
        if (v[5] !== '')
            B.prop('auth', v[5]);
        if (v[7] !== '')
            B.prop('admin', v[7]);
        if (v[10] !== '')
            B.prop('sueldo', v[10]);
        $.each(v[12], (i, e) => {
            B.prop('permiso.'+e[0], e[1]);
        });
		if (v[4] !== ''){
			$('.preciomTb').html('Esta persona recibirá un sueldo de tipo <b>'+$('select.sueldo option:selected').html()+'</b> con una jornada de tipo <b>'+$('select.jornada option:selected').html()+'</b>');
		}
		if (Ls.get('Usuarios') == '0'){
			B.prop('admin', 0);
			$('select.admin').attr('disabled', 'disabled');
			$('.adminTb, .privilegiosTb').hide();
		}
        $('.jornada, .auth, .permiso, .sueldo, .admin').formSelect();
        B.sige.uf();
    }
    get(){
        $('.b-gestTrabajadores, .b-delTrabajadores, .b-horarioTrabajadores, .b-gestPermission').addClass('disabled');
        $('.metainfo').show();
        $('.pos-actual').html('<a class="breadcrumb">Trabajadores</a>');
        B.sige.asyn.post({class:'trabajadores', method: 'getAllTrabajadores'}, r => {
            r = Core.json(false, r, false);
            let vldate = [];
            $.each(r, (i, e) => {
                vldate.push(e);
            });
            let value = Form.table('datosTrabajadores', 'datosTrabajadoresC cfm','striped',['id', 'Código', 'Nombre', 'Puesto'], vldate);
            $('.cPart').html(value);
        });
    }
    horarios(id){
        let nombre = $('.datosTrabajadoresC[id="'+id+'"] > td:nth-child(3)').html();
        $('.pos-actual').html('<a class="breadcrumb">Trabajadores</a><a class="breadcrumb">Horarios de '+nombre+'</a>');
        $('.metainfo').hide();
        let tarde = Form.select('tarde','Penalización por llegar tarde (por día)', [[0, 'No hacer nada'], [1, 'Dar una tolerancia de 5 minutos. Después, descontar el día'], [2, 'Dar una tolerancia de 10 minutos. Después, descontar el día'], [3, 'Dar una tolerancia de 15 minutos. Después, descontar el día'], [4, 'Cero tolerancia. Descontar el día'], [5, 'Descontar día de manera proporcional al tiempo que llego tarde']]);
        $('.cPart').html('<div class="row col s12"><div class="col s12 m2 smnb">'+Form.btn(false,'blue-grey','b-getTrabajadores','Regresar')+'</div><div class="col s12 m7">'+tarde+'</div><div class="col s12 m3 smnb">'+Form.btn(false,'green','b-addHorario','Añadir horario')+'</div></div><div class="cMart col s12"></div>');
        this.getHorarios(id);
    }
    getHorarios(id){
        B.sige.asyn.post({class:'trabajadores', method: 'getHorariosById', param: id}, r => {
            r = Core.json(false, r, false);
            let vldate = [], bt1, bt2;
            let ds = {1 : 'Lunes', 2 : 'Martes', 3 : 'Miércoles', 4:'Jueves', 5:'Viernes', 6:'Sábado', 7:'Domingo'};
            $.each(r[1], (i, e) => {
                bt1 = Form.btn(false,'green','b-updateHorario','Cambiar','','target="'+e[0]+'"');
                bt2 = Form.btn(false,'red','b-delHorario','Borrar','','target="'+e[0]+'"');
                vldate.push([e[0], ds[e[1]], e[2], e[3], bt1+' '+bt2]);
            });
            let value = Form.table('datosHorarios', 'datosHorariosC','striped',['id', 'Día', 'Hora de entrada', 'Hora de salida', 'Opciones'], vldate);
            $('.cMart').html(value);
            B.prop('tarde', r[0]);
            $('.tarde').formSelect();
        });
    }
    nhorario(v = ['','','']){
        let j = B.sige.und;
        let dia = Form.select('dia','Seleccione un día', [[1, 'lunes'], [2, 'martes'], [3, 'miércoles'], [4, 'jueves'], [5, 'viernes'], [6, 'sábado'], [7, 'domingo']]);
        let entrada = Form.input('entrada', 'text', 'Hora de entrada', '', '', j, j, v[1]); //v[1]
        let salida = Form.input('salida', 'text', 'Hora de salida', '', '', j, j, v[2]); //v[2]
        B.sige.ui.question('Gestión de horario','<div class="row"><div class="col s12 m12">'+dia+'</div><div class="col s12 m6">'+entrada+'</div><div class="col s12 m6">'+salida+'</div><div class="height400"></div></div>','Guardar',() => {
            let v = [$('select.dia').val(), $('.entrada').val(), $('.salida').val()], flag = true;
            $.each(v, (o, u) => {
                if (u === '' || u == null || u === undefined)
                    flag = false;
            });
            if (!flag){
                B.msn('empty');
                return false;
            }
            B.sige.asyn.post({class:'trabajadores', method: 'saveHorario', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Trabajadores').getHorarios(r[1]);
                    break;
                }
            });
        }, () => {
            B.prop('dia',v[0]);
            $('.dia').formSelect();
            $('.entrada, .salida').timepicker({container : 'body', twelveHour:false});
        });
    }
    clicks(){
		u.change('select.sueldo, select.jornada', () => {
			$('.preciomTb').html('Esta persona recibirá un sueldo de tipo <b>'+$('select.sueldo option:selected').html()+'</b> con una jornada de tipo <b>'+$('select.jornada option:selected').html()+'</b>');
		});
        u.change('select.tarde', v => {
            B.sige.asyn.post({class:'trabajadores', method: 'saveConfTarde', param: v.val()}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
                    case 'true':
                        B.msn(r[0]);
                    break;
                }
            });
        });
        u.click('.b-delHorario', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar este horario?','Aceptar',() => {
                B.sige.asyn.post({class:'trabajadores', method: 'delHorario', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Trabajadores').getHorarios(r[1]);
                        break;
                    }
                });
            });
        });
        u.click('.b-updateHorario', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'trabajadores', method: 'getUniqueHorarioById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Trabajadores').nhorario([r[1], r[2], r[3]]);
            });
        });
        u.click('.b-addHorario', () => {
            B.sige.creator.getInstance('Trabajadores').nhorario();
        });
        u.click('.b-horarioTrabajadores', v => {
            let id = v.attr('target');
            B.sige.creator.getInstance('Trabajadores').horarios(id);
        });
        u.change('select.SelectedEmpresaB', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('sucursales', v.val(),  1), 0, 3);
            $('select.SelectedSucursalB').html(Form.initSelect(filter));
            $('.SelectedSucursalB').formSelect();
            $('.SelectedSucursalB').change();
        });
        u.change('select.SelectedSucursalB', v => {
            let filter = B.sige.validator.forSelect(B.sige.validator.searchIn('departamentos', v.val(), 1), 0, 4);
            $('select.SelectedDepartamentoB').html(Form.initSelect(filter));
            $('.SelectedDepartamentoB').formSelect();
        });
        u.click('.b-getTrabajadores', () => {
            B.sige.creator.getInstance('Trabajadores').get();
        });
        u.click('.datosTrabajadoresC', v => {
            B.sige.validator.vTable(v, 'datosTrabajadoresC', '.b-gestTrabajadores, .b-delTrabajadores, .b-horarioTrabajadores, .b-gestPermission');
        });
        u.click('.b-nuevoTrabajadores', () => {
            B.sige.creator.getInstance('Trabajadores').nuovo();
        });
        u.click('.b-gestTrabajadores', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'trabajadores', method: 'getTrabajadorById', param: id}, r => {
                r = Core.json(false, r, false);
                B.sige.creator.getInstance('Trabajadores').nuovo(r);
            });
        });
		u.click('.b-gestPermission', v => {
            let id = v.attr('target');
            B.sige.asyn.post({class:'trabajadores', method: 'getPermissionById', param: id}, r => {
                r = Core.json(false, r, false);
				if (r[0] == 'notpermisible'){
					Core.toast('Necesita activar la opción Permitir cambio de empresa para utilizar este módulo.','red');
					return false;
				}
				let vldate = [];
				$.each(r, (i, e) => {
					let check = (e[3] != '0') ? 'checked="checked"' : '';
					let disc = '';
					let permx = '<label><input type="checkbox" class="filled-in permission" '+disc+' sucursal="'+e[0]+'" '+check+' /><span></span></label>';
					vldate.push([e[1], e[2], permx]);
				});
				let value = Form.table('buygui', 'bgyuj cfm','striped',['Empresa', 'Sucursal', 'Acceso'], vldate);
				B.sige.ui.question('Acceso a empresas','<div class="col s12 descr">El acceso a empresas permite que un empleado se cambie entre ellas con base en los accesos que se le otorguen. Esto no invalida los permisos que tenga registrados pues, aunque se cambie de empresa, únicamente tendrá acceso a lo que se le está permitido.</div><div class="col s12 permTable">'+value+'</div>','Guardar cambios',() => {
					let v = [];
					$('input.permission').each(function(){
						v.push([$(this).attr('sucursal'), $(this).prop("checked") ? '1' : '0']);
					});
					B.sige.asyn.post({class:'trabajadores', method: 'setPermissionById', param: v}, r => {
						r = Core.json(false, r, false);
						switch(r[0]){
							case 'error':
							case 'denied':
								B.msn(r[0]);
							break;
							case 'true':
								Core.toast('Acceso a empresas actualizado','green');
							break;
						}
					});
				});
            });
        });
        u.click('.b-delTrabajadores', v => {
            let id = v.attr('target');
            B.sige.ui.question('Confirmar acción','¿Está seguro de eliminar a este trabajador? Esta acción es irreversible.','Aceptar',() => {
                B.sige.asyn.post({class:'trabajadores', method: 'delTrabajador', param: id}, r => {
                    r = Core.json(false, r, false);
                    switch(r[0]){
                        case 'error':
                        case 'denied':
                            B.msn(r[0]);
                        break;
                        case 'true':
                            B.msn(r[0]);
                            B.sige.creator.getInstance('Trabajadores').get();
                        break;
                    }
                });
            });
        });
        u.click('.b-saveTrabajador', () => {
            let v = [[
                    $('.nrazon').val(), $('select.genero').val(), $('.curp').val(), $('.rfc').val(),
                    $('select.civil').val(), $('.correo').val(), $('.fechaNac').val(), $('.imss').val(),
                    $('.licencia').val()
                ],[
                    $('.direccion').val(), $('.ciudad').val(), $('.estado').val(), $('.cp').val(),
                    $('.pais').val(), $('.tcasa').val(), $('.tcelular').val()
                ],[
                    $('select.SelectedDepartamentoB').val(), $('.puesto').val(), $('select.jornada').val(), $('select.auth').val(),
                    $('.password1').val(), $('.password2').val(), $('select.admin').val(), $('select.sueldo').val(), $('.lcredito').val()
                ],[]
            ], flag = true;
            $('select.permiso').each(function() {
                let c = $(this).attr('class').split(' ')[1];
                v[3].push([c, $(this).val()]);
            });
            $.each(v,(i, e) => {
                $.each(e, function(o, u){
                    if (u === '' || u == null || u === undefined)
                        flag = false;
                });
            });
            if (v[2][4] !== v[2][5]){
				Core.toast('Error, Las contraseñas no coinciden. Verifique e intente nuevamente','red rounded');
                flag = false;
			}
            if (B.sige.creator.getInstance('Trabajadores').editing)
                if (v[2][4] === '' && v[2][5] === '')
                    flag = true;
            if (!flag){
                Core.toast('Error, debe completar todos los datos para guardar. Si no posee los datos, deje un espacio en blanco.','red rounded');
                return false;
            }
            B.sige.asyn.post({class:'trabajadores', method: 'saveTrabajador', param: v}, r => {
                r = Core.json(false, r, false);
                switch(r[0]){
                    case 'error':
                    case 'denied':
                        B.msn(r[0]);
                    break;
					case 'same':
						Core.toast('Error, el correo electrónico ya se encuentra registrado, intente con uno diferente.','red rounded');
					break;
                    case 'true':
                        B.msn(r[0]);
                        B.sige.creator.getInstance('Trabajadores').get();
                    break;
                }
            });
        });
    }
}

class Access{
    constructor(){
        this.clicks();
        this.formas = [[0, 'Pago en efectivo'], [1, 'Pago con cheque'], [2, 'Pago por transferencia'], [3, 'Depósito a tarjeta de crédito'], [4, 'Depósito a tarjeta de débito'], [5, 'Pago a través de internet'], [6, 'Depósito a cuenta'], [7, 'Otro']];
    }
    clicks(){
        u.change('.genero', v => {
            let zm = $('.curp, .civil, .fechaNac, .imss, .licencia');
            if (v.val() === '2'){
                zm.attr('disabled','disabled').val(' ');
                B.prop('civil', '2');
            } else
                zm.removeAttr('disabled').val('');
            $('.civil').formSelect();
        });
    }
    static direccion(value = ['', '', '', '', '', '', '']){
        let j = B.sige.und;
        let dir = Form.area('direccion','Dirección (calle, número y colonia)', 220, j, value[0]);
        let ciudad = Form.input('ciudad', 'text', 'Ciudad', '', '', j, 120, value[1]);
        let edo = Form.input('estado', 'text', 'Estado', '', '', j, 45, value[2]);
        let cp = Form.input('cp', 'text', 'Código Postal', '', '', j, 5, value[3]);
        let pais = Form.input('pais', 'text', 'País', '', '', j, 44, value[4]);
        let tcasa = Form.input('tcasa', 'text', 'Teléfono de casa', '', '', j, 12, value[5]);
        let tcelular = Form.input('tcelular', 'text', 'Teléfono celular', '', '', j, 12, value[6]);
        return '<div class="command"><div class="tg-title">Dirección</div><div class="cDir-content row"><div class="cDir-dir col s12 input-field">'+dir+'</div><div class="cDir-dir col s12 m3 input-field">'+ciudad+'</div><div class="cDir-dir col s12 m3 input-field">'+edo+'</div><div class="cDir-dir col s12 m3 input-field">'+cp+'</div><div class="cDir-dir col s12 m3 input-field">'+pais+'</div><div class="cDir-dir col s12 m6 input-field">'+tcasa+'</div><div class="cDir-dir col s12 m6 input-field">'+tcelular+'</div></div></div>';
    }
    static personal(value = ['', '', '', '', '', '', '','']){
        let j = B.sige.und;
        let nrazon = Form.input('nrazon', 'text', 'Nombre o razón social', '', '', j, 220, value[0]);
        let genero = Form.select('genero','Género', [[0, 'Mujer'], [1, 'Hombre'], [2, 'Personal Moral']]); //value[1]
        let curp = Form.input('curp', 'text', 'Curp', '', '', j, 18, value[2]);
        let rfc = Form.input('rfc', 'text', 'rfc', '', '', j, 13, value[3]);
        let correo = Form.input('correo', 'email', 'Correo electrónico', '', '', j, j, value[4]);
        let civil = Form.select('civil','Estado civil', [[0, 'Soltera o soltero'], [1, 'Casada o casado'], [2, 'No Aplica']]); //value[5]
        let fechaNac = Form.input('fechaNac', 'text', 'Fecha de nacimiento', '', '', j, j, value[6]);
        let imss = Form.input('imss', 'text', 'Número del IMSS', '', '', j, 25, value[7]);
        let licencia = Form.input('licencia', 'text', 'Número de licencia', '', '', j, 25, value[8]);
        return ['<div class="command"><div class="tg-title">Datos personales</div><div class="cDir-content row"><div class="col s12 m8 input-field">'+nrazon+'</div><div class="col s12 m4 input-field">'+genero+'</div><div class="col s12 m3 input-field">'+curp+'</div><div class="col s12 m3 input-field">'+rfc+'</div><div class="col s12 m3 input-field">'+civil+'</div><div class="col s12 m3 input-field">'+correo+'</div><div class="col s12 m4 input-field">'+fechaNac+'</div><div class="col s12 m4 input-field">'+imss+'</div><div class="col s12 m4 input-field">'+licencia+'</div></div></div>', () => {
            if (value[1] !== '')
                B.prop('genero',value[1]);
            $('.genero').formSelect();
			$('.curp').val(value[2]);
            if (value[5] !== '')
                B.prop('civil',value[5]);
            $('.civil').formSelect();
            B.sige.fecha.Dtpicker('.fechaNac');
        }];
    }
    static formaPago(v = ''){
        return Form.select('formaPago '+v,'Forma de pago', [[0, 'Pago en efectivo'], [1, 'Pago con cheque'], [2, 'Pago por transferencia'], [3, 'Pago a tarjeta de crédito'], [4, 'Pago a tarjeta de débito'], [5, 'Pago a través de internet'], [6, 'Depósito a cuenta'], [7, 'Otro']]);
    }
    static tipo(){
        return Form.select('tipoIngreso','Tipo de operación', [[0, 'Entrada'], [1, 'Salida']]);
    }
    static impuesto(clsid){
        return Form.select('impuestoS '+clsid,'¿Tipo de impuestos?', [[0, 'Ninguno 0%'], [1, 'I.V.A. 16%']],'', undefined, 'clsid="'+clsid+'"');
    }
    static anio(){
        return Form.select('anio','Seleccione año', [[2018, '2018'], [2019, '2019'], [2020, '2020'], [2021, '2021'], [2022, '2022'], [2023, '2023']],'', undefined, '');
    }
    static mes(){
        return Form.select('mes','Seleccione mes', [['01', 'Enero'], ['02', 'Febrero'], ['03', 'Marzo'], ['04', 'Abril'], ['05', 'Mayo'], ['06', 'Junio'], ['07', 'Julio'], ['08', 'Agosto'], ['09', 'Septiembre'], ['10', 'Octubre'], ['11', 'Noviembre'], ['12', 'Diciembre']],'', undefined, '');
    }
    static quincena(){
        return Form.select('quincena','Seleccione quincena', [['01', 'Quincena 1'], ['02', 'Quincena 2']],'', undefined, '');
    }
    static tipoPago(){
        return Form.select('tipoPago','¿Tipo de pago?', [[0, 'CONTADO'], [1, 'CRÉDITO']]);
    }
    static getEmpleadosByDepId(id, fn, letter){
        B.sige.asyn.post({class:'trabajadores', method: 'getForEmpleadosByDepId', param: id}, r => {
            r = Core.json(false, r, false);
            $('select.SelectedEmpleado'+letter).html(Form.initSelect(r));
            if (typeof fn === "function")
                fn();
        });
    }
    static getArticulosByPrecioId(id, fn, letter){
        B.sige.asyn.post({class:'precios', method: 'getArticulosBySucursalAndFamilia', param: id}, r => {
            r = Core.json(false, r, false);
            let a = [];
            $.each(r, (i, e) => {
                a.push([e[0], e[1]+' '+e[3]+' - '+e[8]+' '+e[5]]);
            });
            if (a.length === 0)
                a = [[-1,'No hay coincidencias']];
            $('select.articulo'+letter).html(Form.initSelect(a));
            if (typeof fn === "function")
                fn();
        });
    }
    static comprobacion(){
        let j = B.sige.und;
        let monto = Form.input('monto', 'number', 'Monto a comprobar', '', '', j, j, '');
        let formaPago = Access.formaPago();
        let SelectedEmpresaD = Form.select('SelectedEmpresaD','¿A qué empresa aplica?', B.sige.validator.getForSelect('empresas',0, 1));
        let SelectedSucursalD = Form.select('SelectedSucursalD','¿A qué sucursal aplica?', []);
        //let SelectedDepartamentoD = Form.select('SelectedDepartamentoD','Seleccione un departamento', []);
        //let SelectedEmpleadoD = Form.select('SelectedEmpleadoD','¿Quién lo comprueba?', []);
        let SelectedEmpleadoD = Form.input('SelectedEmpleadoD', 'text', '¿Quién lo comprueba?', '', '', j, j, '');
        let observ = Form.input('observ', 'text', 'Observaciones adicionales', '', '', j, j, '');
        let cuenta = Form.input('cuenta', 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, j, '');
        return ['<div class="command"><div class="tg-title">Comprobación</div><div class="cDir-content row"><div class="col s12 m5 input-field">'+monto+'</div><div class="col s12 m7 input-field">'+formaPago+'</div><div class="col s12 input-field">'+cuenta+'</div><div class="col s12 m6 input-field">'+SelectedEmpresaD+'</div><div class="col s12 m6 input-field">'+SelectedSucursalD+'</div><div class="col s12 input-field">'+SelectedEmpleadoD+'</div><div class="col s12 input-field">'+observ+'</div></div></div>',function(opt){
            if (typeof opt === "function")
                opt();
            $('.SelectedEmpresaD, .SelectedSucursalD, .formaPago').formSelect();
        }];
    }
    static miniComprobacion(v = ['','','','','']){
        let j = B.sige.und;
        let clsid = z.clsid();
        let monto = Form.input('monto '+clsid, 'number', 'Monto a comprobar', '', '', j, j, v[3]);
        let formaPago = Access.formaPago(clsid);
        let observ = Form.input('observ '+clsid, 'text', 'Observaciones adicionales', '', '', j, j, v[6]);
        let cuenta = Form.input('cuenta '+clsid, 'text', 'No. de cheque, cuenta, tarjeta, correo o cuenta de depósito con nombre del banco', '', '', j, j, v[7]);
        return ['<div class="command"><div class="tg-title">Método de pago</div><div class="cDir-content row"><div class="col s12 m5 input-field">'+monto+'</div><div class="col s12 m7 input-field">'+formaPago+'</div><div class="col s12 input-field">'+cuenta+'</div><div class="col s12 input-field">'+observ+'</div></div></div>',function(opt){
            if (typeof opt === "function")
                opt();
            B.prop('formaPago.'+clsid, v[4]);
            $('.formaPago').formSelect();
        }];
    }
	static reportes(clase, fecha = true, cliente = false, proveedor = false, vendedor = false, guardar = true, exportar = true, editional = ['', function(){}], empleado = false){
		if (fecha){
			let rep_finicio = Form.input('rep_finicio', 'text', 'Fecha de inicio','','');
			let rep_ffin = Form.input('rep_ffin', 'text', 'Fecha de inicio','','');
			fecha = ['<div class="col s12 m6 input-field">'+rep_finicio+'</div><div class="col s12 m6 input-field">'+rep_ffin+'</div>', function(){
				B.sige.fecha.Dtpicker('.rep_finicio');
				B.sige.fecha.Dtpicker('.rep_ffin');
			}];
		} else
			fecha = ['',function(){}];
		if (cliente){
			let rep_sCliente = Form.select('rep_sCliente','Seleccione un cliente', B.sige.validator.getForSelect('clientes',0, 1));
			cliente = ['<div class="col s12 m6 input-field">'+rep_sCliente+'</div>', function(){
				$('.rep_sCliente').formSelect();
			}];
		} else
			cliente = ['',function(){}];
		if (empleado){
			let rep_sEmpleado = Form.select('rep_sEmpleado','Seleccione un empleado', B.sige.validator.getForSelect('trabajadores', 0, 2));
			empleado = ['<div class="col s12 m6 input-field">'+rep_sEmpleado+'</div>', function(){
				$('.rep_sEmpleado').formSelect();
			}];
		} else
			empleado = ['',function(){}];
		if (proveedor){
			let rep_sProveedor = Form.select('rep_sProveedor','Seleccione un proveedor', B.sige.validator.getForSelect('proveedores',0, 1));
			proveedor = ['<div class="col s12 m6 input-field">'+rep_sProveedor+'</div>', function(){
				$('.rep_sProveedor').formSelect();
			}];
		} else
			proveedor = ['',function(){}];
		if (vendedor){
			let rep_sVendedor = Form.select('rep_sVendedor','Seleccione un vendedor', B.sige.validator.getForSelect('trabajadores',0, 2));
			vendedor = ['<div class="col s12 m6 input-field">'+rep_sVendedor+'</div>', function(){
				$('.rep_sVendedor').formSelect();
			}];
		} else
			vendedor = ['',function(){}];
		if (guardar)
			guardar = '<div class="col s6 offset-m6 m3 b-repLoadx smnb">'+Form.btn(false, 'green', 'b-'+clase, 'Buscar')+'</div>';
		else
			guardar = '';
		if (exportar)
			exportar = '<div class="col s6 '+((guardar == '') ? 'offset-m9' : '')+' m3 b-repExcel smnb">'+Form.btn(false, 'green', 'b-repExportarExcel', 'Exportar')+'</div>';
		else
			exportar = '';
		return [fecha[0]+cliente[0]+empleado[0]+proveedor[0]+vendedor[0]+editional[0]+'<div class="col s12">&nbsp;</div>'+guardar+exportar, function(){
			$('.contenidoReporte').html('');
			fecha[1]();
			cliente[1]();
			empleado[1]();
			proveedor[1]();
			vendedor[1]();
			editional[1]();
		}];
	}
	static headTable(title, add = ''){
		let empresa = B.sige.validator.searchIn('sucursales', $.cookie('sucursal'), 0);
		
		return '<table class="responsive-table reporteX xxx yyy striped"><tr><th></th><th></th><th></th><th></th><th></th><th></th></tr><tr><th colspan="5" class="center-align">'+title+'</th><th colspan="1" class="center-align">Fecha: '+B.sige.fecha.getDate(4)+'</th></tr><tr><th colspan="3" class="center-align">Empresa: '+empresa[0][2]+'</th><th colspan="3" class="center-align">Sucursal: '+empresa[0][3]+'</th></tr>'+add+'</table>';
	}
}

class Ls{
    static set(k, v, l){
        localStorage.setItem(k,v);
        if (l > 0)
            localStorage.setItem(k+'-duration', Ls.date(l));
    }
    static get(k, l){
        let item = localStorage.getItem(k);
        if (l)
            return item;
        let exp = localStorage.getItem(k+'-duration');
        if (Ls.date(0) > exp){
            Ls.rem(k);
            return null;
        } else
            return item;
    }
    static rem(k){
        localStorage.removeItem(k);
        localStorage.removeItem(k+'-duration');
    }
    static date(time){
        let a = new Date();
        if (time === 0)
            return a.getTime();
        a.setSeconds(time);
        return a.getTime();
    }
}