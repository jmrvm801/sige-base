<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="theme-color" content="#EEE">
        <title>Sistema Integral de Gestión Empresarial</title>
        <link rel="icon" href="media/icon.ico">
        <link type="text/css" rel="stylesheet" href="css/icon.css"/>
    	<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.blurhouse.min.js"></script>
        <script type="text/javascript" src="js/jquery.PrintArea.js"></script>
        <script type="text/javascript" src="js/cookie.js"></script>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/xml2json.js"></script> 
        <script type="text/javascript" src="js/jquery.soap.js"></script>
        <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection,print"/>
        <script type="text/javascript" src="js/materialize.js"></script>
        <script type="text/javascript" src="media/index.js?random=<?php echo filemtime('media/index.js'); ?>"></script>
        <link type="text/css" rel="stylesheet" href="media/index.css?random=<?php echo filemtime('media/index.css'); ?>">
        <link type="text/css" rel="stylesheet" href="media/core.css">
        <link rel="stylesheet" href="css/PrintArea.css" type="text/css" media="screen,projection,print" />
    </head>
    <body class="grey lighten-5">
        <noscript>
            <div class="containerf">
                <div class="card">
                    <div class="card-image non-height red">
                        <span class="card-title">Tecnologías faltantes</span>
                    </div>
                    <div class="card-content">
                        <p>Se necesita javascript para ejecutar este sitio correctamente</p>
                    </div>
                    <div class="card-action">
                        <a href="#" class="z-depth-0 btn white materialize-red-text waves-effect">Activar javascript</a>
                    </div>
                </div>
            </div>
        </noscript>
   	</body>
</html>